<?php
/**
 * talenthunt_kaya functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package talenthunt_kaya
 */

if ( ! function_exists( 'talenthunt_kaya_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function talenthunt_kaya_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on talenthunt_kaya, use a find and replace
	 * to change 'talenthunt_kaya' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'talenthunt_kaya', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'talenthunt' ),
		'footermenu' => esc_html__( 'Footer Menu', 'talenthunt' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'talenthunt_kaya_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
	add_editor_style();
	the_post_thumbnail();
}
endif;
add_action( 'after_setup_theme', 'talenthunt_kaya_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function talenthunt_kaya_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'talenthunt_kaya_content_width', 640 );
}
add_action( 'after_setup_theme', 'talenthunt_kaya_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function talenthunt_kaya_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'talenthunt' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'talenthunt' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'talenthunt_kaya_widgets_init' );

/**
 * Register top search area widgetized areas.
 *
 */
function top_search_widget_area() {

	register_sidebar( array(
		'name'          => 'Top Search Widget Area',
		'id'            => 'top_search_widget_area',
		'description'   => esc_html__( 'Add widgets here.', 'talenthunt' ),
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );

}
add_action( 'widgets_init', 'top_search_widget_area' );
/**
 * Enqueue scripts and styles.
 */
function talenthunt_kaya_scripts() {
	wp_enqueue_script('jquery');
	wp_enqueue_style( 'talenthunt_kaya-style', get_stylesheet_uri() );
	wp_enqueue_style( 'talenthunt_kaya-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '', false );
	wp_enqueue_style( 'talenthunt_kaya-sm-clean', get_template_directory_uri() . '/css/smart-menu.css', array(), '', false );
	wp_enqueue_style( 'talenthunt_kaya-layout', get_template_directory_uri() . '/css/layout.css', array(), '', false );
	wp_enqueue_style( 'talenthunt_kaya-responsive', get_template_directory_uri() . '/css/responsive.css', array(), '', false );

	wp_enqueue_script( 'talenthunt_kaya-jquery.smartmenus.min', get_template_directory_uri() . '/js/jquery.smartmenus.min.js', array(), '', true );

	wp_enqueue_script( 'talenthunt_kaya-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'talenthunt_kaya-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	wp_enqueue_script( 'talenthunt_kaya-custom', get_template_directory_uri() . '/js/custom.js', array(), '', true );
}
add_action( 'wp_enqueue_scripts', 'talenthunt_kaya_scripts' );

/**
 * Admin Enqueue scripts and styles.
 */
function talenthunt_kaya_admin_scripts() {
	wp_enqueue_style( 'talenthunt_kaya-customizer', get_template_directory_uri() . '/css/customizer.css', array(), '', false );
}
add_action( 'admin_enqueue_scripts', 'talenthunt_kaya_admin_scripts' );

/**
 * Enqueue Customizer scripts.
 */
function kaya_customize_controls_enqueue_script(){
	wp_enqueue_script( 'custom-customize', get_template_directory_uri() . '/js/theme-customizer.js', array( 'jquery', 'customize-controls' ), false, true );
}
add_action( 'customize_controls_print_footer_scripts', 'kaya_customize_controls_enqueue_script',1 );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';
require get_template_directory() . '/inc/customizer/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
/**
 * Get all custom functions like page title, logo, menu...
 */
require get_template_directory() . '/inc/functions.php';

/**
 * Onclick demo importer
 */
require get_template_directory() . '/inc/importer/kaya-importer.php';

/**
 * Image Resizer Functionality
 */
require get_template_directory() . '/inc/mr-image-resize.php';

/**
 * Page / Post Meta options
 */
require get_template_directory() . '/inc/meta_boxes.php';

require get_template_directory() . '/inc/widget-sidebar-data-export.php';


/**
 *  Include Theme Rquired Plugins
 */
require get_template_directory() . '/inc/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'talenthunt_kaya_register_required_plugins' );



/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */
function talenthunt_kaya_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		array(
            'name'      => __('Elementor – The most advanced frontend drag & drop page builder.', 'talenthunt'),
            'slug'      => 'elementor',
            'required'  => true,
        ),
        array(
            'name'      => __('Admin Columns', 'talenthunt'),
            'slug'      => 'codepress-admin-columns',
            'required'  => true,
        ), 
        array(
            'name'      => __('Pods – Custom Content Types and Fields', 'talenthunt'),
            'slug'      => 'pods',
            'required'  => true,
        ),
        array(
            'name'      => __('PhotoSwipe', 'talenthunt'),
            'slug'      => 'photo-swipe',
            'required'  => true,
        ),
        array(
            'name'      => __('Caldera Forms – More Than Contact Forms', 'talenthunt'),
            'slug'      => 'caldera-forms',
            'required'  => true,
        ),  
		// Pods Post Display
		array(
		        'name'                  => __('Pods Post Display','talenthunt'),
		        'slug'                  => 'pods-post-display',
		        'source'                => 'http://kayapati.com/pods-plugins/pods-post-display.zip',
		        'required'              => true,
		        'force_activation'      => false,
		        'force_deactivation'    => false,
		        'external_url'          => '',
		    ),
		// Pods Post Shortlist
		array(
		        'name'                  => __('Pods Post Shortlist','talenthunt'),
		        'slug'                  => 'pods-post-shortlist',
		        'source'                =>  'http://kayapati.com/pods-plugins/pods-post-shortlist.zip',
		        'required'              => true,
		        'force_activation'      => false,
		        'force_deactivation'    => false,
		        'external_url'          => '',
		    ),
		// Pods Post Access Manager
		array(
		        'name'                  => __('Pods Post Access Manager','talenthunt'),
		        'slug'                  => 'pods-cpt-post-access-manager',
		        'source'                =>  'http://kayapati.com/pods-plugins/pods-cpt-post-access-manager.zip',
		        'required'              => true,
		        'force_activation'      => false,
		        'force_deactivation'    => false,
		        'external_url'          => '',
		    ),
		// User Registration & Login form
		array(
		        'name'                  => __('User Registration & Login form','talenthunt'),
		        'slug'                  => 'kaya-registration-login-forms',
		        'source'                =>  'http://kayapati.com/pods-plugins/kaya-registration-login-forms.zip',
		        'required'              => true,
		        'force_activation'      => false,
		        'force_deactivation'    => false,
		        'external_url'          => '',
		    ),
		// User Registration & Login form
		array(
		        'name'                  => __('Compcard','talenthunt'),
		        'slug'                  => 'pods-compcard',
		        'source'                =>  'http://kayapati.com/pods-plugins/pods-compcard.zip',
		        'required'              => true,
		        'force_activation'      => false,
		        'force_deactivation'    => false,
		        'external_url'          => '',
		    ),
		array(
                'name'      => __('Smart Slider 3', 'models'),
                'slug'      => 'smart-slider-3',
                'required'  => true,
            ),
		array(
            'name'      => __('Nav Menu Roles', 'models'),
            'slug'      => 'nav-menu-roles',
            'required'  => true,
        ),


	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		'id'           => 'talenthunt_kaya',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.

		
		'strings'      => array(
			'page_title'                      => __( 'Install Required Plugins', 'talenthunt' ),
			'menu_title'                      => __( 'Install Plugins', 'talenthunt' ),
			/* translators: %s: plugin name. */
			'installing'                      => __( 'Installing Plugin: %s', 'talenthunt' ),
			/* translators: %s: plugin name. */
			'updating'                        => __( 'Updating Plugin: %s', 'talenthunt' ),
			'oops'                            => __( 'Something went wrong with the plugin API.', 'talenthunt' ),
			'notice_can_install_required'     => _n_noop(
				/* translators: 1: plugin name(s). */
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'talenthunt'
			),
			'notice_can_install_recommended'  => _n_noop(
				/* translators: 1: plugin name(s). */
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'talenthunt'
			),
			'notice_ask_to_update'            => _n_noop(
				/* translators: 1: plugin name(s). */
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'talenthunt'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
				/* translators: 1: plugin name(s). */
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'talenthunt'
			),
			'notice_can_activate_required'    => _n_noop(
				/* translators: 1: plugin name(s). */
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'talenthunt'
			),
			'notice_can_activate_recommended' => _n_noop(
				/* translators: 1: plugin name(s). */
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'talenthunt'
			),
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'talenthunt'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'talenthunt'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'talenthunt'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'talenthunt' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'talenthunt' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'talenthunt' ),
			/* translators: 1: plugin name. */
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'talenthunt' ),
			/* translators: 1: plugin name. */
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'talenthunt' ),
			/* translators: 1: dashboard link. */
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'talenthunt' ),
			'dismiss'                         => __( 'Dismiss this notice', 'talenthunt' ),
			'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'talenthunt' ),
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'talenthunt' ),

			'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),
	);

	tgmpa( $plugins, $config );
}

add_theme_support( 'woocommerce' );
// Install Pods Components 
if(class_exists('PodsInit')){
	function actors_kaya_enable_pods_componets() {
		$component_settings = PodsInit::$components->settings;
		$component_settings['components']['table-storage'] = array();
		$component_settings['components']['advanced-relationships'] = array();
		$component_settings['components']['migrate-packages'] = array();
		$component_settings['components']['advanced-content-types'] = array();
		update_option( 'pods_component_settings', json_encode($component_settings));
	}
	actors_kaya_enable_pods_componets();
}



?>
