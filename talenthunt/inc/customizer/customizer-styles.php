<?php
function talenthunt_kaya_customizer_styles(){
	// Text Logo Color Section
	$text_logo_color = get_theme_mod('text_logo_color') ? get_theme_mod('text_logo_color') : '#353535';
	$text_logo_tagline_color = get_theme_mod('text_logo_tagline_color') ? get_theme_mod('text_logo_tagline_color') : '#757575';

	// Header Color Section
	$header_bg_color = get_theme_mod('header_bg_color') ? get_theme_mod('header_bg_color') : '#242730';
	
	// Menu Section
	$menu_link_color = get_theme_mod('menu_link_color') ? get_theme_mod('menu_link_color') : '#ffffff';
	$menu_link_hover_color = get_theme_mod('menu_link_hover_color') ? get_theme_mod('menu_link_hover_color') : '#ffffff';
	$menu_link_hover_bg_color = get_theme_mod('menu_link_hover_bg_color') ? get_theme_mod('menu_link_hover_bg_color') : '#ff5722';
	$menu_active_link_color = get_theme_mod('menu_active_link_color') ? get_theme_mod('menu_active_link_color') : '#fff';
	$search_icon_hover_color = get_theme_mod('search_icon_hover_color') ? get_theme_mod('search_icon_hover_color') : '#ffffff';
	$menu_active_bg_color = get_theme_mod('menu_active_bg_color') ? get_theme_mod('menu_active_bg_color') : '#ff5722';
	$child_menu_bg_color = get_theme_mod('child_menu_bg_color') ? get_theme_mod('child_menu_bg_color') : '#1a1a1a';
	$child_menu_link_color = get_theme_mod('child_menu_link_color') ? get_theme_mod('child_menu_link_color') : '#fff';
	$child_menu_link_hover_color = get_theme_mod('child_menu_link_hover_color') ? get_theme_mod('child_menu_link_hover_color') : '#fff';
	$child_menu_hover_bg_color = get_theme_mod('child_menu_hover_bg_color') ? get_theme_mod('child_menu_hover_bg_color') : '#454545';
	$child_menu_active_bg_color = get_theme_mod('child_menu_active_bg_color') ? get_theme_mod('child_menu_active_bg_color') : '#454545';
	$child_menu_active_link_color = get_theme_mod('child_menu_active_link_color') ? get_theme_mod('child_menu_active_link_color') : '#fff';
		
	// Advance Search
	$search_icon_bg_color = get_theme_mod('search_icon_bg_color') ?  get_theme_mod('search_icon_bg_color')  : '#424652';
	$search_icon_color = get_theme_mod('search_icon_color') ?  get_theme_mod('search_icon_color')  : '#fff';
	$search_icon_bg_hover_color = get_theme_mod('search_icon_bg_hover_color') ?  get_theme_mod('search_icon_bg_hover_color')  : '#ff5722';
	$search_icon_color = get_theme_mod('search_icon_color') ?  get_theme_mod('search_icon_color')  : '#fff';
	$search_bg_color = get_theme_mod('search_bg_color') ?  get_theme_mod('search_bg_color')  : '#f2f2f2';
	$advance_search_bg_color = get_theme_mod('advance_search_bg_color') ?  get_theme_mod('advance_search_bg_color')  : '#fff';
	$advance_search_border_color = get_theme_mod('advance_search_border_color') ?  get_theme_mod('advance_search_border_color')  : '#f9f9f9';
	$advance_search_font_color = get_theme_mod('advance_search_font_color') ?  get_theme_mod('advance_search_font_color')  : '#787878';
	$search_ui_slider_bg_color = get_theme_mod('search_ui_slider_bg_color') ?  get_theme_mod('search_ui_slider_bg_color')  : '#242730';
	$search_ui_slider_range_bg_color = get_theme_mod('search_ui_slider_range_bg_color') ?  get_theme_mod('search_ui_slider_range_bg_color')  : '#ff5722';
	$advance_search_btn_bg_color = get_theme_mod('advance_search_btn_bg_color') ?  get_theme_mod('advance_search_btn_bg_color')  : '#ff5722';
	$advance_search_btn_border_color = get_theme_mod('advance_search_btn_border_color') ?  get_theme_mod('advance_search_btn_border_color')  : '#ff5722';
	$advance_search_btn_font_color = get_theme_mod('advance_search_btn_font_color') ?  get_theme_mod('advance_search_btn_font_color')  : '#fff';
	$advance_search_btn_hover_bg_color = get_theme_mod('advance_search_btn_hover_bg_color') ?  get_theme_mod('advance_search_btn_hover_bg_color')  : '#c6380c';
	$advance_search_btn_hover_border_color = get_theme_mod('advance_search_btn_hover_border_color') ?  get_theme_mod('advance_search_btn_hover_border_color')  : '#c6380c';
	$advance_search_btn_hover_font_color = get_theme_mod('advance_search_btn_hover_font_color') ?  get_theme_mod('advance_search_btn_hover_font_color')  : '#fff';
	$search_title_color = get_theme_mod('search_title_color') ?  get_theme_mod('search_title_color')  : '#000';

	// Talent Single Page Setting
	$details_bg_color = get_theme_mod('details_bg_color') ?  get_theme_mod('details_bg_color')  : '#f2f2f2';
	$details_bg_top_border_color = get_theme_mod('details_bg_top_border_color') ?  get_theme_mod('details_bg_top_border_color')  : '#ccc';
	$details_bg_bottom_border_color = get_theme_mod('details_bg_bottom_border_color') ?  get_theme_mod('details_bg_bottom_border_color')  : '#ccc';
	$details_title_color = get_theme_mod('details_title_color') ?  get_theme_mod('details_title_color')  : '#333333';
	$details_list_font_color = get_theme_mod('details_list_font_color') ?  get_theme_mod('details_list_font_color')  : '#787878';
	$details_list_border_color = get_theme_mod('details_list_border_color') ?  get_theme_mod('details_list_border_color')  : '#dadada';

	// Talent Single Page Tabs Setting
	$tab_active_bg = get_theme_mod('tab_active_bg') ?  get_theme_mod('tab_active_bg')  : '#ff5722';
	$tabs_wrapper_border = get_theme_mod('tabs_wrapper_border') ?  get_theme_mod('tabs_wrapper_border')  : '#ff5722';
	$tab_active_color = get_theme_mod('tab_active_color') ?  get_theme_mod('tab_active_color')  : '#fff';
	$tab_bg_color = get_theme_mod('tab_bg_color') ?  get_theme_mod('tab_bg_color')  : '#000';
	$tab_color = get_theme_mod('tab_color') ?  get_theme_mod('tab_color')  : '#fff';
	$tab_hover_bg_color = get_theme_mod('tab_hover_bg_color') ?  get_theme_mod('tab_hover_bg_color')  : '#000';
	$tab_hover_color = get_theme_mod('tab_hover_color') ?  get_theme_mod('tab_hover_color')  : '#787878';

	
	// page title Section
	$page_titlebar_bg_color = get_theme_mod('page_titlebar_bg_color') ?  get_theme_mod('page_titlebar_bg_color')  : '#dedede';
	$page_titlebar_color = get_theme_mod('page_titlebar_color') ?  get_theme_mod('page_titlebar_color')  : '#353535';

	// Page mid content Section
	$page_mid_contant_bg_color = get_theme_mod('page_mid_contant_bg_color') ?  get_theme_mod('page_mid_contant_bg_color')  : '#e5e5e5';
	//$page_mid_content_title_color = get_theme_mod('page_mid_content_title_color') ?  get_theme_mod('page_mid_content_title_color')  : '#353535';
	//$page_mid_content_color = get_theme_mod('page_mid_content_color') ?  get_theme_mod('page_mid_content_color')  : '#757575';
	//$page_mid_contant_links_color = get_theme_mod('page_mid_contant_links_color') ?  get_theme_mod('page_mid_contant_links_color')  : '#353535';
	//$page_mid_contant_links_hover_color = get_theme_mod('page_mid_contant_links_hover_color') ?  get_theme_mod('page_mid_contant_links_hover_color')  : '#ff5722';

	// page sidebar Section
	$sidebar_bg_color = get_theme_mod('sidebar_bg_color') ?  get_theme_mod('sidebar_bg_color')  : '';
	$sidebar_title_color = get_theme_mod('sidebar_title_color') ?  get_theme_mod('sidebar_title_color')  : '#353535';
	$sidebar_content_color = get_theme_mod('sidebar_content_color') ?  get_theme_mod('sidebar_content_color')  : '#757575';
	$sidebar_links_color = get_theme_mod('sidebar_links_color') ?  get_theme_mod('sidebar_links_color')  : '#353535';
	$sidebar_links_hover_color = get_theme_mod('sidebar_links_hover_color') ?  get_theme_mod('sidebar_links_hover_color')  : '#ff5722';
	$sidebar_tagcloud_bg_color = get_theme_mod('sidebar_tagcloud_bg_color') ?  get_theme_mod('sidebar_tagcloud_bg_color')  : '#000000';
	$sidebar_tagcloud_color = get_theme_mod('sidebar_tagcloud_color') ?  get_theme_mod('sidebar_tagcloud_color')  : '#ffffff';
	$sidebar_tagcloud_bg_hover_color = get_theme_mod('sidebar_tagcloud_bg_hover_color') ?  get_theme_mod('sidebar_tagcloud_bg_hover_color')  : '#ff5722';
	$sidebar_tagcloud_hover_color = get_theme_mod('sidebar_tagcloud_hover_color') ?  get_theme_mod('sidebar_tagcloud_hover_color')  : '#ffffff';

	// Blog Section
	$blog_post_title_color = get_theme_mod('blog_post_title_color') ?  get_theme_mod('blog_post_title_color')  : '#353535';
	$blog_post_link_color = get_theme_mod('blog_post_link_color') ?  get_theme_mod('blog_post_link_color')  : '#353535';
	$blog_post_link_hover_color = get_theme_mod('blog_post_link_hover_color') ?  get_theme_mod('blog_post_link_hover_color')  : '#ff5722';

	// Footer Section
	$footer_bg_color = get_theme_mod('footer_bg_color') ?  get_theme_mod('footer_bg_color')  : '#353535';
	$footer_titles_color = get_theme_mod('footer_titles_color') ?  get_theme_mod('footer_titles_color')  : '#bcbcbc';
	$footer_content_color = get_theme_mod('footer_content_color') ?  get_theme_mod('footer_content_color')  : '#bcbcbc';
	$footer_link_color = get_theme_mod('footer_link_color') ?  get_theme_mod('footer_link_color')  : '#bcbcbc';
	$footer_link_hover_color = get_theme_mod('footer_link_hover_color') ?  get_theme_mod('footer_link_hover_color')  : '#fff';

	// Footer Section
	$page_footer_bg_color = get_theme_mod('page_footer_bg_color') ?  get_theme_mod('page_footer_bg_color')  : '#151515';
	$page_footer_content_color = get_theme_mod('page_footer_content_color') ?  get_theme_mod('page_footer_content_color')  : '#f5f5f5';
	$page_footer_link_color = get_theme_mod('page_footer_link_color') ?  get_theme_mod('page_footer_link_color')  : '#f5f5f5';
	$page_footer_link_hover_color = get_theme_mod('page_footer_link_hover_color') ?  get_theme_mod('page_footer_link_hover_color')  : '#fff';

 	/* Font Sizes */
    /* Title Font sizes H1 */
    $h1_title_font_size=get_theme_mod( 'h1_title_fontsize', '' ) ? get_theme_mod( 'h1_title_fontsize', '' ) : '30'; // H1
    $h2_title_font_size=get_theme_mod( 'h2_title_fontsize', '' ) ? get_theme_mod( 'h2_title_fontsize', '' ) : '27'; // H2
    $h3_title_font_size=get_theme_mod( 'h3_title_fontsize', '' ) ? get_theme_mod( 'h3_title_fontsize', '' ) : '25'; // H3
    $h4_title_font_size=get_theme_mod( 'h4_title_fontsize', '' ) ? get_theme_mod( 'h4_title_fontsize', '' ) : '18'; // H4
    $h5_title_font_size=get_theme_mod( 'h5_title_fontsize', '' ) ? get_theme_mod( 'h5_title_fontsize', '' ) : '16'; // H5
    $h6_title_font_size=get_theme_mod( 'h6_title_fontsize', '' ) ? get_theme_mod( 'h6_title_fontsize', '' ) : '12'; // H6
    // Letter Spaceing
    $h1_font_letter_space=get_theme_mod( 'h1_font_letter_space') ? get_theme_mod( 'h1_font_letter_space') : '0'; // H1
    $h2_font_letter_space=get_theme_mod( 'h2_font_letter_space') ? get_theme_mod( 'h2_font_letter_space') : '0'; // H2
    $h3_font_letter_space=get_theme_mod( 'h3_font_letter_space') ? get_theme_mod( 'h3_font_letter_space') : '0'; // H3
    $h4_font_letter_space=get_theme_mod( 'h4_font_letter_space') ? get_theme_mod( 'h4_font_letter_space') : '0'; // H4
    $h5_font_letter_space=get_theme_mod( 'h5_font_letter_space') ? get_theme_mod( 'h5_font_letter_space') : '0'; // H5
    $h6_font_letter_space=get_theme_mod( 'h6_font_letter_space') ? get_theme_mod( 'h6_font_letter_space') : '0'; // H6
    // Font Weight
    $h1_font_weight_bold=get_theme_mod( 'h1_font_weight_bold') ? get_theme_mod( 'h1_font_weight_bold') : 'bold'; // H1
    $h2_font_weight_bold=get_theme_mod( 'h2_font_weight_bold') ? get_theme_mod( 'h2_font_weight_bold') : 'bold'; // H2
    $h3_font_weight_bold=get_theme_mod( 'h3_font_weight_bold') ? get_theme_mod( 'h3_font_weight_bold') : 'bold'; // H3
    $h4_font_weight_bold=get_theme_mod( 'h4_font_weight_bold') ? get_theme_mod( 'h4_font_weight_bold') : 'bold'; // H4
    $h5_font_weight_bold=get_theme_mod( 'h5_font_weight_bold') ? get_theme_mod( 'h5_font_weight_bold') : 'bold'; // H5
    $h6_font_weight_bold=get_theme_mod( 'h6_font_weight_bold') ? get_theme_mod( 'h6_font_weight_bold') : 'bold'; // H6
    // Body & Menu
    $body_font_weight_bold=get_theme_mod( 'body_font_weight_bold') ? get_theme_mod( 'body_font_weight_bold') : 'normal'; // body
    $menu_font_weight=get_theme_mod( 'menu_font_weight') ? get_theme_mod( 'menu_font_weight') : 'normal'; // Menu
    $child_menu_font_weight=get_theme_mod( 'child_menu_font_weight') ? get_theme_mod( 'child_menu_font_weight') : 'normal'; // Child Menu
    // Menu Letter Spacing
    $body_font_letter_space=get_theme_mod( 'body_font_letter_space') ? get_theme_mod( 'body_font_letter_space') : '0'; // H4
    $menu_font_letter_space=get_theme_mod( 'menu_font_letter_space') ? get_theme_mod( 'menu_font_letter_space') : '0'; // H5
    $child_menu_font_letter_space=get_theme_mod( 'child_menu_font_letter_space') ? get_theme_mod( 'child_menu_font_letter_space') : '0'; // H6
    $body_font_size=get_theme_mod( 'body_font_size', '' ) ? get_theme_mod( 'body_font_size', '' ) : '15'; // Body Font Size
    $menu_font_size=get_theme_mod( 'menu_font_size', '' ) ? get_theme_mod( 'menu_font_size', '' ) : '15'; // Body Font Size
    $child_menu_font_size=get_theme_mod( 'child_menu_font_size', '' ) ? get_theme_mod( 'child_menu_font_size', '' ) : '13'; // Body Font Size

    // Font family Names
    $google_body_font=get_theme_mod( 'google_body_font' ) ? get_theme_mod( 'google_body_font') : 'Open Sans';
    $google_bodyfont= ( $google_body_font == '0' ) ? 'arial' : $google_body_font;
    $google_menu_font=get_theme_mod( 'google_menu_font' ) ? get_theme_mod( 'google_menu_font' ) : 'Open Sans';
    $google_menufont= ( $google_menu_font == '0' ) ? 'arial' : $google_menu_font;
    $google_general_titlefont=get_theme_mod( 'google_heading_font') ? get_theme_mod( 'google_heading_font' ) : 'Open Sans';
    $google_generaltitlefont= ( $google_general_titlefont == '0' ) ? 'arial' : $google_general_titlefont;	
	/* Body & Menu & Title's Font Line Height  */
	$lineheight_body = round((1.9 * $body_font_size));
	$lineheight_h1 = round((1.6 * $h1_title_font_size));
	$lineheight_h2 = round((1.6 * $h2_title_font_size));
	$lineheight_h3 = round((1.6 * $h3_title_font_size));
	$lineheight_h4 = round((1.6 * $h4_title_font_size)); 
	$lineheight_h5 = round((1.6 * $h5_title_font_size));
	$lineheight_h6 = round((1.6 * $h6_title_font_size));

	// Buttons
	$button_bg = get_theme_mod('button_bg') ?  get_theme_mod('button_bg')  : '#ff5722';
	$button_color = get_theme_mod('button_color') ?  get_theme_mod('button_color')  : '#fff';
	$button_hover_bg = get_theme_mod('button_hover_bg') ?  get_theme_mod('button_hover_bg')  : '#e74613';
	$button_hover_color = get_theme_mod('button_hover_color') ?  get_theme_mod('button_hover_color')  : '#fff';

	$css = '';
    $css .= 'body, p{
			font-family:'.esc_attr( $google_bodyfont ).';
			line-height:'.$lineheight_body.'px;
			font-size:'.$body_font_size.'px;
			letter-spacing:'.$body_font_letter_space.'px;
			font-weight:'.$body_font_weight_bold.';
			font-style:normal;
    }
    .menu ul li a{
			font-family:'.$google_menufont.';
			font-size:'.$menu_font_size.'px;
			line-height: 100%;
			letter-spacing:'.$menu_font_letter_space.'px;
			font-weight:'.$menu_font_weight.';
    }
    .menu ul ul li a,
    #header-navigation #user-dashboard-menu li a{
	        font-size:'.$child_menu_font_size.'px;
	        font-weight:'.$child_menu_font_weight.';
    }
   	a, span{
        	font-family:'.esc_attr( $google_bodyfont ).';
    }
    p{
        padding-bottom:'.$lineheight_body.'px;
    }
    /* Heading Font Family */
    h1, h2, h3, h4, h5, h6, h1 a, h2 a, h3 a, h4 a, h5 a, h6 a{
       	font-family:'.$google_generaltitlefont.';
    }
    h1{
        font-size:'.$h1_title_font_size.'px;
        line-height:'.$lineheight_h1.'px;
        letter-spacing:'.$h1_font_letter_space.'px;
        font-weight: '.$h1_font_weight_bold.';
    }
    h2{
        font-size:'.$h2_title_font_size.'px;
        line-height:'.$lineheight_h2.'px;
        letter-spacing:'.$h2_font_letter_space.'px;
        font-weight: '.$h2_font_weight_bold.';
    }
    h3{
        font-size:'.$h3_title_font_size.'px;
        line-height:'.$lineheight_h3.'px;
        letter-spacing:'.$h3_font_letter_space.'px;
        font-weight: '.$h3_font_weight_bold.';
    }
    h4{
        font-size:'.$h4_title_font_size.'px;
        line-height:'.$lineheight_h4.'px;
        letter-spacing:'.$h4_font_letter_space.'px;
        font-weight: '.$h4_font_weight_bold.';
    }
    h5{
        font-size:'.$h5_title_font_size .'px;
        line-height:'. $lineheight_h5 .'px;
        letter-spacing:'.$h5_font_letter_space.'px;
        font-weight: '.$h5_font_weight_bold.';
    }
    h6{
        font-size:'.$h6_title_font_size.'px;
        line-height:'.$lineheight_h6.'px;
        letter-spacing:'.$h6_font_letter_space.'px;
        font-weight: '.$h6_font_weight_bold.';
    }';

	// Text Logo Settings
	$css .= '#logo h1.site-title a{
			color:'.$text_logo_tagline_color.';
		}
		#logo p{
			color:'.$text_logo_color.';
		}';
	// Header Color Section
	$css .='#kaya-header-content-wrapper{
		background:'.$header_bg_color.';
	}';
	// Menu  Color settings	
	$css .= '#header-navigation ul li a{
				color:'.$menu_link_color.';
			}
			#header-navigation ul li a:hover,
			ul.filter_submenu li a:hover{
				color:'.$menu_link_hover_color.';
				background:'.$menu_link_hover_bg_color.'!important;
			}
			#header-navigation ul li.current-menu-item.current_page_item > a, #header-navigation .current-menu-ancestor.current-menu-parent.current_page_parent > a,  #header-navigation .current-menu-ancestor.current-menu-parent > a, .current-menu-item{
				color:'.$menu_active_link_color.';
				background-color:'.$menu_active_bg_color.';
			}
			#header-navigation ul ul,
			#header-navigation #user-dashboard-menu li a,
			ul.filter_submenu li a{
				background:'.$child_menu_bg_color.';
				color:'.$child_menu_link_color.';
			}
			#header-navigation ul ul li a,
			#header-navigation #user-dashboard-menu li a{
				color:'.$child_menu_link_color.';
			}
			#header-navigation ul ul li a:hover
			{
				background:'.$child_menu_hover_bg_color.';
				color:'.$child_menu_link_hover_color.';
			}
			#header-navigation ul ul li.current-menu-item.current_page_item > a{
				color:'.$child_menu_active_link_color.';
				
			}
			#header-navigation ul ul > li.current-menu-item.current_page_item{
				background:'.$child_menu_active_bg_color.';
				color:'.$child_menu_active_link_color.';
			}';

	// Page title  Color settings			
	$css .= '.kaya-page-titlebar-wrapper{
				background:'.$page_titlebar_bg_color.';
			}
			.kaya-page-titlebar-wrapper .page-title{
				color:'.$page_titlebar_color.';
			}';

	// Blog Page Settings			
	$css .= '.post-title-meta h2 a{
				color:'.$blog_post_title_color.';
			}
			.post-meta-data a,
			.nav-links a{
				color:'.$blog_post_link_color.';
			}
			.post-meta-data a:hover,
			.nav-links a:hover{
				color:'.$blog_post_link_hover_color.';
			}';


	// Advance Search settings			
	$css .= '#panel{
				background:'.$search_bg_color.';
			}
			.advanced_search_wrapper select{
				background:'.$advance_search_bg_color.';
				border:1px solid '.$advance_search_border_color.';
				color:'.$advance_search_font_color.';
			}
			input.search_data_submit{
				background:'.$advance_search_btn_bg_color.';
				border:1px solid '.$advance_search_btn_border_color.';
				color:'.$advance_search_btn_font_color.';				
			}
			input.search_data_submit:hover{
				background:'.$advance_search_btn_hover_bg_color.';
				border:1px solid '.$advance_search_btn_hover_border_color.';
				color:'.$advance_search_btn_hover_font_color.';				
			}
			.advanced_search_wrapper label{
				color:'.$search_title_color.';
			}
			div#flip i{
				background:'.$search_icon_bg_color.';
				color:'.$search_icon_color.';
			}
			div#flip i:hover{
				background:'.$search_icon_bg_hover_color.';
				color:'.$search_icon_hover_color.';
			}
			.advanced_search_forms .ui-slider{
				background:'.$search_ui_slider_bg_color.';
			}
			.ui-slider .ui-slider-range{
				background:'.$search_ui_slider_range_bg_color.';
			}';	

	// Talent Single Page settings			
	$css .= '.talent-single-page{
				background:'.$details_bg_color.';
				border-top:1px solid '.$details_bg_top_border_color.';
				border-bottom:1px solid '.$details_bg_bottom_border_color.';
			}
			.talenthunt_single_page_details h3{
				color:'.$details_title_color.'!important;
			}
			.talenthunt_single_page_details .general-meta-fields-info-wrapper li{
				color:'.$details_list_font_color.';
				background:1px solid '.$details_list_border_color.';
			}';	

	// Talent Single Tabs settings			
	$css .= 'li.tab-active a{
				background:'.$tab_active_bg.'!important;
				color:'.$tab_active_color.'!important;
			}
			ul.tabs_content_wrapper{
				border-bottom:3px solid '.$tabs_wrapper_border.';
			}
			ul.tabs_content_wrapper li a{
				background:'.$tab_bg_color.';
				color:'.$tab_color.'!important;
			}
			ul.tabs_content_wrapper li a:hover{
				background:'.$tab_hover_bg_color.';
				color:'.$tab_hover_color.'!important;
			}';				

	// Page Mid Content	Color settings		
	$css .= '#kaya-mid-content-wrapper{
				background:'.$page_mid_contant_bg_color.';
			}
			#kaya-mid-content-wrapper h1, #kaya-mid-content-wrapper h2, #kaya-mid-content-wrapper h3, #kaya-mid-content-wrapper h4, #kaya-mid-content-wrapper h5, #kaya-mid-content-wrapper h6,
			#kaya-mid-content-wrapper h1 a, #kaya-mid-content-wrapper h2 a, #kaya-mid-content-wrapper h3 a, #kaya-mid-content-wrapper h4 a, #kaya-mid-content-wrapper h5 a, #kaya-mid-content-wrapper h6 a{
				
			}
			#kaya-mid-content-wrapper a{
				
			}
			#kaya-mid-content-wrapper a:hover{
				
			}';

	// Page sidebar Color settings	
	$css .= '#sidebar{
				background:'.$sidebar_bg_color.';
				color:'.$sidebar_content_color.';
			}
			#sidebar h1, #sidebar h2, #sidebar h3, #sidebar h4, #sidebar h5, #sidebar h6{
				color:'.$sidebar_title_color.';
			}
			#sidebar a{
				color:'.$sidebar_links_color.';
			}
			#sidebar a:hover{
				color:'.$sidebar_links_hover_color.';
			}
			.tagcloud a {
				background:'.$sidebar_tagcloud_bg_color.';
				color:'.$sidebar_tagcloud_color.';
			}
			.tagcloud a:hover {
				background:'.$sidebar_tagcloud_bg_hover_color.';
				color:'.$sidebar_tagcloud_hover_color.';
			}';

	// Button Settings
	$css .= 'a.more-link, .form-submit #submit, a.scrolltop, .caldera-grid .btn-default {
				background:'.$button_bg.';
				color:'.$button_color.'!important;
			}
			.search-submit{
				background:'.$button_bg.'!important;
				color:'.$button_color.'!important;
			}
			a.more-link:hover, .form-submit #submit:hover, .search-submit:hover, a.scrolltop:hover, .caldera-grid .btn-default:hover{
				background:'.$button_hover_bg.';
				color:'.$button_hover_color.';
			}
			.search-submit:hover{
				background:'.$button_hover_bg.'!important;
				color:'.$button_hover_color.'!important;
			}';
				
	// Footer Color settings	
	$css .= '#kaya-footer-content-wrapper{
				background:'.$footer_bg_color.';
				color:'.$footer_content_color.';
			}
			#kaya-footer-content-wrapper h1, #kaya-footer-content-wrapper h2, #kaya-footer-content-wrapper h3, #kaya-footer-content-wrapper h4, #kaya-footer-content-wrapper h5, #kaya-footer-content-wrapper h6{
				color:'.$footer_titles_color.';
			}
			#kaya-footer-content-wrapper a{
				color:'.$footer_link_color.';
			}
			#kaya-footer-content-wrapper a:hover{
				color:'.$footer_link_hover_color.';
			}';	

	// Footer Footer Color settings	
	$css .= '.kaya-page-content-footer{
				background:'.$page_footer_bg_color.';
				color:'.$page_footer_content_color.';
			}
			.kaya-page-content-footer p, .kaya-page-content-footer span {
				color:'.$page_footer_content_color.';
			}
			.kaya-page-content-footer a{
				color:'.$page_footer_link_color.';
			}
			.kaya-page-content-footer a:hover{
				color:'.$page_footer_link_hover_color.';
			}';				
	// End Styles
			
	$css = preg_replace( '/\s+/', ' ', $css ); 
    echo "<style type=\"text/css\">\n" .trim( $css ). "\n</style>";	
}

add_action('wp_head', 'talenthunt_kaya_customizer_styles');
function top_nav_social_media_icons(){
$src=get_template_directory_uri() . '/images/social_icons';
$socialmedia_icon1_img_src = get_option('social_icon1');// social icon1
$socialmediaicon1 = $socialmedia_icon1_img_src['upload_social_icon1'] ? str_replace('&nbsp;', '', $socialmedia_icon1_img_src['upload_social_icon1'])  : $src.'/facebook.png';
$right_social_icon_link1=get_theme_mod('right_social_icon_link1') ? get_theme_mod('right_social_icon_link1'): '';
$socialmedia_icon2_img_src = get_option('social_icon2'); // social icon2
$socialmediaicon2 = $socialmedia_icon2_img_src['upload_social_icon2'] ? str_replace('&nbsp;', '',$socialmedia_icon2_img_src['upload_social_icon2']) : $src.'/skype.png';
 $right_social_icon_link2=get_theme_mod('right_social_icon_link2') ? get_theme_mod('right_social_icon_link2'): '';
$socialmedia_icon3_img_src = get_option('social_icon3'); //social icon3
$socialmediaicon3 = $socialmedia_icon3_img_src['upload_social_icon3'] ? str_replace('&nbsp;', '', $socialmedia_icon3_img_src['upload_social_icon3'])  : $src.'/youtube.png';
$right_social_icon_link3=get_theme_mod('right_social_icon_link3') ? get_theme_mod('right_social_icon_link3'): '';
$socialmedia_icon4_img_src = get_option('social_icon4'); //social icon4
$socialmediaicon4 = $socialmedia_icon4_img_src['upload_social_icon4'] ? str_replace('&nbsp;', '', $socialmedia_icon4_img_src['upload_social_icon4'])  : $src.'/tumblr.png';
$right_social_icon_link4=get_theme_mod('right_social_icon_link4') ? get_theme_mod('right_social_icon_link4'): '';
$socialmedia_icon5_img_src = get_option('social_icon5'); //social icon5
$socialmediaicon5 = $socialmedia_icon5_img_src['upload_social_icon5'] ? str_replace('&nbsp;', '', $socialmedia_icon5_img_src['upload_social_icon5'] ) : $src.'/vimeo.png';
$right_social_icon_link5=get_theme_mod('right_social_icon_link5') ? get_theme_mod('right_social_icon_link5'): '';
   //$default_social_mediaicon1= esc_attr( get_template_directory_uri().'/images/facebook.png' );
  //$src=get_template_directory_uri() . '/images/social_icons';
  if(!empty($socialmediaicon1 ) || (!empty($socialmediaicon2)) || (!empty($socialmediaicon3)) || (!empty($socialmediaicon4)) || (!empty($socialmediaicon5))){
    echo '<div class="right_section_social_icons">';
        if( trim($socialmediaicon1) ){
          echo '<a href="'.$right_social_icon_link1.'" target="_blank"><img class="icon1" src="'.$socialmediaicon1.'"  /></a>';
         }
         //end
         if( trim($socialmediaicon2) ){
         echo '<a href="'.$right_social_icon_link2.'" target="_blank"><img  class="icon2" src="'.$socialmediaicon2.'"/></a>';
        }//end
            //$default_social_mediaicon2= esc_attr( get_template_directory_uri().'/images/twitter.png' );

        if( trim($socialmediaicon3) ){
         echo '<a href="'.$right_social_icon_link3.'" target="_blank"><img class="icon3" src="'.$socialmediaicon3.'" /></a>';
         } 

        if( trim($socialmediaicon4) ){
         echo '<a href="'.$right_social_icon_link4.'" target="_blank"><img class="icon4" src="'.$socialmediaicon4.'" /></a>';
        }
         //end

        if( trim($socialmediaicon5) ){
         echo '<a href="'.$right_social_icon_link5.'" target="_blank"><img class="icon5" src="'.$socialmediaicon5.'" /></a>';
        }
      echo '</div>';
 }?>
 <?php  }
?>