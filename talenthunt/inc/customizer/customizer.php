<?php
/**
 * talenthunt_kaya Theme Customizer
 *
 * @package talenthunt_kaya
 */
/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
require_once get_template_directory() . '/inc/customizer/customizer-styles.php';

 // header functions

get_template_part('inc/customizer/customizer-controles');
get_template_part('inc/customizer/customize-import-export-settings');
/**
 * Remove wordpress default panels and sections from theme customizer
 */
add_action("customize_register", "talenthunt_kaya_remove_customize_register");

function talenthunt_kaya_remove_customize_register($wp_customize)
	{
	$wp_customize->remove_control("header_image");
	$wp_customize->remove_section("colors");
	$wp_customize->remove_section("background_image");
	$wp_customize->remove_section("static_front_page");
	$wp_customize->remove_control('display_header_text');
	}

/**
 * Change default wordpress "site identity" section name our our new name
 */

function talenthunt_kaya_change_default_logo_title($wp_customize)
	{
	$wp_customize->get_section('title_tagline')->title = __('Header Settings', 'talenthunt');
	}

add_action('customize_register', 'talenthunt_kaya_change_default_logo_title', 1000);
/**
 * Header Section
 */
add_action('customize_register', 'talenthunt_kaya_header_customize_register');

function talenthunt_kaya_header_customize_register($wp_customize)
	{
	$wp_customize->add_panel('kaya_header_panel_section', array(
		'priority' => 10,
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => __('Header Section', 'talenthunt') ,
	));
	$wp_customize->add_section('title_tagline', array(
		'panel' => 'kaya_header_panel_section',
		'title' => __('Header Settings', 'talenthunt') ,
		'priority' => 1,
	));
	$wp_customize->add_setting('header_bg_color', array(
		'default' => '#242730',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'header_bg_color', array(
		'label' => __('Header Background Color', 'talenthunt') ,
		'section' => 'title_tagline',
		'settings' => 'header_bg_color',
		'priority' => 0,
	)));
	$wp_customize->add_setting('choose_logo', array(
		'default' => 'img_logo',
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control('choose_logo', array(
		'label' => __('Choose Logo', 'talenthunt') ,
		'section' => 'title_tagline',
		'settings' => 'choose_logo',
		'type' => 'select',
		'choices' => array(
			'img_logo' => __('Image Logo', 'talenthunt') ,
			'text_logo' => __('Text Logo', 'talenthunt') ,
		) ,
		'priority' => '1'
	));

	// Upload header Logo Image

	$wp_customize->add_setting('logo_image', array(
		'type' => 'theme_mod',
		'default' => '',
		'capability' => 'edit_theme_options',
		'transport' => 'postMessage',
		'sanitize_callback' => 'esc_url_raw'
	));
	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'logo_image', array(
		'label' => __('Logo', 'talenthunt') ,
		'section' => 'title_tagline',
		'settings' => 'logo_image',
		'priority' => '2'
	)));

	// Text Logo Colors

	$wp_customize->add_setting('text_logo_color', array(
		'default' => '#353535',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'text_logo_color', array(
		'label' => __('Site Title color', 'talenthunt') ,
		'section' => 'title_tagline',
		'settings' => 'text_logo_color',
	)));
	$wp_customize->add_setting('text_logo_tagline_color', array(
		'default' => '#757575',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'text_logo_tagline_color', array(
		'label' => __('Tag Line color', 'talenthunt') ,
		'section' => 'title_tagline',
		'settings' => 'text_logo_tagline_color',
	)));
	}

/**
 * Menu Section
 */
add_action('customize_register', 'talenthunt_kaya_menu_customize_register');

function talenthunt_kaya_menu_customize_register($wp_customize)
	{
	$wp_customize->add_section('kaya_menu_section', array(
		'panel' => 'kaya_header_panel_section',
		'title' => __('Menu Settings', 'talenthunt') ,
		'priority' => 35,
	));
	$wp_customize->add_setting('menu_link_color', array(
		'default' => '#ffffff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'menu_link_color', array(
		'label' => __('Menu Links color', 'talenthunt') ,
		'section' => 'kaya_menu_section',
		'settings' => 'menu_link_color',
	)));
	$wp_customize->add_setting('menu_link_hover_color', array(
		'default' => '#ffffff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'menu_link_hover_color', array(
		'label' => __('Menu Links Hover color', 'talenthunt') ,
		'section' => 'kaya_menu_section',
		'settings' => 'menu_link_hover_color',
	)));
	$wp_customize->add_setting('menu_link_hover_bg_color', array(
		'default' => '#ff5722',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'menu_link_hover_bg_color', array(
		'label' => __('Menu Links Hover Background color', 'talenthunt') ,
		'section' => 'kaya_menu_section',
		'settings' => 'menu_link_hover_bg_color',
	)));
	$wp_customize->add_setting('menu_active_link_color', array(
		'default' => '#ffffff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'menu_active_link_color', array(
		'label' => __('Menu Links Active color', 'talenthunt') ,
		'section' => 'kaya_menu_section',
		'settings' => 'menu_active_link_color',
	)));
	$wp_customize->add_setting('menu_active_bg_color', array(
		'default' => '#ff5722',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'menu_active_bg_color', array(
		'label' => __('Menu Links Active Background color', 'talenthunt') ,
		'section' => 'kaya_menu_section',
		'settings' => 'menu_active_bg_color',
	)));
	$wp_customize->add_setting('child_menu_bg_color', array(
		'default' => '#1a1a1a',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'child_menu_bg_color', array(
		'label' => __('Child Menu Background color', 'talenthunt') ,
		'section' => 'kaya_menu_section',
		'settings' => 'child_menu_bg_color',
	)));
	$wp_customize->add_setting('child_menu_link_color', array(
		'default' => '#fff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'child_menu_link_color', array(
		'label' => __('Child Menu links color', 'talenthunt') ,
		'section' => 'kaya_menu_section',
		'settings' => 'child_menu_link_color',
	)));
	$wp_customize->add_setting('child_menu_hover_bg_color', array(
		'default' => '#454545',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'child_menu_hover_bg_color', array(
		'label' => __('Child Menu Hover Background color', 'talenthunt') ,
		'section' => 'kaya_menu_section',
		'settings' => 'child_menu_hover_bg_color',
	)));
	$wp_customize->add_setting('child_menu_link_hover_color', array(
		'default' => '#fff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'child_menu_link_hover_color', array(
		'label' => __('Child Menu Links Hover color', 'talenthunt') ,
		'section' => 'kaya_menu_section',
		'settings' => 'child_menu_link_hover_color',
	)));
	$wp_customize->add_setting('child_menu_active_bg_color', array(
		'default' => '#454545',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'child_menu_active_bg_color', array(
		'label' => __('Child Menu Active Background color', 'talenthunt') ,
		'section' => 'kaya_menu_section',
		'settings' => 'child_menu_active_bg_color',
	)));
	$wp_customize->add_setting('child_menu_active_link_color', array(
		'default' => '#fff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'child_menu_active_link_color', array(
		'label' => __('Child Menu Active Link color', 'talenthunt') ,
		'section' => 'kaya_menu_section',
		'settings' => 'child_menu_active_link_color',
	)));
	}

/**
 * Talent Section
 */
add_action('customize_register', 'talenthunt_kaya_talent_customize_register');

function talenthunt_kaya_talent_customize_register($wp_customize)
	{
	$wp_customize->add_section('kaya_talent_section', array(
		'panel' => 'kaya_header_panel_section',
		'title' => __('Advance Search Settings', 'talenthunt') ,
		'priority' => 35,
	));
	$wp_customize->add_setting('search_icon_bg_color', array(
		'default' => '#424652',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'search_icon_bg_color', array(
		'label' => __('Search Icon Background Color', 'talenthunt') ,
		'section' => 'kaya_talent_section',
		'settings' => 'search_icon_bg_color',
	)));
	$wp_customize->add_setting('search_icon_color', array(
		'default' => '#fff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'search_icon_color', array(
		'label' => __('Search Icon Color', 'talenthunt') ,
		'section' => 'kaya_talent_section',
		'settings' => 'search_icon_color',
	)));
	$wp_customize->add_setting('search_icon_bg_hover_color', array(
		'default' => '#ff5722',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'search_icon_bg_hover_color', array(
		'label' => __('Search Icon Hover Background Color', 'talenthunt') ,
		'section' => 'kaya_talent_section',
		'settings' => 'search_icon_bg_hover_color',
	)));
	$wp_customize->add_setting('search_icon_hover_color', array(
		'default' => '#fff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'search_icon_hover_color', array(
		'label' => __('Search Icon Hover Color', 'talenthunt') ,
		'section' => 'kaya_talent_section',
		'settings' => 'search_icon_hover_color',
	)));
	$wp_customize->add_setting('search_bg_color', array(
		'default' => '#f2f2f2',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'search_bg_color', array(
		'label' => __('Background Color', 'talenthunt') ,
		'section' => 'kaya_talent_section',
		'settings' => 'search_bg_color',
	)));
	$wp_customize->add_setting('search_title_color', array(
		'default' => '#000',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'search_title_color', array(
		'label' => __('Title Color', 'talenthunt') ,
		'section' => 'kaya_talent_section',
		'settings' => 'search_title_color',
	)));
	$wp_customize->add_setting('advance_search_bg_color', array(
		'default' => '#fff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'advance_search_bg_color', array(
		'label' => __('Select Option Background Color', 'talenthunt') ,
		'section' => 'kaya_talent_section',
		'settings' => 'advance_search_bg_color',
	)));
	$wp_customize->add_setting('advance_search_border_color', array(
		'default' => '#f9f9f9',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'advance_search_border_color', array(
		'label' => __('Select Option Border Color', 'talenthunt') ,
		'section' => 'kaya_talent_section',
		'settings' => 'advance_search_border_color',
	)));
	$wp_customize->add_setting('advance_search_font_color', array(
		'default' => '#787878',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'advance_search_font_color', array(
		'label' => __('Select Option Font Color', 'talenthunt') ,
		'section' => 'kaya_talent_section',
		'settings' => 'advance_search_font_color',
	)));
	$wp_customize->add_setting('search_ui_slider_bg_color', array(
		'default' => '#242730',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'search_ui_slider_bg_color', array(
		'label' => __('Search Ui Slider Bg Color', 'talenthunt') ,
		'section' => 'kaya_talent_section',
		'settings' => 'search_ui_slider_bg_color',
	)));
	$wp_customize->add_setting('search_ui_slider_range_bg_color', array(
		'default' => '#ff5722',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'search_ui_slider_range_bg_color', array(
		'label' => __('Search Ui Slider Bg Color', 'talenthunt') ,
		'section' => 'kaya_talent_section',
		'settings' => 'search_ui_slider_range_bg_color',
	)));
	$wp_customize->add_setting('advance_search_btn_bg_color', array(
		'default' => '#ff5722',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'advance_search_btn_bg_color', array(
		'label' => __('Advance Search Button Background Color', 'talenthunt') ,
		'section' => 'kaya_talent_section',
		'settings' => 'advance_search_btn_bg_color',
	)));
	$wp_customize->add_setting('advance_search_btn_border_color', array(
		'default' => '#ff5722',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'advance_search_btn_border_color', array(
		'label' => __('Advance Search Button Border Color', 'talenthunt') ,
		'section' => 'kaya_talent_section',
		'settings' => 'advance_search_btn_border_color',
	)));
	$wp_customize->add_setting('advance_search_btn_font_color', array(
		'default' => '#ffffff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'advance_search_btn_font_color', array(
		'label' => __('Advance Search Button Font Color', 'talenthunt') ,
		'section' => 'kaya_talent_section',
		'settings' => 'advance_search_btn_font_color',
	)));
	$wp_customize->add_setting('advance_search_btn_hover_bg_color', array(
		'default' => '#ff5722',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'advance_search_btn_hover_bg_color', array(
		'label' => __('Advance Search Button Hover Background Color', 'talenthunt') ,
		'section' => 'kaya_talent_section',
		'settings' => 'advance_search_btn_hover_bg_color',
	)));
	$wp_customize->add_setting('advance_search_btn_hover_border_color', array(
		'default' => '#ff5722',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'advance_search_btn_hover_border_color', array(
		'label' => __('Advance Search Button Hover Border Color', 'talenthunt') ,
		'section' => 'kaya_talent_section',
		'settings' => 'advance_search_btn_hover_border_color',
	)));
	$wp_customize->add_setting('advance_search_btn_hover_font_color', array(
		'default' => '#ffffff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'advance_search_btn_hover_font_color', array(
		'label' => __('Advance Search Button Hover Font Color', 'talenthunt') ,
		'section' => 'kaya_talent_section',
		'settings' => 'advance_search_btn_hover_font_color',
	)));
	}

/**
 * Page Section
 */
add_action('customize_register', 'talenthunt_kaya_page_customize_register');

function talenthunt_kaya_page_customize_register($wp_customize)
	{
	$wp_customize->add_panel('kaya_page_panel_section', array(
		'priority' => 20,
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => __('Page Section', 'talenthunt') ,
	));
	/**
	$wp_customize->add_section('kaya_page_section', array(
		'panel' => 'kaya_page_panel_section',
		'title' => __('Page Titlebar Settings', 'talenthunt') ,
		'priority' => 35,
	));
	$wp_customize->add_setting('page_titlebar_bg_color', array(
		'default' => '#dedede',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'page_titlebar_bg_color', array(
		'label' => __('Page Titlebar color', 'talenthunt') ,
		'section' => 'kaya_page_section',
		'settings' => 'page_titlebar_bg_color',
	)));
	$wp_customize->add_setting('page_titlebar_color', array(
		'default' => '#353535',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'page_titlebar_color', array(
		'label' => __('Page Titlebar Title color', 'talenthunt') ,
		'section' => 'kaya_page_section',
		'settings' => 'page_titlebar_color',
	)));
	**/
	}

/**
 * Page Middle Content
 */
add_action('customize_register', 'talenthunt_kaya_page_mid_content_customize_register');

function talenthunt_kaya_page_mid_content_customize_register($wp_customize)
	{
	$wp_customize->add_section('kaya_mid_content_section', array(
		'panel' => 'kaya_page_panel_section',
		'title' => __('Page Middle Content Settings', 'talenthunt') ,
		'priority' => 35,
	));
	$wp_customize->add_setting('page_mid_contant_bg_color', array(
		'default' => '#fff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'page_mid_contant_bg_color', array(
		'label' => __('Background color', 'talenthunt') ,
		'section' => 'kaya_mid_content_section',
		'settings' => 'page_mid_contant_bg_color',
	)));
	/**
	$wp_customize->add_setting('page_mid_content_title_color', array(
		'default' => '#353535',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'page_mid_content_title_color', array(
		'label' => __('Titles color', 'talenthunt') ,
		'section' => 'kaya_mid_content_section',
		'settings' => 'page_mid_content_title_color',
	)));
	$wp_customize->add_setting('page_mid_content_color', array(
		'default' => '#787878',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'page_mid_content_color', array(
		'label' => __('Content color', 'talenthunt') ,
		'section' => 'kaya_mid_content_section',
		'settings' => 'page_mid_content_color',
	)));
	$wp_customize->add_setting('page_mid_contant_links_color', array(
		'default' => '#333',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'page_mid_contant_links_color', array(
		'label' => __('Links color', 'talenthunt') ,
		'section' => 'kaya_mid_content_section',
		'settings' => 'page_mid_contant_links_color',
	)));
	$wp_customize->add_setting('page_mid_contant_links_hover_color', array(
		'default' => '#ff5722',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'page_mid_contant_links_hover_color', array(
		'label' => __('Links Hover color', 'talenthunt') ,
		'section' => 'kaya_mid_content_section',
		'settings' => 'page_mid_contant_links_hover_color',
	)));
	**/
	}

/**
 * Sidebar Color Settings
 */
add_action('customize_register', 'talenthunt_kaya_sidebar_customize_register');

function talenthunt_kaya_sidebar_customize_register($wp_customize)
	{
	$wp_customize->add_section('kaya_sidebar_section', array(
		'panel' => 'kaya_page_panel_section',
		'title' => __('Sidebar Content Settings', 'talenthunt') ,
		'priority' => 35,
	));
	/**
	$wp_customize->add_setting('sidebar_bg_color', array(
		'default' => '',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sidebar_bg_color', array(
		'label' => __('Background color', 'talenthunt') ,
		'section' => 'kaya_sidebar_section',
		'transport' => 'postMessage',
	)));
	**/
	$wp_customize->add_setting('sidebar_title_color', array(
		'default' => '#353535',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sidebar_title_color', array(
		'label' => __('Titles color', 'talenthunt') ,
		'section' => 'kaya_sidebar_section',
		'settings' => 'sidebar_title_color',
	)));
	$wp_customize->add_setting('sidebar_content_color', array(
		'default' => '#787878',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sidebar_content_color', array(
		'label' => __('Content color', 'talenthunt') ,
		'section' => 'kaya_sidebar_section',
		'settings' => 'sidebar_content_color',
	)));
	$wp_customize->add_setting('sidebar_links_color', array(
		'default' => '#333',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sidebar_links_color', array(
		'label' => __('Links color', 'talenthunt') ,
		'section' => 'kaya_sidebar_section',
		'settings' => 'sidebar_links_color',
	)));
	$wp_customize->add_setting('sidebar_links_hover_color', array(
		'default' => '#ff5722',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sidebar_links_hover_color', array(
		'label' => __('Links Hover color', 'talenthunt') ,
		'section' => 'kaya_sidebar_section',
		'settings' => 'sidebar_links_hover_color',
	)));
	$wp_customize->add_setting('sidebar_tagcloud_bg_color', array(
		'default' => '#000',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sidebar_tagcloud_bg_color', array(
		'label' => __('Tagcloud Background Color', 'talenthunt') ,
		'section' => 'kaya_sidebar_section',
		'settings' => 'sidebar_tagcloud_bg_color',
	)));
	$wp_customize->add_setting('sidebar_tagcloud_color', array(
		'default' => '#fff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sidebar_tagcloud_color', array(
		'label' => __('Tagcloud Link Color', 'talenthunt') ,
		'section' => 'kaya_sidebar_section',
		'settings' => 'sidebar_tagcloud_color',
	)));
	$wp_customize->add_setting('sidebar_tagcloud_bg_hover_color', array(
		'default' => '#ff5722',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sidebar_tagcloud_bg_hover_color', array(
		'label' => __('Tagcloud Background Hover Color', 'talenthunt') ,
		'section' => 'kaya_sidebar_section',
		'settings' => 'sidebar_tagcloud_bg_hover_color',
	)));
	$wp_customize->add_setting('sidebar_tagcloud_hover_color', array(
		'default' => '#fff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sidebar_tagcloud_hover_color', array(
		'label' => __('Tagcloud Link Hover Color', 'talenthunt') ,
		'section' => 'kaya_sidebar_section',
		'settings' => 'sidebar_tagcloud_hover_color',
	)));
	}

/**
 * Talent Single Page Section
 */
add_action('customize_register', 'talenthunt_kaya_talent_single_page_customize_register');

function talenthunt_kaya_talent_single_page_customize_register($wp_customize)
	{
	$wp_customize->add_panel('kaya_talent_single_page_panel_section', array(
		'priority' => 20,
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => __('Talent Single Page Settings', 'talenthunt') ,
	));
	$wp_customize->add_section('kaya_talent_single_page_section', array(
		'panel' => 'kaya_talent_single_page_panel_section',
		'title' => __('Single Page Settings', 'talenthunt') ,
		'priority' => 35,
	));
	$wp_customize->add_setting('details_bg_color', array(
		'default' => '#f2f2f2',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'details_bg_color', array(
		'label' => __('Background Color', 'talenthunt') ,
		'section' => 'kaya_talent_single_page_section',
		'settings' => 'details_bg_color',
	)));
	$wp_customize->add_setting('details_bg_top_border_color', array(
		'default' => '#ccc',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'details_bg_top_border_color', array(
		'label' => __('Top Border Color', 'talenthunt') ,
		'section' => 'kaya_talent_single_page_section',
		'settings' => 'details_bg_top_border_color',
	)));
	$wp_customize->add_setting('details_bg_bottom_border_color', array(
		'default' => '#ccc',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'details_bg_bottom_border_color', array(
		'label' => __('Bottom Border Color', 'talenthunt') ,
		'section' => 'kaya_talent_single_page_section',
		'settings' => 'details_bg_bottom_border_color',
	)));
	$wp_customize->add_setting('details_title_color', array(
		'default' => '#333',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'details_title_color', array(
		'label' => __('Detail Title Color', 'talenthunt') ,
		'section' => 'kaya_talent_single_page_section',
		'settings' => 'details_title_color',
	)));
	$wp_customize->add_setting('details_list_font_color', array(
		'default' => '#787878',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'details_list_font_color', array(
		'label' => __('Detail List Font Color', 'talenthunt') ,
		'section' => 'kaya_talent_single_page_section',
		'settings' => 'details_list_font_color',
	)));
	$wp_customize->add_setting('details_list_border_color', array(
		'default' => '#dadada',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'details_list_border_color', array(
		'label' => __('Detail List Border Color', 'talenthunt') ,
		'section' => 'kaya_talent_single_page_section',
		'settings' => 'details_list_border_color',
	)));
	}

/**
 * Talent Single Page Tabs Section
 */
add_action('customize_register', 'talenthunt_kaya_talent_single_page_tab_customize_register');

function talenthunt_kaya_talent_single_page_tab_customize_register($wp_customize)
	{
	$wp_customize->add_section('kaya_talent_single_page_tabs_section', array(
		'panel' => 'kaya_talent_single_page_panel_section',
		'title' => __('Single Page Tabs Settings', 'talenthunt') ,
		'priority' => 35,
	));
	$wp_customize->add_setting('tab_active_bg', array(
		'default' => '#ff5722',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'tab_active_bg', array(
		'label' => __('Active Background Color', 'talenthunt') ,
		'section' => 'kaya_talent_single_page_tabs_section',
		'settings' => 'tab_active_bg',
	)));
	$wp_customize->add_setting('tabs_wrapper_border', array(
		'default' => '#ff5722',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'tabs_wrapper_border', array(
		'label' => __('Tab Wrapper Border', 'talenthunt') ,
		'section' => 'kaya_talent_single_page_tabs_section',
		'settings' => 'tabs_wrapper_border',
	)));
	$wp_customize->add_setting('tab_active_color', array(
		'default' => '#fff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'tab_active_color', array(
		'label' => __('Tab Active Color', 'talenthunt') ,
		'section' => 'kaya_talent_single_page_tabs_section',
		'settings' => 'tab_active_color',
	)));
	$wp_customize->add_setting('tab_bg_color', array(
		'default' => '#000',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'tab_bg_color', array(
		'label' => __('Tab Background Color', 'talenthunt') ,
		'section' => 'kaya_talent_single_page_tabs_section',
		'settings' => 'tab_bg_color',
	)));
	$wp_customize->add_setting('tab_color', array(
		'default' => '#fff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'tab_color', array(
		'label' => __('Tab Color', 'talenthunt') ,
		'section' => 'kaya_talent_single_page_tabs_section',
		'settings' => 'tab_color',
	)));
	$wp_customize->add_setting('tab_hover_bg_color', array(
		'default' => '#000',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'tab_hover_bg_color', array(
		'label' => __('Tab Hover Background Color', 'talenthunt') ,
		'section' => 'kaya_talent_single_page_tabs_section',
		'settings' => 'tab_hover_bg_color',
	)));
	$wp_customize->add_setting('tab_hover_color', array(
		'default' => '#ff5722',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'tab_hover_color', array(
		'label' => __('Tab Hover Color', 'talenthunt') ,
		'section' => 'kaya_talent_single_page_tabs_section',
		'settings' => 'tab_hover_color',
	)));
	}

/**
 * Button Setting
 */
add_action('customize_register', 'talenthunt_button_settings');

function talenthunt_button_settings($wp_customize)
	{
	$wp_customize->add_section('kaya_button_section', array(
		'title' => __('Button Settings', 'talenthunt') ,
		'description' => __('This Settings are Customizable for <strong>Blog Buttons</strong>, <strong>Post Comment Buttons</strong>, <strong>Search Button</strong> and for <strong>Scroll Top Button</strong>.') ,
		'priority' => 37,
	));
	$wp_customize->add_setting('button_bg', array(
		'default' => '#ff5722',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'button_bg', array(
		'label' => __('Button Background Color', 'talenthunt') ,
		'section' => 'kaya_button_section',
		'settings' => 'button_bg',
	)));
	$wp_customize->add_setting('button_color', array(
		'default' => '#fff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'button_color', array(
		'label' => __('Button Color', 'talenthunt') ,
		'section' => 'kaya_button_section',
		'settings' => 'button_color',
	)));
	$wp_customize->add_setting('button_hover_bg', array(
		'default' => '#e74613',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'button_hover_bg', array(
		'label' => __('Button Hover Background Color', 'talenthunt') ,
		'section' => 'kaya_button_section',
		'settings' => 'button_hover_bg',
	)));
	$wp_customize->add_setting('button_hover_color', array(
		'default' => '#fff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'button_hover_color', array(
		'label' => __('Button Hover Color', 'talenthunt') ,
		'section' => 'kaya_button_section',
		'settings' => 'button_hover_color',
	)));
	}

/**
 * Footer Section
 */
add_action('customize_register', 'talenthunt_kaya_footer_customize_register');

function talenthunt_kaya_footer_customize_register($wp_customize)
	{
	$wp_customize->add_panel('kaya_footer_panel_section', array(
		'priority' => 25,
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => __('Footer Section', 'talenthunt') ,
	));
	$wp_customize->add_section('kaya_footer_section', array(
		'panel' => 'kaya_footer_panel_section',
		'title' => __('Footer Settings', 'talenthunt') ,
		'priority' => 35,
	));
	$wp_customize->add_setting('footer_copy_rights', array(
		'default' => __('Footer Copy Left Text', 'talenthunt') ,
		'transport' => 'postMessage',
		//'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control('footer_copy_rights', array(
		'label' => __('Footer Copy Rights Text', 'talenthunt') ,
		'section' => 'kaya_footer_section',
		'type' => 'textarea',
	));
	$wp_customize->add_setting('footer_bg_color', array(
		'default' => '#353535',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_bg_color', array(
		'label' => __('Background color', 'talenthunt') ,
		'section' => 'kaya_footer_section',
		'settings' => 'footer_bg_color',
	)));
	$wp_customize->add_setting('footer_content_color', array(
		'default' => '#bcbcbc',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_content_color', array(
		'label' => __('Content color', 'talenthunt') ,
		'section' => 'kaya_footer_section',
		'settings' => 'footer_content_color',
	)));
	$wp_customize->add_setting('footer_link_color', array(
		'default' => '#bcbcbc',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_link_color', array(
		'label' => __('Link color', 'talenthunt') ,
		'section' => 'kaya_footer_section',
		'settings' => 'footer_link_color',
	)));
	$wp_customize->add_setting('footer_link_hover_color', array(
		'default' => '#fff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_link_hover_color', array(
		'label' => __('Link Hover color', 'talenthunt') ,
		'section' => 'kaya_footer_section',
		'settings' => 'footer_link_hover_color',
	)));
	}

/**
 * Page Footer
 */
add_action('customize_register', 'talenthunt_kaya_page_footer');

function talenthunt_kaya_page_footer($wp_customize)
	{
	$wp_customize->add_section('kaya_page_footer_section', array(
		'panel' => 'kaya_footer_panel_section',
		'title' => __('Page Footer Settings', 'talenthunt') ,
		'priority' => 35,
	));
	$wp_customize->add_setting('main_footer_page', array(
		'default' => 0,
		'transport' => '',
		'sanitize_callback' => 'absint',
	));
	$wp_customize->add_control('main_footer_page', array(
		'label' => __('Page Footer', 'talenthunt') ,
		'section' => 'kaya_page_footer_section',
		'type' => 'dropdown-pages',
	));
	$wp_customize->add_setting('page_footer_bg_color', array(
		'default' => '#151515',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'page_footer_bg_color', array(
		'label' => __('Background color', 'talenthunt') ,
		'section' => 'kaya_page_footer_section',
		'settings' => 'page_footer_bg_color',
	)));
	$wp_customize->add_setting('page_footer_content_color', array(
		'default' => '#fff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'page_footer_content_color', array(
		'label' => __('Content color', 'talenthunt') ,
		'section' => 'kaya_page_footer_section',
		'settings' => 'page_footer_content_color',
	)));
	$wp_customize->add_setting('page_footer_link_color', array(
		'default' => '#f5f5f5',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'page_footer_link_color', array(
		'label' => __('Link color', 'talenthunt') ,
		'section' => 'kaya_page_footer_section',
		'settings' => 'page_footer_link_color',
	)));
	$wp_customize->add_setting('page_footer_link_hover_color', array(
		'default' => '#fff',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'page_footer_link_hover_color', array(
		'label' => __('Link Hover color', 'talenthunt') ,
		'section' => 'kaya_page_footer_section',
		'settings' => 'page_footer_link_hover_color',
	)));
	}

// Typography

function talenthunt_kaya_typography($wp_customize)
	{
	global $talenthunt_kaya_customze_note_settings;
	$wp_customize->add_panel('typography_panel_section', array(
		'priority' => 140,
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => esc_html__('Typography Section', 'talenthunt') ,
	));
	$wp_customize->add_section(

	// ID

	'typography_section',

	// Arguments array

	array(
		'title' => esc_html__('Google Font Family', 'talenthunt') ,
		'priority' => 140,
		'capability' => 'edit_theme_options',
		'panel' => 'typography_panel_section',
	));
	$wp_customize->add_setting('google_body_font', array(
		'default' => 'Nova Square',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_google_fonts_Control($wp_customize, 'google_body_font', array(
		'label' => esc_html__('Select font for Body', 'talenthunt') ,
		'section' => 'typography_section',
		'settings' => 'google_body_font',
		'priority' => 0,
	)));
	$wp_customize->add_setting('google_heading_font', array(
		'default' => '2',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_google_fonts_Control($wp_customize, 'google_heading_font', array(
		'label' => esc_html__('Select font for Headings', 'talenthunt') ,
		'section' => 'typography_section',
		'settings' => 'google_heading_font',
		'priority' => 10,
	)));
	$wp_customize->add_setting('google_menu_font', array(
		'default' => '2',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_google_fonts_Control($wp_customize, 'google_menu_font', array(
		'label' => esc_html__('Select font for Menu', 'talenthunt') ,
		'section' => 'typography_section',
		'settings' => 'google_menu_font',
		'priority' => 20,
	)));
	}

add_action('customize_register', 'talenthunt_kaya_typography');
/* --------------------------------------------
Typography
-----------------------------------------------*/

function talenthunt_kaya_font_panel_section($wp_customize)
	{
	$wp_customize->add_section(

	// ID

	'font-panel-section',

	// Arguments array

	array(
		'title' => esc_html__('Font Settings', 'talenthunt') ,
		'priority' => 140,
		'capability' => 'edit_theme_options',
		'panel' => 'typography_panel_section'
	));
	$font_weight_names = array(
		'normal' => 'Normal',
		'bold' => 'Bold',
		'lighter' => 'Lighter'
	);

	// Body Font Size

	$wp_customize->add_setting('body_font_size', array(
		'default' => '15',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_Sliderui_Control($wp_customize, 'body_font_size', array(
		'label' => esc_html__('Body Font Size', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'body_font_size',
		'priority' => 3,
		'choices' => array(
			'min' => 10,
			'max' => 30,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('body_font_letter_space', array(
		'default' => '0',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_Sliderui_Control($wp_customize, 'body_font_letter_space', array(
		'label' => esc_html__('Body Font Letter Spacing', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'body_font_letter_space',
		'priority' => 4,
		'choices' => array(
			'min' => 0,
			'max' => 30,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('body_font_weight_bold', array(
		'default' => 'normal',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control('body_font_weight_bold', array(
		'type' => 'select',
		'label' => esc_html__('Select Body Font Weight', 'talenthunt') ,
		'section' => 'font-panel-section',
		'choices' => $font_weight_names,
		'priority' => 9,
	));

	// Menu Font Size

	$wp_customize->add_setting('menu_font_size', array(
		'default' => '15',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_Sliderui_Control($wp_customize, 'menu_font_size', array(
		'label' => esc_html__('Menu Font Size', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'menu_font_size',
		'priority' => 11,
		'choices' => array(
			'min' => 10,
			'max' => 30,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('menu_font_letter_space', array(
		'default' => '0',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_Sliderui_Control($wp_customize, 'menu_font_letter_space', array(
		'label' => esc_html__('Menu Font Letter Spacing', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'menu_font_letter_space',
		'priority' => 20,
		'choices' => array(
			'min' => 0,
			'max' => 30,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('menu_font_weight', array(
		'default' => 'normal',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control('menu_font_weight', array(
		'type' => 'select',
		'label' => esc_html__('Select Menu Font Weight', 'talenthunt') ,
		'section' => 'font-panel-section',
		'choices' => $font_weight_names,
		'priority' => 21,
	));
	$wp_customize->add_setting('main_menu_uppercase', array(
		'default' => 0,

		// 'type'           => 'option',

		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
		'capability' => 'edit_theme_options'
	));
	$wp_customize->add_control('main_menu_uppercase', array(
		'label' => esc_html__('Enable Uppercase Letters ', 'talenthunt') ,
		'section' => 'font-panel-section',
		'type' => 'checkbox',
		'priority' => 22
	));

	// Menu Font Size

	$wp_customize->add_setting('child_menu_font_size', array(
		'default' => '13',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_Sliderui_Control($wp_customize, 'child_menu_font_size', array(
		'label' => esc_html__('Child Menu Font Size', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'child_menu_font_size',
		'priority' => 60,
		'choices' => array(
			'min' => 10,
			'max' => 30,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('child_menu_font_letter_space', array(
		'default' => '0',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_Sliderui_Control($wp_customize, 'child_menu_font_letter_space', array(
		'label' => esc_html__('Child Menu Font Letter Spacing', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'child_menu_font_letter_space',
		'priority' => 70,
		'choices' => array(
			'min' => 0,
			'max' => 30,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('child_menu_font_weight', array(
		'default' => 'normal',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control('child_menu_font_weight', array(
		'type' => 'select',
		'label' => esc_html__('Select Child Menu Font Weight', 'talenthunt') ,
		'section' => 'font-panel-section',
		'choices' => $font_weight_names,
		'priority' => 80,
	));
	$wp_customize->add_setting('child_menu_uppercase', array(
		'default' => 0,

		// 'type'           => 'option',

		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
		'capability' => 'edit_theme_options'
	));
	$wp_customize->add_control('child_menu_uppercase', array(
		'label' => esc_html__('Enable Uppercase Letters ', 'talenthunt') ,
		'section' => 'font-panel-section',
		'type' => 'checkbox',
		'priority' => 90
	));

	// Title Font Sizes
	// H1

	$wp_customize->add_setting('h1_title_fontsize', array(
		'default' => '30',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_Sliderui_Control($wp_customize, 'h1_title_fontsize', array(
		'label' => esc_html__('Font size for heading - H1', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'h1_title_fontsize',
		'priority' => 105,
		'choices' => array(
			'min' => 10,
			'max' => 80,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('h1_font_letter_space', array(
		'default' => '0',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_customIze_slideRui_control($wp_customize, 'h1_font_letter_space', array(
		'label' => esc_html__('Font Letter Spacing - H1', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'h1_font_letter_space',
		'priority' => 110,
		'choices' => array(
			'min' => 0,
			'max' => 30,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('h1_font_weight_bold', array(
		'default' => 'normal',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control('h1_font_weight_bold', array(
		'type' => 'select',
		'label' => esc_html__('Select Font Weight - H1', 'talenthunt') ,
		'section' => 'font-panel-section',
		'choices' => $font_weight_names,
		'priority' => 120,
	));

	// H2

	$wp_customize->add_setting('h2_title_fontsize', array(
		'default' => '24',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_Sliderui_Control($wp_customize, 'h2_title_fontsize', array(
		'label' => esc_html__('Font size for heading - H2', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'h2_title_fontsize',
		'priority' => 140,
		'choices' => array(
			'min' => 10,
			'max' => 80,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('h2_font_letter_space', array(
		'default' => '0',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_Sliderui_Control($wp_customize, 'h2_font_letter_space', array(
		'label' => esc_html__('Font Letter Spacing - H2', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'h2_font_letter_space',
		'priority' => 150,
		'choices' => array(
			'min' => 0,
			'max' => 30,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('h2_font_weight_bold', array(
		'default' => 'normal',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control('h2_font_weight_bold', array(
		'type' => 'select',
		'label' => esc_html__('Select Font Weight - H2', 'talenthunt') ,
		'section' => 'font-panel-section',
		'choices' => $font_weight_names,
		'priority' => 160,
	));

	// H3

	$wp_customize->add_setting('h3_title_fontsize', array(
		'default' => 'bold',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_Sliderui_Control($wp_customize, 'h3_title_fontsize', array(
		'label' => esc_html__('Font size for heading - H3', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'h3_title_fontsize',
		'priority' => 180,
		'choices' => array(
			'min' => 10,
			'max' => 80,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('h3_font_letter_space', array(
		'default' => '2',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_Sliderui_Control($wp_customize, 'h3_font_letter_space', array(
		'label' => esc_html__('Font Letter Spacing - H3', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'h3_font_letter_space',
		'priority' => 190,
		'choices' => array(
			'min' => 0,
			'max' => 30,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('h3_font_weight_bold', array(
		'default' => 'bold',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control('h3_font_weight_bold', array(
		'type' => 'select',
		'label' => esc_html__('Select Font Weight - H3', 'talenthunt') ,
		'section' => 'font-panel-section',
		'choices' => $font_weight_names,
		'priority' => 200,
	));
	$wp_customize->add_setting('h4_title_fontsize', array(
		'default' => '18',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_Sliderui_Control($wp_customize, 'h4_title_fontsize', array(
		'label' => esc_html__('Font size for heading - H4', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'h4_title_fontsize',
		'priority' => 220,
		'choices' => array(
			'min' => 10,
			'max' => 80,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('h4_font_letter_space', array(
		'default' => '0',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_Sliderui_Control($wp_customize, 'h4_font_letter_space', array(
		'label' => esc_html__('Font Letter Spacing - H4', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'h4_font_letter_space',
		'priority' => 230,
		'choices' => array(
			'min' => 0,
			'max' => 30,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('h4_font_weight_bold', array(
		'default' => 'normal',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control('h4_font_weight_bold', array(
		'type' => 'select',
		'label' => esc_html__('Select Font Weight - H4', 'talenthunt') ,
		'section' => 'font-panel-section',
		'choices' => $font_weight_names,
		'priority' => 240,
	));

	// H5

	$wp_customize->add_setting('h5_title_fontsize', array(
		'default' => '16',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_Sliderui_Control($wp_customize, 'h5_title_fontsize', array(
		'label' => esc_html__('Font size for heading - H5', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'h5_title_fontsize',
		'priority' => 260,
		'choices' => array(
			'min' => 10,
			'max' => 80,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('h5_font_letter_space', array(
		'default' => '0',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_Sliderui_Control($wp_customize, 'h5_font_letter_space', array(
		'label' => esc_html__('Font Letter Spacing - H5', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'h5_font_letter_space',
		'priority' => 270,
		'choices' => array(
			'min' => 0,
			'max' => 30,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('h5_font_weight_bold', array(
		'default' => 'normal',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control('h5_font_weight_bold', array(
		'type' => 'select',
		'label' => esc_html__('Select Font Weight - H5', 'talenthunt') ,
		'section' => 'font-panel-section',
		'choices' => $font_weight_names,
		'priority' => 280
	));

	// H6

	$wp_customize->add_setting('h6_title_fontsize', array(
		'default' => '14',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_Sliderui_Control($wp_customize, 'h6_title_fontsize', array(
		'label' => esc_html__('Font size for heading - H6', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'h6_title_fontsize',
		'priority' => 300,
		'choices' => array(
			'min' => 10,
			'max' => 80,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('h6_font_letter_space', array(
		'default' => '0',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(new talenthunt_kaya_Customize_Sliderui_Control($wp_customize, 'h6_font_letter_space', array(
		'label' => esc_html__('Font Letter Spacing - H6', 'talenthunt') ,
		'section' => 'font-panel-section',
		'settings' => 'h6_font_letter_space',
		'priority' => 310,
		'choices' => array(
			'min' => 0,
			'max' => 30,
			'step' => 1
		) ,
	)));
	$wp_customize->add_setting('h6_font_weight_bold', array(
		'default' => 'normal',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control('h6_font_weight_bold', array(
		'type' => 'select',
		'label' => esc_html__('Select Font Weight - H6', 'talenthunt') ,
		'section' => 'font-panel-section',
		'choices' => $font_weight_names,
		'priority' => 320,
	));
	}

add_action('customize_register', 'talenthunt_kaya_font_panel_section');
/**
 * Blog Page Settings
 */
add_action('customize_register', 'talenthunt_kaya_blog_page_settings');

function talenthunt_kaya_blog_page_settings($wp_customize)
	{
	$wp_customize->add_section('kaya_blog_page_section', array(
		'title' => __('Blog page', 'talenthunt') ,
		'priority' => 5,
	));
	$wp_customize->add_setting('blog_page_post_limit', array(
		'default' => '10',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control('blog_page_post_limit', array(
		'label' => __('Posts Limit', 'talenthunt') ,
		'section' => 'kaya_blog_page_section',
		'type' => 'text',
		'priority' => 1,
	));

	// Blog Page Sidebar Position

	$wp_customize->add_setting('blog_page_sidebar_position', array(
		'default' => 'right',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control('blog_page_sidebar_position', array(
		'type' => 'select',
		'label' => __('Blog Page Sidebar Position', 'talenthunt') ,
		'section' => 'kaya_blog_page_section',
		'choices' => array(
			'right' => __('Right', 'talenthunt') ,
			'left' => __('Left', 'talenthunt') ,
			'none' => __('None', 'talenthunt') ,
		) ,
		'priority' => 2,
	));
	$wp_customize->add_setting('blog_post_title_color', array(
		'default' => '#353535',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'blog_post_title_color', array(
		'label' => __('Title color', 'talenthunt') ,
		'section' => 'kaya_blog_page_section',
		'settings' => 'blog_post_title_color',
	)));
	$wp_customize->add_setting('blog_post_link_color', array(
		'default' => '#353535',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'blog_post_link_color', array(
		'label' => __('Link color', 'talenthunt') ,
		'section' => 'kaya_blog_page_section',
		'settings' => 'blog_post_link_color',
	)));
	$wp_customize->add_setting('blog_post_link_hover_color', array(
		'default' => '#ff5722',
		'transport' => 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'blog_post_link_hover_colorblog_post_link_hover_color', array(
		'label' => __('Link Hover color', 'talenthunt') ,
		'section' => 'kaya_blog_page_section',
		'settings' => 'blog_post_link_hover_color',
	)));
	}