/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */
(function($) {
  "use strict";
 	$(function() {
		// Logo Change
		$('#customize-control-choose_logo select').change(function(){
			$('#customize-control-logo_image').hide();
			$('#customize-control-blogname').hide();
			$('#customize-control-blogdescription').hide();
			$('#customize-control-display_header_text').hide();
			$('#customize-control-text_logo_color').hide();
			$('#customize-control-text_logo_tagline_color').hide();
			var choose_logo = $('#customize-control-choose_logo select option:selected').val();
			switch( choose_logo ){
				case 'img_logo':
					$('#customize-control-logo_image').show();
					break;
				case 'text_logo':
					$('#customize-control-text_logo_color').show();
					$('#customize-control-text_logo_tagline_color').show();				
					$('#customize-control-blogname').show();
					$('#customize-control-blogdescription').show();
					$('#customize-control-display_header_text').show();
					break;
			}
		}).change();
	});
})(jQuery);