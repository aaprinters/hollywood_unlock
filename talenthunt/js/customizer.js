/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */
(function($) {

    // Site title and description.
    wp.customize('blogname', function(value) {
        value.bind(function(to) {
            $('.site-title a').text(to);
        });
    });
    wp.customize('blogdescription', function(value) {
        value.bind(function(to) {
            $('.site-description').text(to);
        });
    });

    wp.customize('text_logo_color', function(value) {
        value.bind(function(to) {
            var text_logo_color = '#logo h1.site-title a{ color:' + to + '; }';
            if ($(document).find('#text_logo_color').length) {
                $(document).find('#text_logo_color').remove();
            }
            $(document).find('head').append($('<style id=text_logo_color>' + text_logo_color + '</style>'));
        });
    });

    wp.customize('text_logo_tagline_color', function(value) {
        value.bind(function(to) {
            $('#logo p').css('color', to);
        });
    });

    // Header background Color
    wp.customize('header_bg_color', function(value) {
        value.bind(function(to) {
            var header_bg_color = '#kaya-header-content-wrapper{ background:' + to + '; }';
            if ($(document).find('#header_bg_color').length) {
                $(document).find('#header_bg_color').remove();
            }
            $(document).find('head').append($('<style id=header_bg_color>' + header_bg_color + '</style>'));
        });
    });

    // Logo Section
    wp.customize('logo_image', function(value) {
        value.bind(function(to) {
            $('#logo a img').attr('src', to);
        });
    });

    // Menu Color Section
    wp.customize('menu_link_color', function(value) {
        value.bind(function(to) {
            var menu_link_color = '#header-navigation ul li a{ color:' + to + '; }';
            if ($(document).find('#menu_link_color').length) {
                $(document).find('#menu_link_color').remove();
            }
            $(document).find('head').append($('<style id=menu_link_color>' + menu_link_color + '</style>'));
        });
    });
    wp.customize('menu_link_hover_color', function(value) {
        value.bind(function(to) {
            var menu_link_hover_color = '#header-navigation ul li a:hover{ color:' + to + '; }';
            if ($(document).find('#menu_link_hover_color').length) {
                $(document).find('#menu_link_hover_color').remove();
            }
            $(document).find('head').append($('<style id=menu_link_hover_color>' + menu_link_hover_color + '</style>'));
        });
    });
    wp.customize('menu_link_hover_bg_color', function(value) {
        value.bind(function(to) {
            var menu_link_hover_bg_color = '#header-navigation ul li a:hover{ background-color:' + to + '; }';
            if ($(document).find('#menu_link_hover_bg_color').length) {
                $(document).find('#menu_link_hover_bg_color').remove();
            }
            $(document).find('head').append($('<style id=menu_link_hover_bg_color>' + menu_link_hover_bg_color + '</style>'));
        });
    });
    wp.customize('menu_active_link_color', function(value) {
        value.bind(function(to) {
            $('#header-navigation ul li.current-menu-item a').css('color', to);
        });
    });
    wp.customize('menu_active_bg_color', function(value) {
        value.bind(function(to) {
            $('#header-navigation ul li.current-menu-item a').css('background-color', to);
        });
    });
    wp.customize('child_menu_bg_color', function(value) {
        value.bind(function(to) {
            var child_menu_bg_color = '#header-navigation ul ul li a{ background:' + to + '; }';
            if ($(document).find('#child_menu_bg_color').length) {
                $(document).find('#child_menu_bg_color').remove();
            }
            $(document).find('head').append($('<style id=child_menu_bg_color>' + child_menu_bg_color + '</style>'));
        });
    });
    wp.customize('child_menu_link_color', function(value) {
        value.bind(function(to) {
            var child_menu_link_color = '#header-navigation ul ul li a{ color:' + to + '; }';
            if ($(document).find('#child_menu_link_color').length) {
                $(document).find('#child_menu_link_color').remove();
            }
            $(document).find('head').append($('<style id=child_menu_link_color>' + child_menu_link_color + '</style>'));
        });
    });
    wp.customize('child_menu_hover_bg_color', function(value) {
        value.bind(function(to) {
            var child_menu_hover_bg_color = '#header-navigation ul ul li a:hover{ background:' + to + '; }';
            if ($(document).find('#child_menu_hover_bg_color').length) {
                $(document).find('#child_menu_hover_bg_color').remove();
            }
            $(document).find('head').append($('<style id=child_menu_hover_bg_color>' + child_menu_hover_bg_color + '</style>'));
        });
    });

    wp.customize('child_menu_link_hover_color', function(value) {
        value.bind(function(to) {
            var child_menu_link_hover_color = '#header-navigation ul ul li a:hover{ color:' + to + '; }';
            if ($(document).find('#child_menu_link_hover_color').length) {
                $(document).find('#child_menu_link_hover_color').remove();
            }
            $(document).find('head').append($('<style id=child_menu_link_hover_color>' + child_menu_link_hover_color + '</style>'));
        });
    });
    wp.customize('child_menu_active_bg_color', function(value) {
        value.bind(function(to) {
            var child_menu_active_bg_color = '#header-navigation ul ul li.current-menu-item a{ background:' + to + '; }';
            if ($(document).find('#child_menu_active_bg_color').length) {
                $(document).find('#child_menu_active_bg_color').remove();
            }
            $(document).find('head').append($('<style id=child_menu_active_bg_color>' + child_menu_active_bg_color + '</style>'));
        });
    });
    wp.customize('child_menu_active_link_color', function(value) {
        value.bind(function(to) {
            var child_menu_active_link_color = '#header-navigation ul ul li.current-menu-item a{ color:' + to + '; }';
            if ($(document).find('#child_menu_active_link_color').length) {
                $(document).find('#child_menu_active_link_color').remove();
            }
            $(document).find('head').append($('<style id=child_menu_active_link_color>' + child_menu_active_link_color + '</style>'));
        });
    });

    // Advance Search color section
    wp.customize('search_icon_bg_color', function(value) {
        value.bind(function(to) {
            var search_icon_bg_color = 'div#flip i{ background:' + to + '; }';
            if ($(document).find('#search_icon_bg_color').length) {
                $(document).find('#search_icon_bg_color').remove();
            }
            $(document).find('head').append($('<style id=search_icon_bg_color>' + search_icon_bg_color + '</style>'));
        });
    });

    wp.customize('search_icon_color', function(value) {
        value.bind(function(to) {
            var search_icon_color = 'div#flip i{ color:' + to + '; }';
            if ($(document).find('#search_icon_color').length) {
                $(document).find('#search_icon_color').remove();
            }
            $(document).find('head').append($('<style id=search_icon_color>' + search_icon_color + '</style>'));
        });
    });

    wp.customize('search_icon_bg_hover_color', function(value) {
        value.bind(function(to) {
            var search_icon_bg_hover_color = 'div#flip i:hover{ background:' + to + '; }';
            if ($(document).find('#search_icon_bg_hover_color').length) {
                $(document).find('#search_icon_bg_hover_color').remove();
            }
            $(document).find('head').append($('<style id=search_icon_bg_hover_color>' + search_icon_bg_hover_color + '</style>'));
        });
    });

    wp.customize('search_icon_hover_color', function(value) {
        value.bind(function(to) {
            var search_icon_hover_color = 'div#flip i:hover{ color:' + to + '; }';
            if ($(document).find('#search_icon_hover_color').length) {
                $(document).find('#search_icon_hover_color').remove();
            }
            $(document).find('head').append($('<style id=search_icon_hover_color>' + search_icon_hover_color + '</style>'));
        });
    });

    wp.customize('search_bg_color', function(value) {
        value.bind(function(to) {
            var search_bg_color = '#panel{ background:' + to + '; }';
            if ($(document).find('#search_bg_color').length) {
                $(document).find('#search_bg_color').remove();
            }
            $(document).find('head').append($('<style id=search_bg_color>' + search_bg_color + '</style>'));
        });
    });
    wp.customize('search_title_color', function(value) {
        value.bind(function(to) {
            var search_title_color = '.advanced_search_wrapper label{ color:' + to + '; }';
            if ($(document).find('#search_title_color').length) {
                $(document).find('#search_title_color').remove();
            }
            $(document).find('head').append($('<style id=search_title_color>' + search_title_color + '</style>'));
        });
    });
    wp.customize('advance_search_bg_color', function(value) {
        value.bind(function(to) {
            var advance_search_bg_color = '.advanced_search_wrapper select{ background:' + to + '; }';
            if ($(document).find('#advance_search_bg_color').length) {
                $(document).find('#advance_search_bg_color').remove();
            }
            $(document).find('head').append($('<style id=advance_search_bg_color>' + advance_search_bg_color + '</style>'));
        });
    });
    wp.customize('advance_search_border_color', function(value) {
        value.bind(function(to) {
            var advance_search_border_color = '.advanced_search_wrapper select{ border:1px solid ' + to + '; }';
            if ($(document).find('#advance_search_border_color').length) {
                $(document).find('#advance_search_border_color').remove();
            }
            $(document).find('head').append($('<style id=advance_search_border_color>' + advance_search_border_color + '</style>'));
        });
    });
    wp.customize('advance_search_font_color', function(value) {
        value.bind(function(to) {
            var advance_search_font_color = '.advanced_search_wrapper select{ color:' + to + '; }';
            if ($(document).find('#advance_search_font_color').length) {
                $(document).find('#advance_search_font_color').remove();
            }
            $(document).find('head').append($('<style id=advance_search_font_color>' + advance_search_font_color + '</style>'));
        });
    });

    wp.customize('search_ui_slider_bg_color', function(value) {
        value.bind(function(to) {
            var search_ui_slider_bg_color = '.advanced_search_forms .ui-slider{ background:' + to + '; }';
            if ($(document).find('#search_ui_slider_bg_color').length) {
                $(document).find('#search_ui_slider_bg_color').remove();
            }
            $(document).find('head').append($('<style id=search_ui_slider_bg_color>' + search_ui_slider_bg_color + '</style>'));
        });
    });

    wp.customize('search_ui_slider_range_bg_color', function(value) {
        value.bind(function(to) {
            var search_ui_slider_range_bg_color = '.ui-slider .ui-slider-range{ background:' + to + '; }';
            if ($(document).find('#search_ui_slider_range_bg_color').length) {
                $(document).find('#search_ui_slider_range_bg_color').remove();
            }
            $(document).find('head').append($('<style id=search_ui_slider_range_bg_color>' + search_ui_slider_range_bg_color + '</style>'));
        });
    });


    wp.customize('advance_search_btn_bg_color', function(value) {
        value.bind(function(to) {
            var advance_search_btn_bg_color = 'input.search_data_submit{ background-color:' + to + '; }';
            if ($(document).find('#advance_search_btn_bg_color').length) {
                $(document).find('#advance_search_btn_bg_color').remove();
            }
            $(document).find('head').append($('<style id=advance_search_btn_bg_color>' + advance_search_btn_bg_color + '</style>'));
        });
    });
    wp.customize('advance_search_btn_border_color', function(value) {
        value.bind(function(to) {
            var advance_search_btn_border_color = 'input.search_data_submit{ border:1px solid ' + to + '; }';
            if ($(document).find('#advance_search_btn_border_color').length) {
                $(document).find('#advance_search_btn_border_color').remove();
            }
            $(document).find('head').append($('<style id=advance_search_btn_border_color>' + advance_search_btn_border_color + '</style>'));
        });
    });
    wp.customize('advance_search_btn_font_color', function(value) {
        value.bind(function(to) {
            var advance_search_btn_font_color = 'input.search_data_submit{ color:' + to + '; }';
            if ($(document).find('#advance_search_btn_font_color').length) {
                $(document).find('#advance_search_btn_font_color').remove();
            }
            $(document).find('head').append($('<style id=advance_search_btn_font_color>' + advance_search_btn_font_color + '</style>'));
        });
    });

    wp.customize('advance_search_btn_hover_bg_color', function(value) {
        value.bind(function(to) {
            var advance_search_btn_hover_bg_color = 'input.search_data_submit:hover{ background-color:' + to + '; }';
            if ($(document).find('#advance_search_btn_hover_bg_color').length) {
                $(document).find('#advance_search_btn_hover_bg_color').remove();
            }
            $(document).find('head').append($('<style id=advance_search_btn_hover_bg_color>' + advance_search_btn_hover_bg_color + '</style>'));
        });
    });
    wp.customize('advance_search_btn_hover_border_color', function(value) {
        value.bind(function(to) {
            var advance_search_btn_hover_border_color = 'input.search_data_submit:hover{ border:1px solid ' + to + '; }';
            if ($(document).find('#advance_search_btn_hover_border_color').length) {
                $(document).find('#advance_search_btn_hover_border_color').remove();
            }
            $(document).find('head').append($('<style id=advance_search_btn_hover_border_color>' + advance_search_btn_hover_border_color + '</style>'));
        });
    });
    wp.customize('advance_search_btn_hover_font_color', function(value) {
        value.bind(function(to) {
            var advance_search_btn_hover_font_color = 'input.search_data_submit:hover{ color:' + to + '; }';
            if ($(document).find('#advance_search_btn_hover_font_color').length) {
                $(document).find('#advance_search_btn_hover_font_color').remove();
            }
            $(document).find('head').append($('<style id=advance_search_btn_hover_font_color>' + advance_search_btn_hover_font_color + '</style>'));
        });
    });

    // Page titlebar color section
    wp.customize('page_titlebar_bg_color', function(value) {
        value.bind(function(to) {
            var page_titlebar_bg_color = '.kaya-page-titlebar-wrapper{ background:' + to + '; }';
            if ($(document).find('#page_titlebar_bg_color').length) {
                $(document).find('#page_titlebar_bg_color').remove();
            }
            $(document).find('head').append($('<style id=page_titlebar_bg_color>' + page_titlebar_bg_color + '</style>'));
        });
    });

    wp.customize('page_titlebar_color', function(value) {
        value.bind(function(to) {
            var page_titlebar_color = '.kaya-page-titlebar-wrapper .page-title{ color:' + to + '; }';
            if ($(document).find('#page_titlebar_color').length) {
                $(document).find('#page_titlebar_color').remove();
            }
            $(document).find('head').append($('<style id=page_titlebar_color>' + page_titlebar_color + '</style>'));
        });
    });

    //Talent Single Page Settings
    wp.customize('details_bg_color', function(value) {
        value.bind(function(to) {
            var details_bg_color = '.talent-single-page{ background:' + to + '; }';
            if ($(document).find('#details_bg_color').length) {
                $(document).find('#details_bg_color').remove();
            }
            $(document).find('head').append($('<style id=details_bg_color>' + details_bg_color + '</style>'));
        });
    });
    wp.customize('details_bg_top_border_color', function(value) {
        value.bind(function(to) {
            var details_bg_top_border_color = '.talent-single-page{ border-top:1px solid ' + to + '; }';
            if ($(document).find('#details_bg_top_border_color').length) {
                $(document).find('#details_bg_top_border_color').remove();
            }
            $(document).find('head').append($('<style id=details_bg_top_border_color>' + details_bg_top_border_color + '</style>'));
        });
    });
    wp.customize('details_bg_bottom_border_color', function(value) {
        value.bind(function(to) {
            var details_bg_bottom_border_color = '.talent-single-page{ border-bottom:1px solid ' + to + '; }';
            if ($(document).find('#details_bg_bottom_border_color').length) {
                $(document).find('#details_bg_bottom_border_color').remove();
            }
            $(document).find('head').append($('<style id=details_bg_bottom_border_color>' + details_bg_bottom_border_color + '</style>'));
        });
    });
    wp.customize('details_title_color', function(value) {
        value.bind(function(to) {
            var details_title_color = '.talenthunt_single_page_details h3{ color:' + to + '!important; }';
            if ($(document).find('#details_title_color').length) {
                $(document).find('#details_title_color').remove();
            }
            $(document).find('head').append($('<style id=details_title_color>' + details_title_color + '</style>'));
        });
    });
    wp.customize('details_list_font_color', function(value) {
        value.bind(function(to) {
            var details_list_font_color = '.talenthunt_single_page_details .general-meta-fields-info-wrapper li{ color:' + to + '; }';
            if ($(document).find('#details_list_font_color').length) {
                $(document).find('#details_list_font_color').remove();
            }
            $(document).find('head').append($('<style id=details_list_font_color>' + details_list_font_color + '</style>'));
        });
    });
    wp.customize('details_list_border_color', function(value) {
        value.bind(function(to) {
            var details_list_border_color = '.talenthunt_single_page_details .general-meta-fields-info-wrapper li{border-bottom:1px solid ' + to + '; }';
            if ($(document).find('#details_list_border_color').length) {
                $(document).find('#details_list_border_color').remove();
            }
            $(document).find('head').append($('<style id=details_list_border_color>' + details_list_border_color + '</style>'));
        });
    });

    //Talent Single Page Tabs Settings
    wp.customize('tab_active_bg', function(value) {
        value.bind(function(to) {
            var tab_active_bg = 'li.tab-active a{background-color:' + to + '!important; }';
            if ($(document).find('#tab_active_bg').length) {
                $(document).find('#tab_active_bg').remove();
            }
            $(document).find('head').append($('<style id=tab_active_bg>' + tab_active_bg + '</style>'));
        });
    });
    wp.customize('tabs_wrapper_border', function(value) {
        value.bind(function(to) {
            var tabs_wrapper_border = 'ul.tabs_content_wrapper{border-bottom:3px solid ' + to + '!important; }';
            if ($(document).find('#tabs_wrapper_border').length) {
                $(document).find('#tabs_wrapper_border').remove();
            }
            $(document).find('head').append($('<style id=tabs_wrapper_border>' + tabs_wrapper_border + '</style>'));
        });
    });
    wp.customize('tab_active_color', function(value) {
        value.bind(function(to) {
            var tab_active_color = 'li.tab-active a{color:' + to + '!important; }';
            if ($(document).find('#tab_active_color').length) {
                $(document).find('#tab_active_color').remove();
            }
            $(document).find('head').append($('<style id=tab_active_color>' + tab_active_color + '</style>'));
        });
    });
    wp.customize('tab_bg_color', function(value) {
        value.bind(function(to) {
            var tab_bg_color = 'ul.tabs_content_wrapper li a{background-color:' + to + '; }';
            if ($(document).find('#tab_bg_color').length) {
                $(document).find('#tab_bg_color').remove();
            }
            $(document).find('head').append($('<style id=tab_bg_color>' + tab_bg_color + '</style>'));
        });
    });
    wp.customize('tab_hover_bg_color', function(value) {
        value.bind(function(to) {
            var tab_hover_bg_color = 'ul.tabs_content_wrapper li a:hover{background-color: ' + to + '!important; }';
            if ($(document).find('#tab_hover_bg_color').length) {
                $(document).find('#tab_hover_bg_color').remove();
            }
            $(document).find('head').append($('<style id=tab_hover_bg_color>' + tab_hover_bg_color + '</style>'));
        });
    });
    wp.customize('tab_hover_color', function(value) {
        value.bind(function(to) {
            var tab_hover_color = 'ul.tabs_content_wrapper li a:hover{color: ' + to + '!important; }';
            if ($(document).find('#tab_hover_color').length) {
                $(document).find('#tab_hover_color').remove();
            }
            $(document).find('head').append($('<style id=tab_hover_color>' + tab_hover_color + '</style>'));
        });
    });

    // Page middle color section
    wp.customize('page_mid_contant_bg_color', function(value) {
        value.bind(function(to) {
            var page_mid_contant_bg_color = '#kaya-mid-content-wrapper{ background:' + to + '; }';
            if ($(document).find('#page_mid_contant_bg_color').length) {
                $(document).find('#page_mid_contant_bg_color').remove();
            }
            $(document).find('head').append($('<style id=page_mid_contant_bg_color>' + page_mid_contant_bg_color + '</style>'));
        });
    });
    wp.customize('page_mid_content_title_color', function(value) {
        value.bind(function(to) {
            var page_mid_content_title_color = '#kaya-mid-content-wrapper .mid-content h1, #kaya-mid-content-wrapper .mid-content h2, #kaya-mid-content-wrapper .mid-content h3, #kaya-mid-content-wrapper .mid-content h4, #kaya-mid-content-wrapper .mid-content h5, #kaya-mid-content-wrapper .mid-content h6, #kaya-mid-content-wrapper .mid-content h1 a, #kaya-mid-content-wrapper .mid-content h2 a, #kaya-mid-content-wrapper .mid-content h3 a, #kaya-mid-content-wrapper .mid-content h4 a, #kaya-mid-content-wrapper .mid-content h5 a, #kaya-mid-content-wrapper .mid-content h6 a{ color:' + to + '; }';
            if ($(document).find('#page_mid_content_title_color').length) {
                $(document).find('#page_mid_content_title_color').remove();
            }
            $(document).find('head').append($('<style id=page_mid_content_title_color>' + page_mid_content_title_color + '</style>'));
        });
    });
    wp.customize('page_mid_content_color', function(value) {
        value.bind(function(to) {
            var page_mid_content_color = '#kaya-mid-content-wrapper .mid-content p, #kaya-mid-content-wrapper .mid-content span{ color:' + to + '; }';
            if ($(document).find('#page_mid_content_color').length) {
                $(document).find('#page_mid_content_color').remove();
            }
            $(document).find('head').append($('<style id=page_mid_content_color>' + page_mid_content_color + '</style>'));
        });
    });
    wp.customize('page_mid_contant_links_color', function(value) {
        value.bind(function(to) {
            var page_mid_contant_links_color = '#kaya-mid-content-wrapper .mid-content a{ color:' + to + '; }';
            if ($(document).find('#page_mid_contant_links_color').length) {
                $(document).find('#page_mid_contant_links_color').remove();
            }
            $(document).find('head').append($('<style id=page_mid_contant_links_color>' + page_mid_contant_links_color + '</style>'));
        });
    });
    wp.customize('page_mid_contant_links_hover_color', function(value) {
        value.bind(function(to) {
            var page_mid_contant_links_hover_color = '#kaya-mid-content-wrapper .mid-content a:hover{ color:' + to + '; }';
            if ($(document).find('#page_mid_contant_links_hover_color').length) {
                $(document).find('#page_mid_contant_links_hover_color').remove();
            }
            $(document).find('head').append($('<style id=page_mid_contant_links_hover_color>' + page_mid_contant_links_hover_color + '</style>'));
        });
    });

    // Sidebar color section
    wp.customize('sidebar_bg_color', function(value) {
        value.bind(function(to) {
            var sidebar_bg_color = '#sidebar{ background:' + to + '; }';
            if ($(document).find('#sidebar_bg_color').length) {
                $(document).find('#sidebar_bg_color').remove();
            }
            $(document).find('head').append($('<style id=sidebar_bg_color>' + sidebar_bg_color + '</style>'));
        });
    });
    wp.customize('sidebar_title_color', function(value) {
        value.bind(function(to) {
            var sidebar_title_color = '#sidebar h1, #sidebar h2, #sidebar h3, #sidebar h4, #sidebar h5, #sidebar h6{ color:' + to + '; }';
            if ($(document).find('#sidebar_title_color').length) {
                $(document).find('#sidebar_title_color').remove();
            }
            $(document).find('head').append($('<style id=sidebar_title_color>' + sidebar_title_color + '</style>'));
        });
    });
    wp.customize('sidebar_content_color', function(value) {
        value.bind(function(to) {
            var sidebar_content_color = '#sidebar, #sidebar p, #sidebar span{ color:' + to + '; }';
            if ($(document).find('#sidebar_content_color').length) {
                $(document).find('#sidebar_content_color').remove();
            }
            $(document).find('head').append($('<style id=sidebar_content_color>' + sidebar_content_color + '</style>'));
        });
    });
    wp.customize('sidebar_links_color', function(value) {
        value.bind(function(to) {
            var sidebar_links_color = '#sidebar a{ color:' + to + '; }';
            if ($(document).find('#sidebar_links_color').length) {
                $(document).find('#sidebar_links_color').remove();
            }
            $(document).find('head').append($('<style id=sidebar_links_color>' + sidebar_links_color + '</style>'));
        });
    });
    wp.customize('sidebar_links_hover_color', function(value) {
        value.bind(function(to) {
            var sidebar_links_hover_color = '#sidebar a:hover{ color:' + to + '; }';
            if ($(document).find('#sidebar_links_hover_color').length) {
                $(document).find('#sidebar_links_hover_color').remove();
            }
            $(document).find('head').append($('<style id=sidebar_links_hover_color>' + sidebar_links_hover_color + '</style>'));
        });
    });

    wp.customize('sidebar_tagcloud_bg_color', function(value) {
        value.bind(function(to) {
            var sidebar_links_hover_color = '.tagcloud a { background:' + to + '; }';
            if ($(document).find('#sidebar_tagcloud_bg_color').length) {
                $(document).find('#sidebar_tagcloud_bg_color').remove();
            }
            $(document).find('head').append($('<style id=sidebar_tagcloud_bg_color>' + sidebar_tagcloud_bg_color + '</style>'));
        });
    });

    wp.customize('sidebar_tagcloud_color', function(value) {
        value.bind(function(to) {
            var sidebar_links_hover_color = '.tagcloud a { color:' + to + '; }';
            if ($(document).find('#sidebar_tagcloud_color').length) {
                $(document).find('#sidebar_tagcloud_color').remove();
            }
            $(document).find('head').append($('<style id=sidebar_tagcloud_color>' + sidebar_tagcloud_color + '</style>'));
        });
    });

    wp.customize('sidebar_tagcloud_bg_hover_color', function(value) {
        value.bind(function(to) {
            var sidebar_links_hover_color = '.tagcloud a:hover { background:' + to + '; }';
            if ($(document).find('#sidebar_tagcloud_bg_hover_color').length) {
                $(document).find('#sidebar_tagcloud_bg_hover_color').remove();
            }
            $(document).find('head').append($('<style id=sidebar_tagcloud_bg_hover_color>' + sidebar_tagcloud_bg_hover_color + '</style>'));
        });
    });

    wp.customize('sidebar_tagcloud_hover_color', function(value) {
        value.bind(function(to) {
            var sidebar_links_hover_color = '.tagcloud a:hover { color:' + to + '; }';
            if ($(document).find('#sidebar_tagcloud_hover_color').length) {
                $(document).find('#sidebar_tagcloud_hover_color').remove();
            }
            $(document).find('head').append($('<style id=sidebar_tagcloud_hover_color>' + sidebar_tagcloud_hover_color + '</style>'));
        });
    });

    // Button Settings 
    wp.customize('button_bg', function(value) {
        value.bind(function(to) {
            var button_bg = 'a.more-link, .form-submit #submit, .search-submit, a.scrolltop{ background:' + to + '; }';
            if ($(document).find('#button_bg').length) {
                $(document).find('#button_bg').remove();
            }
            $(document).find('head').append($('<style id=button_bg>' + button_bg + '</style>'));
        });
    });

    wp.customize('button_color', function(value) {
        value.bind(function(to) {
            var button_color = 'a.more-link, .form-submit #submit, .search-submit, a.scrolltop{ color:' + to + '; }';
            if ($(document).find('#button_color').length) {
                $(document).find('#button_color').remove();
            }
            $(document).find('head').append($('<style id=button_color>' + button_color + '</style>'));
        });
    });

    wp.customize('button_hover_bg', function(value) {
        value.bind(function(to) {
            var button_hover_bg = 'a.more-link:hover, .form-submit #submit:hover, .search-submit:hover, a.scrolltop:hover{ background:' + to + '; }';
            if ($(document).find('#button_hover_bg').length) {
                $(document).find('#button_hover_bg').remove();
            }
            $(document).find('head').append($('<style id=button_hover_bg>' + button_hover_bg + '</style>'));
        });
    });

    wp.customize('button_hover_color', function(value) {
        value.bind(function(to) {
            var button_hover_color = 'a.more-link:hover, .form-submit #submit:hover, .search-submit:hover, a.scrolltop:hover { color:' + to + '; }';
            if ($(document).find('#button_hover_color').length) {
                $(document).find('#button_hover_color').remove();
            }
            $(document).find('head').append($('<style id=button_hover_color>' + button_hover_color + '</style>'));
        });
    });


    // Footer Section
    wp.customize('footer_copy_rights', function(value) {
        value.bind(function(to) {
            $('.copyright').html(to);
        });
    });

    wp.customize('footer_bg_color', function(value) {
        value.bind(function(to) {
            var footer_bg_color = '#kaya-footer-content-wrapper{ background:' + to + '; }';
            if ($(document).find('#footer_bg_color').length) {
                $(document).find('#footer_bg_color').remove();
            }
            $(document).find('head').append($('<style id=footer_bg_color>' + footer_bg_color + '</style>'));
        });
    });
    wp.customize('footer_content_color', function(value) {
        value.bind(function(to) {
            var footer_content_color = '#kaya-footer-content-wrapper p, #kaya-footer-content-wrapper span, #kaya-footer-content-wrapper{ color:' + to + '; }';
            if ($(document).find('#footer_content_color').length) {
                $(document).find('#footer_content_color').remove();
            }
            $(document).find('head').append($('<style id=footer_content_color>' + footer_content_color + '</style>'));
        });
    });
    wp.customize('footer_link_color', function(value) {
        value.bind(function(to) {
            var footer_link_color = '#kaya-footer-content-wrapper a{ color:' + to + '; }';
            if ($(document).find('#footer_link_color').length) {
                $(document).find('#footer_link_color').remove();
            }
            $(document).find('head').append($('<style id=footer_link_color>' + footer_link_color + '</style>'));
        });
    });
    wp.customize('footer_link_hover_color', function(value) {
        value.bind(function(to) {
            var footer_link_hover_color = '#kaya-footer-content-wrapper a:hover{ color:' + to + '; }';
            if ($(document).find('#footer_link_hover_color').length) {
                $(document).find('#footer_link_hover_color').remove();
            }
            $(document).find('head').append($('<style id=footer_link_hover_color>' + footer_link_hover_color + '</style>'));
        });
    });

    // Page Footer
    wp.customize('page_footer_bg_color', function(value) {
        value.bind(function(to) {
            var page_footer_bg_color = '.kaya-page-content-footer{ background:' + to + '; }';
            if ($(document).find('#page_footer_bg_color').length) {
                $(document).find('#page_footer_bg_color').remove();
            }
            $(document).find('head').append($('<style id=page_footer_bg_color>' + page_footer_bg_color + '</style>'));
        });
    });
    wp.customize('page_footer_content_color', function(value) {
        value.bind(function(to) {
            var page_footer_content_color = '.kaya-page-content-footer p, .kaya-page-content-footer span, .kaya-page-content-footer{ color:' + to + '; }';
            if ($(document).find('#page_footer_content_color').length) {
                $(document).find('#page_footer_content_color').remove();
            }
            $(document).find('head').append($('<style id=page_footer_content_color>' + page_footer_content_color + '</style>'));
        });
    });
    wp.customize('page_footer_link_color', function(value) {
        value.bind(function(to) {
            var page_footer_link_color = '.kaya-page-content-footer a{ color:' + to + '; }';
            if ($(document).find('#page_footer_link_color').length) {
                $(document).find('#page_footer_link_color').remove();
            }
            $(document).find('head').append($('<style id=page_footer_link_color>' + page_footer_link_color + '</style>'));
        });
    });
    wp.customize('page_footer_link_hover_color', function(value) {
        value.bind(function(to) {
            var page_footer_link_hover_color = '.kaya-page-content-footer a:hover{ color:' + to + '; }';
            if ($(document).find('#page_footer_link_hover_color').length) {
                $(document).find('#page_footer_link_hover_color').remove();
            }
            $(document).find('head').append($('<style id=page_footer_link_hover_color>' + page_footer_link_hover_color + '</style>'));
        });
    });

    // Heading Fonts Sizes
    wp.customize('body_font_size', function(value) {
        value.bind(function(to) {
            var body_font_line_height = Math.round(1.6 * to);
            var body_font_size = 'body, p{ font-size:' + to + 'px!important; line-height:' + body_font_line_height + 'px;}';
            if ($(document).find('#body_font_size').length) {
                $(document).find('#body_font_size').remove();
            }
            $(document).find('head').append($('<style id="body_font_size">' + body_font_size + '</style>'));
        });
    });

    wp.customize('menu_font_size', function(value) {
        value.bind(function(to) {
            var menu_font_size = '.menu ul li a{ font-size:' + to + 'px!important;}';
            if ($(document).find('#menu_font_size').length) {
                $(document).find('#menu_font_size').remove();
            }
            $(document).find('head').append($('<style id="menu_font_size">' + menu_font_size + '</style>'));
        });
    });
    wp.customize('child_menu_font_size', function(value) {
        value.bind(function(to) {
            var child_menu_font_size = '.menu ul ul li a{ font-size:' + to + 'px!important;}';
            if ($(document).find('#child_menu_font_size').length) {
                $(document).find('#child_menu_font_size').remove();
            }
            $(document).find('head').append($('<style id="child_menu_font_size">' + child_menu_font_size + '</style>'));
        });
    });
    wp.customize('h1_title_fontsize', function(value) {
        value.bind(function(to) {
            var line_height_h1 = Math.round(1.1 * to);
            var h1_title_fontsize = 'h1{ font-size:' + to + 'px!important; line-height:' + line_height_h1 + 'px;}';
            if ($(document).find('#h1_title_fontsize').length) {
                $(document).find('#h1_title_fontsize').remove();
            }
            $(document).find('head').append($('<style id="h1_title_fontsize">' + h1_title_fontsize + '</style>'));
        });
    });

    wp.customize('h2_title_fontsize', function(value) {
        value.bind(function(to) {
            var line_height_h2 = Math.round(1.1 * to);
            var h2_title_fontsize = 'h2{ font-size:' + to + 'px!important; line-height:' + line_height_h2 + 'px;}';
            if ($(document).find('#h2_title_fontsize').length) {
                $(document).find('#h2_title_fontsize').remove();
            }
            $(document).find('head').append($('<style id="h2_title_fontsize">' + h2_title_fontsize + '</style>'));
        });
    });

    wp.customize('h3_title_fontsize', function(value) {
        value.bind(function(to) {
            var line_height_h3 = Math.round(1.1 * to);
            var h3_title_fontsize = 'h3{ font-size:' + to + 'px!important; line-height:' + line_height_h3 + 'px;}';
            if ($(document).find('#h3_title_fontsize').length) {
                $(document).find('#h3_title_fontsize').remove();
            }
            $(document).find('head').append($('<style id="h3_title_fontsize">' + h3_title_fontsize + '</style>'));
        });
    });
    wp.customize('h4_title_fontsize', function(value) {
        value.bind(function(to) {
            var line_height_h4 = Math.round(1.1 * to);
            var h4_title_fontsize = 'h4{ font-size:' + to + 'px!important; line-height:' + line_height_h4 + 'px;}';
            if ($(document).find('#h4_title_fontsize').length) {
                $(document).find('#h4_title_fontsize').remove();
            }
            $(document).find('head').append($('<style id="h4_title_fontsize">' + h4_title_fontsize + '</style>'));
        });
    });

    wp.customize('h5_title_fontsize', function(value) {
        value.bind(function(to) {
            var line_height_h5 = Math.round(1.1 * to);
            var h5_title_fontsize = 'h5{ font-size:' + to + 'px!important; line-height:' + line_height_h5 + 'px;}';
            if ($(document).find('#h5_title_fontsize').length) {
                $(document).find('#h5_title_fontsize').remove();
            }
            $(document).find('head').append($('<style id="h5_title_fontsize">' + h5_title_fontsize + '</style>'));
        });
    });
    wp.customize('h6_title_fontsize', function(value) {
        value.bind(function(to) {
            var line_height_h6 = Math.round(1.1 * to);
            var h6_title_fontsize = 'h6{ font-size:' + to + 'px!important; line-height:' + line_height_h6 + 'px;}';
            if ($(document).find('#h6_title_fontsize').length) {
                $(document).find('#h6_title_fontsize').remove();
            }
            $(document).find('head').append($('<style id="h6_title_fontsize">' + h6_title_fontsize + '</style>'));
        });
    });

    // Fonts
    var subset = ['latin,latin-ext,cyrillic,cyrillic-ext,greek,greek-ext,vietnamese'];
    var font_weights = ['100', '100italic', '200', '200italic', '300', '300italic', '400', '400italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'];
    // Frame Border
    wp.customize('frame_border_text_font_family', function(value) {
        value.bind(function(to) {
            if ('0' != to) {
                var replacestring = to.split(' ').join('+');
                var google_frame_border_text_font_family = 'http://fonts.googleapis.com/css?family=' + replacestring;
                var frame_border_text_font_family = '.toggle_menu_wrapper span, .header_contact_info span, .header_contact_info a,  .user_login_info span, .user_login_info a, .bottom_footer_bar_wrapper, .bottom_footer_bar_wrapper a, .bottom_footer_bar_wrapper span{ font-family:' + to + '!important}';
                if ($(document).find('#google_frame_border_text_font_family').length) {
                    $(document).find('#google_frame_border_text_font_family').remove();
                }
                if ($(document).find('#frame_border_text_font_family').length) {
                    $(document).find('#frame_border_text_font_family').remove();
                }
                $(document).find('head').append($("<link id='google_frame_border_text_font_family' href='" + google_frame_border_text_font_family + ":" + font_weights + "&subset=" + subset + "' rel='stylesheet' type='text/css'><style id='frame_border_text_font_family'>" + frame_border_text_font_family + "</style>"));
            } else {
                $(document).find('#frame_border_text_font_family').remove();
                $(document).find('#google_frame_border_text_font_family').remove();
                var frame_border_text_font_family = '.header_logo_wrapper h1.logo a, .header_logo_wrapper h1.sticky_logo a{ font-family:arial!important}';
                $(document).find('head').append($("<style>" + frame_border_text_font_family + "</style>"));
            }
        });
    });
    wp.customize('google_body_font', function(value) {
        value.bind(function(to) {
            if ('0' != to) {
                var replacestring = to.split(' ').join('+');
                var google_body_font = 'http://fonts.googleapis.com/css?family=' + replacestring;
                var body_font_family = 'body ,p, a{ font-family:' + to + '!important}';
                if ($(document).find('#google_body_font').length) {
                    $(document).find('#google_body_font').remove();
                }
                if ($(document).find('#body_font_family').length) {
                    $(document).find('#body_font_family').remove();
                }
                $(document).find('head').append($("<link id='google_body_font' href='" + google_body_font + ":" + font_weights + "&subset=" + subset + "' rel='stylesheet' type='text/css'><style id='body_font_family'>" + body_font_family + "</style>"));
            } else {
                $(document).find('#body_font_family').remove();
                $(document).find('#google_body_font').remove();
                var body_font_family = 'body ,p, a{ font-family:arial!important}';
                $(document).find('head').append($("<style>" + body_font_family + "</style>"));
            }
        });
    });

    wp.customize('google_heading_font', function(value) {
        value.bind(function(to) {
            if ('0' != to) {
                var replacestring = to.split(' ').join('+');
                var google_heading_font = 'http://fonts.googleapis.com/css?family=' + replacestring;
                var heading_font_family = 'h1,h2,h3,h4,h5,h6{ font-family:' + to + '!important}';
                if ($(document).find('#google_heading_font').length) {
                    $(document).find('#google_heading_font').remove();
                }
                if ($(document).find('#heading_font_family').length) {
                    $(document).find('#heading_font_family').remove();
                }
                $(document).find('head').append($("<link id='google_heading_font' href='" + google_heading_font + ":" + font_weights + "&subset=" + subset + "' rel='stylesheet' type='text/css'><style id='heading_font_family'>" + heading_font_family + "</style>"));
            } else {
                $(document).find('#google_heading_font').remove();
                $(document).find('#heading_font_family').remove();
                var heading_font_family = 'h1,h2,h3,h4,h5,h6{ font-family:arial!important}';
                $(document).find('head').append($("<style" + heading_font_family + "</style>"));
            }
        });
    });

    wp.customize('google_menu_font', function(value) {
        value.bind(function(to) {
            if ('0' != to) {
                var replacestring = to.split(' ').join('+');
                var google_menu_font = 'http://fonts.googleapis.com/css?family=' + replacestring;
                var menu_font_family = '.menu ul li a{ font-family:' + to + '!important}';
                if ($(document).find('#google_menu_font').length) {
                    $(document).find('#google_menu_font').remove();
                }
                if ($(document).find('#menu_font_family').length) {
                    $(document).find('#menu_font_family').remove();
                }
                $(document).find('head').append($("<link id='google_menu_font' href='" + google_menu_font + ":" + font_weights + "&subset=" + subset + "' rel='stylesheet' type='text/css'><style id='menu_font_family'>" + menu_font_family + "</style>"));

            } else {
                $(document).find('#google_menu_font').remove();
                $(document).find('#menu_font_family').remove();
                var menu_font_family = '.menu ul li a{ font-family:arial!important}';
                $(document).find('head').append($("<style>" + menu_font_family + "</style>"));
            }
        });
    });
    wp.customize('google_all_desc_font', function(value) {
        value.bind(function(to) {
            if ('0' != to) {
                var replacestring = to.split(' ').join('+');
                var google_all_desc_font = 'http://fonts.googleapis.com/css?family=' + replacestring;
                var titles_desc_font_family = 'span.menu_description, .portfolio_content_wrapper span.pf_title_wrapper, .pf_content_wrapper span, .search_box_style input, .search_box_style select, #mid_container_wrapper .pf_model_info_wrapper ul li span, .social_media_sharing_icons span.share_on_title, span.image_side_title, .custom_title_wrapper p, .testimonial_slider p, .meta_post_info span a, .blog_post_wrapper .readmore_button, span.meta_date_month, .quote_format h3, .widget_container .tagcloud a, .recent_posts_date, .comment_posted_date, div#comments input, div#comments textarea, blockquote p, .related_post_slider span, #slidecaption p{ font-family:' + to + '!important}';
                if ($(document).find('#google_all_desc_font').length) {
                    $(document).find('#google_all_desc_font').remove();
                }
                if ($(document).find('#titles_desc_font_family').length) {
                    $(document).find('#titles_desc_font_family').remove();
                }
                $(document).find('head').append($("<link id='google_all_desc_font' href='" + google_all_desc_font + ":" + font_weights + "&subset=" + subset + "' rel='stylesheet' type='text/css'><style id='titles_desc_font_family'>" + titles_desc_font_family + "</style>"));

            } else {
                $(document).find('#google_all_desc_font').remove();
                $(document).find('#titles_desc_font_family').remove();
                var titles_desc_font_family = 'span.menu_description, .portfolio_content_wrapper span.pf_title_wrapper, .pf_content_wrapper span, .search_box_style input, .search_box_style select, #mid_container_wrapper .pf_model_info_wrapper ul li span, .social_media_sharing_icons span.share_on_title, span.image_side_title, .custom_title_wrapper p, .testimonial_slider p, .meta_post_info span a, .blog_post_wrapper .readmore_button, span.meta_date_month, .quote_format h3, .widget_container .tagcloud a, .recent_posts_date, .comment_posted_date, div#comments input, div#comments textarea, blockquote p, .related_post_slider span, #slidecaption p{ font-family:arial!important}';
                $(document).find('head').append($("<style>" + titles_desc_font_family + "</style>"));
            }
        });
    });
    // Letter Spacing
    wp.customize('h1_font_letter_space', function(value) {
        value.bind(function(to) {
            var h1_font_letter_space = 'h1{ letter-spacing:' + to + 'px;}';
            if ($(document).find('#h1_font_letter_space').length) {
                $(document).find('#h1_font_letter_space').remove();
            }
            $(document).find('head').append($('<style id="h1_font_letter_space">' + h1_font_letter_space + '</style>'));
        });
    });

    wp.customize('h2_font_letter_space', function(value) {
        value.bind(function(to) {
            var h2_font_letter_space = 'h2{ letter-spacing:' + to + 'px;}';
            if ($(document).find('#h2_font_letter_space').length) {
                $(document).find('#h2_font_letter_space').remove();
            }
            $(document).find('head').append($('<style id="h2_font_letter_space">' + h2_font_letter_space + '</style>'));
        });
    });

    wp.customize('h3_font_letter_space', function(value) {
        value.bind(function(to) {
            var h3_font_letter_space = 'h3{ letter-spacing:' + to + 'px;}';
            if ($(document).find('#h3_font_letter_space').length) {
                $(document).find('#h3_font_letter_space').remove();
            }
            $(document).find('head').append($('<style id="h3_font_letter_space">' + h3_font_letter_space + '</style>'));
        });
    });

    wp.customize('h4_font_letter_space', function(value) {
        value.bind(function(to) {
            var h4_font_letter_space = 'h4{ letter-spacing:' + to + 'px;}';
            if ($(document).find('#h4_font_letter_space').length) {
                $(document).find('#h4_font_letter_space').remove();
            }
            $(document).find('head').append($('<style id="h4_font_letter_space">' + h4_font_letter_space + '</style>'));
        });
    });

    wp.customize('h5_font_letter_space', function(value) {
        value.bind(function(to) {
            var h5_font_letter_space = 'h5{ letter-spacing:' + to + 'px;}';
            if ($(document).find('#h5_font_letter_space').length) {
                $(document).find('#h5_font_letter_space').remove();
            }
            $(document).find('head').append($('<style id="h5_font_letter_space">' + h5_font_letter_space + '</style>'));
        });
    });
    wp.customize('h6_font_letter_space', function(value) {
        value.bind(function(to) {
            var h6_font_letter_space = 'h6{ letter-spacing:' + to + 'px;}';
            if ($(document).find('#h6_font_letter_space').length) {
                $(document).find('#h6_font_letter_space').remove();
            }
            $(document).find('head').append($('<style id="h6_font_letter_space">' + h6_font_letter_space + '</style>'));
        });
    });


    wp.customize('body_font_letter_space', function(value) {
        value.bind(function(to) {
            var body_font_letter_space = 'body,p{ letter-spacing:' + to + 'px;}';
            if ($(document).find('#body_font_letter_space').length) {
                $(document).find('#body_font_letter_space').remove();
            }
            $(document).find('head').append($('<style id="body_font_letter_space">' + body_font_letter_space + '</style>'));
        });
    });

    wp.customize('menu_font_letter_space', function(value) {
        value.bind(function(to) {
            var menu_font_letter_space = '.menu ul li a{ letter-spacing:' + to + 'px;}';
            if ($(document).find('#menu_font_letter_space').length) {
                $(document).find('#menu_font_letter_space').remove();
            }
            $(document).find('head').append($('<style id="menu_font_letter_space">' + menu_font_letter_space + '</style>'));
        });
    });

    wp.customize('child_menu_font_letter_space', function(value) {
        value.bind(function(to) {
            var child_menu_font_letter_space = '.menu ul ul li a, .wide_menu strong{ letter-spacing:' + to + 'px;}';
            if ($(document).find('#child_menu_font_letter_space').length) {
                $(document).find('#child_menu_font_letter_space').remove();
            }
            $(document).find('head').append($('<style id="child_menu_font_letter_space">' + child_menu_font_letter_space + '</style>'));
        });
    });
    wp.customize('child_menu_font_size', function(value) {
        value.bind(function(to) {
            var child_menu_font_size = '.menu ul ul li a, .wide_menu strong{ font-size:' + to + 'px!important;}';
            if ($(document).find('#child_menu_font_size').length) {
                $(document).find('#child_menu_font_size').remove();
            }
            $(document).find('head').append($('<style id="child_menu_font_size">' + child_menu_font_size + '</style>'));
        });
    });
    // Desc Font Size
    wp.customize('menu_desc_font_size', function(value) {
        value.bind(function(to) {
            var menu_desc_font_size = '.menu span.menu_description{ font-size:' + to + 'px!important;}';
            if ($(document).find('#menu_desc_font_size').length) {
                $(document).find('#menu_desc_font_size').remove();
            }
            $(document).find('head').append($('<style id="menu_desc_font_size">' + menu_desc_font_size + '</style>'));
        });
    });
    wp.customize('menu_desc_letter_space', function(value) {
        value.bind(function(to) {
            var menu_desc_letter_space = '.menu span.menu_description{ letter-spacing:' + to + 'px;}';
            if ($(document).find('#menu_desc_letter_space').length) {
                $(document).find('#menu_desc_letter_space').remove();
            }
            $(document).find('head').append($('<style id="menu_desc_letter_space">' + menu_desc_letter_space + '</style>'));
        });
    });
    wp.customize('menu_desc_font_weight', function(value) {
        value.bind(function(to) {
            var menu_desc_font_weight = '.menu span.menu_description{ font-weight:' + to + '; }';
            if ($(document).find('#menu_desc_font_weight').length) {
                $(document).find('#menu_desc_font_weight').remove();
            }
            $(document).find('head').append($('<style id="menu_desc_font_weight">' + menu_desc_font_weight + '</style>'));
        });
    });
    // Typography
    // Body
    wp.customize('body_font_weight_bold', function(value) {
        value.bind(function(to) {
            var body_font_weight_bold = 'body, p{ font-weight:' + to + ';}';
            if ($(document).find('#body_font_weight_bold').length) {
                $(document).find('#body_font_weight_bold').remove();
            }
            $(document).find('head').append($('<style id="body_font_weight_bold">' + body_font_weight_bold + '</style>'));
        });
    });
    // Menu
    wp.customize('menu_font_weight', function(value) {
        value.bind(function(to) {
            var menu_font_weight = '.menu ul li a{ font-weight:' + to + ';}';
            if ($(document).find('#menu_font_weight').length) {
                $(document).find('#menu_font_weight').remove();
            }
            $(document).find('head').append($('<style id="menu_font_weight">' + menu_font_weight + '</style>'));
        });
    });
    wp.customize('child_menu_font_weight', function(value) {
        value.bind(function(to) {
            var child_menu_font_weight = '.menu ul ul li a{ font-weight:' + to + ';}';
            if ($(document).find('#child_menu_font_weight').length) {
                $(document).find('#child_menu_font_weight').remove();
            }
            $(document).find('head').append($('<style id="child_menu_font_weight">' + child_menu_font_weight + '</style>'));
        });
    });
    //titles
    wp.customize('h1_font_weight_bold', function(value) {
        value.bind(function(to) {
            var h1_font_weight_bold = 'h1{ font-weight:' + to + ';}';
            if ($(document).find('#h1_font_weight_bold').length) {
                $(document).find('#h1_font_weight_bold').remove();
            }
            $(document).find('head').append($('<style id="h1_font_weight_bold">' + h1_font_weight_bold + '</style>'));
        });
    });

    wp.customize('h2_font_weight_bold', function(value) {
        value.bind(function(to) {
            var h2_font_weight_bold = 'h2{ font-weight:' + to + ';}';
            if ($(document).find('#h2_font_weight_bold').length) {
                $(document).find('#h2_font_weight_bold').remove();
            }
            $(document).find('head').append($('<style id="h2_font_weight_bold">' + h2_font_weight_bold + '</style>'));
        });
    });

    wp.customize('h3_font_weight_bold', function(value) {
        value.bind(function(to) {
            var h3_font_weight_bold = 'h3, .woocommerce ul.products li.product h3, .woocommerce-page ul.products li.product h3{ font-weight:' + to + ';}';
            if ($(document).find('#h3_font_weight_bold').length) {
                $(document).find('#h3_font_weight_bold').remove();
            }
            $(document).find('head').append($('<style id="h3_font_weight_bold">' + h3_font_weight_bold + '</style>'));
        });
    });

    wp.customize('h4_font_weight_bold', function(value) {
        value.bind(function(to) {
            var h4_font_weight_bold = 'h4{ font-weight:' + to + ';}';
            if ($(document).find('#h4_font_weight_bold').length) {
                $(document).find('#h4_font_weight_bold').remove();
            }
            $(document).find('head').append($('<style id="h4_font_weight_bold">' + h4_font_weight_bold + '</style>'));
        });
    });

    wp.customize('h5_font_weight_bold', function(value) {
        value.bind(function(to) {
            var h5_font_weight_bold = 'h5{ font-weight:' + to + ';}';
            if ($(document).find('#h5_font_weight_bold').length) {
                $(document).find('#h5_font_weight_bold').remove();
            }
            $(document).find('head').append($('<style id="h5_font_weight_bold">' + h5_font_weight_bold + '</style>'));
        });
    });

    wp.customize('h6_font_weight_bold', function(value) {
        value.bind(function(to) {
            var h6_font_weight_bold = 'h6{ font-weight:' + to + ';}';
            if ($(document).find('#h6_font_weight_bold').length) {
                $(document).find('#h6_font_weight_bold').remove();
            }
            $(document).find('head').append($('<style id="h6_font_weight_bold">' + h6_font_weight_bold + '</style>'));
        });
    });

})(jQuery);