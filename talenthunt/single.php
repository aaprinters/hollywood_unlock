<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package talenyhunt_kaya
 */

get_header(); ?>
	<div class="mid-content"> <!-- Middle content align -->
	<div class="talent-single-page">
		<?php
		$cpt_slug_name = get_query_var( 'post_type' ); // Change your own cpt slug name here
		while ( have_posts() ) : the_post();
			// Cpt Post shortlist session
			$selected = '';
			if(isset($_SESSION['shortlist'])) {
				if ( in_array(get_the_ID(), $_SESSION['shortlist']) ) {
					$selected = 'item_selected';
				}
			}
			$img_url = wp_get_attachment_url(get_post_thumbnail_id());
			$img_caption = wp_get_attachment_caption(get_post_thumbnail_id());
			$author_id = get_post_field ('post_author', get_the_ID());
			$user_info = get_userdata($author_id);
			$display_name = $user_info->user_login;
			$user_name = basename(get_permalink(get_the_ID()));
			$contct_link = site_url().'/message-box/?fepaction=newmessage&user_name='.$user_name;
			// Post Data content wrapper note:don't delete this ID and class
			echo '<div class="post_single_page_content_wrapper item '.$selected.'" id="'.get_the_ID().'">'; // Post Data content wrapper note:don't delete this ID and class
			echo '<div class="talenthunt_single_page">';
			echo '<div class="talenthunt_single_page_img">';
			if( !empty($img_url) ){
				echo '<img src="'.talenthunt_kaya_image_sizes($img_url,'0','0').'" />';
				echo '<p class="image_caption">'.$img_caption.'</p>';
			}else{
				echo '<img src="'.get_template_directory_uri().'/images/default_image.png" />';
			}
			echo '<div class="contact_user"><a href="'.$contct_link.'" class="action add" data-action="add"><i class="fas fa-envelope"></i>  Contact Now</a></div>';
			if (function_exists('kaya_pods_cpt_shortlist_text_buttons'))
			{
			echo kaya_pods_cpt_shortlist_text_buttons();
			}
			if (function_exists('kaya_pods_cpt_compcard_images'))
			{
				echo kaya_pods_cpt_compcard_images(get_query_var( 'post_type' ));
			}
			$categories = get_the_terms(get_the_ID(), 'talent_category' );

			//$category = $category[0];

			echo '</div>';
			echo '<div class="talenthunt_single_page_details">';
			echo '<div class="title_section"><h3 class="talent_title">'.get_the_title().'</h3>';
			if(is_user_logged_in()){
				$user_id = get_current_user_id();
				$user_post_id = get_user_meta( $user_id, 'talent_post_id',true); //$user_id is passed as a parameter
				$post_type = get_post_type( get_the_ID() );
				$current_post_id = get_the_ID();
				if($user_post_id == $current_post_id){
					$edit_profile = '';
					if($post_type == 'talent'){
						$edit_profile = site_url().'/talent-profile/?action=edit&id='.get_the_ID();
					}else if($post_type == 'project'){
						$edit_profile = site_url().'/project/?action=edit&id='.get_the_ID();
					}
					echo '<span class="edit_link"><i class="fa fa-pencil"></i><a href="'.$edit_profile.'">Edit</a></span>';
				}
			}
			echo '</div>';
			if($categories){
			echo '<div class="category_section">';
				foreach($categories as $category){
					echo'<h3 class="talent_category">'.ucfirst($category->name).'</h3>';
				}
				echo '</div>';
			}
			if( function_exists('kaya_general_info_section') ){
				kaya_general_info_section($cpt_slug_name); // Fields Information
			}
			echo '</div>';			
			echo '</div>';
			echo '</div>';
			
			// Images & Videos
			echo '</div>'; // End post Single content Wrapper
		endwhile; // End of the loop.
		if( function_exists('kaya_media_section') ){
				echo '<div class="single_tabs_content_wrapper">';
				kaya_tab_section($cpt_slug_name);
				kaya_media_section($cpt_slug_name); // Images, Videos & Rich Textarea Information
				echo '</div>';
			}
		?>
	</div> <!-- End -->
	
	
<?php get_footer(); ?>