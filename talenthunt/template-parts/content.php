<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package talenthunt_kaya
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php
		
		if ( 'post' === get_post_type() ) : 
			if ( has_post_thumbnail() ) {
				$img_url = wp_get_attachment_url(get_post_thumbnail_id());
					if( !empty($img_url) ){
						echo '<div class="post_image">';
							echo '<a href="'.get_the_permalink().'">';
							  echo '<img src="'.talenthunt_kaya_image_sizes($img_url, '780', 450, 't').'" class="" alt="'.get_the_title().'" />';  
							echo '</a>';
						echo '</div>';
					}   
			} 	?>

		<div class="post-title-meta">	
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
		?>
		<div class="post-meta-data">
			<?php talenthunt_kaya_posted_on(); ?>
		</div><!-- .entry-meta -->
		</div>
       <?php $kaya_readmore_blog=get_theme_mod('readmore_button_text') ? get_theme_mod('readmore_button_text') : 'Read More'; ?>
		<div class="description">
			<?php 
			if ( is_single() ) :
				echo talenthunt_kaya_content_display_words(5000); 
			else:
				echo talenthunt_kaya_content_display_words(30); 
			endif;  
		   ?>
		</div>
		 <?php  echo the_content($kaya_readmore_blog,'');	?>
		<?php
		endif; ?>
</article><!-- #post-## -->

