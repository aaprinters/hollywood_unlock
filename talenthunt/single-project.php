<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package talenyhunt_kaya
 */

get_header(); ?>
	<div class="mid-content"> <!-- Middle content align -->
	<div class="talent-single-page">
		<?php
		$cpt_slug_name = get_query_var( 'post_type' ); // Change your own cpt slug name here
		while ( have_posts() ) : the_post();
			// Cpt Post shortlist session
			$selected = '';
			if(isset($_SESSION['shortlist'])) {
				if ( in_array(get_the_ID(), $_SESSION['shortlist']) ) {
					$selected = 'item_selected';
				}
			}
			$img_url = wp_get_attachment_url(get_post_thumbnail_id());
			$img_caption = wp_get_attachment_caption(get_post_thumbnail_id());

			// Post Data content wrapper note:don't delete this ID and class
			echo '<div class="post_single_page_content_wrapper item '.$selected.'" id="'.get_the_ID().'">'; // Post Data content wrapper note:don't delete this ID and class
			echo '<div class="talenthunt_single_page">';
		/* 	echo '<div class="talenthunt_single_page_img">';
			if( !empty($img_url) ){
				echo '<img src="'.talenthunt_kaya_image_sizes($img_url,'0','0').'" />';
				echo '<p class="image_caption">'.$img_caption.'</p>';
			}else{
				echo '<img src="'.get_template_directory_uri().'/images/default_image.png" />';
			}
			if (function_exists('kaya_pods_cpt_shortlist_text_buttons'))
			{
			echo kaya_pods_cpt_shortlist_text_buttons();
			}
			if (function_exists('kaya_pods_cpt_compcard_images'))
			{
				echo kaya_pods_cpt_compcard_images(get_query_var( 'post_type' ));
			}

			//$category = $category[0];

			echo '</div>'; */
			$project_genres = get_the_terms(get_the_ID(), 'project_genre' );
			$project_types = get_the_terms(get_the_ID(), 'project_type' );

			echo '<div class="talenthunt_single_page_details">';
			echo '<div class="title_section"><h3 class="talent_title">'.get_the_title().'</h3>';
			$author_id = get_post_field ('post_author', get_the_ID());
			$display_name = get_the_author_meta( 'display_name' , $author_id );

			$contct_link = site_url().'/message-box/?fepaction=newmessage&user_name='.$display_name;
			if(is_user_logged_in()){
				$user_id = get_current_user_id();
				$user_post_id = get_post_field( 'post_author', $post_id );; //$user_id is passed as a parameter
				$post_type = get_post_type( get_the_ID() );
				if($user_post_id == $user_id){
					$edit_profile = '';
					if($post_type == 'project'){
						$edit_profile = site_url().'/projects/?action=edit&id='.get_the_ID();
					}
					echo '<span class="edit_link"><i class="fa fa-pencil"></i><a href="'.$edit_profile.'">Edit</a></span>';
				}else{
					echo '<div class="apply_now"><a href="'.$contct_link.'" class="elementor-button-link elementor-button elementor-size-sm" data-action="add">Apply Now</a>'; 

				}

			}
			echo '</div>';
			if($project_genres){
			echo '<div class="category_section">';
				foreach($project_genres as $project_genre){
					echo'<h3 class="project_genre">'.ucfirst($project_genre->name).'</h3>';
				}
				echo '</div>';
			}	
			if($project_types){
			echo '<div class="general-meta-fields-info-wrapper project_type">';
			echo '<ul><li><strong>Project Type: </strong>';
				foreach($project_types as $project_type){
					echo ucfirst($project_type->name).', ';
				}
				echo '</li></ul></div>';
			}
			if( function_exists('kaya_general_info_section') ){
				kaya_general_info_section($cpt_slug_name); // Fields Information
			}
			echo '</div>';			
			echo '</div>';
			echo '</div>';
			
			// Images & Videos
			echo '</div>'; // End post Single content Wrapper
		endwhile; // End of the loop.
		if( function_exists('kaya_media_section') ){
				echo '<div class="single_tabs_content_wrapper">';
				kaya_tab_section($cpt_slug_name);
				kaya_media_section($cpt_slug_name); // Images, Videos & Rich Textarea Information
				echo '</div>';
			}
		?>
	</div> <!-- End -->
	
	
<?php get_footer(); ?>