<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package talenthunt_kaya
 */

get_header(); ?>

	<div class="cpt-post-wrapper fullwidth"> <!-- Middle content align -->
		<?php
		echo '<div class="kaya-post-content-wrapper">';
		if ( have_posts() ) : ?>
			<?php
			/* Start the Loop */
			//echo '<div class="kaya-post-content-wrapper">';
			?>

<div class="filter_tabs"><div class="filter filter13" id="filter"><ul><li class="all"><button type="button" onclick="history.back();">All</button></li></ul></div></div>
<?php
				echo '<ul class="column-extra">';
				while ( have_posts() ) : the_post();
					/**
					 * Run the loop for the search to output the results.
					 * If you want to overload this in a child theme then include a file
					 * called content-search.php and that will be used instead.
					 */
					// POD CPT Search Start here					
					if( function_exists('kaya_get_template_part') ){ 
						kaya_get_template_part( 'loop', 'content' );
					}else{
						get_template_part( 'template-parts/content', 'search' );
					}					
					// POD CPT Search Start here					
				endwhile;
				echo '</ul>';
				talenthunt_kaya_pagination();
			else :
			get_template_part( 'template-parts/content', 'none' );		
		//echo '</div>';
		endif;
		echo '</div>'; ?>
		</div> <!-- End -->
	
<?php get_footer(); ?>