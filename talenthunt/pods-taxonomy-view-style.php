<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package casting_kaya
 */
global $kaya_options, $kaya_shortlist_options;
$cpt_slug_name = kaya_get_post_type();
if( !is_author() ){
	$columns = !empty($kaya_options->taxonomy_columns) ? $kaya_options->taxonomy_columns : '4';
}else{
	$columns = '3';
}
$image_cropping_type = !empty($kaya_options->choose_image_sizes) ? $kaya_options->choose_image_sizes : 'wp_image_sizes';
if( $image_cropping_type == 'wp_image_sizes' ){
	$image_sizes = !empty($kaya_options->choose_image_sizes) ? $kaya_options->choose_image_sizes : 'full';
}else{
	$image_size_width = !empty($kaya_options->taxonomy_gallery_width) ? $kaya_options->taxonomy_gallery_width : '380';
	$image_size_height = !empty($kaya_options->taxonomy_gallery_height) ? $kaya_options->taxonomy_gallery_height : '600';
	$image_sizes = array( $image_size_width, $image_size_height );
}

// Session for shortlist data
if(isset($_SESSION['shortlist'])) {
	if ( in_array(get_the_ID(), $_SESSION['shortlist']) ) {
		$selected = 'item_selected';
	}else{
		$selected = '';
	}
}else{
	$selected = '';
}
		$img_url = wp_get_attachment_url(get_post_thumbnail_id());
	echo '<li class="column'.$columns.' item" id="'.get_the_ID().'">';
		echo '<div class="talent_image_details_wrapper">';
			//echo '<strong>'.get_the_title().'</strong>';
			echo '<a href="'.get_the_permalink().'" class="img_hover_effect">';
				echo kaya_pod_featured_img( $image_sizes, $image_cropping_type );
				if( function_exists('kaya_general_info_section') ){
					echo '<div class="talents_details">';
					kaya_general_info_section($cpt_slug_name);
					echo '</div>';
				}
			echo '</a>';
			// check this function to enabled shortlist icons or not
			if(!empty($kaya_shortlist_options['enable_cpt_shortlist'])){
				if( in_array($cpt_slug_name, $kaya_shortlist_options['enable_cpt_shortlist']) ){ 
					do_action('kaya_pods_cpt_shortlist_icons'); // Shortlist Icons
				}
			}	
			
		
		echo '</div>';
		//echo '<div class="talent_title">';
			//echo '<h4><a href="'.get_the_permalink().'">'.get_the_title().'</a></h4>';
		//echo '</div>';
	echo '</li>';

?>