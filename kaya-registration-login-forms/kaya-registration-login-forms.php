<?php
/**
 * Plugin Name:User Registration & Login form.
 * Plugin URI: http://themeforest.net/user/kayapati
 * Description: Simple plugin to display the Registration Form, Login Form, Forgot Password Form and User Profile page using widgets.
 * Version: 1.0.2
 * Author: Venisha IT Team
 * Author URI: http://themeforest.net/user/kayapati
 * Text Domain: kaya_forms
 * Domain Path: /languages
 */

/**
* Main Registration Login Form Class
*/

// Plugin Auto Updates

class Kaya_Reg_Login_Forms{
	function __construct(){
		$this->kaya_include_widgets();
		$this->kaya_include_files();
		add_action( 'admin_enqueue_scripts', array(&$this,'kaya_admin_enqueue_styles')); //Admin style files
		add_action( 'wp_enqueue_scripts', array(&$this,'kaya_load_styles')); //style files	
		add_action('after_setup_theme', array(&$this, 'kaya_user_dashboard_menu'));
	}
	/**
	 * Including plugin files
	 */
	function kaya_include_files(){
		require_once 'inc/functions.php';
		require_once 'inc/class_admin_settings.php';
		require_once 'inc/admin_color_settings.php';
		require_once 'inc/reg_login_export_settings.php';
	}
	/**
	 * Including plugin widgets
	 */
	function kaya_include_widgets(){
		require_once 'widgets/kaya_user_register.php';
		require_once 'widgets/kaya_user_login.php'; 
		require_once 'widgets/kaya_user_fogot_password.php';
		require_once 'widgets/kaya_user_profile.php';
	}
	/**
	 * Load plugin front end styles
	 */
	function kaya_load_styles(){
		wp_enqueue_style( 'kaya-styles', plugin_dir_url( __FILE__ ) . 'css/styles.css', array(), time(), 'all' );
		wp_enqueue_script( 'kaya-script', plugin_dir_url( __FILE__ ) . 'js/custom.js',array(), time(), true);
		wp_enqueue_style( 'bootstrap-styles', plugin_dir_url( __FILE__ ) . 'css/bootstrap.css', array(), '', 'all' );
	}
	/**
	 * Load plugin Admin styles & scripts
	 */
	function kaya_admin_enqueue_styles(){
		wp_enqueue_style( 'kaya-admin-styles', plugin_dir_url( __FILE__ ) . 'css/admin-styles.css', array(), '', 'all' );
		wp_enqueue_style( 'wp-color-picker' );
	}
	/**
	 * admin user dashboard menu
	 */
	function kaya_user_dashboard_menu(){
		register_nav_menu( 'user-dashboard', __( 'User Dashboard', 'talentagency' ) );
	}
}
new Kaya_Reg_Login_Forms;