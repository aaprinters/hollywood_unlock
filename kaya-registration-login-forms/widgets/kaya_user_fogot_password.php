<?php
class Kaya_User_Forgotpassword_Widget extends WP_Widget{
	public function __construct(){
		global $login_page;
	    parent::__construct('kaya-user-forgot_password',
	    __('Kaya - Forgot Password Form','kaya_forms'),
	    array('description' => __('Use this widget to create user forgot password form', 'kaya_forms'))
	    );
	    add_filter('send_password_change_email', '__return_false');
	}
	public function widget($args, $instance){		
		echo $args['before_widget'];
		global $login_page, $success, $err, $kaya_settings;
		$instance = wp_parse_args( $instance, array(
			'forgot_password_form_title' => __('Forgot Password', 'kaya_forms'),
		));
		global $success, $err, $current_user;
		$current_user_data = get_userdata($current_user->ID);
		$logout_redirect_page = $kaya_settings['logout_redirect_page'] ? $kaya_settings['logout_redirect_page'] : '';
		$logout_button_text = !empty( $kaya_settings['logout_button_text'] ) ? trim($kaya_settings['logout_button_text']) :  __( 'Logout', 'kaya_forms' );
		$hi_button_text = !empty( $kaya_settings['hi_button_text'] ) ? trim($kaya_settings['hi_button_text']) :  __( 'Hi', 'kaya_forms' );
		if(is_user_logged_in()){
			echo '<p class="kta-notice kta-center">'.$hi_button_text.' '.$current_user_data->user_login.' <a href="'.wp_logout_url( get_the_permalink($logout_redirect_page) ).'">'.$logout_button_text.'</a></p>';
			return;
		}

		$form_type = 'forgot_password';
		echo '<div class="kaya-user-form kaya-user-registration-login-form">';
			 echo '<div class="user_forget_password_form">';
				//echo '<h4>'.( !empty($instance['forgot_password_form_title']) ? $instance['forgot_password_form_title'] : __('Forgot Password','kaya_forms') ).'</h4>';

			// echo ( !empty($instance['forgot_form_title'] ) ? '<h3>'. $instance['forgot_form_title'].'</h3>' : '');
	 		if( isset( $_POST['user_get_new_password'] ) && 'reset' == $_POST['user_get_new_password'] ) 
	        {
	            $email = trim($_POST['user_login']);            
	            if( empty( $email ) ) {
	                $error = __('Enter e-mail address..', 'kaya_forms');
	            } else if( ! is_email( $email )) {
	                 $error = __('Invalid e-mail address.','kaya_forms');
	            } else if( ! email_exists( $email ) ) {
	                 $error = __('There is no user registered with that email address.','kaya_forms');
	            } else {                
	                $random_password = wp_generate_password( 12, false );
	                $user = get_user_by( 'email', $email );                
	                $update_user = wp_update_user( array (
	                        'ID' => $user->ID, 
	                        'user_pass' => $random_password
	                    )
	                );                
	                // if  update user return true then lets send user an email containing the new password
	                if( $update_user ) {
	                    $to = $email;
		                $subject 	= __('Your new password','kaya_forms');
		                $sender 	= get_option('name');
		                $message   .= __('User Name is','kaya_forms').': '.$user->user_login."<br />";
		                $message   .= __('Your New Password is','kaya_forms').': '.$random_password;		                
		                $headers[] 	= 'MIME-Version: 1.0' . "\r\n";
		                $headers[]  = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		                $headers[]  = "X-Mailer: PHP \r\n";
		                $headers[] 	= 'From: '.$sender.' < '.$email.'>' . "\r\n";
               			$mail = wp_mail( $to, $subject, $message, $headers );
	                    if( $mail )
	                        $success = $kaya_settings['forget_password_success_msg'] ? $kaya_settings['forget_password_success_msg'] : __('Please check your email id for new password', 'kaya_forms');                        
		                } else {
		                    $error = $kaya_settings['forget_password_error_msg'] ? $kaya_settings['forget_password_error_msg'] : __('sorry, we could not find entered email id', 'kaya_forms'); 
		                }                
	            	}            
	            
	        	}
	   		 ?> 
		    <form method="post" class="kaya-form">
	            <?php if( !empty($instance['user_forgot_password_msg']) ){ echo '<p>'.$instance['user_forgot_password_msg'].'</p>'; } ?>
	            <div class="kaya-panel">
	            <?php echo '<h4>'.( !empty($instance['forgot_password_form_title']) ? $instance['forgot_password_form_title'] : __('Forgot Password','kaya_forms') ).'</h4>';
	            if( ! empty( $error ) )
                	echo '<div class="message"><p class="error_login">'. $error .'</p></div>';            
            	if( ! empty( $success ) )
                	echo '<div class="error_login"><p class="success">'. $success .'</p></div>';
                ?>	
	            <p><label for="user_login"><?php esc_html_e('User E-mail', 'kaya_forms'); ?>:</label>
	                <?php $user_login = isset( $_POST['user_login'] ) ? $_POST['user_login'] : ''; ?>
	                <input type="text" name="user_login" id="user_login" value="<?php echo $user_login; ?>" /> 
	            </p>
	                <input type="hidden" name="user_get_new_password" value="reset" />
	                <input type="submit" value="<?php echo !empty($instance['register_form_button_text']) ? $instance['register_form_button_text'] :  __('Get New Password', 'kaya_forms'); ?>" class="readmore_button" id="wp-submit" />
		    	</div>
		    </form>	 
		 </div>
	<?php 
	echo '</div>';
	echo $args['after_widget'];
}
public function form($instance){
		$instance = wp_parse_args($instance,array(
			'forgot_password_form_title' => __('Forgot Password', 'kaya_forms'),
		));
		?>
		<p>
			<label for="<?php echo $this->get_field_id('forgot_password_form_title') ?>">  <?php _e("Forgot Form Title",'kaya_forms')?>  </label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('forgot_password_form_title') ?>" value="<?php echo esc_attr($instance['forgot_password_form_title']) ?>" name="<?php echo $this->get_field_name('forgot_password_form_title') ?>" />
		</p>
		<?php
	}
}
function kaya_forgot_password_widgets() {
	register_widget( 'Kaya_User_Forgotpassword_Widget' );
}
add_action( 'widgets_init', 'kaya_forgot_password_widgets' );
?>