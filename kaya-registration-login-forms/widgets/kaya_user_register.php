<?php
class Kaya_User_Registration_Widget extends WP_Widget{
	public function __construct(){
	    parent::__construct('kaya-user-register',
	    __('Kaya - Registration Form','kaya_forms'),
	    array('description' => __('Use this widget to create user registration form', 'kaya_forms'))
	    );
	    add_filter('send_password_change_email', '__return_false');
	}
	function user_registration_form(){
		global $login_page, $success, $err, $kaya_settings, $success, $current_user;
		$user_profile = get_page_by_path( 'user-profile' );
		$logout_redirect_page = $kaya_settings['logout_redirect_page'] ? $kaya_settings['logout_redirect_page'] : '';
		$user_reg_redirect_page = $kaya_settings['user_reg_redirect_page'] ? $kaya_settings['user_reg_redirect_page'] : ( !empty($user_profile->ID) ? $user_profile->ID : '' );
	    $email = esc_sql(trim($_POST['email']));
	    $username = esc_sql(trim($_POST['username']));
	    $password = esc_sql(trim($_POST['password']));
	    $cpassword = esc_sql(trim($_POST['cpassword']));
	    //$password = wp_generate_password(12, false);
	    if( $email == "" || $username == "") {
	        $err = __('Please don\'t leave the required fields.', 'kaya_forms');
	    }else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	        $err = !empty($kaya_settings['email_valid_error_msg']) ? $kaya_settings['email_valid_error_msg'] : __('Invalid email address.', 'kaya_forms');
	    }else if(email_exists($email) ) {
	        $err = !empty($kaya_settings['email_exist_error_msg']) ? $kaya_settings['email_exist_error_msg'] : __('Email already exist.', 'kaya_forms');
	    }elseif($password != $cpassword){
	    	 $err = __('Passwords Don\'t Match', 'kaya_forms');
	    }else {
			add_role( $_POST['user_role_other'], $_POST['user_role_other'], array( 'read' => true, 'edit_posts' => true, 'edit_talents' => true,'upload_files' => true,'edit_project' => true,'create_projects' => true,'read_project' => true ) );
			//$_POST['user_role_name']
			if($_POST['user_role_name'] == 'other_role'){
				if(!empty($_POST['user_role_other'])){
				$user_id = wp_insert_user( array ('user_pass' => apply_filters('pre_user_user_pass', $password), 'user_login' => apply_filters('pre_user_user_login', $username), 'user_email' => apply_filters('pre_user_user_email', $email), 'role' => $_POST['user_role_other']) );
				}else{
					echo '<p class="kaya-error-message" style="">Please enter other role name</p>';
					return false;
				}
			 // $user_id = wp_insert_user( array ('user_pass' => apply_filters('pre_user_user_pass', $password), 'user_login' => apply_filters('pre_user_user_login', $username), 'user_email' => apply_filters('pre_user_user_email', $email), 'role' => $_POST['user_role_other']) );
			}else{
				$user_id = wp_insert_user( array ('user_pass' => apply_filters('pre_user_user_pass', $password), 'user_login' => apply_filters('pre_user_user_login', $username), 'user_email' => apply_filters('pre_user_user_email', $email), 'role' => $_POST['user_role_name']) );
			}
	        if( is_wp_error($user_id) ) {
	            $err = !empty($kaya_settings['reg_error_msg']) ? $kaya_settings['reg_error_msg'] : __('Registration Fail', 'kaya_forms');
	        } else {
				$user_role = array();
				if($_POST['user_role_name'] == 'other_role'){
					$user_role = $_POST['user_role_other'];
				}else{
					$user_role = $_POST['user_role_name'];
				}
				$my_post = array(
					  'post_title'    		 => $username,
					  'post_content' 		 => '',
					  'post_status'          => 'publish',				  
					  'post_type'            => 'talent'
					 );
					// Insert the post into the database
				$post_ins_id = wp_insert_post( $my_post );
				update_user_meta( $user_id, 'talent_post_id', $post_ins_id );
				wp_set_object_terms( $post_ins_id, $user_role, 'talent_category');
	            do_action('user_register', $user_id);
	            // Auto Login
	            $user_data = array();
				$user_data['user_login'] = $username;
				$user_data['user_password'] = $password;
				$user_data['remember'] = true;
				$user_verify = wp_signon( $user_data, true );
				if ( is_wp_error($user_verify) ) {
					$error = $user_verify->get_error_message();
					exit();
				} else {
					wp_set_current_user( $user_verify->ID, $username );
					wp_set_auth_cookie( $user_verify->ID, true, false );
					do_action( 'wp_login', $username );								
					wp_redirect(get_the_permalink($user_reg_redirect_page).'?registred=true');  
					exit(); 
				}
				// End
	            $admin_email = get_option('admin_email');
				$confirmation_subject = !empty($kaya_settings['reg_email_subject_msg']) ? $kaya_settings['reg_email_subject_msg'] : __('Thankyou for Registration', 'kaya_forms');
				$headers = "MIME-Version: 1.0\r\n";
				$headers .= "From:".$admin_email."\r\n";
				$headers .= "Reply-To: ".$admin_email."" . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
				$confirmation_message = '<html><body>';
				$confirmation_message .= __('Hi ','kaya_forms').',<br /></br />'.(!empty($kaya_settings['reg_email_confirmation_msg'] ) ? $kaya_settings['reg_email_confirmation_msg'] : __('Thank you for registering with ', 'kaya_forms'). get_bloginfo('name')).'<br /><br />'
				.'<strong>'.__('Username: ','kaya_forms').'</strong> '.trim($username)
				.'<br /><strong>Password:</strong> '.trim($password)
				.'<br /><br />';
				$confirmation_message .=  '</body></html>';// Sending email
				mail($email, $confirmation_subject, $confirmation_message, $headers);
				$success = !empty($kaya_settings['reg_success_msg']) ? $kaya_settings['reg_success_msg'] : __('Registration successful and auto generated password has been sent to your email proceed to login.', 'kaya_forms');
	    	} 
	    }		
	}	
	public function widget($args, $instance){		
		echo $args['before_widget'];
		?>
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
		<?php
		global $login_page, $success, $err, $kaya_settings;
		$instance = wp_parse_args( $instance, array(
			'user_role_name' => 'user',
			'registration_as_a_text' => __('Registration as a', 'kaya_forms'),
			'registration_form_title' => __('Registration', 'kaya_forms'),
			'user_reg_note' => '',
		));
		global $success, $err, $current_user, $kaya_settings;
		$logout_button_text = !empty( $kaya_settings['logout_button_text'] ) ? trim($kaya_settings['logout_button_text']) :  __( 'Logout', 'kaya_forms' );
		$hi_button_text = !empty( $kaya_settings['hi_button_text'] ) ? trim($kaya_settings['hi_button_text']) :  __( 'Hi', 'kaya_forms' );
		$current_user_data = get_userdata($current_user->ID);
		if(is_user_logged_in()){
			echo '<p class="kta-notice kta-center">'.$hi_button_text.' '.$current_user_data->user_login.' <a href="'.wp_logout_url( get_the_permalink() ).'">'.$logout_button_text .'</a></p>';
			return;
		}

		echo '<div class="kaya-user-form kaya-user-registration-login-form">';	
			$cpatcha_site_key = !empty( $kaya_settings['cpatcha_site_key']) ? $kaya_settings['cpatcha_site_key'] : '';		
				if(isset($_POST['user_register_data']) && $_POST['user_register_data'] == 'register' ) { 

					if( !empty($cpatcha_site_key) ){
						if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){ // Recaptcha
							$secret = $kaya_settings['cpatcha_secret_key'] ? $kaya_settings['cpatcha_secret_key'] : '';
				       		//get verify response data
				        	$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
				        	$responseData = json_decode($verifyResponse);
	        				if($responseData->success){
	        					$this->user_registration_form();			
					       	} // Recaptcha Success

					    }else{
					       	 $err = !empty($kaya_settings['cpatcha_error_message']) ? $kaya_settings['cpatcha_error_message'] : __('Plese Select captcha', 'kaya_forms');
					    } // End recaptcha
					}else{
						$this->user_registration_form();
					}

			    } 
     
			     ?>
				
			    <form method="post" action="#" class="kaya-form">
			    	<div class="kaya-panel">
			    		<h4><?php echo (!empty($instance['registration_form_title'] ) ? $instance['registration_form_title'] : __('Registration','kaya_forms')); ?></h4>
			    		     <div id="message">
								<?php 
								if(! empty($err) ) :
									echo '<p class="error" style="">'.$err.'</p>';
								endif;
								if(! empty($success) ) :
									echo '<p class="success" style="">'.$success.'</p>';
								endif;
								?>
							</div> 
							<?php  if(!empty($instance['user_reg_note'])){
								echo '<p>'.$instance['user_reg_note'].'</p>';	
							}  ?>
			    			<?php
			    			global $wp_roles;
							if ( !isset( $wp_roles ) )
							$wp_roles = new WP_Roles();
							$user_roles = rsort($wp_roles->role_names);
							$role_array = $instance['user_role_name'];
							if( !empty($role_array) && ( is_array($role_array) ) ){
								$role_array = array_combine($role_array, $role_array);
							}else{
								$role_array = '';
							}

							
							$roles = $wp_roles->get_names();
							foreach ( $wp_roles->roles as $role => $name ) {
								$user_roles_val[$role] = $name['name'];
							}
							if( !empty($role_array) ){
								$user_roles_data = array_intersect_key($user_roles_val, $role_array);
								unset($user_roles_data["user"]); 
								if( !empty($user_roles_data) && ( count($user_roles_data) > 1 ) ){ ?>
									<div class="row">
										<div class="col-sm-6">
										<label><?php _e('User Role', 'kaya_forms'); ?></label>
											<select id="user_role_name" name="user_role_name">
												<option value=""><?php echo !empty($instance['registration_as_a_text']) ? $instance['registration_as_a_text'] :  __('Registration as a', 'kaya_forms'); ?></option>
												<?php foreach ( $user_roles_data as $role => $name ) { 
													if( esc_attr( $role ) !== 'administrator' || esc_attr( $role ) !== 'user'){ ?>
														<option value="<?php echo $role; ?>" ><?php echo $name; ?></option>
													<?php  }
												}  ?>
												<option value="other_role">Other</option>
											</select>
										</div>
										
										<div id="user_role_other" class="col-sm-6" style="display:none;" >
										<label><?php _e('Other Role', 'kaya_forms'); ?></label>
										<input type="text" name="user_role_other" placeholder="Role Name"/>
										</div>
									</div>
								<?php
								}
								else{
									if( !empty($user_roles_data) ){
										echo '<input type="hidden" value="'.key($user_roles_data).'" name="user_role_name" id="user_role_name" placeholder=""  />';
									}else{
										echo '<input type="hidden" value="'.key($user_roles_data).'" name="user_role_name" id="user_role_name" placeholder=""  />';
									}
								}
							}else{
								echo '<input type="hidden" value="user" name="user_role_name" id="user_role_name" placeholder=""  />';
							}
						
						
			    	?>
			        <div class="row">
						<div class="col-sm-6">
						<label><?php _e('Email', 'kaya_forms'); ?></label>
						<input type="text" value="" name="email" id="email" pattern="[^ @]*@[^ @]*"  placeholder="<?php _e('Email ( abc@gmail.com )', 'kaya_forms'); ?>" required />
						</div>
						<div class="col-sm-6">
						 <p><label><?php _e('User Name','kaya_forms'); ?></label><input type="text" value="" name="username" id="username" placeholder="<?php _e('User Name', 'kaya_forms'); ?>" required /></p>
						</div>
					</div>
			        <div class="row">
						<div class="col-sm-6">
						<p><label><?php _e('Password','kaya_forms'); ?></label><input type="password" value="" onchange="form.cpassword.pattern = RegExp.escape(this.value);" name="password" id="password" placeholder="<?php _e('Password', 'kaya_forms'); ?>" required /></p>
						</div>
						<div class="col-sm-6">
						 <p><label><?php _e('Confirm Password','kaya_forms'); ?></label><input type="password" value="" name="cpassword" id="cpassword" placeholder="<?php _e('Confirm Password', 'kaya_forms'); ?>" required /></p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 submit_form">
						<?php if( !empty($cpatcha_site_key) ){ ?>
							<div class="g-recaptcha" data-sitekey="<?php echo $cpatcha_site_key; ?>"></div><br />
						<?php } ?>
						<button type="submit" name="user_register" class="button readmore_button form_button" ><?php echo !empty($instance['register_form_button_text']) ? $instance['register_form_button_text'] : __( 'REGISTER','kaya_forms' ); ?></button>
						<input type="hidden" name="user_register_data" value="register" />
						</div>
					</div>
			    </div>
			    </form>
				
				<?php
			
	echo '</div>';
	echo $args['after_widget'];
}
public function form($instance){
		$instance = wp_parse_args($instance,array(
			'user_role_name' => 'user',
			'registration_as_a_text' => __('Registration as a', 'kaya_forms'), 
			'registration_form_title' =>  __('Registration', 'kaya_forms'),
			'user_reg_note' => '',
		));
		?>
		<p>
			<label for="<?php echo $this->get_field_id('registration_form_title') ?>">  <?php _e("Registration Form Title",'kaya_forms')?>  </label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('registration_form_title') ?>" value="<?php echo esc_attr($instance['registration_form_title']) ?>" name="<?php echo $this->get_field_name('registration_form_title') ?>" />
		</p>
		<p>
			<label for="role"><?php _e('Registration Form User Roles', 'kaya_forms'); ?></label>
		        <?php  global $wp_roles;
		        if ( !isset( $wp_roles ) )
		        $wp_roles = new WP_Roles();
		        $user_roles = rsort($wp_roles->role_names);
		        foreach ( $wp_roles->roles as $role => $name ) { 
		        	$checked = in_array($role, (array) $instance['user_role_name']) ? 'checked' : '';
		        	if( esc_attr( $role ) !== 'administrator' ){ ?>
		            <input type="checkbox" class="" id="<?php echo $this->get_field_id("user_role_name"); ?>"  value="<?php echo trim($role) ?>" name="<?php echo $this->get_field_name("user_role_name"); ?>[]" <?php echo $checked; ?> /><?php echo $name['name']; ?>
		        <?php  }
		    	}  ?>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('registration_as_a_text') ?>">  <?php _e("'Registration as a' Text Change",'kaya_forms')?>  </label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('registration_as_a_text') ?>" value="<?php echo esc_attr($instance['registration_as_a_text']) ?>" name="<?php echo $this->get_field_name('registration_as_a_text') ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('user_reg_note') ?>">  <?php _e("user Note",'kaya_forms')?>  </label>
			<textarea  class="widefat" id="<?php echo $this->get_field_id('user_reg_note') ?>" name="<?php echo $this->get_field_name('user_reg_note') ?>" > <?php echo esc_attr($instance['user_reg_note']) ?>  </textarea>
		</p>
		<?php
	}
}
function kaya_user_register_widgets() {
	register_widget( 'Kaya_User_Registration_Widget' );
}
add_action( 'widgets_init', 'kaya_user_register_widgets' );
?>