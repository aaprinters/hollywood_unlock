<?php
/**
* Profile Widget
*/
class Kaya_User_Profile_Widget extends WP_Widget
{	
	var $plugin_name;
	function __construct()
	{
		$this->plugin_name = 'kaya_forms';
		parent::__construct(
			'kaya-user-profile',
			__('Kaya - User Profile',$this->plugin_name),
			array( 'description' => __('Displays the user profile form','kaya_forms'),'class' => '')
		);
	}
	function widget($arg,$instance){
		global $notice_array, $success_array;
		$instance = wp_parse_args($instance, array(
		));
		echo $arg['before_widget']; 
			global $kaya_settings, $success_array;
			$error = '';
		$logout_button_text = !empty( $kaya_settings['logout_button_text'] ) ? trim($kaya_settings['logout_button_text']) :  __( 'Logout', 'kaya_forms' );
		if ( !is_user_logged_in() ) :
		return '<p class="kaya-error kaya-center">'.(!empty($kaya_settings['non_logged_users_msg']) ? stripslashes($kaya_settings['non_logged_users_msg']) : __('Please Login', 'kaya_forms')).'</p>';
		else : 
			global $current_user,$ktafd_options, $wp_roles, $kta_options, $success_array;
			$success_array = array();
			wp_get_current_user();
			/* Load the registration file. */
			//$error = array();   
			/* If profile was saved, update profile. */
			if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'update-user' ) {
			/* Update user password. */
			if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {
				if ( $_POST['pass1'] == $_POST['pass2'] )
					wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST['pass1'] ) ) );
				else
					$error = !empty($kaya_settings['update_error_pwd_match_msg']) ? stripslashes($kaya_settings['update_error_pwd_match_msg']) : __('The passwords you entered do not match.  Your password was not updated.', 'kaya_forms');
			}
			/* Update user information. */
			if ( !empty( $_POST['url'] ) )
				wp_update_user( array ('ID' => $current_user->ID, 'user_url' => esc_attr( $_POST['url'] )));
			if ( !empty( $_POST['email'] ) ){
			if (!is_email(esc_attr( $_POST['email'] )))
					$error = !empty($kaya_settings['email_valid_error_msg']) ? stripslashes($kaya_settings['email_valid_error_msg']) : __('The Email you entered is not valid.  please try again.', 'kaya_forms');
				elseif(email_exists(esc_attr( $_POST['email'] )) != $current_user->ID  )
					$error = !empty($kaya_settings['email_exist_error_msg']) ? stripslashes($kaya_settings['email_exist_error_msg']) :__('This email is already used by another user.  try a different one.', 'kaya_forms');
				else{
					wp_update_user( array ('ID' => $current_user->ID, 'user_email' => esc_attr( $_POST['email'] )));
				}
			}
			//wp_update_user( array ('ID' => $current_user->ID, 'role' => esc_attr( $_POST['user_role'] )));
			// Profile Link	
			$talent_post_id = get_the_author_meta( 'talent_post_id', $current_user->ID );
			// Talent Image
			if ( !empty( $_POST['talent_image'] ) ){
			$talent_image = $_POST['talent_image'];
			$tel_talent_image_arr = explode(',',$talent_image);
			$talent_image = $tel_talent_image_arr[0];
			$image_url= $talent_image; // Define the image URL here
			$image_name       = 'telent.png';
			$upload_dir       = wp_upload_dir(); // Set upload folder
			$image_data       = file_get_contents($image_url); // Get image data
			$unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
			$filename         = basename( $unique_file_name ); // Create image file name

			// Check folder permission and define file location
			if( wp_mkdir_p( $upload_dir['path'] ) ) {
				$file = $upload_dir['path'] . '/' . $filename;
			} else {
				$file = $upload_dir['basedir'] . '/' . $filename;
			}

			// Create the image  file on the server
			file_put_contents( $file, $image_data );

			// Check image file type
			$wp_filetype = wp_check_filetype( $filename, null );

			// Set attachment data
			$attachment = array(
				'post_mime_type' => $wp_filetype['type'],
				'post_title'     => sanitize_file_name( $filename ),
				'post_content'   => '',
				'post_status'    => 'inherit'
			);

			// Create the attachment
			$attach_id = wp_insert_attachment( $attachment, $file, $talent_post_id );

			// Include image.php
			require_once(ABSPATH . 'wp-admin/includes/image.php');

			// Define attachment metadata
			$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

			// Assign metadata to attachment
			wp_update_attachment_metadata( $attach_id, $attach_data );

			// And finally assign featured image to post
			set_post_thumbnail( $talent_post_id, $attach_id );
			}else{
			$gender_check = 'male';
			if ($gender_check == 'male') {
				set_post_thumbnail( $talent_post_id, 4825 );
			}
			 else if ($gender_check == 'female') {
				set_post_thumbnail( $talent_post_id, 4826 );
			}	
			}
			
			

			// Update the post into the database
				
			if ( !empty( $_POST['first-name'] ) ){
				$my_post_tel = array(
				  'ID'           => $talent_post_id,
				  'post_title'   => $_POST['first-name'],
				);
				wp_update_post( $my_post_tel );
				update_user_meta( $current_user->ID, 'first_name', esc_attr( $_POST['first-name'] ) );
				update_post_meta( $talent_post_id, 'talent_name', esc_attr( $_POST['first-name'] ) );
			}
			if ( !empty( $_POST['description'] ) )
				update_user_meta( $current_user->ID, 'description', esc_attr( $_POST['description'] ) );
				update_post_meta( $talent_post_id, 'short_bio', esc_attr( $_POST['description'] ) );			
			if ( !empty( $_POST['user_location'] ) )
				update_user_meta( $current_user->ID, 'user_location', esc_attr( $_POST['user_location'] ) );
				update_post_meta( $talent_post_id, 'user_location', esc_attr( $_POST['user_location'] ) );

			if ( !empty( $_POST['preferred_genre'] ) )
				update_user_meta( $current_user->ID, 'preferred_genre', esc_attr( $_POST['preferred_genre'] ) );
				update_post_meta( $talent_post_id, 'preferred_genre', esc_attr( $_POST['preferred_genre'] ) );
			
			if ( !empty( $_POST['typecasting_notes'] ) )
 				update_user_meta( $current_user->ID, 'typecasting_notes', esc_attr( $_POST['typecasting_notes'] ) );
				update_post_meta( $talent_post_id, 'typecasting_notes', esc_attr( $_POST['typecasting_notes'] ) );
			$user_role = array();
			if($_POST['user_role_name'] == 'other_role'){
				$user_role = $_POST['user_role_other'];
			}else{
				$user_role = $_POST['user_role_name'];
			}
			wp_set_object_terms( $post_ins_id, $user_role, 'talent_category');
			if ( empty($error)) {
				//action hook for plugins and extra fields saving
				do_action('edit_user_profile_update', $current_user->ID);
				wp_redirect( get_permalink().'?updated=true' ); 
				exit;
			}       
		}
		$talent_post_id = get_the_author_meta( 'talent_post_id', $current_user->ID );
		$profile_link = get_permalink($talent_post_id);
		//print_r($error);

		 ?>

		<?php if (	 !empty($error)){ echo '<p class="kaya-error-message">' .$error . '</p>'; } ?>

		<form method="post" id="adduser" class="user_profile kaya-form" action="<?php the_permalink(); ?>">
			<div class="">
				<?php if(isset($_REQUEST['updated'])&&($_REQUEST['updated'])=='true'){
		        	$success = !empty($kaya_settings['update_profile_update_msg']) ? stripslashes($kaya_settings['update_profile_update_msg']) : __('Your Profile Has Been Updated Successfully', 'kaya_forms');  
						echo '<p class="kaya-success-message" style="">'.$success.'</p>';
		        	}
		       		do_action('form-message'); ?>
   					<div class="user_personal_information kaya-panel">
					<h4><?php _e('PERSONAL INFORMATION','kaya_forms'); ?></h4>
						<div class="row">
							<div class="col-sm-6">
								<label for="first-name"><?php _e('Full Name', 'kaya_forms'); ?></label>
								<input class="text-input" name="first-name" type="text" id="first-name" value="<?php the_author_meta( 'first_name', $current_user->ID ); ?>" />
							</div><!-- .form-username -->
							<!-- .form-location -->
							<div class="col-sm-6">
							<?php $user_location = get_the_author_meta( 'user_location', $current_user->ID ); ?>
								<label for="location"><?php _e('Location', 'kaya_forms'); ?></label>
								<select class="text-input" name="user_location" type="text" id="user_location" value="" />
								<?php if($user_location){
								echo "<option value =".$user_location." selected>".$user_location."</option>";
								}else{
									echo '<option value="">Select Location</option>';
								}
								?>
								 <option value="Alabama">Alabama</option> 
								 <option value="Alaska">Alaska</option> 
								 <option value="Arizona">Arizona</option> 
								 <option value="Arkansas">Arkansas</option> 
								 <option value="California">California</option> 
								 <option value="Colorado">Colorado</option> 
								 <option value="Connecticut">Connecticut</option> 
								 <option value="Delaware">Delaware</option> 
								 <option value="Florida">Florida</option> 
								 <option value="Georgia">Georgia</option> 
								 <option value="Hawaii">Hawaii</option> 
								 <option value="Idaho">Idaho</option> 
								 <option value="Illinois">Illinois</option> 
								 <option value="Indiana">Indiana</option> 
								 <option value="Iowa">Iowa</option> 
								 <option value="Kansas">Kansas</option> 
								 <option value="Kentucky">Kentucky</option> 
								 <option value="Louisiana">Louisiana</option> 
								 <option value="Maine">Maine</option> 
								 <option value="Maryland">Maryland</option> 
								 <option value="Massachusetts">Massachusetts</option> 
								 <option value="Michigan">Michigan</option> 
								 <option value="Minnesota">Minnesota</option> 
								 <option value="Mississippi">Mississippi</option> 
								 <option value="Missouri">Missouri</option> 
								 <option value="Montana">Montana</option> 
								 <option value="Nebraska">Nebraska</option> 
								 <option value="Nevada">Nevada</option> 
								 <option value="New Hampshire">New Hampshire</option> 
								 <option value="New Jersey">New Jersey</option> 
								 <option value="New Mexico">New Mexico</option> 
								 <option value="New York">New York</option> 
								 <option value="North Carolina">North Carolina</option> 
								 <option value="North Dakota">North Dakota</option> 
								 <option value="Ohio">Ohio</option> 
								 <option value="Oklahoma">Oklahoma</option> 
								 <option value="Oregon">Oregon</option> 
								 <option value="Pennsylvania">Pennsylvania</option> 
								 <option value="Rhode Island">Rhode Island</option> 
								 <option value="South Carolina">South Carolina</option> 
								 <option value="South Dakota">South Dakota</option> 
								 <option value="Tennessee">Tennessee</option> 
								 <option value="Texas">Texas</option> 
								 <option value="Utah">Utah</option> 
								 <option value="Vermont">Vermont</option> 
								 <option value="Virginia">Virginia</option> 
								 <option value="Washington">Washington</option> 
								 <option value="West Virginia">West Virginia</option> 
								 <option value="Wisconsin">Wisconsin</option> 
								 <option value="Wyoming">Wyoming</option> 	
								</select>
							</div><!-- .form-location -->		
						</div><!-- .form-location -->		
							<!-- .form-role -->
							<div class="row">
							<div class="col-sm-6">
							<?php
							global $wp_roles;
							if ( !isset( $wp_roles ) )
							$wp_roles = new WP_Roles();
							$user_roles = rsort($wp_roles->role_names);
							$role_array = $instance['user_role_name'];
							if( !empty($role_array) && ( is_array($role_array) ) ){
								$role_array = array_combine($role_array, $role_array);
							}else{
								$role_array = '';
							}
							$roles = $wp_roles->get_names();
						/* ?><pre><?php // print_r($roles); ?></pre><?php */
								?>
								<label for="location"><?php _e('User Role', 'kaya_forms'); ?></label>
								<select id="user_role_name" name="user_role_name" disabled>
								<?php 
								if( is_user_logged_in() ) {
									$user = wp_get_current_user();
									$c_roles = $user->roles;
									$current_role = $c_roles[0];
									?>
									<option value="<?php echo $current_role; ?>" selected><?php echo $current_role; ?></option>
						  <?php } ?>
											<?php foreach ( $roles as $role) { 
												if( esc_attr( $role ) !== 'administrator' ){ ?>
													<option value="<?php echo $role; ?>" ><?php echo $role; ?></option>
												<?php  }
											}  ?>
											<option value="other_role">Other</option>
								</select>
								<input type="text" name="user_role_other" id="user_role_other" placeholder="Role Name" style="display:none;" />
							</div><!-- .form-role -->
						<div class="col-sm-6">
						<?php $preferred_genre = get_the_author_meta( 'preferred_genre', $current_user->ID ); ?>
								<label for="location"><?php _e('Preferred Genre', 'kaya_forms'); ?></label>
								<select class="text-input" name="preferred_genre" type="text" id="preferred_genre" value="" />
								<?php if($preferred_genre){
									echo "<option value =".$preferred_genre." selected>".$preferred_genre."</option>";
								}else{
									echo '<option value="">Select Preferred Genre</option>';
								}
								?><option value="Comedy">Comedy</option> 
								 <option value="Romance">Romance</option> 
								 <option value="Horror">Horror</option> 
								 <option value="Drama">Drama</option> 
								 <option value="Western">Western</option> 
								 <option value="Arizona">Documentary</option> 
								 </select>
						</div><!-- .form-role -->	
						</div><!-- .form-role -->	
					<div class="row">
						<div class="col-md-12 telent_image">
							<label for="email">Profile Image</label>
							
							<?php echo do_shortcode('[ajax-file-upload unique_identifier="profile_image" set_image_source="img.profile-pic" disallow_remove_button="1" upload_button_value="Click here to upload" on_success_set_input_value="#talent_image" allowed_extensions="jpg,png,jpeg" on_success_alert="Your images has been successfully uploaded !"]'); ?>
							<div class="click_here">click here to upload !</div>
							<div class="show_upload_img">
								<img src="" class="profile-pic"/>
							</div>
							<textarea class="form-control hide" name="talent_image" value="" id="talent_image"><?php echo $featured_img_url; ?></textarea>
							
						</div>
					</div>							 
					<div class="user_contact_information kaya-panel">
						<h4><?php _e('CONTACT INFORMATION','kaya_forms'); ?></h4>
						<div class="row">
							<div class="col-sm-6 form-email">
								<label for="email"><?php _e('E-mail *', 'kaya_forms'); ?></label>
								<input class="text-input" name="email" type="text" id="email" value="<?php the_author_meta( 'user_email', $current_user->ID ); ?>" />
							</div><!-- .form-email -->
						</div>
					</div>
					<label><input type="checkbox" id="update_password" name="update_password">Change or reset your password ?</label>
					<div id="password_feilds" class="user_contact_information kaya-panel" style="display:none;">
					<h4><?php _e('ACCOUNT INFORMATION','kaya_forms'); ?></h4>
						<div class="row">
							<div class="col-sm-6 form-password">
								<label for="pass1"><?php _e('Password *', 'kaya_forms'); ?> </label>
								<input class="text-input" name="pass1" type="password" id="pass1" />
							</div><!-- .form-password -->
							<div class="col-sm-6 form-password">
								<label for="pass2"><?php _e('Repeat Password *', 'kaya_forms'); ?></label>
								<input class="text-input" name="pass2" type="password" id="pass2" />
							</div><!-- .form-password -->
						</div><!-- .form-password -->
					</div>
					<div class="user_information kaya-panel">
					<h4><?php _e('ABOUT YOURSELF','kaya_forms'); ?></h4>
						<p class="form-textarea">
							<label for="typecasting_notes" style="width: 100%;display: block;margin-bottom: 10px;"><?php _e('Typecasting Notes', 'kaya_forms') ?></label>
							<textarea name="typecasting_notes" id="typecasting_notes" rows="3" cols="50"><?php the_author_meta( 'typecasting_notes', $current_user->ID ); ?></textarea>
						</p><!-- .form-textarea -->
						<p class="form-textarea">
							<label for="description" style="width: 100%;display: block;margin-bottom: 10px;"><?php _e('Short Bio', 'kaya_forms') ?></label>
							<textarea name="description" id="description" rows="3" cols="50"><?php the_author_meta( 'description', $current_user->ID ); ?></textarea>
						</p><!-- .form-textarea -->
					</div>
					
				<?php 
							//action hook for plugin and extra fields
							do_action('edit_user_profile',$current_user);  
							?>
							<p class="form-submit">
							<div class="row">
								<div class="col-sm-4">
								</div>
								<div class="col-sm-4">
									<input name="updateuser" type="submit" id="ktafd_button" class="submit button kaya-button update_profile" value="<?php echo $kaya_settings['update_button_text'] ? stripslashes($kaya_settings['update_button_text']) : __('Update Profile', 'kaya_forms'); ?>" />
									 <a href="<?php echo $profile_link; ?>"><input  type="button" tabindex="4" class="view_profile view_profile submit button kaya-button" value="View Profile"></a>
								</div>
								<?php wp_nonce_field( 'update-user_'. $current_user->ID ) ?>
								<input name="action" type="hidden" id="action" value="update-user" />
							</div>
							</p><!-- .form-submit -->		
			</form><!-- #adduser -->
			<?php endif;	
		echo $arg['after_widget'];
	}
	function form($instance){ 
		$instance = wp_parse_args($instance, array(

		));		
    }
}
function kaya_user_profile_widgets() {
	register_widget( 'Kaya_User_Profile_Widget' );
}
add_action( 'widgets_init', 'kaya_user_profile_widgets' );
?>