<?php
class Kaya_User_Login_Widget extends WP_Widget{
	public function __construct(){
		global $login_page;
	    parent::__construct('kaya-user-register-login',
	    __('Kaya - Login Form','kaya_forms'),
	    array('description' => __('Use this widget to create user login form', 'kaya_forms'))
	    );
	    add_filter('send_password_change_email', '__return_false');
	}
	public function widget($args, $instance){		
		echo $args['before_widget'];
		global $login_page, $success, $err, $kaya_settings;
		$instance = wp_parse_args( $instance, array(
			'login_form_title' => __('Login Form', 'kaya_forms'),
		));
		global $success, $err, $current_user;
		$current_user_data = get_userdata($current_user->ID);
		$logout_redirect_page = $kaya_settings['logout_redirect_page'] ? $kaya_settings['logout_redirect_page'] : '';
		$logout_button_text = !empty( $kaya_settings['logout_button_text'] ) ? trim($kaya_settings['logout_button_text']) :  __( 'Logout', 'kaya_forms' );
		$hi_button_text = !empty( $kaya_settings['hi_button_text'] ) ? trim($kaya_settings['hi_button_text']) :  __( 'Hi', 'kaya_forms' );
		if(is_user_logged_in()){
			echo '<p class="kta-notice kta-center">'.$hi_button_text.' '.$current_user_data->user_login.' <a href="'.wp_logout_url( get_the_permalink($logout_redirect_page) ).'">'.$logout_button_text.'</a></p>';
			return;
		}
		
		$form_type = 'forgot_password';
		echo '<div class="kaya-user-form kaya-form kaya-user-registration-login-form">';
				echo '<div class="kaya-panel">';
				echo '<h4>'.( !empty($instance['login_form_title']) ? $instance['login_form_title'] : __('Login Form','kaya_forms') ).'</h4>';
		  	 	// User Login Form
		  	 	$user_reg_page_link = !empty( $kaya_settings['user_reg_page_link'] ) ? trim($kaya_settings['user_reg_page_link']) : 'select-page';
				$forgot_pwd_page_link = !empty( $kaya_settings['forgot_pwd_page_link'] ) ? trim($kaya_settings['forgot_pwd_page_link']) : 'select-page';
		  	 	$login_redirect = $kaya_settings['login_redirect_page'] ? $kaya_settings['login_redirect_page'] : 'select-page ';
				$login_redirect_page = ( $login_redirect !='select-page' ) ? get_permalink($login_redirect) : admin_url('index.php');
		  	 	
		       	if(isset($_GET['login']) && $_GET['login'] == 'failed')
		        { ?>
		            <p class=""><?php echo !empty( $kaya_settings['login_error_msg'] ) ? $kaya_settings['login_error_msg'] : __('Error username / password', 'kaya_forms') ?></span>
		        <?php  }
		         	if (is_user_logged_in()) {
		            } else {   
		            	$args = array(
		                    'echo'           => true,
		                    'form_id'        => 'loginform',
		                    'label_username' => __('Username', 'kaya_forms'),
		                    'redirect'		 => $login_redirect_page,
		                    'label_password' =>__('Password', 'kaya_forms'),
		                    'label_remember' => __( 'Remember Me', 'kaya_forms' ),
		                    'label_log_in'   => !empty($instance['login_form_button_text']) ? $instance['login_form_button_text'] : __( 'Log In', 'kaya_forms' ),
		                    'id_username'    => 'user_login',
		                    'id_password'    => 'user_pass',
		                    'id_remember'    => 'rememberme',
		                    'id_submit'      => 'wp-submit',
		                    'remember'       => true,
		                    'value_username' => NULL,
		                    'value_remember' => true
		                ); 
		                wp_login_form($args);
		                echo '<div class="user-reg-link">';
			        		if( $user_reg_page_link !='select-page' ){ 
				        		echo '<a href="'.get_the_permalink($user_reg_page_link).'">'.get_the_title($user_reg_page_link).'</a>';
				        	}
				        	if( ( $user_reg_page_link !='select-page' ) && ( $forgot_pwd_page_link !='select-page' ) ){
				        		echo ' / ';
				        	}
				        	if( $forgot_pwd_page_link !='select-page' ){ 
				        		//echo '<a href="'.get_the_permalink($forgot_pwd_page_link).'">'.get_the_title($forgot_pwd_page_link).'</a>';
				        	}
						echo '<a href="'.get_the_permalink($forgot_pwd_page_link).'">'.get_the_title($forgot_pwd_page_link).'</a>';

				    	echo '</div>';
		            }
		     ?>
		 </div>
	<?php
	echo '</div>';
	echo '</div>';
}
public function form($instance){
		$instance = wp_parse_args($instance,array(
			'login_form_title' => __('Login Form', 'kaya_forms'),
		)); ?>

		<p>
			<label for="<?php echo $this->get_field_id('login_form_title') ?>">  <?php _e("Login Form Title",'kaya_forms')?>  </label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('login_form_title') ?>" value="<?php echo esc_attr($instance['login_form_title']) ?>" name="<?php echo $this->get_field_name('login_form_title') ?>" />
		</p>

	<?php }
}
function kaya_login_widgets() {
	register_widget( 'Kaya_User_Login_Widget' );
}
add_action( 'widgets_init', 'kaya_login_widgets' );
?>