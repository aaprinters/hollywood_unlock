=== User Registration & Login form.  ===
Author: kayapati
Tags: registration, login, forgot password, profile.
Requires at least: 3.5
Tested up to: 4.8.1
Stable tag: 1.0.1


== Description ==

 Simple plugin to display the Registration Form, Login Form, Forgot Password Form and User Profile page using widgets.


== Installation ==

1. Upload and install User Registration & Login form’ in the same way you'd install any other plugin, make sure that you installed and activated “Pods - Custom Content Types and Fields” https://wordpress.org/plugins/pods/


== Widgets Included ==

1. Kaya - Registration Form : Using this widget you can create Registration form either on the widget area or page.
2. Kaya - Login Form : Using this widget you can create login form either on the widget area or page.
3. Kaya - Forgot Password Form : Using this widget you can create Forgot Password either on the widget area or page.
4. Kaya - User Profile : Using this widget you can create user profile either on the widget area or page.



== Change-log == 


= 1.0.2 =
* Issue fixed, "Kaya - User Profile" widget, profile form update display errors fixed.


= 1.0.1 =
* Added new feature, password fields added in "Kaya - Registration Form" widget.
* Added new feature, after completion of the registration process user can automatically logged in.
* Re-design all the widgets titles & description.
* Updated language files.

= 1.0 =
* First version.