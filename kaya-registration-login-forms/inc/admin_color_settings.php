<?php
function Kaya_color_settings(){
	global $kaya_settings;
	$settings = get_option( "kaya_settings" );
	$css ='';
	$form_bg_color = $kaya_settings['form_bg_color'] ? $kaya_settings['form_bg_color'] : '#ffffff';
	$form_title_background_color = $kaya_settings['form_title_background_color'] ? $kaya_settings['form_title_background_color'] : '#e5e5e5';
	$kta_form_title_color = !empty($kaya_settings['kta_form_title_color']) ? $kaya_settings['kta_form_title_color'] : '#333333';
	$form_input_field_border_color = !empty($kaya_settings['form_input_field_border_color']) ? $kaya_settings['form_input_field_border_color'] : '#e5e5e5';
	$form_input_field_text_color = !empty($kaya_settings['form_input_field_text_color']) ? $kaya_settings['form_input_field_text_color'] : '#757575';
	$button_bg_color = !empty($kaya_settings['button_bg_color']) ? $kaya_settings['button_bg_color'] : '#333333';
	$button_text_color = !empty($kaya_settings['button_text_color']) ? $kaya_settings['button_text_color'] : '#ffffff';
	$form_input_field_bg_color = !empty($kaya_settings['form_input_field_bg_color']) ? $kaya_settings['form_input_field_bg_color'] : '#ffffff';
	$form_border_color = !empty($kaya_settings['form_border_color']) ? $kaya_settings['form_border_color'] : '#ebebeb';
	$button_bg_hover_color = !empty($kaya_settings['button_bg_hover_color']) ? $kaya_settings['button_bg_hover_color'] : '#ffffff';
	$button_text_hover_color = !empty($kaya_settings['button_text_hover_color']) ? $kaya_settings['button_text_hover_color'] : '#000000';
	// Table 
	$tabel_th_bg_colors = !empty($kaya_settings['tabel_th_bg_colors']) ? $kaya_settings['tabel_th_bg_colors'] : '#ebebeb';
	$tabel_th_text_colors = !empty($kaya_settings['tabel_th_text_colors']) ? $kaya_settings['tabel_th_text_colors'] : '#333';
	$tabel_tr_even_bg_colors = !empty($kaya_settings['tabel_tr_even_bg_colors']) ? $kaya_settings['tabel_tr_even_bg_colors'] : '#fff';
	$tabel_tr_odd_bg_colors = !empty($kaya_settings['tabel_tr_odd_bg_colors']) ? $kaya_settings['tabel_tr_odd_bg_colors'] : '#f8f8f8';
	$tabel_tr_even_colors = !empty($kaya_settings['tabel_tr_even_colors']) ? $kaya_settings['tabel_tr_even_colors'] : '#787878';
	$tabel_tr_odd_colors = !empty($kaya_settings['tabel_tr_odd_colors']) ? $kaya_settings['tabel_tr_odd_colors'] : '#787878';

	// Compcard Color Settings
	$talent_details_bg_color = !empty($kaya_settings['talent_details_bg_color']) ? $kaya_settings['talent_details_bg_color'] : '#151515';
	$talent_details_color_color = !empty($kaya_settings['talent_details_color_color']) ? $kaya_settings['talent_details_color_color'] : '#ffffff';

	$css .=
	'.kaya-panel{
		background:'.$form_bg_color.';
	}
	.kaya-form h4, .panel_title_desc_wrapper{
		background:'.$form_title_background_color.';
		color:'.$kta_form_title_color.'!important;
	}
	.panel_title_desc_wrapper p, .panel_title_desc_wrapper h4{
		color:'.$kta_form_title_color.'!important;	
	}
	.kaya-form input:not(.kaya-button):not(.ed_button), .kaya-form select, .kaya-form textarea{
		border: 1px solid '.$form_input_field_border_color.'!important;
		color:'.$form_input_field_text_color.';
		background-color:'.$form_input_field_bg_color.';
	}
	.kaya-form .kaya-button, .form_button, #wp-submit{
		background:'.$button_bg_color.'!important;
		color:'.$button_text_color.'!important;
	}
	.kaya-form .kaya-button:hover, .form_button:hover, #wp-submit:hover{
		background:'.$button_bg_hover_color.'!important;
		color:'.$button_text_hover_color.'!important;
	}
	.kaya-panel {
    border-color:'.$form_border_color.';
	}
	.kaya-panel ::-webkit-input-placeholder { /* WebKit, Blink, Edge */
    color:'.$form_input_field_text_color.'!important;
	}
	.kaya-panel :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
	   color:'.$form_input_field_text_color.'!important;
	}
	.kaya-panel ::-moz-placeholder { /* Mozilla Firefox 19+ */
	   color:'.$form_input_field_text_color.'!important;
	}
	.kaya-panel :-ms-input-placeholder { /* Internet Explorer 10-11 */
	   color:'.$form_input_field_text_color.'!important;
	}
	.kaya-table th {
	    background:'.$tabel_th_bg_colors.';
	    color: '.$tabel_th_text_colors.';
	}
	.kaya-table tr:nth-child(2n) {
    	background:'.$tabel_tr_even_bg_colors.';
	    color: '.$tabel_tr_even_colors.';
	}
	.kaya-table tr {
    	background:'.$tabel_tr_odd_bg_colors.';
	    color: '.$tabel_tr_odd_colors.';
	}
	.kaya-table tr:nth-child(2n) td a{
		color: '.$tabel_tr_even_colors.'!important;
	}
	.kaya-table tr td a{
		color: '.$tabel_tr_odd_colors.'!important;
	}';

	$css = preg_replace( '/\s+/', ' ', $css ); 
	echo "<style>\n" .trim( $css ). "\n</style>";
}
add_action('wp_head','Kaya_color_settings');
?>