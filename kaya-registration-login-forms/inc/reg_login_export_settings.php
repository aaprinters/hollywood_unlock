<?php
add_action( 'admin_menu', 'kaya_reg_login_export_option_settings' );
/**
 * Import and export settings
 */
function kaya_reg_login_export_option_settings() 
{
    add_submenu_page('kaya_settings', __('Export','kaya_forms'), __('Export','kaya_forms'), 'edit_theme_options', 'reg-login-options-export', 'kaya_reg_login_export_option_page');
    add_submenu_page('kaya_settings', __('Import','kaya_forms'), __('Import','kaya_forms'), 'edit_theme_options', 'reg-login-options-import', 'kaya_reg_login_import_option_page');
}

function kaya_reg_login_export_option_page() {
    if (!isset($_POST['reg-login-options-export'])) { ?>
        <div class="wrap">
            <div id="icon-tools" class="icon32"><br /></div>
            <h2><?php esc_html_e('Export Registrer Login Page Form Settings','kaya_forms'); ?> </h2>
            <p><?php _e('When you click <tt>Dowload Pod CPT Views Settings</tt> button, system will generate a JSON file for you to save on your computer.','kaya_forms'); ?></p>
            <form method='post'>
                <p class="submit">
                    <?php wp_nonce_field('reg-login-options-export'); ?>
                    <input type='submit' name='reg-login-options-export' value='<?php esc_html_e('Dowload Settings','kaya_forms'); ?>' class="button"/>
                </p>
            </form>
        </div>
        <?php
    }
    elseif (check_admin_referer('reg-login-options-export')) {
        $blogname = str_replace(" ", "", get_option('blogname'));
        $date = date("m-d-Y");
        $json_name = $blogname."-".$date; // Namming the filename will be generated.
        $options = get_option('kaya_settings'); // Get all options data, return array        
            foreach ($options as $key => $value) {
            $value = maybe_unserialize($value);
            $need_options[$key] = $value;
        }
        $json_file = json_encode($need_options); // Encode data into json data
        ob_clean();
        echo $json_file;
        header("Content-Type: text/json; charset=" . get_option( 'blog_charset'));
        header("Content-Disposition: attachment; filename=$json_name.json");
        exit();
    }
}
 function kaya_reg_login_import_option_page() {
        WP_Filesystem();
        global $wp_filesystem;
    ?>
    <div class="wrap">
        <div id="icon-tools" class="icon32"><br /></div>
        <h2><?php _e('Import Registration / Login  Options', 'pcv'); ?></h2>
        <?php
            if (isset($_FILES['reg-login-options-import']) && check_admin_referer('reg-login-options-import')) {
                if ($_FILES['reg-login-options-import']['error'] > 0) {
                    wp_die("Please Choose Upload json format file");
                }
                else {
                    $file_name = $_FILES['reg-login-options-import']['name']; // Get the name of file
                    $file_path = explode('.', $file_name);
                    $file_ext = end($file_path);
                    $file_size = $_FILES['reg-login-options-import']['size']; // Get size of file
                    /* Ensure uploaded file is JSON file type and the size not over 500000 bytes
                     * You can modify the size you want
                     */
                    if (($file_ext == "json") && ($file_size < 500000)) {
                        $encode_options = $wp_filesystem->get_contents($_FILES['reg-login-options-import']['tmp_name']);
                        $pod_data = json_decode($encode_options, true);
                        $kaya_settings = array();
                         foreach ($pod_data as $key => $opt_val) {
                            $kaya_settings[$key] = $opt_val;
                            update_option( 'kaya_settings', $kaya_settings );
                         }

                        echo "<div class='updated'><p>".__('All options are restored successfully','pcv')."</p></div>";
                    }
                    else {
                        echo "<div class='error'><p>".__('Invalid file or file size too big.','pcv')."</p></div>";
                    }
                }
            }
        ?>
        <p><?php _e('Click Browse button and choose a json file that you backup before.','pcv'); ?> </p>
        <p><?php _e('Press Upload File and Import, WordPress do the rest for you.','pcv'); ?></p>
        <form method='post' enctype='multipart/form-data'>
            <p class="submit">
                <?php wp_nonce_field('reg-login-options-import'); ?>
                <input type='file' name='reg-login-options-import' class="primary-button"  />
                <input type='submit' name='submit' value='<?php _e('Upload File and Import', 'pcv') ?>' class="button"/>
            </p>
        </form>
    </div>
    <?php
}
?>
