<?php 
if( !function_exists('kaya_user_login_failed') ){
    add_action( 'wp_login_failed', 'kaya_user_login_failed' ); // hook failed login
    function kaya_user_login_failed( $user ) {
            // check what page the login attempt is coming from
        $referrer = $_SERVER['HTTP_REFERER'];
            // check that were not on the default login page
        if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') && $user!=null ) {
            // make sure we don’t already have a failed login attempt
        if ( !strstr($referrer, '?login=failed' )) {
            wp_redirect( $referrer . '?login=failed');
        }else {
            wp_redirect( $referrer );
        }
        exit;
        }
    }
}

/* Registration / Login Page link */
function kaya_user_reg_login_link() {
    global $kaya_settings, $current_user;
    $current_user_data = get_userdata($current_user->ID);
    $menu_lik = '';
    $user_reg_page_link = !empty( $kaya_settings['user_reg_page_link'] ) ? trim($kaya_settings['user_reg_page_link']) : 'select-page';
    $user_login_page_link = !empty( $kaya_settings['user_login_page_link'] ) ? trim($kaya_settings['user_login_page_link']) : 'select-page';
    $user_profile_page_link = !empty( $kaya_settings['user_profile_page_link'] ) ? trim($kaya_settings['user_profile_page_link']) : 'select-page';
    $logout_button_text = !empty( $kaya_settings['logout_button_text'] ) ? trim($kaya_settings['logout_button_text']) :  __( 'Logout', 'kaya_forms' );
     $dashboard_button_text = !empty( $kaya_settings['dashboard_button_text'] ) ? trim($kaya_settings['dashboard_button_text']) : '';
    $register_page = get_page_by_path( 'register' );
    $login_page = get_page_by_path( 'login' );

    $user_reg_page_link = !empty( $kaya_settings['user_reg_page_link'] ) ? trim($kaya_settings['user_reg_page_link']) : 'select-page';
    $user_login_page_link = !empty( $kaya_settings['user_login_page_link'] ) ? trim($kaya_settings['user_login_page_link']) : 'select-page';
    $registration_page = $user_reg_page_link ? $user_reg_page_link : $register_page->ID;
    $login_page = $user_login_page_link ? $user_login_page_link : $login_page->ID;

    $logout_redirect_page = (!empty( $kaya_settings['logout_redirect_page'] ) && ( $kaya_settings['logout_redirect_page'] != 'select-page' ) ) ? trim($kaya_settings['logout_redirect_page']) : '';
    $hi_button_text = !empty( $kaya_settings['hi_button_text'] ) ? trim($kaya_settings['hi_button_text']) :  __( 'Hi', 'kaya_forms' );

    if( is_user_logged_in() ){

    }else{
        if( !empty($registration_page) && ( $registration_page != 'select-page' )  ){
            $menu_lik .= '<li class="user_registration_page"><a href="'.get_the_permalink($registration_page).'">' . get_the_title($registration_page) . '</a></li>';
        }
        if( !empty($login_page) && ( $login_page != 'select-page' )  ){
            $menu_lik .= '<li class="user_registration_page"><a href="'.get_the_permalink($login_page).'">' .  get_the_title($login_page) . '</a></li>'; 
        }       
    }
    return $menu_lik;
}
/**
 * User Dashbaord Menu
 */
function kaya_user_dashboard_menu(){
     global $kaya_settings, $current_user;
    $current_user_data = get_userdata($current_user->ID);
    $user_reg_page_link = !empty( $kaya_settings['user_reg_page_link'] ) ? trim($kaya_settings['user_reg_page_link']) : 'select-page';
    $user_login_page_link = !empty( $kaya_settings['user_login_page_link'] ) ? trim($kaya_settings['user_login_page_link']) : 'select-page';
    $registration_page = $user_reg_page_link ? $user_reg_page_link : $register_page->ID;
    $login_page = $user_login_page_link ? $user_login_page_link : $login_page->ID;

    $hi_button_text = !empty( $kaya_settings['hi_button_text'] ) ? trim($kaya_settings['hi_button_text']) :  __( 'Hi', 'kaya_forms' );
    $logout_redirect_page = (!empty( $kaya_settings['logout_redirect_page'] ) && ( $kaya_settings['logout_redirect_page'] != 'select-page' ) ) ? trim($kaya_settings['logout_redirect_page']) : '';
    $dashboard_button_text = !empty( $kaya_settings['dashboard_button_text'] ) ? trim($kaya_settings['dashboard_button_text']) : '';
    $user_logout_text = !empty( $kaya_settings['logout_button_text'] ) ? trim($kaya_settings['logout_button_text']) :  __( 'Logout', 'kaya_forms' );
        if ( ( is_user_logged_in() ) ) {
            echo '<div id="nav-user-dashboard-menu" class="menu">';
               echo '<ul id="user-main-menu" class="top-nav">';
                echo '<li>';
                    echo '<a href="#">' .$hi_button_text. ' '. ucfirst($current_user_data->user_login) .'</a>';
                echo '</li>';
               echo '</ul>';
              if (has_nav_menu('user-dashboard')) {
                    wp_nav_menu(array('container_id' => 'main-nav','menu_id'=> 'user-dashboard-menu', 'container_class' => 'menu user-dashboard','theme_location' => 'user-dashboard', 'menu_class'=> 'top-nav'));
                  //  wp_nav_menu( array( 'menu' => 'user-dashboard', 'container_id' => 'user-dashboard-menu',  'container_class' => '', 'theme_location' => 'user-dashboard', 'menu_class' => 'user-dashboard', 'menu_id' => ''));
              }else{   
                    echo '<div class="menu user-dashboard" id="main-nav">';             
                  echo '<ul id="user-dashboard-menu" class="top-nav">';
                    if( !empty($dashboard_button_text) ){
                      echo '<li class=""><a href="'.admin_url('index.php').'">'.$dashboard_button_text.'</a></li>';
                    }
                    if( !empty($user_logout_text) ){
                      echo '<li class=""><a class="" href="'.wp_logout_url( get_the_permalink($logout_redirect_page) ).'">'.$user_logout_text.'</a></li>';
                    }
                  echo '</ul>';
                echo '</div>';
              }
              echo '</div>';
    }else{
         echo '<div id="nav-user-dashboard-menu" class="menu">';
               echo '<ul id="user-main-menu" class="top-nav">';
           if( !empty($registration_page) && ( $registration_page != 'select-page' )  ){
            echo '<li class="user_registration_page"><a href="'.get_the_permalink($registration_page).'">' . get_the_title($registration_page) . '</a></li>';
            }
            if( !empty($login_page) && ( $login_page != 'select-page' )  ){
                echo '<li class="user_registration_page"><a href="'.get_the_permalink($login_page).'">' .  get_the_title($login_page) . '</a></li>'; 
            }  
          echo '</ul>';
        echo '</div>';
    }
}

// User registration & Login Menu
function kaya_user_register_login_link($items, $args) {
    global $kaya_settings;
    $logout_redirect_page = (!empty( $kaya_settings['logout_redirect_page'] ) && ( $kaya_settings['logout_redirect_page'] != 'select-page' ) ) ? trim($kaya_settings['logout_redirect_page']) : '';
    $dashboard_button_text = isset( $kaya_settings['dashboard_button_text'] ) ? trim($kaya_settings['dashboard_button_text']) :  '';
    $user_logout_text = !empty( $kaya_settings['logout_button_text'] ) ? trim($kaya_settings['logout_button_text']) :  __( 'Logout', 'kaya_forms' );
    if($args->theme_location == 'user-dashboard') {
        if( !empty($dashboard_button_text) ){
          $items .=  '<li class=""><a href="'.admin_url('index.php').'">'.$dashboard_button_text.'</a></li>';
        }
        if( !empty($user_logout_text) ){
          $items .=  '<li class=""><a class="" href="'.wp_logout_url( get_the_permalink($logout_redirect_page) ).'">'.$user_logout_text.'</a></li>';
        }
    } 
    return $items;
}
add_filter('wp_nav_menu_items', 'kaya_user_register_login_link', 10, 2);
/* Creating Custom User role */


/**
* Display Media Library Images Based on user roles
*/
if( !function_exists('kaya_media_editor_images') ){
    add_filter( 'ajax_query_attachments_args', 'kaya_media_editor_images', 1, 1 );
    function kaya_media_editor_images( $query ) 
    {
        $id = get_current_user_id();
        if( !current_user_can('edit_others_attachments') && !current_user_can('administrator') && !current_user_can('editor') )
        $query['author'] = $id;
        return $query;
    } 
}
function kaya_media_upload_images( $wp_query ) {
    if ( strpos( $_SERVER[ 'REQUEST_URI' ], '/wp-admin/upload.php' ) !== false ) {
        if ( !current_user_can( 'edit_others_attachments' ) && !current_user_can('administrator') && !current_user_can('editor')) {
            $current_user = wp_get_current_user();
            $wp_query->set( 'author', $current_user->ID );
          //$wp_query->set( 'author', '1' );
        }
    }
}
add_filter('parse_query', 'kaya_media_upload_images' );

/**
 * Creating user role
 */
if( !function_exists('kaya_add_user_role') ){
    function kaya_add_user_role(){
        add_role('user', 'User', array(
        'read' => true, // True allows that capability, False specifically removes it.
        'edit_posts' => false,
        'delete_posts' => false,
        'edit_published_posts' => false,
        'upload_files' => true //last in array needs no comma!
    ));
    }
    kaya_add_user_role(); // Creation New User Role
}

add_action( 'admin_init', 'kta_admin_capabilities');
function kta_admin_capabilities($cap){
    if( !function_exists('pods_api') ){
        return false;
    }

    $pods_options = pods_api()->load_pods( array( 'fields' => false ) );
    $admins = get_role( 'administrator' );

    foreach ($pods_options as $key => $cpts) {
        if( $cpts['options']['capability_type'] == 'custom' ){
            if($cpts['type'] == 'post_type'){        
                $admins->add_cap( 'create_'.$cpts['options']['capability_type_custom'].'s' );
                $admins->add_cap( 'edit_'.$cpts['options']['capability_type_custom'] ); 
                $admins->add_cap( 'edit_'.$cpts['options']['capability_type_custom'].'s' ); 
                $admins->add_cap( 'edit_others_'.$cpts['options']['capability_type_custom'].'s' ); 
                $admins->add_cap( 'publish_'.$cpts['options']['capability_type_custom'].'s' ); 
                $admins->add_cap( 'read_'.$cpts['options']['capability_type_custom'] ); 
                $admins->add_cap( 'read_private_'.$cpts['options']['capability_type_custom'].'s' ); 
                $admins->add_cap( 'delete_'.$cpts['options']['capability_type_custom'] );
                $admins->add_cap( 'edit_private_'.$cpts['options']['capability_type_custom'].'s' );
                $admins->add_cap( 'edit_published_'.$cpts['options']['capability_type_custom'].'s' );
                $admins->add_cap( 'delete_others_'.$cpts['options']['capability_type_custom'].'s' );
                $admins->add_cap( 'delete_published_'.$cpts['options']['capability_type_custom'].'s' );
                $admins->add_cap( 'delete_private_'.$cpts['options']['capability_type_custom'].'s' );
            }
            if($cpts['type'] == 'taxonomy'){
                $admins->add_cap( 'manage_'.$cpts['options']['capability_type_custom'].'_terms' );
                $admins->add_cap( 'delete_'.$cpts['options']['capability_type_custom'].'_terms' );
                $admins->add_cap( 'delete_'.$cpts['options']['capability_type_custom'].'_terms' );
                $admins->add_cap( 'assign_'.$cpts['options']['capability_type_custom'].'_terms' );
                $admins->add_cap( 'edit_'.$cpts['options']['capability_type_custom'].'_terms' );
            }
        }
        else{
        if($cpts['type'] == 'post_type'){
            $admins->add_cap( 'create_'.$cpts['name'].'s' );
            $admins->add_cap( 'edit_'.$cpts['name'] ); 
            $admins->add_cap( 'edit_'.$cpts['name'].'s' ); 
            $admins->add_cap( 'edit_others_'.$cpts['name'].'s' ); 
            $admins->add_cap( 'publish_'.$cpts['name'].'s' ); 
            $admins->add_cap( 'read_'.$cpts['name'] ); 
            $admins->add_cap( 'read_private_'.$cpts['name'].'s' ); 
            $admins->add_cap( 'delete_'.$cpts['name'] );
            $admins->add_cap( 'edit_private_'.$cpts['name'].'s' );
            $admins->add_cap( 'edit_published_'.$cpts['name'].'s' );
            $admins->add_cap( 'delete_others_'.$cpts['name'].'s' );
            $admins->add_cap( 'delete_published_'.$cpts['name'].'s' );
            $admins->add_cap( 'delete_private_'.$cpts['name'].'s' );
        }
        if($cpts['type'] == 'taxonomy'){
            $admins->add_cap( 'manage_'.$cpts['name'].'_terms' );
            $admins->add_cap( 'delete_'.$cpts['name'].'_terms' );
            $admins->add_cap( 'delete_'.$cpts['name'].'_terms' );
            $admins->add_cap( 'assign_'.$cpts['name'].'_terms' );
            $admins->add_cap( 'edit_'.$cpts['name'].'_terms' );
        }
    }
    }
}

?>