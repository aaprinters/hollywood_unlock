<?php
   /**
   * KTA Dashboard Settings
   */
   class Kaya_Admin_Settings{
       function __construct(){
         add_action( 'admin_menu', array($this, 'kaya_settings_page_init'),0,11 );
         add_action( 'init', array($this, 'kaya_options_data') );
         add_action( "init", array($this, 'kaya_load_settings_page') );
      }
      function kaya_settings_page_init() {
         add_menu_page(__('User Registration & Login form', 'kaya_forms'), __('User Registration & Login form', 'kaya_forms'), 'manage_options', 'kaya_settings', array($this, 'kaya_page_setings'));      
      }
      function kaya_options_data() {
         $settings = get_option( "kaya_settings" );
         if ( empty( $settings ) ) {
            $settings = array(
            'non_logged_users_msg' => __('Please Login', 'kaya_forms'),
            
            'user_reg_page_link' => '',
            'email_exist_error_msg' => '',
            'email_valid_error_msg' => '',
            'user_reg_redirect_page' => '',
            'reg_success_msg'   => __('Registration successful and auto generated password has been sent to your email proceed to login.','kaya_forms'),
            'reg_error_msg'   => __('Registration Failure','kaya_forms'),
            'reg_email_subject_msg' => '',
            'terms_condition_text'  => '',
            'terms_condition_page'  => '',
            'disable_last_name'  => '',
            'disable_first_name' => '',
            'disable_nick_name'  => '',
            'disable_phone_number' => '',
            'disable_website_url'  => '',
            'disable_user_role' => '',
            'talent_biography_text' => __('Biography', 'kaya_forms'),
            'reg_email_confirmation_msg' => __('Your account has been set up and you can log in using the following details. Once you have logged in, please ensure that you visit the Site Admin and change you password so that you dont forget it in the future.','kaya_forms'),
            'user_login_page_link' => '',
            'reg_admin_email_msg' => __('Add Admin Email Confirmation Message Goto > Talent Agency > Login / Registration Section', 'kaya_forms'),
            'login_redirect_page' => '',
            'logout_redirect_page' => '',
            'login_error_msg'   => __('Incorrect User Name or Password','kaya_forms'),
            'logout_button_text' => __('Logout', 'kaya_forms'),
            'dashboard_button_text' => __('Dashboard', 'kaya_forms'),
            'hi_button_text' => __('Hi', 'kaya_forms'),
            'user_logout_text' => __('Logout', 'kaya_forms'),
            'disable_user_dashboard_menu' => '',
            'user_profile_page_link' => '',
            'forget_password_success_msg'  => __('Please check your email id for new password','kaya_forms'),
            'forgot_pwd_page_link' => '',
            'forget_password_error_msg'  => __('sorry, we could not find entered email id','kaya_forms') ,
            'update_profile_success_msg' => '',
            'update_profile_error_msg' => '',
            'update_profile_update_msg' => '',
            'update_error_pwd_match_msg' => '',
            'update_button_text' => 'Update',
            'form_bg_color' => '#ffffff',
            'form_border_color' =>'#ebebeb',
            'form_title_background_color' => '#f5f5f5',
            'kta_form_title_color' => '#333333',
            'form_input_field_border_color' => '#e5e5e5',
            'form_input_field_bg_color' => '#ffffff',
            'form_input_field_text_color' => '#757575',
            'button_bg_color' => '#333333',
            'button_bg_hover_color' =>'#ffffff',
            'button_text_hover_color' => '#000000',
            'button_text_color' => '#ffffff',
            'tabel_th_bg_colors' => '#ebebeb',
            'tabel_th_text_colors' => '#333',
            'tabel_tr_even_bg_colors' => '#ffffff',
            'tabel_tr_odd_bg_colors' => '#f8f8f8',
            'tabel_tr_even_colors' => '#787878',
            'tabel_tr_odd_colors' => '#787878',
            'cpatcha_error_message' => __('Please Select Captcha', 'kaya_forms'),
            'cpatcha_site_key' => '',
            'cpatcha_secret_key' => '',
            );
            add_option( "kaya_settings", $settings, '', 'yes' );
         }   
      }
      function kaya_load_settings_page() {
         if ( isset($_POST["kaya_settings-submit"]) && ( $_POST["kaya_settings-submit"] == 'Y' ) ) {
            check_admin_referer( "kaya_settings-page" );
            $this->kaya_save_theme_settings();
            $url_parameters = isset($_GET['tab'])? 'updated=true&tab='.$_GET['tab'] : 'updated=true';
            wp_redirect(admin_url('admin.php?page=kaya_settings&'.$url_parameters));
            exit;
         }
      }
      function kaya_save_theme_settings(){
         global $pagenow;
         $settings = get_option( "kaya_settings" );

         if ( $pagenow == 'admin.php' && $_GET['page'] == 'kaya_settings' ){ 
            if ( isset ( $_GET['tab'] ) )
            $tab = $_GET['tab'];
            else
            $tab = 'general'; 
            switch ( $tab ){ 
                case 'general' :
                  $settings['cpatcha_site_key']  = $_POST['cpatcha_site_key'];
                  $settings['cpatcha_secret_key']  = $_POST['cpatcha_secret_key'];
                  $settings['cpatcha_error_message']  = $_POST['cpatcha_error_message'];
               break;
               case 'reg_options' :
                  $settings['non_logged_users_msg']     = $_POST['non_logged_users_msg'];
                  $settings['user_reg_page_link']     = $_POST['user_reg_page_link'];
                  $settings['user_reg_redirect_page']     = $_POST['user_reg_redirect_page'];
                  $settings['reg_success_msg']        = $_POST['reg_success_msg'];
                  $settings['reg_error_msg']          = $_POST['reg_error_msg'];
                  $settings['reg_email_subject_msg']  = $_POST['reg_email_subject_msg'];
                  $settings['terms_condition_text']   = $_POST['terms_condition_text'];
                  $settings['terms_condition_page']   = $_POST['terms_condition_page'];
                  $settings['reg_email_confirmation_msg']   = $_POST['reg_email_confirmation_msg'];
                  $settings['reg_admin_email_msg']   = $_POST['reg_admin_email_msg'];
                  $settings['disable_last_name']   = $_POST['disable_last_name'];
                  $settings['disable_first_name']   = $_POST['disable_first_name'];
                  $settings['disable_nick_name']   = $_POST['disable_nick_name'];
                  $settings['disable_phone_number']   = $_POST['disable_phone_number'];
                  $settings['disable_website_url']   = $_POST['disable_website_url'];
                  $settings['disable_user_role']   = $_POST['disable_user_role'] ? $_POST['disable_user_role'] : 'off';
                  $settings['user_login_page_link']     = $_POST['user_login_page_link'];
                  $settings['login_redirect_page']   = $_POST['login_redirect_page'];
                  $settings['logout_redirect_page']   = $_POST['logout_redirect_page'];
                  $settings['login_error_msg']   = $_POST['login_error_msg'];
                  $settings['logout_button_text']   = $_POST['logout_button_text'];
                  $settings['dashboard_button_text']   = $_POST['dashboard_button_text'];
                  $settings['hi_button_text']   = $_POST['hi_button_text'];
                  $settings['user_logout_text']   = $_POST['user_logout_text'];
                  $settings['disable_user_dashboard_menu']   = $_POST['disable_user_dashboard_menu'];
                  $settings['user_profile_page_link']   = $_POST['user_profile_page_link'];
                  $settings['user_hi_text_change']   = $_POST['user_hi_text_change'];
                  $settings['forgot_pwd_page_link']   = $_POST['forgot_pwd_page_link'];
                  $settings['forget_password_success_msg']   = $_POST['forget_password_success_msg'];
                  $settings['forget_password_error_msg']   = $_POST['forget_password_error_msg'];
                  $settings['update_profile_success_msg'] = $_POST['update_profile_success_msg'];
                  $settings['update_profile_error_msg']   = $_POST['update_profile_error_msg'];
                  $settings['update_profile_update_msg']   = $_POST['update_profile_update_msg'];
                  $settings['update_error_pwd_match_msg']   = $_POST['update_error_pwd_match_msg'];
                  $settings['update_button_text']   = $_POST['update_button_text'];
                  $settings['email_valid_error_msg']   = $_POST['email_valid_error_msg'];
                  $settings['email_exist_error_msg']   = $_POST['email_exist_error_msg'];
               break;
               case 'form_colors' :
                  $settings['form_bg_color']                   = $_POST['form_bg_color'];
                  $settings['form_border_color']               = $_POST['form_border_color'];
                  $settings['form_title_background_color']     = $_POST['form_title_background_color'];
                  $settings['kta_form_title_color']            = $_POST['kta_form_title_color'];
                  $settings['form_input_field_border_color']   = $_POST['form_input_field_border_color'];
                  $settings['form_input_field_bg_color']       = $_POST['form_input_field_bg_color'];
                  $settings['form_input_field_text_color']     = $_POST['form_input_field_text_color'];
                  $settings['button_bg_color']                 = $_POST['button_bg_color'];
                  $settings['button_bg_hover_color']           = $_POST['button_bg_hover_color'];
                  $settings['button_text_color']               = $_POST['button_text_color'];
                  $settings['button_text_hover_color']         = $_POST['button_text_hover_color'];
                  $settings['tabel_th_bg_colors']              = $_POST['tabel_th_bg_colors'];
                  $settings['tabel_th_text_colors']            = $_POST['tabel_th_text_colors'];
                  $settings['tabel_tr_even_bg_colors']         = $_POST['tabel_tr_even_bg_colors'];
                  $settings['tabel_tr_odd_bg_colors']          = $_POST['tabel_tr_odd_bg_colors'];
                  $settings['tabel_tr_even_colors']            = $_POST['tabel_tr_even_colors'];
                  $settings['tabel_tr_odd_colors']             = $_POST['tabel_tr_odd_colors'];
               break;
            }
         }
         $updated = update_option( "kaya_settings", $settings );
      }
      function kaya_admin_tabs( $current = 'general' ) { 
         $tabs = array(
            'general' => __('General', 'kaya_forms'),
            'reg_options' => __('Login / Registration','kaya_forms'), 
            'form_colors' => __('Color Settings','kaya_forms'),
            );

         $links = array();
         echo '<div id="icon-admin" class="icon32"><br></div>';
         echo '<h2 class="nav-tab-wrapper">';
         foreach( $tabs as $tab => $name ){
            $class = ( $tab == $current ) ? ' nav-tab-active' : '';
            echo "<a class='nav-tab$class' href='?page=kaya_settings&tab=$tab'>$name</a>";
         }
         echo '</h2>';
      }
      function kaya_page_setings() {
      global $pagenow;
      $settings = get_option( "kaya_settings" ); 
      ?>
      <div class="wrap kaya-admin-dashboard">
         <script type='text/javascript'>
            jQuery(document).ready(function($) {
               jQuery('.kaya_admin_color_pickr').each(function(){
                  jQuery(this).wpColorPicker();
               });
            });
         </script>
         <div class="kaya-header-title">
           <h1><?php _e('Login / Registration  Settings','kaya_forms'); ?></h1> 
         </div>
         <?php
         if ( isset($_GET['updated']) && ( 'true' == esc_attr( $_GET['updated'] ) ) ) echo '<div class="updated" ><p>Theme Settings updated.</p></div>';
         if ( isset ( $_GET['tab'] ) ) $this->kaya_admin_tabs($_GET['tab']); else $this->kaya_admin_tabs('general');
         ?>
         <div id="poststuff">
            
            <form method="post" action="<?php admin_url( 'admin.php?page=kaya_settings' ); ?>"> 
               <?php
               wp_nonce_field( "kaya_settings-page" );                
               if ( $pagenow == 'admin.php' && $_GET['page'] == 'kaya_settings' ){ 
               if ( isset ( $_GET['tab'] ) ) $tab = $_GET['tab'];
                  else $tab = 'general';
                  switch ( $tab ){   
                   case 'general' : 
                     echo '<div class="kaya-admin-panel">';
                        echo '<table class="form-table">'; ?>
                         <h4><?php _e('Captcha Settings','kaya_forms'); ?></h4>
                         <p><?php _e('For more information about google re-captcha click ', 'kaya_forms') ?> <a href="https://www.google.com/recaptcha/" target="_blan"><?php _e('here', 'kaya_forms'); ?></a></p> 
                           <tr>
                              <th><label for="cpatcha_site_key"><?php _e('Cpatcha Sitekey','kaya_forms'); ?></label></th>
                                 <td><input type="text" name="cpatcha_site_key" value="<?php echo isset($settings['cpatcha_site_key']) ? __($settings['cpatcha_site_key'], 'kaya_forms') : ''; ?>" />
                                 </td>
                           </tr>
                          <tr>
                              <th><label for="cpatcha_secret_key"><?php _e('Cpatcha Secret key','kaya_forms'); ?></label></th>
                                 <td><input type="text" name="cpatcha_secret_key" value="<?php echo isset($settings['cpatcha_secret_key']) ? __($settings['cpatcha_secret_key'], 'kaya_forms') : ''; ?>" />
                                 </td>
                           </tr>
                           <tr>
                              <th><label for="cpatcha_error_message"><?php _e('Cpatcha Error Message','kaya_forms'); ?></label></th>
                              <td>
                                 <textarea id="cpatcha_error_message" rows="3" cols="45" name="cpatcha_error_message" type="text" placeholder="<?php _e('Please Select Captcha', 'kaya_forms') ?>" ><?php echo !empty($settings["cpatcha_error_message"]) ? stripslashes(__($settings["cpatcha_error_message"], 'kaya_forms')) : __('Please Select Captcha', 'kaya_forms'); ?></textarea>
                              </td>
                           </tr> 
                        <?php echo '</table>';
                     echo '</div>'; 
                     break;             
                  case 'reg_options' : 
                     echo '<table class="form-table">';
                        echo '<tr>'; ?>
                              <th><label for="non_logged_users_msg"><?php _e('Non logged in Users Message','kaya_forms'); ?></label></th>
                                 <td><textarea id="non_logged_users_msg" rows="3" cols="45" name="non_logged_users_msg" type="text" placeholder="<?php _e('Please Login', 'kaya_forms') ?>" ><?php echo !empty($settings["non_logged_users_msg"]) ? stripslashes(__($settings["non_logged_users_msg"], 'kaya_forms')) : __('Please Login', 'kaya_forms'); ?></textarea>
                                 </td>
                         <?php  echo '</tr>';
                     echo '</table>';
                     echo '<div class="kaya-admin-panel">';
                        echo '<table class="form-table">';
                         ?>
                         <h4><?php _e('Login / Registration  Settings','kaya_forms'); ?></h4> 
                           <tr>
                              <th>
                                 <label for="user_reg_page_link"><?php _e('Registration Page Link','kaya_forms'); ?> </label>
                                 </th>
                                 <td>
                                 <?php $args = array(
                                 'depth'                 => 0,
                                 'child_of'              => 0,
                                 'selected'              => isset($settings['user_reg_page_link']) ? $settings['user_reg_page_link'] : '',
                                 'echo'                  => 1,
                                 'name'                  => 'user_reg_page_link',
                                 'id'                    => 'user_reg_page_link', // string
                                 'class'                 => null, // string
                                 'show_option_none'      => __('Select Page', 'kaya_forms'), // string
                                    'show_option_no_change' => null, // string
                                    'option_none_value'     => 'select-page', // string
                                 ); ?> 
                                 <?php wp_dropdown_pages( $args ); ?>
                              </td>
                           </tr>
                            <tr>
                              <th>
                                 <label for="user_reg_redirect_page"><?php _e('Registration Redirect page Link','kaya_forms'); ?> </label>
                                 </th>
                                 <td>
                                 <?php $args = array(
                                 'depth'                 => 0,
                                 'child_of'              => 0,
                                 'selected'              => isset($settings['user_reg_redirect_page']) ? $settings['user_reg_redirect_page'] : '',
                                 'echo'                  => 1,
                                 'name'                  => 'user_reg_redirect_page',
                                 'id'                    => 'user_reg_redirect_page', // string
                                 'class'                 => null, // string
                                 'show_option_none'      => __('Select Page', 'kaya_forms'), // string
                                    'show_option_no_change' => null, // string
                                    'option_none_value'     => 'select-page', // string
                                 ); ?> 
                                 <?php wp_dropdown_pages( $args ); ?>
                              </td>
                           </tr>
                           <tr>
                              <th><label for="email_valid_error_msg"><?php _e('Valid Email Error Message','kaya_forms'); ?></label></th>
                                 <td><textarea id="email_valid_error_msg" rows="3" cols="45" name="email_valid_error_msg" type="text" placeholder="<?php _e('The Email you entered is not valid.  please try again.', 'kaya_forms') ?>" ><?php echo !empty($settings["email_valid_error_msg"]) ? stripslashes($settings["email_valid_error_msg"]) : ''; ?></textarea>
                                 </td>
                           </tr>
                           <tr>
                              <th><label for="email_exist_error_msg"><?php _e('Email Exist Error Message','kaya_forms'); ?></label></th>
                                 <td><textarea id="email_exist_error_msg" rows="3" cols="45" name="email_exist_error_msg" type="text" placeholder="<?php _e('This email is already used by another user.  try a different one.', 'kaya_forms') ?>" ><?php echo !empty($settings["email_exist_error_msg"]) ? stripslashes(__($settings["email_exist_error_msg"], 'kaya_forms')) : __('This email is already used by another user.  try a different one.', 'kaya_forms'); ?></textarea>
                                 </td>
                           </tr>
                           <tr>
                              <th>
                                 <label for="reg_error_msg"><?php _e('Registration Error Message','kaya_forms'); ?>
                                 </label></th>
                                 <td>
                                 <textarea id="reg_error_msg" rows="3" cols="45" name="reg_error_msg" type="text" placeholder="<?php _e('Registration Error Message', 'kaya_forms') ?>" ><?php echo !empty($settings["reg_error_msg"]) ? stripslashes(__($settings["reg_error_msg"], 'kaya_forms')) : __('Registration Failure', 'kaya_forms'); ?></textarea>
                              </td>
                           </tr>
                           <tr>
                              <th>
                                 <label for="reg_success_msg"><?php _e('Registration Success Message','kaya_forms'); ?>
                                 </label></th>
                                 <td>
                                 <textarea id="reg_success_msg" rows="3" cols="45" name="reg_success_msg" type="text" placeholder="<?php _e('Registration Error Message', 'kaya_forms') ?>" ><?php echo !empty($settings["reg_success_msg"]) ? stripslashes(__($settings["reg_success_msg"], 'kaya_forms')) : __('Registration successful and auto generated password has been sent to your email proceed to login.', 'kaya_forms'); ?></textarea>
                              </td>
                           </tr>
                           <tr>
                              <th><label for="reg_email_subject_msg"><?php _e('Registration Email Subject Message','kaya_forms'); ?></label></th>
                                 <td><textarea id="reg_email_subject_msg" rows="3" cols="45" name="reg_email_subject_msg" type="text" placeholder="<?php _e('Registration Email Subject Message', 'kaya_forms') ?>" ><?php echo !empty($settings["reg_email_subject_msg"]) ? stripslashes(__($settings["reg_email_subject_msg"], 'kaya_forms')) : __('Confirmation - Registration To ','kaya_forms').' '.get_bloginfo('name'); ?></textarea>
                              </td>
                           </tr>
                           <tr>
                              <th><label for="reg_email_confirmation_msg"><?php _e('Registration Email Confirmation Message','kaya_forms'); ?></label></th>
                              <td>
                                 <textarea id="reg_email_confirmation_msg" rows="3" cols="45" name="reg_email_confirmation_msg" type="text" placeholder="<?php _e('Registration Email Confirmation Message', 'kaya_forms') ?>" ><?php echo !empty($settings["reg_email_confirmation_msg"]) ? stripslashes(__($settings["reg_email_confirmation_msg"], 'kaya_forms')) : __('Your account has been set up and you can log in using the following details. Once you have logged in, please ensure that you visit the Site Admin and change you password so that you dont forget it in the future.', 'kaya_forms'); ?></textarea>
                              </td>
                           </tr> 
                        <?php echo '</table>';
                     echo '</div>'; 
                     $login_page = get_page_by_path( 'login' );
                      echo '<div class="kaya-admin-panel">';
                        echo '<table class="form-table">'; ?>
                        <h4><?php _e('Login Form Settings','kaya_forms'); ?></h4>
                        <tr>
                              <th><label for="user_login_page_link"><?php _e('Login Page Link','kaya_forms'); ?> </label></th>
                                 <td>
                                    <?php $args = array(
                                    'depth'                 => 0,
                                    'child_of'              => 0,
                                    'selected'              => !empty($settings['user_login_page_link']) ? $settings['user_login_page_link'] : ( !empty($login_page->ID) ? $login_page->ID : '' ),
                                    'echo'                  => 1,
                                    'name'                  => 'user_login_page_link',
                                    'id'                    => 'user_login_page_link', // string
                                    'class'                 => null, // string
                                    'show_option_none'      => __('Select Page', 'kaya_forms'), // string
                                    'show_option_no_change' => null, // string
                                    'option_none_value'     => 'select-page', // string
                                    ); ?> 
                                    <?php wp_dropdown_pages( $args ); ?>
                              </td>
                           </tr>
                           <tr>
                              <th>
                                 <label for="login_redirect_page"><?php _e('Login Redirect page','kaya_forms'); ?>
                                 </label>
                                 </th>
                                 <td>
                                    <?php $args = array(
                                    'depth'                 => 0,
                                    'child_of'              => 0,
                                    'selected'              => isset($settings['login_redirect_page']) ? $settings['login_redirect_page'] :'',
                                    'echo'                  => 1,
                                    'name'                  => 'login_redirect_page',
                                    'id'                    => 'login_redirect_page', // string
                                    'class'                 => null, // string
                                    'show_option_none'      => __('Select Page', 'kaya_forms'), // string
                                    'show_option_no_change' => null, // string
                                    'option_none_value'     => 'select-page', // string
                                    ); ?> 
                                    <?php wp_dropdown_pages( $args ); ?>
                              </td>
                           </tr>
                           <tr>
                                 <th>
                                    <label for="logout_redirect_page"><?php _e('Logout Redirect page','kaya_forms'); ?>
                                    </label>
                                    </th>
                                    <td>
                                    <?php $args = array(
                                       'depth'                 => 0,
                                       'child_of'              => 0,
                                       'selected'              => isset( $settings['logout_redirect_page'] ) ? $settings['logout_redirect_page'] : '',
                                       'echo'                  => 1,
                                       'name'                  => 'logout_redirect_page',
                                       'id'                    => 'logout_redirect_page', // string
                                       'class'                 => null, // string
                                       'show_option_none'      => __('Select Page', 'kaya_forms'), // string
                                       'show_option_no_change' => null, // string
                                       'option_none_value'     => 'select-page', // string
                                    ); ?> 
                                    <?php wp_dropdown_pages( $args ); ?>
                                 </td>
                              </tr>
                              <tr>
                                 <th><label for="login_error_msg"><?php _e('Login Error Message','kaya_forms'); ?> </label></th>
                                 <td>
                                    <textarea id="login_error_msg" rows="3" cols="40" name="login_error_msg" type="text" placeholder="<?php _e('Incorrect User Name or Password', 'kaya_forms') ?>" ><?php echo !empty($settings["login_error_msg"]) ? stripslashes(__($settings["login_error_msg"], 'kaya_forms')) : __('Incorrect User Name or Password', 'kaya_forms'); ?></textarea>
                                    </td>
                              </tr>
                              <th> <label for="logout_button_text"><?php _e('Logout Link Text','kaya_forms'); ?></label></th>
                                 <td><input type="text" name="logout_button_text" value="<?php echo isset($settings['logout_button_text']) ? __($settings['logout_button_text'], 'kaya_forms') : __('Logout', 'kaya_forms'); ?>" />
                                 </td>
                              </tr>
                              <th> <label for="dashboard_button_text"><?php _e('Dashboard Link Text','kaya_forms'); ?></label></th>
                                 <td><input type="text" name="dashboard_button_text" value="<?php echo isset($settings['dashboard_button_text']) ? __($settings['dashboard_button_text'], 'kaya_forms') : __('', 'kaya_forms'); ?>" />
                                 </td>
                              </tr>
                              <th> <label for="hi_button_text"><?php _e("'Hi' Text Change",'kaya_forms'); ?></label></th>
                                 <td><input type="text" name="hi_button_text" value="<?php echo isset($settings['hi_button_text']) ? __($settings['hi_button_text'], 'kaya_forms') : __('Hi', 'kaya_forms'); ?>" />
                                 </td>
                              </tr>
                        <?php echo '</table>'; 
                     echo '</div>'; ?>
                     <div class="clear"></div>  
                     <?php echo '<div class="kaya-admin-panel">';
                        echo '<table class="form-table">'; ?>
                        <h4><?php _e('Forgot Password Settings','kaya_forms'); ?></h4>
                        <tr>
                              <th><label for="forgot_pwd_page_link"><?php _e('Forgot Password  page','kaya_forms'); ?> </label> </th>
                                 <td>
                                    <?php $args = array(
                                    'depth'                 => 0,
                                    'child_of'              => 0,
                                    'selected'              => isset($settings['forgot_pwd_page_link']) ? $settings['forgot_pwd_page_link'] :'',
                                    'echo'                  => 1,
                                    'name'                  => 'forgot_pwd_page_link',
                                    'id'                    => 'forgot_pwd_page_link', // string
                                    'class'                 => null, // string
                                    'show_option_none'      => __('Select Page', 'kaya_forms'), // string
                                    'show_option_no_change' => null, // string
                                    'option_none_value'     => 'select-page', // string
                                    ); ?> 
                                    <?php wp_dropdown_pages( $args ); ?>
                              </td>
                           </tr>
                        <tr>
                           <th>
                              <label for="forget_password_success_msg"><?php _e('Forgot Password Success Message','kaya_forms'); ?>  
                                </label></th>
                                <td>
                              <textarea id="forget_password_success_msg" rows="3" cols="70" name="forget_password_success_msg" type="text" placeholder="<?php _e('Please check your email id for new password', 'kaya_forms') ?>" ><?php echo !empty($settings["forget_password_success_msg"]) ? stripslashes(__($settings["forget_password_success_msg"],'kaya_forms')) : __('Please check your email id for new password', 'kaya_forms'); ?></textarea>
                           </td>
                        </tr>
                        <tr>
                           <th>
                              <label for="forget_password_error_msg"><?php _e('Forgot Password Error Message','kaya_forms'); ?></label>
                           </th>
                              <td>
                              <textarea id="forget_password_error_msg" rows="3" cols="70" name="forget_password_error_msg" type="text" placeholder="<?php _e('Sorry, we could not find entered email id', 'kaya_forms') ?>" ><?php echo !empty($settings["forget_password_error_msg"]) ? stripslashes(__($settings["forget_password_error_msg"], 'kaya_forms')) : __('Sorry, we could not find entered email id', 'kaya_forms'); ?></textarea>
                           </td>
                        </tr>
                        <?php echo '</table>'; 
                     echo '</div>';  ?>
                        <?php
                         $user_profile = get_page_by_path( 'user-profile' );
                        echo '<div class="kaya-admin-panel">';
                           echo '<table class="form-table">'; ?>
                              <h4><?php _e('Profile Settings','kaya_forms'); ?></h4>
                              <tr>
                                 <th>
                                    <label for="user_profile_page_link"><?php _e('User Profile Page','kaya_forms'); ?>
                                    </label>
                                    </th>
                                    <td>
                                    <?php $args = array(
                                       'depth'                 => 0,
                                       'child_of'              => 0,
                                       'selected'              => isset( $settings['user_profile_page_link'] ) ? $settings['user_profile_page_link'] : ( !empty($user_profile->ID) ? $user_profile->ID : '' ),
                                       'echo'                  => 1,
                                       'name'                  => 'user_profile_page_link',
                                       'id'                    => 'user_profile_page_link', // string
                                       'class'                 => null, // string
                                       'show_option_none'      => __('Select Page', 'kaya_forms'), // string
                                       'show_option_no_change' => null, // string
                                       'option_none_value'     => 'select-page', // string
                                    ); ?> 
                                    <?php wp_dropdown_pages( $args ); ?>
                                 </td>
                              </tr>
                              <tr>
                                 <th><label for="update_profile_update_msg"><?php _e('Profile Update Success Message','kaya_forms'); ?></label>  </th>
                                 <td>
                                    <textarea id="update_profile_update_msg" rows="3" cols="70" name="update_profile_update_msg" type="text" placeholder="<?php _e('Your Profile Has Been Updated Successfully', 'kaya_forms') ?>" ><?php echo !empty($settings["update_profile_update_msg"]) ? stripslashes(__($settings["update_profile_update_msg"], 'kaya_forms')) : __('Your Profile Has Been Updated Successfully', 'kaya_forms'); ?></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <th><label for="update_error_pwd_match_msg"><?php _e('Password Mismatch Error Message','kaya_forms'); ?></label>  </th>
                                 <td>
                                    <textarea id="update_error_pwd_match_msg" rows="3" cols="70" name="update_error_pwd_match_msg" type="text" placeholder="<?php _e('Error Password Match', 'kaya_forms') ?>" ><?php echo !empty($settings["update_error_pwd_match_msg"]) ? stripslashes(__($settings["update_error_pwd_match_msg"], 'kaya_forms')) : __('Error Password Match', 'kaya_forms'); ?></textarea>
                                 </td>
                              </tr>
                              <tr>
                              <th> <label for="update_button_text"><?php _e('Update Button Text','kaya_forms'); ?></label></th>
                                 <td><input type="text" name="update_button_text" value="<?php echo isset($settings['update_button_text']) ? __($settings['update_button_text'], 'kaya_forms') : __('Update', 'kaya_forms'); ?>" />
                                 </td>
                              </tr> 
                           <?php echo '</table>';
                        echo '</div>';
                        break;
                     case 'form_colors' : 
                        echo '<div class="kaya-admin-panel">';
                              echo '<table class="form-table">'; ?>
                              <h4><?php _e('Form Input Fields Colors','kaya_forms'); ?></h4>
                         <tr>
                           <th><label for="form_bg_color"><?php _e('Form background Color','kaya_forms') ?></label></th>
                           <td>
                              <input type="text" name="form_bg_color" id="'form_bg_color" class="kaya_admin_color_pickr" value="<?php echo !empty($settings['form_bg_color']) ? $settings['form_bg_color'] : '#ffffff' ?>" />
                           </td>
                         </tr>
                         <tr>
                           <th><label for="form_border_color"><?php _e('Form border Color','kaya_forms') ?></label></th>
                           <td>
                              <input type="text" name="form_border_color" id="'form_border_color" class="kaya_admin_color_pickr" value="<?php echo !empty($settings['form_border_color']) ? $settings['form_border_color'] : '#ffffff' ?>" />
                           </td>
                         </tr>
                        <tr>
                           <th><label for="form_title_background_color"><?php _e('Form Section Title background Color','kaya_forms'); ?></label></th>
                           <td>
                              <input type="text" name="form_title_background_color" id="'form_title_background_color" class="kaya_admin_color_pickr" value="<?php echo isset($settings['form_title_background_color']) ? $settings['form_title_background_color'] : '#e5e5e5'; ?>" />
                           </td>
                        </tr>
                        <tr>
                           <th><label for="kta_form_title_color"><?php _e('Form Section Title Color','kaya_forms'); ?></label></th>
                           <td>
                              <input type="text" name="kta_form_title_color" id="'kta_form_title_color" class="kaya_admin_color_pickr" value="<?php echo isset($settings['kta_form_title_color']) ? $settings['kta_form_title_color'] : '#333333'; ?>" />
                           </td>
                        </tr>
                        <tr>
                           <th> <label for="form_input_field_border_color"><?php _e('Input Field Border Color','kaya_forms'); ?> </label></th>
                           <td>   
                              <input type="text" name="form_input_field_border_color" id="'form_input_field_border_color" class="kaya_admin_color_pickr" value="<?php echo isset($settings['form_input_field_border_color']) ? $settings['form_input_field_border_color'] : '#e5e5e5'; ?>" />
                           </td>
                        </tr>
                         <tr>
                           <th> <label for="form_input_field_bg_color"><?php _e('Input Field Bg Color','kaya_forms'); ?> </label></th>
                           <td>   
                              <input type="text" name="form_input_field_bg_color" id="'form_input_field_bg_color" class="kaya_admin_color_pickr" value="<?php echo isset($settings['form_input_field_bg_color']) ? $settings['form_input_field_bg_color'] : '#ffffff'; ?>" />
                           </td>
                        </tr>
                        <tr>
                           <th><label for="form_input_field_text_color"><?php _e('Input Field Text Color','kaya_forms'); ?> </label></th>
                           <td>
                              <input type="text" name="form_input_field_text_color" id="'form_input_field_text_color" class="kaya_admin_color_pickr" value="<?php echo isset($settings['form_input_field_text_color']) ? $settings['form_input_field_text_color'] : '#757575'; ?>" />
                           </td>
                        </tr>
                        <tr>
                           <th><label for="button_bg_color"><?php _e('Button Background Color','kaya_forms'); ?> </label></th>
                           <td>
                              <input type="text" name="button_bg_color" id="'button_bg_color" class="kaya_admin_color_pickr" value="<?php echo isset($settings['button_bg_color']) ? $settings['button_bg_color'] : '#333333'; ?>" />
                           </td>
                        </tr>
                        <tr>
                           <th><label for="button_bg_hover_color"><?php _e('Button Hover Background Color','kaya_forms'); ?> </label></th>
                           <td>
                              <input type="text" name="button_bg_hover_color" id="'button_bg_hover_color" class="kaya_admin_color_pickr" value="<?php echo isset($settings['button_bg_hover_color']) ? $settings['button_bg_hover_color'] : '#ffffff'; ?>" />
                           </td>
                        </tr>
                        <tr>
                           <th><label for="button_text_color"><?php _e('Button Text Color','kaya_forms'); ?> </label></th>
                           <td>
                              <input type="text" name="button_text_color" id="'button_text_color" class="kaya_admin_color_pickr" value="<?php echo isset($settings['button_text_color']) ? $settings['button_text_color'] : '#ffffff'; ?>" />
                           </td>
                        </tr>
                        <tr>
                           <th><label for="button_text_hover_color"><?php _e('Button Text Hover Color','kaya_forms'); ?> </label></th>
                           <td>
                              <input type="text" name="button_text_hover_color" id="'button_text_hover_color" class="kaya_admin_color_pickr" value="<?php echo isset($settings['button_text_hover_color']) ? $settings['button_text_hover_color'] : '#ffffff'; ?>" />
                           </td>
                        </tr>
                        <?php
                        echo '</table>';
                        echo '</div>';                      
                        break;                     
                  }
               echo '</table>';
               }
               ?>
               <p class="submit" style="clear: both;">
                  <input type="submit" name="Submit"  class="button-primary" value="<?php esc_html_e('Update Settings', 'kaya_forms'); ?>" />
                  <input type="hidden" name="kaya_settings-submit" value="Y" />
               </p>
            </form>
         </div>
      </div>
      <?php
      }
   }
   new Kaya_Admin_Settings;
   function kaya_admin_options(){
      global  $kaya_settings;
      $kaya_settings = get_option('kaya_settings');
      return $kaya_settings;
   }
   kaya_admin_options();
?>