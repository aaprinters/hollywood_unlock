<?php
/**
 * Frontend Reset Password - Main lost password form
 * 
 * @version	1.0.5
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<div id="password-lost-form-wrap">

	<?php if ( ! empty( $errors ) ) : ?>

		<?php foreach ( $errors as $error ) : ?>
			<p class="som-password-sent-message som-password-error-message">
				<span><?php echo $error; ?></span>
			</p>
		<?php endforeach; ?>

	<?php endif; ?>
<?php
/*
	echo '<p>' . $url . '</p>';
*/
?>
	<?php $email_confirmed = isset( $_REQUEST['email_confirmed'] ) ? $_REQUEST['email_confirmed'] : '' ;
	 $reset_message = ' Password reset link is on its way and should arrive in your inbox in the next 10 to 15 minutes.';
	if ( $email_confirmed ) : ?>
		<p class="alert alert-success fade in">
			<i class="fa fa-check" aria-hidden="true"></i><span><?php echo $reset_message; ?></span>
		</p>
	<?php endif; ?>
	<div class="kaya-user-form kaya-form kaya-user-registration-login-form">
		<div class="kaya-panel"><h4><?php echo $form_title; ?>	</h4>
			<form id="lostpasswordform" method="post" class="account-page-form">

					<div class="somfrp-lost-pass-form-text">
						<?php echo $lost_text_output;?>
					</div>

					<p class="no-margin">
						<label for="email"><?php _e( 'Email Address or Username', 'frontend-reset-password' ); ?></label>
						<input type="text" name="somfrp_user_info" id="somfrp_user_info">
					</p>

					<div class="lostpassword-submit">
						<?php wp_nonce_field( 'somfrp_lost_pass', 'somfrp_nonce' ); ?>
						<input type="hidden" name="submitted" id="submitted" value="true">
						<input type="hidden" name="somfrp_action" id="somfrp_post_action" value="somfrp_lost_pass">
						<button type="submit" id="wp-submit" name="reset-pass-submit" class="button big-btn"><?php echo $button_text; ?></button>
					</div>
			</form>
		</div>
	</div>

</div>