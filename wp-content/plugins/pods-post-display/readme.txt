=== Pods Post Display - Add on for Pods CPT Fields & Taxonomies  ===
Author: kayapati
Tags: grid view, slider view, post display, taxonomy styles
Requires at least: 3.5
Tested up to: 4.7.1
Stable tag: 1.0.1

== Description ==

Pods Add-on allow you to display pods CPT post’s custom fields data and Taxonomy layout styles like number of column, thumbnail sizes and number of posts to display.

== Installation ==

1. Upload and install ‘Pods Post Display’ in the same way you'd install any other plugin, make sure that you installed and activated “Pods - Custom Content Types and Fields” https://wordpress.org/plugins/pods/


== Widgets Included ==

1. Pods Post Grid View : Using this widget you can display pods posts in any where on the pages.
2. Pods Slider : Use this widget to create post slider on the pages.
3. Advance Search Filter : Using this widget you can create simple and advanced search filter either on the widget area or page.


== Change-log ==

= 1.2.4 ==
* add_action int issue fixed. arising bugs for other plugins.

= 1.2.3 ==
* Gallery image alignment issue while uploading images in user profile from front end fixed.

= 1.2.2 ==
* New option added i.e.,Age ange Fields in "Pods-Taxonomy-Search-Filter" Widget.

= 1.2.1 ==
* Ajax search result page columns issue fixed.

= 1.2.0 ==
* New Option added for Pods-Search-Filter Widget i.e., Search Result Text.
* Language File Updated.

= 1.1.9 ==
* New option added for Pods-cpt-front-end-multiple-posting-form widget i.e.,User Profile Talent Message note.
* .po Language File Updated. 

= 1.1.8 ==
* Pods posting form alignment issues fixed.

= 1.1.7 ==
* site loading slow issues fixed.

= 1.1.6 =
* Minor changes in post-grid-view, post-slider-view widgets.

= 1.1.5 =
* Replaced Title name in form for "pod-cpt-front-end-multiple-view-edit-forms" widget.

= 1.1.4 =
* WP default Search Issue Fixed.

= 1.1.3 =
* Pods form custom fields editing issue Fixed.

= 1.1.2 =
* New Feature Added For Talents Single Page Enable/Disable option For Tabs Section.

= 1.1.1 =
* New "Gray Scale" option added in Pods Post Display Admin Page -> Taxonomy Tab.

= 1.1.0 =
* "pods-cpt-front-end-multiple-view-edit-form" widget Post Title String changeble option added.
* "pods-cpt-front-end-multiple-view-edit-form" widget styles added.

= 1.0.9 =
* General Profile Tab added.
* General Information Section Shortcode Feature added [general_profile_info].

= 1.0.8 =
* Custom Profile Tab Styles Added.

= 1.0.7 =
* New Widget "Kaya-Custom-Selectbox" Added.

= 1.0.6 =
* Issue Fixed In seach filter slider.
* Third profile tab visibility issue fixed.
 
= 1.0.5 =
* Added User Drop-Down Menu
* Renamed Pods - Frontend posting Form Widget

= 1.0.4 =
* Added new feature, wordpress default tags added in front end cpt forms.
* Re-design all the widgets titles & description.
* issue fixed, search form selecting fields.
* Updated language files.

= 1.0.3 =
* Pods CPT fields "video & Audio" display issue fixed.

= 1.0.2 =
* Select box issue fixed in search form.
* Enable search form CPT fields based on selection added in Admin section Search form.
* Search form display issue issue fixed.

= 1.0.1 =
* "Pods CPT Post Grid View" widget Multi levels cpt & taxonomies added as a menu.
* "Pods CPT Post Grid View" widget Age relating issue fixed on model thumbnail.
* "Pods CPT Post Slider" widget  Age relating issue fixed on model thumbnail.

= 1.0 =
* First version.