(function($) {
    "use strict";
    $(function() {
        $('.select-box-wraper').each(function() {
            $('.description-wrapper').hide();
            $('.title1').show();
            $(this).find('#option-select').change(function() {
                $('.description-wrapper').hide();
                var select_data = $(this).find('option:selected').val();
                $('.' + select_data).show()
            })
        });

        function resize_window_width() {
            if ($(window).width() >= 782) {
                $('.filtertabs').removeClass('mobile_filter_menu');
                $('.filtertabs').addClass('main_filter_menu')
            } else {
                $('.filtertabs').addClass('mobile_filter_menu');
                $('.filtertabs').removeClass('main_filter_menu')
            }
        }
        resize_window_width();
        $(window).resize(function() {
            resize_window_width();
            $(".filtertabs.mobile_filter_menu > ul > li").on('click', function() {
                if ($(this).hasClass('active_tab_menu')) {
                    $(this).removeClass('active_tab_menu')
                } else {
                    $(this).addClass('active_tab_menu')
                }
            })
        });
        $('.cpt-post-content-wrapper .owl-carousel').each(function() {
            var $column = $(this).data('columns');
            var $responsive_columns2 = (($column == '3') || ($column == '4') || ($column == '5') || ($column == '6')) ? '2' : $column;
            var $responsive_columns3 = (($column == '3') || ($column == '4') || ($column == '5') || ($column == '6')) ? '3' : $column;
            $(this).owlCarousel({
                loop: !0,
                margin: 10,
                nav: !1,
                items: $column,
                responsive: {
                    0: {
                        items: 1
                    },
                    500: {
                        items: $responsive_columns2,
                        loop: !0,
                    },
                    768: {
                        items: $responsive_columns3,
                        loop: !0,
                    },
                    1000: {}
                }
            })
        });
        $(window).load(function() {
            $(function() {
                $('.cpt-post-content-wrapper:not(.shortlist-page-wrapper) > ul.masonry, .taxonomy-content-wrapper > ul, .kaya-post-content-wrapper:not(.shortlist-page-wrapper) > ul, .single-page-meta-content-wrapper .gallery, .ajax-search-results-page > ul, .post-content-wrapper > ul').masonry()
            })
        });
        $('.advanced_search_box_wrrapper').each(function() {
            var $this = $(this);
            $(this).find('select.form_options_data').on('change', function() {
                var $search_opt_val = $(this).find('option:selected').val();
                $this.find('.advancedsearch_form_wrapper').hide();
                $this.find('#' + $search_opt_val).show()
            }).change()
        });
        /*$('.search-content-wrapper .page-numbers a.page-numbers ').live('click', function(e) {
            e.preventDefault();
            var $this_id = $('.search-content-wrapper').data('div_id');
            var div_id = 'div#' + $this_id;
            var $this = $('div#' + $this_id);
            var ajax_search = $this.data('ajax');
            if (ajax_search == 'on') {
                var url = $(this).attr('href');
                var paged = $(this).text();
                ajax_search_data($this, paged)
            }
        });*/
        var $length = $('ul.tabs_content_wrapper li');
        if ($length.length <= 1) {
            $length.parent().addClass('remove-tab-border');
            $length.parent().parent().addClass('remove-tab-padding')
        }
        $(window).load(function() {
            $('.user-data-wrapper > ul, .search-content-wrapper > ul').masonry()
        });
        $('.users-pagination .page-numbers li .page-numbers').live('click', function(e) {
            e.preventDefault();
            var $this_id = $('.user-data-wrapper').attr('id');
            var $this_class = '.' + $this_id;
            var $ajax_search = $('.pods-users-search-form').data('ajax');
            if ($ajax_search == 'yes') {
                $(this).removeClass('current');
                kaya_ajax_search($this_class, $(this).text());
                return !1
            }
        })
        $('.users_advanced_search_form_wrapper').each(function() {
            var $this = $(this);
            $(this).find('.search_data_submit').on('click', function() {
                var $search_calss = '.' + ($this.find('form').data('item'));
                var $ajax_search = $($search_calss + '.pods-users-search-form').data('ajax');
                if ($ajax_search == 'yes') {
                    kaya_ajax_search($search_calss, '1');
                    return !1
                }
            })
        });

        function kaya_ajax_search($search_calss = '', $paged = '1') {
            var $fields_data = $($search_calss + '.pods-users-search-form').serialize();
            var $search_result = $($search_calss + '.pods-users-search-form').data('results');
            $('.' + $search_result).css('opacity', 0.5);
            $.ajax({
                type: 'POST',
                url: kaya_ajax_url.ajaxurl,
                data: {
                    action: 'kaya_users_ajax_search',
                    search_data: $fields_data,
                    paged: $paged,
                },
                success: function(data, status) {
                    $('.' + $search_result).next('.loading_msg').remove();
                    $('.' + $search_result).css('opacity', 1);
                    $('.' + $search_result).html(data);
                    var $container = $('.user-data-wrapper > ul');
                    $container.imagesLoaded(function() {
                        $container.masonry()
                    });
                    $(window).trigger('resize');
                    $('.users-pagination .page-numbers li .page-numbers').removeClass('current');
                    $('.ajax-pagination .page-numbers li').eq($paged - 1).addClass('current')
                },
                error: function(status, error) {
                    $('.search_ajax_response').next('.loading_msg').remove();
                    $('.search_ajax_response').css('opacity', 1);
                    $('.search_ajax_response').html(error)
                }
            });
            return !1
        }
        $('.search-content-wrapper .page-numbers li .page-numbers').live('click', function(e) {
            e.preventDefault();
            var $this_id = $('.search-content-wrapper').attr('id');
            var $this_class = $this_id;
            var $ajax_search = $('.pods-users-search-form').data('ajax');
            if ($ajax_search == 'yes') {
                $(this).removeClass('current');
                kaya_cpt_ajax_search($this_class, $(this).text());
                return !1
            }
        })
        $('.taxonomy_advanced_search_form_wrapper').each(function() {
            var $this = $(this);
            $(this).find('.search_data_submit').on('click', function() {
                var $search_calss = '.' + ($this.find('form').data('item'));
                var $ajax_search = $($search_calss + '.pods-users-search-form').data('ajax');
                if ($ajax_search == 'yes') {
                    kaya_cpt_ajax_search($search_calss, '1');
                    return !1
                }
            })
        });

        function kaya_cpt_ajax_search($search_calss = '', $paged = '1') {
            $('.search-content-wrapper').find('ul.page-numbers li a.next.page-numbers').parent().hide();
            $('.search-content-wrapper').find('ul.page-numbers li a.prev.page-numbers').parent().hide();
            var $tab_count = $($search_calss + '.pods-users-search-form').data('tab-count');
            var $fields_data = $($search_calss + '.pods-users-search-form').serialize();
            var $search_result = $($search_calss + '.pods-users-search-form').data('results');
            $('.' + $search_result).css('opacity', 0.5);
            $.ajax({
                type: 'POST',
                url: kaya_ajax_url.ajaxurl,
                data: {
                    action: 'kaya_cpt_post_ajax_search',
                    search_data: $fields_data,
                    paged: $paged,
                    search_class: $search_calss,
                    tab_count: $tab_count,
                },
                success: function(data, status) {
                    $('.' + $search_result).next('.loading_msg').remove();
                    $('.' + $search_result).css('opacity', 1);
                    $('.' + $search_result).html(data);
                    var $container = $('.search-content-wrapper > ul');
                    $container.imagesLoaded(function() {
                        $container.masonry()
                    });
                    $(window).trigger('resize');
                    $('.users-pagination .page-numbers li .page-numbers').removeClass('current');
                    $('.ajax-pagination .page-numbers li').eq($paged - 1).addClass('current');
                    $('ul.page-numbers li a.next').addClass('same').parent().addClass('parent').hide();
                    $('ul.page-numbers li a.prev').parent().hide();
                    $('.talent-info').each(function() {
                        $('a.item_button.add').hide();
                        $(this).hover(function() {
                            $(this).find('a.item_button.add').show(250)
                        }, function() {
                            $(this).find('a.item_button.add').hide(150)
                        })
                    });
                    var action = $('a.item_button, .cpt_posts_add_remove a.action');
                    shortlist.getItemTotal();
                    shortlist.itemActions(action)
                },
                error: function(status, error) {
                    $('.search_ajax_response').next('.loading_msg').remove();
                    $('.search_ajax_response').css('opacity', 1);
                    $('.search_ajax_response').html(error)
                }
            });
            return !1
        }

        function talents_image_hover_details() {
            $('.talent_image_details_wrapper').each(function() {
                $(this).hover(function() {
                    $(this).find('.general-meta-fields-info-wrapper').stop(!0, !0).animate({
                        'bottom': '0',
                        'opacity': 1,
                        'left': 0
                    }, 300)
                }, function() {
                    $(this).find('.general-meta-fields-info-wrapper').stop(!0, !0).animate({
                        'bottom': '-100%',
                        'opacity': 0,
                        'left': '-100%'
                    }, 500)
                })
            })
        }

        function ajax_search_data($this, paged) {
            var $div_id = $this.attr('id');
            var paged_val = paged;
            var ajax_search = $this.data('ajax');
            var append_search_class = $this.data('class') ? ' .' + $this.data('class') : '.mid-content';
            if (ajax_search == 'on') {
                if ($this.find('.advanced_search_forms').length > 1) {
                    var pods_cpt_name = $this.find('#pods_cpt_name').val()
                } else {
                    var pods_cpt_name = $this.find('#pods_cpt_name').data('cpt_name')
                }
                var search_data = $('.searchbox-wrapper.pods_' + pods_cpt_name + '_fields_info').serialize();
                $('#kaya-mid-content-wrapper #mid-content .mid-content, .main-pages-slider-wrapper , ' + append_search_class).stop(!0, !0).animate({
                    'opacity': '0.2'
                }, 300);
                $.ajax({
                    type: "POST",
                    url: kaya_ajax_url.ajaxurl,
                    data: {
                        action: 'ajx_search_query',
                        advance_search: 'advance_search ',
                        search_data: search_data,
                        paged: paged_val,
                        div_id: $div_id,
                    },
                    success: function(data) {
                        $('.main-pages-slider-wrapper').remove();
                        $(append_search_class).stop(!0, !0).animate({
                            'opacity': '1'
                        }, 300).html(data);
                        talents_image_hover_details();
                        $('.talent-info').each(function() {
                            $('a.item_button.add').hide();
                            $(this).hover(function() {
                                $(this).find('a.item_button.add').show(250)
                            }, function() {
                                $(this).find('a.item_button.add').hide(150)
                            })
                        });
                        var action = $('a.item_button, .cpt_posts_add_remove a.action');
                        shortlist.getItemTotal();
                        shortlist.itemActions(action)
                    }
                })
            }
        }
    })
})(jQuery);