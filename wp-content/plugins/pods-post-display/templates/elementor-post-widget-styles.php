<?php
if( !empty($kaya_shortlist_options['enable_cpt_shortlist']) ){
//	do_action('kaya_pods_cpt_shortlist_icons'); // Shortlist Icons
}
echo '<a href="'.get_the_permalink().'">';
	$image_sizes = ( $settings['image_size_size'] == 'custom' ) ? array_values($settings['image_size_custom_dimension']) : $settings['image_size_size'];
	echo kaya_pod_featured_img($image_sizes, $settings['image_size_size']);
echo '</a>';
echo '<div class="title-meta-data-wrapper">';
	echo '<h3 style="color:'.$settings['grid_posts_title_color'].'"><a style="color:'.$settings['grid_posts_title_color'].'" href="'.get_the_permalink().'">'.get_the_title().'</a></h3>';
	echo kaya_post_post_general_info($settings);	
echo '</div>';
?>