<?php
namespace KayaWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Image_Size;
use Elementor\Utils;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly


class Kaya_Post_Slider_Widget extends Widget_Base {

    public function get_name() {
        return 'kaya-post-slider-view';
    }

    public function get_title() {
        return __('Kaya - Post Slider View', 'ppd');
    }

    public function get_icon() {
        return 'eicon-slider-3d';
    }

    public function get_categories() {
        return array('pods-post-view-addons');
    }

    protected function _register_controls() {

        $this->start_controls_section(
            'section_query',
            [
                'label' => __('Post Query', 'ppd'),
            ]
        );

        $this->add_control(
            'post_types',
            [
                'label' => __('Post Types', 'ppd'),
                'type' => Controls_Manager::SELECT2,
                'default' => 'post',
                'options' => kaya_cpt_post_types(),
                'multiple' => true
            ]
        );
        /*$this->add_control(
            'gray_scale_mode',
            [
                'label' => __('Gray Scale', 'ppd'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('Yes', 'ppd'),
                'label_off' => __('No', 'ppd'),
                'return_value' => 'yes',
                'default' => 'no',
                //'options' => gray_scale_grid_view(),
                'multiple' => true
            ]
        );*/
        $this->add_control(
            'tax_query',
            [
                'label' => __('Taxonomies', 'ppd'),
                'type' => Controls_Manager::SELECT2,
                'options' => kaya_all_taxonomies(),
                'multiple' => true,
                'label_block' => true
            ]
        );
        $this->add_control(
            'advanced',
            [
                'label' => __('Advanced', 'ppd'),
                'type' => Controls_Manager::HEADING,
            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('Order By', 'ppd'),
                'type' => Controls_Manager::SELECT,
                'options' => array(
                    'none' => __('No order', 'ppd'),
                    'ID' => __('Post ID', 'ppd'),
                    'author' => __('Author', 'ppd'),
                    'title' => __('Title', 'ppd'),
                    'date' => __('Published date', 'ppd'),
                    'rand' => __('Random order', 'ppd'),
                    'menu_order' => __('Menu order', 'ppd'),
                ),
                'default' => 'date',
            ]
        );

        $this->add_control(
            'order',
            [
                'label' => __('Order', 'ppd'),
                'type' => Controls_Manager::SELECT,
                'options' => array(
                    'ASC' => __('Ascending', 'ppd'),
                    'DESC' => __('Descending', 'ppd'),
                ),
                'default' => 'DESC',
            ]
        );
       
        $this->end_controls_section();
        // Image Settings
        $this->start_controls_section(
            'section_responsive',
            [
                'label' => __('Column Settings', 'ppd'),
            ]
        );

        $this->add_group_control(
            Group_Control_Image_Size::get_type(),
            [
                'name' => 'image_size', // Actually its `image_size`.
                'label' => __( 'Image Size', 'elementor' ),
                'default' => 'medium',
            ]
        );
       

         $this->add_control(
            'per_line',
            [
                'label' => __('Columns', 'ppd'),
                'type' => Controls_Manager::NUMBER,
                'min' => 1,
                'max' => 8,
                'step' => 1,
                'default' => 4,
            ]
        );

        $this->end_controls_section();

         $this->start_controls_section(
            'title_settings',
            [
                'label' => __('Post Title Settings', 'ppd'),
                'tab' => Controls_Manager::TAB_SETTINGS,
            ]
        );


         $this->add_control(
            'title_font_size',
           [
                'label' => __( 'Title Font Size', 'elementor' ),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 20,
                ],
                'range' => [
                    'px' => [
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper h3' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'title_font_weight',
            [
                'label' => __( 'Title Font Weight', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'bold' => __( 'Bold', 'ppd' ),
                ],
                'default' => 'normal',
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper h3' => 'font-weight:{{VALUE}}!important;',
                ],
            ]
        );

         $this->add_control(
            'title_font_style',
            [
                'label' => __( 'Title Font Style', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'italic' => __( 'Italic', 'ppd' ),
                ],
                'default' => 'normal',
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper h3' => 'font-style: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_section();
        $this->start_controls_section(
            'section_post_meta_fields',
            [
                'label' => __('Post Meta Fields', 'ppd'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'post_meta_color',
            [
                'label' => __( 'Post Meta Hover Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper span' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->add_control(
            'post_meta_bg_color',
            [
                'label' => __( 'Post Meta Background Hover Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .title-meta-data-wrapper' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();

        // Post title Section
        $this->start_controls_section(
            'section_grid_thumbnail_styling',
            [
                'label' => __('Post Title', 'ppd'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'grid_posts_title_color',
            [
                'label' => __( 'Title Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper h3 a, .post-content-wrapper h3, {{WRAPPER}} .post-content-wrapper .title-category-wrapper p' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'title_hover_border_color',
            [
                'label' => __( 'Title Hover Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper h3 a:hover, .grid-view-container a:hover h3' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();

        // Post Meta Data
        $this->start_controls_section(
            'section_post_meta_list',
            [
                'label' => __('Custom Post Post Meta list', 'ppd'),
            ]
        );

        $this->add_control(
            'post_meta_keys',
            [
                'label' => __('Choose Meta Key Name', 'ppd'),
                'type' => Controls_Manager::SELECT2,
                'options' => kaya_get_all_metakeys(),
                'multiple' => true,
                'label_block' => true
            ]
        ); 

         $this->add_control(
            'enable_category_name',
            [
                'label' => __('Enable Category', 'ppd'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('Yes', 'ppd'),
                'label_off' => __('No', 'ppd'),
                'return_value' => 'yes',
                'default' => 'no',
            ]
        );

        $this->add_control(
            'meta_data_font_size',
           [
                'label' => __( 'Meta Data Font Size', 'elementor' ),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 13,
                ],
                'range' => [
                    'px' => [
                        'max' => 50,
                    ],
                ],
                'selectors' => [
                     '{{WRAPPER}} .general-meta-fields-info-wrapper span' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'meta_data_font_weight',
            [
                'label' => __( 'Meta Data Font Weight', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'bold' => __( 'Bold', 'ppd' ),
                ],
                'default' => 'bold',
                'selectors' => [
                    '{{WRAPPER}} .general-meta-fields-info-wrapper span b' => 'font-weight:{{VALUE}};',
                ],
            ]
        );

         $this->add_control(
            'meta_data_font_style',
            [
                'label' => __( 'Meta Data Font Style', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'italic' => __( 'Italic', 'ppd' ),
                ],
                'default' => 'normal',
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper  .general-meta-fields-info-wrapper span' => 'font-style: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_section();

    }


    protected function render() {
        $settings = $this->get_settings();
        ?>
        <script>
            // Model Short list Data added to ajax call back
    var shortlist = (function($) {
      // define object literal container for parameters and methods
      var method = {};
       method.getItemTotal = function() {
        var counter = $('.shortlist-count'),
            clearAll = $('.shortlist-clear a');

        $.ajax({
          type: 'POST',
          url: kaya_ajax_url.ajaxurl,
           data : {
            action : 'kaya_pods_cpt_shortlist_items_count',
          },
          success: function(data) {
            counter.text('('+data+')');
          },
          error: function() {
            log('error with getItemTotal function');
          }
        });
      };
      /* Add & remove actions for individual items
       *
       * ajax method is wrapped inside itemActions() function
       *
       * method has 'button' parameter so jQuery object
       * can be passed in and run via $(button).on('click'...)
      -------------------------------------------------------- */

      method.itemActions = function(button) {
        $(button).on('click', function(e) {
            var target    = $(this),
              item      = target.closest('.item'),
              itemID    = item.attr('id'),
              itemAction= target.data('action');
            $.ajax({
              type: 'POST',
              url: kaya_ajax_url.ajaxurl,
              data : {
            action : 'kaya_pods_cpt_shortlist_items_remove_add',
            item_action : itemAction,
            item_id : itemID
          },
              success: function(data) {
                method.getItemTotal();
                log(itemAction + ' item ' + itemID);
                return false;
              },
              error: function(data) {
                log('error with itemActions function');
            }
          });

    if (itemAction === 'remove') {
      item.removeClass('item_selected');
    } else {
      item.addClass('item_selected');
    }
      e.preventDefault();
    });

  }; // end fn
  /* make methods accessible
  -------------------------- */
  return method;

}(jQuery)); // end of shortlist constructor

            (function($) {
              "use strict";
              $(function() {
                  function shortlist_icons(){
                    var action = $('a.item_button, .cpt_posts_add_remove a.action');
                    shortlist.getItemTotal();
                    shortlist.itemActions( action );
                  }

                  shortlist_icons();
                  $('.post-content-slider-view').each(function(){
                  var $column = $(this).data('columns');
                  var $responsive_columns2 = ( ($column == '3' ) || ( $column == '4'  ) || ( $column == '5' )  || ( $column == '6'  ) ) ? '2' : $column;
                  var $responsive_columns3 = ( ($column == '3' ) || ( $column == '4'  ) || ( $column == '5' )  || ( $column == '6'  ) ) ? '3' : $column;
                  $(this).owlCarousel({
                    rtl:true,
                    loop:true,
                    margin:10,
                    nav:true,
                    navText: ["",""],
                     onRefreshed: shortlist_icons,
                      onInitialized:shortlist_icons,
                    items:$column,
                    responsive:{
                        0:{
                            items:1
                        },
                        500:{
                            items:$responsive_columns2,
                             
                        },
                        768:{
                          items:$responsive_columns3,
                           loop:false,
                        },
                        1000:{
                        }
                    }
                })
              });
            })
}(jQuery));
        </script>
        <?php
        global $kaya_options,$grid_scale_view_mode;
        $taxonomy_cats = !empty($settings['tax_query']) ? $settings['tax_query'] : '';
        $image_sizes = $settings['image_size_custom_dimension'];
        $tax_terms_data = array();
        if( !empty($taxonomy_cats) ){ 
            foreach ($taxonomy_cats as $tax_data) {
                list($tax, $term) = explode(':', $tax_data);
                $tax_terms_data[$tax][] = $term;
            }
        }
        if( !isset($_REQUEST['tax']) ){            
            if( $taxonomy_cats ){
                foreach ($taxonomy_cats as $tax_data) {
                    list($tax, $term) = explode(':', $tax_data);
                    if (!empty($tax) || !empty($term)){
                        $the_taxes[] = array(  
                            'taxonomy' => $tax,
                            'field'    => 'slug',
                            'terms'    => $term,
                        );
                    }
                }
            }else{
               
            }

        }else{
           $the_taxes[] = array(  
                'taxonomy' => $_REQUEST['tax'],
                'field'    => 'slug',
                'terms'    => $_REQUEST['slug'],
            );
        }
        $the_taxes['relation'] = 'OR';
        // Post Loop
        $loop_query = new \WP_Query( array(  
            'post_type' => $settings['post_types'],
            'posts_per_page' =>  -1,
            'orderby' => $settings['orderby'],
            'order' => $settings['order'],
            'tax_query' => $the_taxes,
        ));

        echo '<div class="post-content-wrapper post-content-slider-view owl-carousel" data-columns="'.$settings['per_line'].'">';
        if ( $loop_query->have_posts() ) : 
            while ( $loop_query->have_posts() ) : $loop_query->the_post();
                $template_file = locate_template( 'elementor-post-widget-styles.php' );
                if(isset($_SESSION['shortlist'])) {
                        if ( in_array(get_the_ID(), $_SESSION['shortlist']) ) {
                            $selected = 'item_selected';
                        }else{
                            $selected = '';
                        }
                    }else{
                        $selected = '';
                    }
                echo '<div class="item '.$selected.'" id="'.get_the_ID().'">';
                if( function_exists('gray_scale_grid_view')){
                    if(!empty($kaya_options->gray_scale_mode) ){
                        echo gray_scale_grid_view();
                    }
                    else{
                        echo normal_grid_style();
                    }
                }
                   /*if( $settings['gray_scale_mode'] == 'yes' ){
                        echo gray_scale_grid_view();
                    }
                    else{
                        echo normal_grid_style();
                    }*/         
                if( $template_file ){
                    include $template_file;
                }else{
                    include KAYA_PCV_PLUGIN_PATH.'templates/elementor-post-widget-styles.php';
                }

                echo '</div>';
            endwhile;
            wp_reset_postdata();
            wp_reset_query();
           if(function_exists('kaya_pagination')){
                echo kaya_pagination();
            }
            wp_reset_postdata();
            endif; 
        echo '</div>';
    }

    protected function content_template() {
    }
}