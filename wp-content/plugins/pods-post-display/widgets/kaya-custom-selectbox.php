<?php
namespace KayaWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Image_Size;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly
class Kaya_custom_select_box extends Widget_Base {
    public function get_name() {
        return 'kaya-custom-select-box';
    }
    public function get_title() {
        return __('Kaya Custom Select Box', 'ppd');
    }
    public function get_icon() {
        return 'eicon-posts-carousel';
    }
    public function get_categories() {
        return array('ppd');
    }
    protected function _register_controls() {
        $this->start_controls_section(
            'section_text',
            [
                'label' => __('Selection Text', 'ppd'),
            ]
        );

        $this->add_control(
            'tabs',
            [
                'label' => __( 'Designation information:', 'ppd' ),
                'type' => Controls_Manager::REPEATER,
                'default' => [
                    [
                        'tab_title' => __( 'Toggle #1', 'ppd' ),
                        'tab_content' => __( 'I am item content 1. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'ppd' ),
                    ],
                    [
                        'tab_title' => __( 'Toggle #2', 'ppd' ),
                        'tab_content' => __( 'I am item content 2. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'ppd' ),
                    ],
                ],
                'fields' => [
                    [
                        'name' => 'tab_title',
                        'label' => __( 'Title & Content', 'ppd' ),
                        'type' => Controls_Manager::TEXT,
                        'default' => __( 'Toggle Title' , 'ppd' ),
                        'label_block' => true,
                    ],
                    [
                        'name' => 'tab_content',
                        'label' => __( 'Content', 'ppd' ),
                        'type' => Controls_Manager::WYSIWYG,
                        'default' => __( 'Toggle Content', 'ppd' ),
                        'show_label' => false,
                    ],
                ],
                'title_field' => '{{{ tab_title }}}',
            ]
        );

        $this-> end_controls_section();
        $this-> start_controls_section(
            'section_font_settings',
            [
                'label' => __('Font Settings', 'ppd'),
                'tab' => Controls_Manager::TAB_SETTINGS,
            ]
        );
        $this-> add_control(
            'title_font_size',
            [
                'label' => __('Title Font Size', 'ppd'),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 14,
                ],
                'range' => [
                    'px' => [
                        'max' => 40,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .dropdown select' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        $this-> add_control(
            'title_font_weight',
            [
                'label' => __('Title Font Weight', 'ppd'),
                'type' => Controls_Manager::SELECT,
                'default' => 'bold',
                'options' => [
                    'normal' => __('Normal', 'ppd'),
                    'bold' => __('Bold', 'ppd'),
                    '100' => __('100', 'ppd'),
                    '200' => __('200', 'ppd'),
                    '300' => __('300', 'ppd'),
                    '400' => __('400', 'ppd'),
                    '500' => __('500', 'ppd'),
                    '600' => __('600', 'ppd'),
                    '700' => __('700', 'ppd'),
                    '800' => __('800', 'ppd'),
                    '900' => __('900', 'ppd'),
                ],
                'selectors' => [
                    '{{WRAPPER}} .dropdown select' => 'font-weight: {{VALUE}};',
                ],
            ]
        );
        $this-> add_control(
            'title_font_style',
            [
                'label' => __('Title Font Style', 'ppd'),
                'type' => Controls_Manager::SELECT,
                'default' => 'normal',
                'options' => [
                    'normal' => __('Normal', 'ppd'),
                    'italic' => __('Italic', 'ppd'),
                    'oblique' => __('Oblique', 'ppd'),
                ],
                'selectors' => [
                    '{{WRAPPER}} .dropdown select' => 'font-style: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();
        $this-> start_controls_section(
            'section_title_color',
            [
                'label' => __('Title Colors', 'ppd'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        $this-> add_control(
            'title_color',
            [
                'label' => __('Title Color', 'ppd'),
                'type' => Controls_Manager::COLOR,
                'default' => '#333',
                'selectors' => [
                    '{{WRAPPER}} .dropdown select' => 'Color:{{VALUE}};',
                ],
            ]
        );
        $this-> end_controls_section();
    }

    protected function render() {
      $settings = $this->get_settings();
      $rand=rand(1,10000);
      ?>
        <script>
          var $i = jQuery.noConflict();
             (function($) {
                  "use strict";
                  $(function() {
              //Displa7y Select Box 
                $('#items<?php echo $rand;?>').change(function(){
                  $('.agency<?php echo $rand; ?>').hide();
                  $('#' + $(this).val()).show();
                }); 
              // End Jquery
                  });
            })(jQuery);
        </script>
        <div class="kaya_selectbox" role="tablist">
            <div class="button dropdown">
                <select id="items<?php echo $rand; ?>">
                    <?php foreach ( $settings['tabs'] as $index => $item ) :
                        $tab_count = $index + 1;
                        $tab_title_setting_key = $this->get_repeater_setting_key( 'tab_title', 'tabs', $index );
                    ?>
                        <div class="tabs-title">
                            <option value="tab-<?php echo $index.'-'.$rand; ?>"><?php echo $item['tab_title']; ?></option>
                        </div>
                        <?php endforeach; ?>
                </select>
            </div>
            <div class="tabs-content">
                <div class="output">
                    <?php foreach ( $settings['tabs'] as $index => $item ) :
                        $tab_count = $index + 1;
                        $tab_content_setting_key = $this->get_repeater_setting_key( 'tab_content', 'tabs', $index );
                    ?>
                    <div id="tab-<?php echo $index.'-'.$rand; ?>" class="agency<?php echo $rand; ?>">
                        <?php echo $this->parse_text_editor( $item['tab_content'] ); ?>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <?php
 }
    protected function content_template() {
    }
}