<?php
class Kaya_View_Edit_Cpt_Form_Widget extends WP_Widget{
	public function __construct(){
		parent::__construct('cpt-front-end-view-edit-forms',__('Pods - Frontend Multiple posting Form','ppd'),
			array('description' => __('Use this widget to add pods cpt posts form in Multiple Front End','ppd'))
		);
	}
	public function pods_data_save($pods_id, $field_id, $post_id, $related_pod_id, $related_field_id, $related_id, $related_weight){
		global $wpdb;
		$wpdb->query( $wpdb->prepare(				 "
			INSERT INTO `wp_podsrel`
			    (
			        `pod_id`,
			        `field_id`,
			        `item_id`,
			        `related_pod_id`,
			        `related_field_id`,
			        `related_item_id`,
			        `weight`
			    )
			VALUES ( %d, %d, %d, %d, %d, %d, %d )
			", array(
			$pods_id,
			$field_id,
			$post_id,
			$related_pod_id,
			$related_field_id,
			$related_id,
			$related_weight
		)
		) );
	}
	public function get_pods_data($pod_id, $field_id, $item_id){
		global $wpdb, $related_item_ids;
		$related_data_ids = $wpdb->get_results("SELECT related_item_id FROM wp_podsrel WHERE pod_id= $pod_id AND field_id=$field_id AND item_id = $item_id ");
		foreach ($related_data_ids as $related_data_id) { 
		    $related_item_ids[$related_data_id->related_item_id] = $related_data_id->related_item_id;
		}
		return $related_item_ids;
	}

	public function kaya_pods_options_data() {
		global $kaya_profile_options;
        $settings = get_option( "kaya_profile_options" );
          if( !function_exists('pods_api') ){
             return false;
         }
         $pods_fields = array();
           foreach ($this->pods_options as $key => $options) {
               if( $options['type'] == 'user' ){ 
                  $pods_fields['enable_'.$options['name'].'_data'] = '0';
               }
          }
         // Settings Options Data
         if ( empty( $settings ) ) {
            $settings = array(
               'talent_profile_notification' => '',
            );
            $fields_opt_data = array_merge( $settings, $pods_fields );
            add_option( "kaya_profile_options", $fields_opt_data, '', 'yes' );
         }   
    }

	public function widget( $args,$instance){
	global $pods_success_msg, $current_user, $pods_success_msg, $kaya_settings, $talent_msg_note;
		$instance = wp_parse_args($instance,array(
			'post_type' => '',
			'disable_pod_cpt_fields' => '',
			'thank_you' =>'',
			'submit_button_text' => __('Save Changes', 'ppd'),
			'post_delete_msg' => __('Your post has been deleted', 'ppd'),
			'form_update_message' => __( 'Form Updated successfully', 'ppd' ),
			'post_limit_exceeded_msg' => __("Your limit has been exceeded.", 'ppd'),
			'non_user_role_form_access_msg' => __("You Don't Have Permissions to create post", 'ppd'),
			'add_new_button_text' => __('Add New', 'ppd'),
			'user_prof_talent_text_msg' => __('No Talent Post Has Been Created, To Create Click "Add New" Button And Fill The Form', 'ppd'),
			'multiple_cpt_form_title' => __('Enter Post Title', 'ppd'),
			'featured_img_text' => __('Featured Image', 'ppd'),
		));
		echo $args['before_widget'];
		if( !function_exists('pods_api') ){
			return false;
		}
		$post_count = $this->kaya_pods_cpt_post_count($current_user->ID, $instance['post_type']);
		$user_id = $current_user->ID;	
		global $current_user;
		$current_user_data =  wp_get_current_user($current_user->ID);
		$edit_post = isset( $current_user_data->allcaps['edit_'.$instance['post_type'].'s'] ) ? 'true' : 'false';
		$edit_published = isset( $current_user_data->allcaps['edit_published_'.$instance['post_type'].'s'] ) ? 'true' : 'false';
		$delete_published = isset( $current_user_data->allcaps['delete_published_'.$instance['post_type'].'s'] ) ? 'true' : 'false';
		$page_slug = get_post($instance['thank_you']);
		$talent_msg_note = ($instance['user_prof_talent_text_msg'] ? $instance['user_prof_talent_text_msg'] : __('No Talent Post Has Been Created, To Create Click "Add New" Button And Fill The Form','ppd'));
		if ( !is_user_logged_in() ){ // when user not logged in return false
			echo '<p class="kaya-error-message">'.(!empty($kaya_settings['non_logged_users_msg']) ? stripslashes($kaya_settings['non_logged_users_msg']) : __('Please Login', 'ppd')).'</p>';
			return;
		}
		wp_enqueue_style( 'pods-form' );
		$role_post_access = get_option('role_post_restrict');
			if( is_user_logged_in() ){
				$limit = !empty($role_post_access[$current_user->roles[0]]['limit'][$instance['post_type']]) ? $role_post_access[$current_user->roles[0]]['limit'][$instance['post_type']] : '100000';
			}else{
				$limit = '1000';
			}
			$edit_post = isset( $current_user_data->allcaps['edit_'.$instance['post_type'].'s'] ) ? 'true' : 'false';
			if( $edit_post == 'true' ){
			// Checking post capability
			if( $edit_post == 'true' ){
				// Add New Button Add or remove based on roles
				if( $post_count >= $limit ){
					if( isset($_REQUEST['success']) && ( $_REQUEST['success'] == '1' ) ){
						echo '<p class="kaya-success-message">';
						echo __('Form submitted successfully', 'ppd');
						echo '</p>';
					}

					if( !isset($_REQUEST['success'])){
						echo '<p class="kaya-success-message">';
							echo  !empty($role_post_access[$current_user->roles[0]]['limit']['message'][$instance['post_type']]) ? $role_post_access[$current_user->roles[0]]['limit']['message'][$instance['post_type']] : __('Your limit has been exceeded', 'ppd');
						echo '</p>';
					}
				}else{
					echo '<div class="user-add-new-button">';
						echo '<a href="'.get_the_permalink().'?add_new=add_new_form" >'.($instance['add_new_button_text'] ? $instance['add_new_button_text'] : __('Add New', 'ppd')).'</a>';
					echo '</div>';
				}
			if( isset($_REQUEST['add_new']) && ( $_REQUEST['add_new'] == 'add_new_form' ) ){
				$cpt_types = kaya_get_cpt_fields($instance['post_type']);
				foreach ($cpt_types as $key => $options) {
						$cpt_meta_opts[$key] = $key;
				}
				$pod = pods($instance['post_type']);
				$page_slug = get_post($instance['thank_you']);
				$post_meta_opts = !empty($instance['disable_pod_cpt_fields'][$instance['post_type']]) ? array_diff($cpt_meta_opts, $instance['disable_pod_cpt_fields'][$instance['post_type']]) : $cpt_meta_opts;
				$fields_data = !empty($post_meta_opts) ? (', '.implode(', ',$post_meta_opts)) : '';
				$thank_you = !empty($instance['thank_you']) ? "thank_you='".$page_slug->post_name."'" : '';
				$pods_options = kaya_get_cpt_fields($instance['post_type']);
				$pods_cpt_taxonomies = get_object_taxonomies($instance['post_type']);
				$pods_taxonomy_info = get_object_taxonomies($instance['post_type'],  'objects' );
				// Save Form Fields data
				if ( 'POST' == $_SERVER['REQUEST_METHOD'] && isset( $_POST['post_action'] ) && !empty( $_POST['post_action'] ) && $_POST['post_action'] == 'user_edit_pods_cpt_profile' ){

						if( current_user_can('administrator')) { 
							$post_status = 'publish';
						}else{
							$post_status = $pod->api->pod_data['options']['default_status'];
						}
						// echo $_POST['post_tag_autocomplete'];
						$pods_cpt_categories = $_POST['pods_cpt_categories'] ? $_POST['pods_cpt_categories'] : '';
						$pods_cpt_post_data = array(
							'post_type' => $instance['post_type'],
							'post_title'    => wp_strip_all_tags( $_POST['pods-cpt-post-title'] ),
							'tags_input'    => array($_POST['post_tag_autocomplete'] ),
							'post_status'   => $post_status,
							'post_author'   => $user_id,
							);					 
							$post_id = wp_insert_post( $pods_cpt_post_data );
							if( $post_id ){
								// Update featured image to the current cpt post
								if( $_FILES['upload_pods_post_featured_img'] ){
									
						          pods_get_featured_image_id_by_base64('upload_pods_post_featured_img', $post_id);
						        }
						        // Attach Pods Cpt post categories
								if( !empty($pods_cpt_taxonomies) ){
									foreach ($pods_cpt_taxonomies as $key => $pods_taxonomies) {
										
										if( !empty($pods_taxonomies) ){
											if( $pods_taxonomies == 'post_tag' ){
												wp_set_post_tags($post_id, $_POST['post_tag_autocomplete']);
											}else{
												wp_set_object_terms( $post_id, $pods_cpt_categories, $pods_taxonomies);
											}
										}
									}							
								}
								foreach ($pods_options as $key => $pods_keys) {
									if($pods_keys['type'] == 'file'){
										$fields_data = $pod->fields ($pods_keys['name']);
										if( !empty($_POST[$pods_keys['name']]) ){
											$pods_file_data = array_keys($_POST[$pods_keys['name']]);
											pods_update_form_post_meta($post_id, '_pods_'.$pods_keys['name'], array_combine($pods_file_data, $pods_file_data));
											pods_update_form_post_meta($post_id, $pods_keys['name'], array_combine($pods_file_data, $pods_file_data));
											foreach ( array_combine( $pods_file_data, $pods_file_data ) as $key => $image_id) {
												if( !empty($image_id) ){
													$this->pods_data_save($fields_data['pod_id'], $fields_data['id'], $post_id, '0', '0', $image_id, $fields_data['pod_id'] );
												}
											}
										}
									}
									else{
										pods_update_form_post_meta($post_id, $pods_keys['name'], $_POST[$pods_keys['name']]);
									}
								}
							wp_redirect(get_the_permalink().'/?success=1');

						}else{
							wp_redirect(get_the_permalink().'/?success=0');
						}							
					} // End Here
					if( isset($_REQUEST['success']) && ($_REQUEST['success'] == '1') ){ // Success Results
						echo '<p class="kaya-success-message">'. ( !empty($instance['form_success_message']) ? $instance['form_success_message'] : __('Form submitted successfully','ppd')).'</p>';
					}

				$cpt_types = kaya_get_cpt_fields($instance['post_type']);
				foreach ($cpt_types as $key => $options) {
					$cpt_meta_opts[$key] = $key;
				}
				$pods_success_msg = !empty($instance['form_update_message']) ? $instance['form_update_message'] : __( 'Form Updated successfully', 'ppd' );			
				$this->kaya_user_form_fields('',$pods_cpt_taxonomies, $pods_taxonomy_info,$instance['featured_img_text'], $pods_options, $pod,$instance['multiple_cpt_form_title'], $instance['submit_button_text']);
			}
			elseif( isset($_REQUEST['action']) && ( $_REQUEST['action'] == 'edit' ) ){ // Post Edit absed on post ID
				$post_id = $_REQUEST['id'];
				$cpt_types = kaya_get_cpt_fields($instance['post_type']);
				foreach ($cpt_types as $key => $options) {
						$cpt_meta_opts[$key] = $key;
				}
				$pods_success_msg = !empty($instance['form_update_message']) ? $instance['form_update_message'] : __( 'Form Updated successfully', 'ppd' );
				$pod = pods($instance['post_type'], $post_id);
				$page_slug = get_post($instance['thank_you']);
				$post_meta_opts = !empty($instance['disable_pod_cpt_fields'][$instance['post_type']]) ? array_diff($cpt_meta_opts, $instance['disable_pod_cpt_fields'][$instance['post_type']]) : $cpt_meta_opts;
				$fields_data = !empty($post_meta_opts) ? (', '.implode(', ',$post_meta_opts)) : '';
				$thank_you = !empty($instance['thank_you']) ? "thank_you='".$page_slug->post_name."'" : '';
				$pods_options = kaya_get_cpt_fields($instance['post_type']);
				$pods_cpt_taxonomies = get_object_taxonomies($instance['post_type']);
				$pods_taxonomy_info = get_object_taxonomies($instance['post_type'],  'objects' );
				echo '<div class="pods_cpt_form_wrapper">';				// Submitting the post data
						if ( 'POST' == $_SERVER['REQUEST_METHOD'] && isset( $_POST['post_action'] ) && !empty( $_POST['post_action'] ) && $_POST['post_action'] == 'user_edit_pods_cpt_profile' ){
							$pods_cpt_categories = $_POST['pods_cpt_categories'] ? $_POST['pods_cpt_categories'] : '';
							$post_status = ( get_post_status ( $post_id ) == 'publish' ) ? 'publish' : $pod->api->pod_data['options']['default_status'];	 
							$update_post = array('ID' => $post_id,  'post_status' => $post_status,  'post_title' =>  wp_strip_all_tags( $_POST['pods-cpt-post-title'] ));
							wp_update_post( $update_post );
							if( $_FILES['upload_pods_post_featured_img'] ){
					          pods_get_featured_image_id_by_base64('upload_pods_post_featured_img', $post_id);
					        }
					        // Attach Pods Cpt post categories
							if( !empty($pods_cpt_taxonomies) ){
								foreach ($pods_cpt_taxonomies as $key => $pods_taxonomies) {
									if( !empty($pods_taxonomies) ){
										if( $pods_taxonomies == 'post_tag' ){
											wp_set_post_tags($post_id, $_POST['post_tag_autocomplete']);
										}else{
											wp_set_object_terms( $post_id, $pods_cpt_categories, $pods_taxonomies);
										}
									}
								}							
							}
							foreach ($pods_options as $key => $pods_keys) {
								if($pods_keys['type'] == 'file'){
									global $wpdb;
									$fields_data = $pod->fields ($pods_keys['name']);
									if( !empty($_POST[$pods_keys['name']]) ){
										
										$pods_file_data = array_keys($_POST[$pods_keys['name']]);
										$related_data = array_combine( $pods_file_data, $pods_file_data );
										pods_update_form_post_meta($post_id, '_pods_'.$pods_keys['name'], array_combine($pods_file_data, $pods_file_data));
										pods_update_form_post_meta($post_id, $pods_keys['name'], array_combine($pods_file_data, $pods_file_data));

										$pod_related_data = $this->get_pods_data($fields_data['pod_id'], $fields_data['id'], $post_id);
										$new_vals = $related_data;
										$pod_id= $fields_data['pod_id'];
										$field_id = $fields_data['id'];
										if( array_diff($pod_related_data,$new_vals) === array_diff($new_vals, $pod_related_data) ){
											$pods_array_data = $new_vals;
										}else{
											$pods_array_data = array_diff($pod_related_data,$new_vals);
										}
										foreach ($pods_array_data as $key => $rel_item_id) {
											$wpdb->query( "DELETE FROM wp_podsrel WHERE  pod_id= $pod_id AND field_id=$field_id AND item_id = $post_id AND related_item_id = $rel_item_id" );
										}
										foreach ( array_combine( $pods_file_data, $pods_file_data ) as $key => $image_id) {
											if( !empty($image_id) ){
												$this->pods_data_save($fields_data['pod_id'], $fields_data['id'], $post_id, '0', '0', $image_id, $fields_data['pod_id'] );
											}
										}
									}
								}
								else{
									pods_update_form_post_meta($post_id, $pods_keys['name'], $_POST[$pods_keys['name']]);
								}
							}
							 wp_redirect('?action=edit&id='.$post_id.'&updated=true');
													
						} // End Here
						if( isset($_REQUEST['updated']) && ($_REQUEST['updated'] == 'true') ){ // Success Results
							echo '<p class="kaya-success-message">'.$pods_success_msg.'</p>';
						}
						// Form Start Here
						$this->kaya_user_form_fields($post_id, $pods_cpt_taxonomies, $pods_taxonomy_info, $instance['featured_img_text'], $pods_options, $pod, $instance['multiple_cpt_form_title'], $instance['submit_button_text']);

						//$this->kaya_user_form_fields($post_id, $pods_cpt_taxonomies, $pods_taxonomy_info, $pods_options, $pod, $instance['submit_button_text']);

				echo '</div>';
			}elseif( isset($_REQUEST['action']) && ( $_REQUEST['action'] == 'delete' ) ){ // Post Edit absed on post ID
				echo '<p class="kaya-success-message">';
					echo !empty($instance['post_delete_msg']) ? $instance['post_delete_msg'] : __('Your post has been deleted', 'ppd');
				echo '</p>';
				wp_delete_post($_REQUEST['id']);
				$this->pods_cpt_post_list($instance['post_type']);
			}else{
				$this->pods_cpt_post_list($instance['post_type']);
			//}else{

			} // End Else
		}else{
			echo '<p class="kaya-success-message">'. ( !empty($instance['non_user_role_form_access_msg']) ? $instance['non_user_role_form_access_msg'] : __('You Don\'t Have Permissions to create post','ppd')).'</p>';
		}
		}else{
		   echo '<p class="kaya-success-message">'. ( !empty($instance['non_user_role_form_access_msg']) ? $instance['non_user_role_form_access_msg'] : __('You Don\'t Have Permissions to create post','ppd')).'</p>';
			}
		echo $args['after_widget'];
	}
	function kaya_user_form_fields($post_id, $pods_cpt_taxonomies, $pods_taxonomy_info,$featured_img_text = '', $pods_options, $pod,$multiple_cpt_form_title = '', $submit_button_text = ''){
		global $current_user;
		$current_user_data =  wp_get_current_user($current_user->ID);
		$settings = $this->get_settings();
		$multiple_cpt_form_title = 'Enter Title';
		if(function_exists('kaya_pods_cpt_pam_get_restricted_cat')){
			$restrict_cat = kaya_pods_cpt_pam_get_restricted_cat($current_user_data->roles[0]);
		}
			$cpt_restrict_cats = !empty($restrict_cat) ? explode(',', $restrict_cat) : array(NULL);
				$user_id = $current_user->ID;
				$user_post_id = get_user_meta( $user_id, 'talent_post_id',true); //$user_id is passed as a parameter
				$post_type = get_post_type( get_the_ID() );
				$current_post_id = $_GET['id'];

		if($user_post_id == $current_post_id){
					$edit_profile = '';
					if($post_type == 'talent'){
						$edit_profile = site_url().'/talent-profile/?action=edit&id='.get_the_ID();
					}else if($post_type == 'project'){
						$edit_profile = site_url().'/project/?action=edit&id='.get_the_ID();
					}
					echo '<span class="edit_link"><i class="fa fa-pencil"></i><a href="'.$edit_profile.'">Edit</a></span>';
				
		echo '<form action="" method="post" class="pods-submittable pods-form pods-form-front" enctype="multipart/form-data">';
			echo '<div class="pods-submittable-fields">';
				echo '<ul class="pods-form-fields">';
					echo '<li>';  // Post title
						echo '<div class="pods-field-label">';
						if(!empty($multiple_cpt_form_title)){
							echo '<label class="pods-form-ui-label pods-form-ui-label-pods-field-post-title" for="pods-form-ui-pods-field-post-title">'.$multiple_cpt_form_title .'</label>';
						}
						else{
							echo '<p>'.__('Enter Title (required)', 'ppd'). '</p>';
						}
						echo '</div>';
						echo '<div class="pods-field-input">';
						 ?>
							<input type="text" id ="pods-cpt-post-title" required placeholder="" name="pods-cpt-post-title" value="<?php echo !empty($post_id) ? get_the_title($post_id) : ''; ?>" />
						<?php
						echo '</div>';
					echo '</li>'; 
					echo '<li id="feature_img">'; // Post Featured Image
						echo '<div class="pods-field-label">';
						if(!empty($featured_img_text)){
							echo '<label class="pods-form-ui-label" for="">'.$featured_img_text.'</label>';
						}
						else{
							echo '<p>' .__('Featured Image', 'ppd'). '</p>';
						}
						echo '</div>';
						echo '<div class="pods-field-input">';
							$featured_image = wp_get_attachment_url( get_post_thumbnail_id($post_id) );
			                if( !empty($featured_image) ){ 
			                    echo '<img id="featured_image" src="'.kaya_image_sizes( $featured_image, 0,0 ).'" />'; 
			                }	 ?>
							
	
					  <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
					  <script src="http://demo.itsolutionstuff.com/plugin/croppie.js"></script>
					  <link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/bootstrap-3.min.css">
					  <link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/croppie.css">


							<div class="row">
								<div class="col-md-12 text-center">
									<div id="upload-demo" style="width:350px"></div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style="padding-top:30px;">
									<input name="upload_pods_post_featured_img" type="file" id="upload">
									<div class="btn btn-success upload-result">Update Image</div>
									<input type="text" id="img_data" value="" style="display:none;" name="feature_img_data">
								</div>
								<!--
								<div class="col-md-4" style="">
									<div id="upload-demo-i" style="display:none;background:#e1e1e1;width:300px;padding:30px;height:300px;margin-top:30px"></div>
								</div> -->
							</div>



					<script type="text/javascript">
					img_url = jQuery("#featured_image").attr('src');
					console.log(img_url);
					$uploadCrop = jQuery('#upload-demo').croppie({
						viewport: {
							width: 250,
							height: 250,
							type: 'square'
						},
						url: img_url,
						size:'original',
						quality:1,
						boundary: {
							width: 350,
							height: 350
						}
					});


					jQuery('#upload').on('change', function () { 
						var reader = new FileReader();
						reader.onload = function (e) {
							$uploadCrop.croppie('bind', {
								url: e.target.result
							}).then(function(){
								console.log('jQuery bind complete');
							});
							
						}
						reader.readAsDataURL(this.files[0]);
					});


					jQuery('.upload-result').on('click', function (ev) {
						$uploadCrop.croppie('result', {
							type: 'canvas',
							size: {
							width: 400
						},
						}).then(function (resp) {
							
					console.log(resp);
					//html = '<img src="' + resp + '" />';
					//jQuery("#upload-demo-i").html(html);

						jQuery("#featured_image").attr('src',resp);
						jQuery("#img_data").val(resp);

						});
					});


					</script>

						<!--<input type="file" id="upload_pods_post_featured_img" name="upload_pods_post_featured_img" > -->
						<?php 
						echo '</div>';
					echo '</li>';
					if(count($pods_cpt_taxonomies) > 0)  // Cpt Post Taxonomies list
					{	
						foreach ($pods_cpt_taxonomies as $key => $pods_cpt_taxonomy) {
							$pods_cpt_terms = get_terms($pods_cpt_taxonomy, array('hide_empty' => false) );
							if( !empty($pods_cpt_terms) ){
								$terms = wp_get_object_terms($post_id, $pods_cpt_taxonomy);
								$category_data = array();
							    $tags_data = array();
							    foreach($terms as $term)
							    {
							        $category_data[] = $term->slug;
							         $tags_data[] = $term->name;

							    }
							    //echo implode(', ', $tags_data);
								echo '<li class="taxonomy_feilds">';
									echo '<ul>';
										echo '<div class="pods-field-label">
											<label class="pods-form-ui-label" for="">'.trim($pods_taxonomy_info[$pods_cpt_taxonomy]->label).'</label>
										</div>';
										echo '<div class="pods-field-input">';
											if( $pods_cpt_terms[0]->taxonomy != 'post_tag' ){
												echo '<div class="pods-pick-values pods-pick-checkbox">';
													$post_terms = wp_get_post_terms($post_id, $pods_cpt_terms[0]->taxonomy);
													$category_data = array();
												    foreach($post_terms as $term)
												    {
												        $category_data[] = $term->slug;
												    }
													foreach ($pods_cpt_terms as $key => $pods_cpt_term) {
														//echo $pods_cpt_terms[0]->taxonomy.' -- '.$post_id;
														
														if( !in_array($pods_cpt_term->term_id, $cpt_restrict_cats) ){
															if( !empty($pods_cpt_term) ){
																$checked = in_array($pods_cpt_term->slug, $category_data) ? 'checked' : '';
																echo '<li class="pods-field"><div class="pods-field pods-boolean">';
																	echo '<input name="pods_cpt_categories[]" data-name-clean=""  id="pods_cpt_categories" class="pods_cpt_categories" type="checkbox" value="'.trim($pods_cpt_term->slug).'" '.$checked.'>';
																	echo '<label class="pods-form-ui-label">'.trim($pods_cpt_term->name).'</label>';
																echo '</div></li>';
															}
														}
													}
												echo '</div>';
											}else{ 														
													$tag_args = array(
												    'taxonomy'   => 'post_tag',
												    'hide_empty' => 0,
												    'post_type' => $pods_cpt_taxonomy,
												);
												$posts = get_terms( $tag_args );
														if( $posts ) :
															foreach( $posts as $k => $post ) {
																$source[$k]['ID'] = $post->slug;
																$source[$k]['label'] = $post->name; // The name of the post
															}
													?>
													
													<?php
													endif;?>	
																										
												<input id="post_tag_autocomplete" name="post_tag_autocomplete" type="text" tabindex="2" autocomplete="on" value="<?php echo implode(', ', $tags_data); ?>" placeholder="typing name..." />
											<?php }
										echo '</div>';
									echo '</ul>';
								echo '</li>';
							}
						}					 
					}
					foreach ($pods_options as $slug => $fields_info) {
						$field_prefix = '';				
						if( isset($fields_info['options']['restrict_role']) && ( $fields_info['options']['restrict_role'] == '1' ) ){
							if( in_array( $current_user->roles[0], $fields_info['options']['roles_allowed']) ){
									?><li class="pods-field <?php echo esc_attr( 'pods-form-ui-row-type-' . $fields_info[ 'type' ] . ' pods-form-ui-row-name-' . PodsForm::clean( $fields_info[ 'name' ], true ) ); ?>">
								<?php	if($fields_info['pick_object'] != 'taxonomy'){
										echo '<div class="pods-field-label">';
											echo PodsForm::label( $field_prefix . $fields_info[ 'name' ], $fields_info[ 'label' ], $fields_info[ 'help' ], $fields_info );
										echo '</div>';
									}
									echo '<div class="pods-field-input">';	
										echo PodsForm::field( $field_prefix . $fields_info[ 'name' ], $pod->field( array( 'name' => $fields_info[ 'name' ], 'in_form' => true ) ), $fields_info[ 'type' ], $fields_info, $pod, $pod->id() );									
										echo PodsForm::comment( $field_prefix . $fields_info[ 'name' ], null, $fields_info );
									echo '</div>';
								echo '</li>';
							}
						}
						else{	
						//echo '<li class="pods-field pods-form-ui-row-type-text pods-form-ui-row-name-'.str_replace('_', '-', $fields_info['name']).'">';
							?><li class="pods-field <?php echo esc_attr( 'pods-form-ui-row-type-' . $fields_info[ 'type' ] . ' pods-form-ui-row-name-' . PodsForm::clean( $fields_info[ 'name' ], true ) ); ?>">
							<?php	if($fields_info['pick_object'] != 'taxonomy'){
									echo '<div class="pods-field-label">';
										echo PodsForm::label( $field_prefix . $fields_info[ 'name' ], $fields_info[ 'label' ], $fields_info[ 'help' ], $fields_info );
									echo '</div>';
								}
								echo '<div class="pods-field-input">';	
									echo PodsForm::field( $field_prefix . $fields_info[ 'name' ], $pod->field( array( 'name' => $fields_info[ 'name' ], 'in_form' => true ) ), $fields_info[ 'type' ], $fields_info, $pod, $pod->id() );									
									echo PodsForm::comment( $field_prefix . $fields_info[ 'name' ], null, $fields_info );
								echo '</div>';
							echo '</li>';
						}
					}
				echo '</ul>';
			echo '</div>';	
			$talent_post_id = $_GET['id'];
			$profile_link = get_permalink($talent_post_id);
			?>
			<ul id="submit_feilds_container" class="pods-form-fields">
				<li>
					<div class="pods-field-label"><label class="pods-form-ui-label pods-form-ui-label-pods-field-post-title" for="pods-form-ui-pods-field-post-title"></label></div>
					<div class="pods-field-input submit_feilds">							
					<input id="pods-cpt-submit-button" class="" type="submit" value="<?php echo !empty($submit_button_text) ? $submit_button_text : __('Save Changes', 'ppd'); ?>" />
					<a href="<?php echo $profile_link; ?>"><input  type="button" tabindex="4" class="view_profile submit button kaya-button" value="View Changes"></a>
					</div>
				</li>
			</ul>
	    	<input type="hidden" name="post_action" value="user_edit_pods_cpt_profile" />
	    	<input type="hidden" name="empty-description" id="empty-description" value="1"/>
	    	<?php wp_nonce_field( 'user-edit-pods-cpt-page' );
		echo '</form>'; // Form End
		}else{
			echo "<p class='kaya-success-message'>You are not allowed to edit other user's profile</p>";
		}
	}
	// Get Current User post limit data
	function pods_cpt_post_list($post_type){
		global $current_user, $current_user_data;
		$settings = $this->get_settings();
		$current_user_data =  wp_get_current_user($current_user->ID);
		$edit_post = isset( $current_user_data->allcaps['publish_'.$post_type.'s'] ) ? 'true' : 'false';
		$edit_published = isset( $current_user_data->allcaps['edit_published_'.$post_type.'s'] ) ? 'true' : 'false';
		$delete_published = isset( $current_user_data->allcaps['delete_published_'.$post_type.'s'] ) ? 'true' : 'false';
		echo '<table class="kaya-table list-table-content-wrapper">';
				echo '<tr>';
					echo '<th class="post_title">'.__('Title','ppd').'</th>';
					echo '<th class="post_category">'.__('Category','ppd').'</th>';
					echo '<th class="post_date">'.__('Date','ppd').'</th>';
					echo '<th class="post_thumb">'.__('Post Image','ppd').'</th>';
					echo '<th class="post_action">'.__('Action','ppd').'</th>';
					echo '<th class="post_status">'.__('Status','ppd').'</th>';
				echo '</tr>';
				if ( is_user_logged_in() ){
					global $current_user,$talents_plugin_name,$post,$wp_query, $talent_msg_note;
					wp_get_current_user();
					$taxonomy = get_object_taxonomies( $post_type);
					$author_query = array('post_type' => $post_type, 'posts_per_page' => '-1', 'author' => $current_user->ID, 'post_status' => array('publish', 'pending', 'draft', 'future', 'private'));
					$author_posts = new WP_Query($author_query);
					if($author_posts->have_posts()) : while($author_posts->have_posts()) : $author_posts->the_post();
						echo '<tr>';
							$id = get_the_ID();
							echo '<td class="post_title">';
								echo '<a href="'.get_permalink( $id ).'">'.get_the_title().'</a>';
							echo '</td>';
							echo '<td class="post_category">';
								if( !empty($taxonomy[0]) ){
									$terms = get_the_terms($post->ID, $taxonomy[0]);
									$post_terms =array();
									if ( !empty($terms) ) {
										foreach ( $terms as $term ){
											$post_terms[] = esc_html(sanitize_term_field('', $term->name, $term->term_id, '', 'edit'));
										}
										echo implode( ', ', $post_terms );
									} else {
										echo '<em>'.__('No terms', 'ppd').'</em>';
									}
								}else{
									echo '<em>'.__('No terms', 'ppd').'</em>';
								}
							echo '</td>';
							echo '<td class="date">';
								echo  get_the_date( get_option('date_format') );
							echo '</td>';
							echo '<td class="post_thumb">';
								$img_url = wp_get_attachment_url( get_post_thumbnail_id() );
								if($img_url){
									echo the_post_thumbnail(array(75,75)); 
								}else{
									echo '<img src="'.KAYA_PCV_PLUGIN_URL.'/images/front_end_thumb.jpg" height="75" width="75" />';
								}
							echo '</td>';
							echo '<td class="form_actions">';
								if ( ((($edit_published == 'false') && ( get_post_status( $id ) !='publish' )) || (($edit_published == 'true') && ( get_post_status( $id ) =='publish' )) ||  (($edit_published == 'true') && ( get_post_status( $id ) !='publish' ))) || ( (($delete_published == 'false') && ( get_post_status( $id ) !='publish' )) || (($delete_published == 'true') && ( get_post_status( $id ) =='publish' )) || (($delete_published == 'true') && ( get_post_status( $id ) !='publish' ))  ) ) {

							if ((($edit_published == 'false') && ( get_post_status( $id ) !='publish' )) || (($edit_published == 'true') && ( get_post_status( $id ) =='publish' )) ||  (($edit_published == 'true') && ( get_post_status( $id ) !='publish' )) ) {
								echo '<a href="'.kaya_get_current_page().'?action=edit&id='.$id.'"><i class="fa fa-pencil"></i></a>';
							}

							if( ((($edit_published == 'false') && ( get_post_status( $id ) !='publish' )) || (($edit_published == 'true') && ( get_post_status( $id ) =='publish' )) ||  (($edit_published == 'true') && ( get_post_status( $id ) !='publish' ))) && ( (($delete_published == 'false') && ( get_post_status( $id ) !='publish' )) || ( ($delete_published == 'true') && ( get_post_status( $id ) =='publish' )) || (($delete_published == 'true') && ( get_post_status( $id ) !='publish' ))  ) ){
								echo ' | ';
							}

							if ( (($delete_published == 'false') && ( get_post_status( $id ) !='publish' )) || (($delete_published == 'true') && ( get_post_status( $id ) =='publish' )) || (($delete_published == 'true') && ( get_post_status( $id ) !='publish' )) ) {
								echo '<a href="'.kaya_get_current_page().'?action=delete&id='.$id.'"><i class="fa fa-trash-o"></i></a>';
							}
						}else{
							echo '-';
						}	
							echo '</td>';
							echo '<td class="status">';
								if(get_post_status( get_the_ID() ) == 'publish'){
									echo '<p title="'.__('Published','ppd').'"><i class="fa fa-eye" style="color:green;"></i></p>';
								}else{
									echo '<p title="'.__('Pending', 'ppd').'"><i class="fa fa-eye-slash" ></i></p>';
								}
							echo '</td>';
						echo '</tr>';   
					endwhile;
					wp_reset_postdata();
					wp_reset_query();
					else :
						echo '<tr><td colspan="8">'.$talent_msg_note.'</td></tr>';
					endif;
				}else{
					echo '<p>'.__('Nothing found','ppd').'</p>';
				}?>
			</table>
	<?php }
	// Current user post limit count
	public function kaya_pods_cpt_post_count($userid, $post_type) {
	    global $wpdb;
	   	$pstatus = "IN ('publish', 'pending', 'draft', 'future', 'private')";
	    $count = $wpdb->get_var("SELECT COUNT(ID) FROM $wpdb->posts WHERE post_status ". $pstatus ." AND post_author = $userid AND post_type ='".$post_type."'");
	    return $count;
	}
	public function form($instance){
		$instance = wp_parse_args($instance,array(
			'post_type' => '',
			'disable_pod_cpt_fields' => '',
			'thank_you' =>'',
			'submit_button_text' => __('Save Changes', 'ppd'),
			'post_delete_msg' => __('Your post has been deleted', 'ppd'),
			'form_update_message' => __( 'Form submitted successfully', 'ppd' ),
			'post_limit_exceeded_msg' => __("You Don't Have Permissions to create post", 'ppd'),
			'non_user_role_form_access_msg' => __("You Don't Have Permissions to create post", 'ppd'),
			'add_new_button_text' => __('Add New', 'ppd'),
			'user_prof_talent_text_msg' => __('No Talent Post Has Been Created, To Create Click "Add New" Button And Fill The Form', 'ppd'),
			'multiple_cpt_form_title' => __('Enter Title', 'ppd'),
			'featured_img_text' => __('Featured Image', 'ppd'),
		));
		?>
		<script>
			jQuery(document).ready(function($) {
				$("#<?php echo $this->get_field_id('post_type'); ?>").change(function(){
					$('.cpt_post_taxonomy').hide();
					$('.cpt_options_fields').hide();
					var $cpt_name = $(this).find('option:selected').val();
					$('.post_type_'+$cpt_name).show();
					
				}).change();

				$("#<?php echo $this->get_field_id('thumbnail_sizes'); ?>").change(function(){
					var $thumbnails_type = $(this).find('option:selected').val();
					$(".<?php echo $this->get_field_id('custom_image_size_w') ?>").hide();
					if( $thumbnails_type == 'custom_thumb_sizes' ){
						$(".<?php echo $this->get_field_id('custom_image_size_w') ?>").show();
					}
					
				}).change();

			});
		</script>
		<?php
		$cpt_types = kaya_get_pod_cpt_fields();		
		// Getting CPT's Names
		echo '<p>';
			echo '<label>'.__('CPT Post Types','ppd').'</label>';
			echo '<select class="widefat" id="'.$this->get_field_id('post_type') .'" name="'.$this->get_field_name('post_type').'">';
			foreach ($cpt_types as $key => $options) {
				if( $options['type'] == 'post_type' ){
					echo '<option value="'.$options['name'].'" '.selected($options['name'], $instance['post_type']).' >'.trim(ucfirst($options['label'])).'</option>';
				}
			}
			echo '</select>';
		echo '</p>'; ?>
		<?php
		echo '<p>';
			echo "<label for=".$this->get_field_id('add_new_button_text').">"._e(' Add New Button Text', 'ppd')."</label>";
			echo '<input type="text" class="widefat" id="'.$this->get_field_id('add_new_button_text').'"  value="'.$instance['add_new_button_text'].'" name="'.$this->get_field_name('add_new_button_text').'" />';
		echo '</p>';

		echo '<p>';
			echo "<label for=".$this->get_field_id('user_prof_talent_text_msg').">"._e('User Profile Talent Note', 'ppd')."</label>";
			echo '<textarea class="widefat" placeholder="No Talent Post Has Been Created, To Create Click Add New Button And Fill The Form" id="'.$this->get_field_id('user_prof_talent_text_msg').'" name="'.$this->get_field_name('user_prof_talent_text_msg').'" >'.$instance['user_prof_talent_text_msg'].'</textarea>';
		echo '</p>';

		echo '<p>';
            echo "<label for=".$this->get_field_id('multiple_cpt_form_title').">"._e('Enter Post Title (required) Text', 'ppd')."</label>";
            echo '<input type="text" class="widefat" id="'.$this->get_field_id('multiple_cpt_form_title').'" placeholder="'.__('Enter Talent Name', 'ppd').'" value="'.$instance["multiple_cpt_form_title"].'" name="'.$this->get_field_name('multiple_cpt_form_title').'" />';
        echo '</p>';
        echo '<p>';
            echo "<label for=".$this->get_field_id('featured_img_text').">"._e('Featured Image (required) Text', 'ppd')."</label>";
            echo '<input type="text" class="widefat" id="'.$this->get_field_id('featured_img_text').'" placeholder="'.__('Featured Image', 'ppd').'" value="'.$instance["featured_img_text"].'" name="'.$this->get_field_name('featured_img_text').'" />';
        echo '</p>';  
		echo '<p>';
			echo "<label for=".$this->get_field_id('thank_you').">"._e('Form Submit Button Text', 'ppd')."</label>";
			echo '<input type="text" class="widefat" id="'.$this->get_field_id('submit_button_text').'"  value="'.$instance['submit_button_text'].'" name="'.$this->get_field_name('submit_button_text').'" />';
		echo '</p>';
		echo '<p>';
			echo "<label for=".$this->get_field_id('post_delete_msg').">"._e('Post Delete Message', 'ppd')."</label>";
			echo '<textarea class="widefat" id="'.$this->get_field_id('post_delete_msg').'"  name="'.$this->get_field_name('post_delete_msg').'" >'.$instance['post_delete_msg'].'</textarea>';
		echo '</p>';

		echo '<p>';
			echo "<label for=".$this->get_field_id('form_update_message').">"._e('Form Button Message', 'ppd')."</label>";
			echo '<textarea class="widefat" id="'.$this->get_field_id('form_update_message').'"  name="'.$this->get_field_name('form_update_message').'" >'.$instance['form_update_message'].'</textarea>';
		echo '</p>';
		echo '<p>';
			echo "<label for=".$this->get_field_id('non_user_role_form_access_msg').">"._e('Non User Role Form Accessing Message', 'ppd')."</label>";
			echo '<textarea class="widefat" id="'.$this->get_field_id('non_user_role_form_access_msg').'"  name="'.$this->get_field_name('non_user_role_form_access_msg').'" >'.$instance['non_user_role_form_access_msg'].'</textarea>';
		echo '</p>';             
	}
	
}
   // Options data object
   new Kaya_View_Edit_Cpt_Form_Widget;

   function Kaya_View_Edit_Cpt_Form_Widget(){
      global $kaya_pods_options_data;
      $kaya_pods_options_data = get_option('kaya_pods_options_data');
   }
add_action('widgets_init', function(){
	register_widget( 'Kaya_View_Edit_Cpt_Form_Widget' );
});

?>