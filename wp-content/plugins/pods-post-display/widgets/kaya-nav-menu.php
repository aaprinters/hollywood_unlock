<?php
namespace KayaWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Border;
use Elementor\Utils;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly
class Kaya_Nav_Menu_Widget extends Widget_Base {

    public function get_name() {
        return 'kaya-nav-menu';
    }

    public function get_title() {
        return __('Kaya - Nav Menu', 'ppd');
    }

    public function get_icon() {
        return 'eicon-nav-menu';
    }

    public function get_categories() {
        return array('nav-menu-icons');
    }

    protected function _register_controls() {

        $this-> start_controls_section(
            'menu_section',
            [
                'label' => __('Menu Navigation', 'ppd'),
            ]
        );

        $this->add_control(
            'menu_selection',
            [
                'label' => __('Select Menu Type', 'ppd'),
                'type'  => Controls_Manager::SELECT,
                'options' => kaya_nav_menu_selction(),
                'default' => '',
            ]
        );

        $this->add_control(
            'menubar_location',
            [
                'label'       => __( 'Menu Location', 'ppd' ),
                'description' => __( 'Select a location for your menu. This option facilitate the ability to create up to 2 mobile enabled menu locations', 'ppd' ),
                'type'        => Controls_Manager::SELECT, 'options' => [
                    'primary'   => __( 'Primary', 'ppd' ),
                    'secondary' => __( 'Secondary', 'ppd' ),
                ],
                'default'     => 'primary',
            ]
        );

        $this->add_responsive_control(
            'navbar_align',
            [
                'label' => __( 'Navbar Alignment', 'ppd' ),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __( 'Left', 'ppd' ),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'ppd' ),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'ppd' ),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} ul.top-nav' => 'text-align: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'menu_font_settings',
            [
                'label' => __('Menu Font Settings','ppd'),
                'tab'   => Controls_Manager::TAB_SETTINGS,
            ]
        );

        $this->add_control(
            'menu_font_size',
            [
                'label' => __( 'Menu Font Size', 'ppd' ),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 16,
                ],
                'range' => [
                    'px' => [
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .top-nav li a' => 'font-size: {{SIZE}}{{UNIT}}!important;',
                ],
            ]
        );

        $this->add_control(
            'menu_font_weight',
            [
                'label' => __( 'Menu Font Weight', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'bold' => __( 'Bold', 'ppd' ),
                ],
                'default' => 'normal',
                'selectors' => [
                    '{{WRAPPER}} .top-nav li a' => 'font-weight:{{VALUE}};',
                ],
            ]
        );

         $this->add_control(
            'menu_font_style',
            [
                'label' => __( 'Menu Font Style', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'italic' => __( 'Italic', 'ppd' ),
                ],
                'default' => 'normal',
                'selectors' => [
                    '{{WRAPPER}} .top-nav li a' => 'font-style: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'child_menu_font_size',
            [
                'label' => __( 'Child Menu Font Size', 'ppd' ),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 16,
                ],
                'range' => [
                    'px' => [
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} ul.sub-menu li a' => 'font-size: {{SIZE}}{{UNIT}}!important;',
                ],
            ]
        );

        $this->add_control(
            'child_menu_font_weight',
            [
                'label' => __( 'Child Menu Font Weight', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'bold' => __( 'Bold', 'ppd' ),
                ],
                'default' => 'normal',
                'selectors' => [
                    '{{WRAPPER}} ul.sub-menu li a' => 'font-weight:{{VALUE}}!important;',
                ],
            ]
        );

         $this->add_control(
            'child_menu_font_style',
            [
                'label' => __( 'Child Menu Font Style', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'italic' => __( 'Italic', 'ppd' ),
                ],
                'default' => 'normal',
                'selectors' => [
                    '{{WRAPPER}} ul.sub-menu li a' => 'font-style: {{VALUE}}!important;',
                ],
            ]
        );

        $this-> end_controls_section();
        $this->start_controls_section(
            'menu_styles',
            [
                'label' => __('Menu Color Settings','ppd'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'menu_link_color',
            [
                'label' => __('Menu Link Color', 'ppd'),
                'type'  => Controls_Manager::COLOR,
                'default' => '#000',
                'selectors' => [
                    '{{WRAPPER}} #header-navigation ul.top-nav li a' => 'color:{{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'child_menu_background_color',
            [
                'label' => __('Child Menu BG Color', 'ppd'),
                'type'  => Controls_Manager::COLOR,
                'default' => '',
                'selectors' => [
                    '{{WRAPPER}} .top-nav ul.sub-menu li a' => 'background-color:{{VALUE}};',
                ],
            ]
        );
        
        $this->add_control(
            'child_menu_link_color',
            [
                'label' => __('Child Menu links Color', 'ppd'),
                'type'  => Controls_Manager::COLOR,
                'default' => '',
                'selectors' => [
                    '{{WRAPPER}} .top-nav ul.sub-menu li a' => 'color:{{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'child_menu_border_bottom_color',
            [
                'label' => __('Child Menu Border Bottom Color', 'ppd'),
                'type'  => Controls_Manager::COLOR,
                'default' => '',
                'selectors' => [
                    '{{WRAPPER}} .top-nav ul.sub-menu li a' => 'border-bottom:1px solid {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name'     => 'menu_border',
                'label'    => __( 'Border', 'ppd' ),
                'default'  => '1px',
                'selector' => '{{WRAPPER}} .top-nav li, .menu-item a',
            ]
        );

        $this->add_control(
            'menu_border_radius',
            [
                'label'      => __( 'Border Radius', 'ppd' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors'  => [
                    '{{WRAPPER}} .top-nav li, .menu-item a' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this-> end_controls_section();

        $this-> start_controls_section(
            'menu_active_color_settings',
            [
                'label' => __('Menu Active Color Settings', 'ppd'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'menu_link_active_BG_color',
            [
                'label' => __('Menu Link Active BG Color', 'ppd'),
                'type'  => Controls_Manager::COLOR,
                'default' => '#fff',
                'selectors' => [
                    '{{WRAPPER}} #header-navigation ul li.current-menu-item.current_page_item a' => 'background:{{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'menu_link_active_color',
            [
                'label' => __('Menu Link Active Color', 'ppd'),
                'type'  => Controls_Manager::COLOR,
                'default' => '',
                'selectors' => [
                    '{{WRAPPER}} #header-navigation ul li.current-menu-item.current_page_item a' => 'color:{{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'child_menu_active_background_color',
            [
                'label' => __('Child Menu Active BG Color', 'ppd'),
                'type'  => Controls_Manager::COLOR,
                'default' => '',
                'selectors' => [
                    '{{WRAPPER}} .sub-menu li.current-menu-item a' => 'background:{{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'child_menu_active_link_color',
            [
                'label' => __('Child Menu Active Link Color', 'ppd'),
                'type'  => Controls_Manager::COLOR,
                'default' => '',
                'selectors' => [
                    '{{WRAPPER}} .sub-menu li.current-menu-item a' => 'color:{{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_section();

        $this-> start_controls_section(
            'menu_hover_color_settings',
            [
                'label' => __('Menu Hover Color Settings', 'ppd'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'menu_links_hover_color',
            [
                'label' => __('Menu Links Hover Color', 'ppd'),
                'type'  => Controls_Manager::COLOR,
                'default' => '#000',
                'selectors' => [
                    '{{WRAPPER}} #header-navigation ul.top-nav li a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'menu_links_hover_BG_color',
            [
                'label' => __('Menu Links Hover BG Color', 'ppd'),
                'type'  => Controls_Manager::COLOR,
                'default' => '#000',
                'selectors' => [
                    '{{WRAPPER}} #header-navigation ul.top-nav li a:hover' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'child_menu_hover_BG_color',
            [
                'label' => __('Child Menu Hover BG Color', 'ppd'),
                'type'  => Controls_Manager::COLOR,
                'default' => '#000',
                'selectors' => [
                    '{{WRAPPER}} .top-nav ul.sub-menu li a:hover' => 'background-color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'child_menu_link_hover_color',
            [
                'label' => __('Child Menu Links Hover Color', 'ppd'),
                'type'  => Controls_Manager::COLOR,
                'default' => '#fff',
                'selectors' => [
                    '{{WRAPPER}} .top-nav ul.sub-menu li a:hover' => 'color: {{VALUE}}!important;',
                ],
            ]
        );
        $this->end_controls_section();
    }
    protected function render() {
        $settings      = $this->get_settings();
        $menu_location = $settings['menubar_location'];
        // Get menu
        $nav_menubar = ! empty( $settings['menu_selection'] ) ? wp_get_nav_menu_object( $settings['menu_selection'] ) : false;
        if ( ! $nav_menubar ) {
            return;
        }
        $nav_menu_args = array(
            'fallback_cb'    => false,
            'container'      => false,
            'menu_id'        => 'main-menu',
            'menu_class'     => 'top-nav',
            'theme_location' => 'kaya_nav_menu_widget', // creating a fake location for better functional control
            'menu'           => $nav_menubar,
            'echo'           => true,
            'depth'          => 0,
            'walker'         => '',
        );
        ?>

        <script>
        /**
            * Jquery Smart Menu 
        */
        $('#main-menu').smartmenus({
            subMenusSubOffsetX: 1,
            subMenusSubOffsetY: -8
          });

           var $mainMenuState = $('#main-menu-state');

          if ($mainMenuState.length) {
          // animate mobile menu
          $mainMenuState.change(function(e) {
            var $menu = $('#header-navigation');
            if (this.checked) {
              $menu.hide().slideDown(250).removeClass('add-mobile-menu').addClass('hide-mobile-menu');
            } else {
              $menu.show().slideUp(250).addClass('add-mobile-menu').removeClass('hide-mobile-menu');
            }
          });
          // Advance Search Panel
 
               $('.toggle_search_icon').click(function(){
                $('.toggle_search_wrapper').css({'display':'block'}).animate({  'opacity':'1','right':'0%'}, 400, 'swing', function(){
                });
              });
              $('.toggle_search_wrapper').each(function(){
                $(this).find('span.search_close').click(function(){
                   $(this).parent().parent('.toggle_search_wrapper').css({'display':'none'}).animate({'opacity':'0','right':'0%'}, 400, 'swing', function(){           
                    });
                });
              });

          // hide mobile menu beforeunload
          $(window).bind('beforeunload unload', function() {
            if ($mainMenuState[0].checked) {
              $mainMenuState[0].click();
            }
          });
          }
        </script>

        <?php echo '<div id="header-navbar-wrapper" class="header-navbar-section-'.$menu_location.'">'?>
        <nav id="header-navigation"> <!-- Header Navigation -->       
               <?php      
                if ( has_nav_menu('primary') ) { 
                    wp_nav_menu(apply_filters('widget_nav_menu_args',$nav_menu_args),array('container_id' => 'main-nav','menu_id'=> 'main-menu', 'container_class' => 'menu','theme_location' => 'primary', 'menu_class'=> 'top-nav'));
                }else{
                    wp_nav_menu(array('container_id' => 'main-nav', 'container'=> 'ul', 'menu_id'=> 'main-nav', 'container_class' => 'menu', 'menu_class'=> 'top-nav'));
                }       
                ?>
            <div class="mobile_toggle_menu_icons">
            <input id="main-menu-state" type="checkbox" />
                <label class="main-menu-btn" for="main-menu-state">
                    <span class="main-menu-btn-icon"></span>
                </label>
            </div>   
    </nav><!-- End navigation -->
</div>

    <?php }
    protected function content_template() {
    }

}