<?php
namespace KayaWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Image_Size;
use Elementor\Utils;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

class Kaya_Taxonomies_Search_Widget extends Widget_Base {

    public function get_name() {
        return 'pods-taxonomies-search';
    }
    public function get_title() {
        return __('Pods - Search Filter', 'ppd');
    }
    public function get_icon() {
        return 'eicon-posts-grid';
    }
    protected function _register_controls() {

        $this->start_controls_section(
            'section_query',
            [
                'label' => __('Post Query', 'ppd'),
            ]
        );

        $this->add_control(
            'cpt_type',
            [
                'label' => __('Choose CPT Type', 'ppd'),
                'type' => Controls_Manager::SELECT,
                'options' => kaya_get_all_cpt_types(),
                'label_block' => true,
                'default' => 'talent',
            ]
        );

        $this->add_control(
            'tabs',
            [
                'label' => __( 'Tabs Items', 'ppd' ),
                'type' => Controls_Manager::REPEATER,
                'default' => [
                    [
                        'tab_title' => __( 'Models', 'ppd' ),
                        'tab_content' => __( 'I am tab content. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'ppd' ),
                    ],
                    [
                        'tab_title' => __( 'Actors', 'ppd' ),
                        'tab_content' => __( 'I am tab content. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'ppd' ),
                    ],
                ],
                'fields' => [
                    [
                        'name' => 'tab_title',
                        'label' => __( 'Title & Content', 'ppd' ),
                        'type' => Controls_Manager::TEXT,
                        'default' => __( 'Title', 'ppd' ),
                        'placeholder' => __( 'Title', 'ppd' ),
                        'label_block' => true,
                    ],
                    [
                        'label' => __('Choose Taxonomy', 'ppd'),
                        'name' => 'user_role_type',
                        'type' => Controls_Manager::SELECT,
                        'default' => 'subscriber',
                        'options' => kaya_cpt_all_taxonomies(),
                        'multiple' => false,
                        'label_block' => true
                    ],
                    /*[
                        'label' => __('Choose Category', 'ppd'),
                        'name' => "tax_cat",
                        'type' => Controls_Manager::SELECT2,
                        'multiple' => true,
                        'options' => kaya_cpt_all_taxonomies_terms(),
                        'label_block' => true,

                    ],*/
                    [
                        'label' => __('Display On Search', 'ppd'),
                        'name' => 'user_meta_fields',
                        'type' => Controls_Manager::SELECT2,
                        'options' => kaya_get_cpt_all_metakeys(),
                        'multiple' => true,
                        'label_block' => true,
                        'description' => '<strong style="color:#ff0000">'.__('Note', 'ppd').': </strong>'.__('Should not use more then 7 fields','ppd')

                    ],
                    [
                        'label' => __('Search Range Fields', 'ppd'),
                        'name' => 'user_meta_search_range_fields',
                        'type' => Controls_Manager::SELECT2,
                        'options' => kaya_cpt_post_range_all_metakeys(),
                        'multiple' => true,
                        'label_block' => true
                    ]
                ],
                'title_field' => '{{{ tab_title }}}',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'section_settings',
            [
                'label' => __('Settings', 'ppd'),
                'tab' => Controls_Manager::TAB_SETTINGS,
            ]
        );
        $this->add_control(
            'search_button_text',
            [
                'label' => __('Button Text', 'ppd'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Search', 'ppd'),
                'label_block' => true
            ]
        );
        $this->add_control(
            'search_result_text',
            [
                'label' => __('Search Result Text', 'ppd'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Search Results :','ppd'),
                'label_block' => true
            ]
        );
        $this->add_control(
            'search_nothing_found_msg',
            [
                'label' => __( 'Search Nothing Found Message', 'ppd' ),
                'type' => Controls_Manager::TEXTAREA,
                'default' => __('<h4>No profiles found </h4><p>Try removing some filters to broaden your search, or check your spelling if you have used a keyword</p>','ppd'),
            ]
        );

        $this->add_control(
            'enable_ajax_search',
            [
                'label' => __( 'Enable Ajax Search', 'ppd' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'ajax_search_result_div',
            [
                'label' => __( 'Ajax Search Result Class', 'ppd' ),
                'type' => Controls_Manager::TEXT,
                 'default' => 'ajax-search-results',
                 'description' => '<strong style="color:#ff0000">'.__('Note', 'ppd').': </strong>'.__('<span style="color:#000">Grid column and post limit can be controlled from "Pods Post Display > Taxonomy > Taxonomy Columns"</span>','ppd'),
                 'condition' => [
                    'enable_ajax_search' => 'yes',
                ],
            ]
        );
        
        $this->add_control(
            'age_min_range',
            [
                'label' => __( 'Age Minimum Range', 'ppd' ),
                'type' => Controls_Manager::NUMBER,
                'default' => '1',
            ]
        );
        $this->add_control(
            'age_max_range',
            [
                'label' => __( 'Age Maximum Range', 'ppd' ),
                'type' => Controls_Manager::NUMBER,
                'default' => '80',
            ]
        );
        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings();
        $post_type = $settings['cpt_type'];
        $pods = pods( $post_type );
        $tabs = $this->get_settings( 'tabs' );
        $id_int = substr( $this->get_id_int(), 0, 3 );
        echo '<div class="advanced_search_wrapper">';
        if( count($tabs) > 1 ){
           // echo '<label><b>'.( !empty($settings['choose_cat_text_change']) ? $settings['choose_cat_text_change'] : __('Select Category', 'ppd')).'</b></label>';
            echo '<select id="search-select-wrapper" class="search-select-wrapper'.$id_int.'">';
                foreach ( $tabs as $index => $item ) :
                    $tab_count = $index + 1;
                    $tab_title_setting_key = $this->get_repeater_setting_key( 'tab_title', 'tabs', $index );
                    $this->add_render_attribute( $tab_title_setting_key, [
                        'id' => 'elementor-tab-title-' . $id_int . $tab_count,
                        'class' => [ 'elementor-tab-title', 'elementor-tab-desktop-title' ],
                        'data-tab' => $tab_count,
                        'tabindex' => $id_int . $tab_count,
                        'role' => 'tab',
                        'aria-controls' => 'elementor-tab-content-' . $id_int . $tab_count,
                    ] );
                      echo '<option value="'.str_replace(array(' ', '/', '&', '-'), '_', strtolower($item['tab_title'])).''.$id_int.'">'.ucwords($item['tab_title']).'</option>'; ?>
                <?php endforeach;
            echo '</select>';
        }
        foreach ( $tabs as $index => $item ) :
            $tab_count = $index + 1;
            $tab_content_setting_key = $this->get_repeater_setting_key( 'user_role_type', 'tabs', $index );

            echo '<div class="advanced_search_form_wrapper taxonomy_advanced_search_form_wrapper advanced_search_form_wrapper'.$id_int.'" id="'.str_replace(array(' ', '/', '&', '-'), '_', strtolower($item['tab_title'])).''.$id_int.'">';
            if( class_exists('PodsField_Pick') ){
                     $pods_fields = new \PodsField_Pick;
                     $pods_countries = $pods_fields->data_countries();
                     $pods_us_states = $pods_fields->data_us_states();
                }else{
                    $pods_countries ='';
                    $pods_us_states = '';
                }
             global $error_message;
                echo '<form method="get" class="searchbox-wrapper pods-users-search-form s '.$item['user_role_type'].$tab_count.'" data-ajax="'.$settings['enable_ajax_search'].'" data-item="'.$item['user_role_type'].$tab_count.'" data-results="'.$settings['ajax_search_result_div'].'" action="'.home_url().'"  data-tab-count="'.$tab_count.'">';
                    //if( $settings['enable_ajax_search'] == 'Yes' ){ 
                        echo '<input type="hidden"  name="s"/>';
                    //}
                     echo '<input type="hidden" value="'.$settings['cpt_type'].'" name="cpt_type" />';
                     echo '<input type="hidden"  name="cpt_post_search" value="cpt_post_search"/>';
                       echo '<input type="hidden"  name="search_result" value="'.htmlentities($settings['search_result_text']).'"/>';
                       echo '<input type="hidden"  name="error_msg" value="'.htmlentities($settings['search_nothing_found_msg']).'"/>';
                        if( count($item['user_role_type']) > 1 ){
                            if( !empty($item['user_role_type']) ){
                                echo '<div class="search_fields checkbox_wrapper">';
                                    echo '<label>'.__('Choose Role', 'ppd').'</label>';
                                    echo '<select name="user_roles">';
                                    foreach ($item['user_role_type'] as $role_key => $role_name) {
                                        //echo '<label><input type="checkbox" name="user_roles[]" value="'.$role_name.'">'.ucwords($role_name).'</label>'; 
                                         echo '<option value="'.$role_name.'">'.ucwords($role_name).'</option>';   
                                    }
                                    echo '</select>';
                                echo '</div>';
                            }
                        }else{
                             echo '<label><input type="hidden" name="user_roles" value="'.$item['user_role_type'].'"></label>'; 
                        }
                         wp_enqueue_style( 'pods-select2' );
                        wp_enqueue_script( 'pods-select2' );
                       //print_r($item['tax_cat']);
                        ?>
                        <script>
                            jQuery(document).ready(function() {
                                jQuery("#<?php echo $item['user_role_type']; ?>_tax_terms_<?php echo $tab_count; ?>").select2();
                            });
                        </script>
                        <?php
                        if( !empty($item['tax_cat']) ){
                          /*echo '<div  class="search_fields">';
                                echo '<label>'.__('Choose Category', 'ppd').'</label>';
                               echo '<select name="'.$item['user_role_type'].'_tax_terms[]" id="'.$item['user_role_type'].'_tax_terms_'.$tab_count.'" multiple>';
                                foreach ($item['tax_cat'] as $role_key => $terms) {
                                     echo '<option value="'.$terms.'">'.ucwords(str_replace('-', ' ', $terms)).'</option>';   
                                }
                                echo '</select>';
                            echo '</div>';*/
                            echo '<input type="hidden" name="'.$item['user_role_type'].'_tax_terms" value="'.$item['tax_cat'].'" />';

                        }
                        $i = '0';
                        if( !empty($item['user_meta_fields']) ){
                            foreach ($item['user_meta_fields'] as $key => $cpt_fields) {                                       
                                        if( !empty($item['user_meta_search_range_fields']) ){                             
                                            foreach ($item['user_meta_search_range_fields'] as $key => $compare_data) {
                                                echo '<input type="hidden"  name="'.$compare_data.'_range" value="true">';
                                            }
                                        }
                                        $fields_info = $pods->fields($cpt_fields);
                                        $pods_cpt = !empty($fields_info['pod']) ? $fields_info['pod'] : '';
                                        //echo $fields_info['type'];
                                        if( @$fields_info['type'] == 'text' ){
                                            echo '<div class="search_fields search_field_'.$fields_info['name'].' '.$post_type.' '.$fields_info['pod'].' search_field'.$i.'">';
                                                echo '<label>'.$fields_info['label'].'</label>';
                                                echo '<input type="text" name="'.$fields_info['name'].'" />';
                                            echo '</div>';
                                        }elseif((@$fields_info['type'] == 'number' ) || ( @$fields_info['type'] == 'date' ) ){
                                            $slider_rand = rand(1,25000);
                                            if( (isset($fields_info['options']['number_format_type'])&&($fields_info['options']['number_format_type'] == 'slider' ) )|| ( $fields_info['name'] == 'age' )){
                                                    if( $fields_info['name'] == 'age' ){
                                                        $range_step = '1';
                                                        $range_min = $settings['age_min_range'];
                                                        $range_max = $settings['age_max_range'];
                                                    }else{
                                                        $range_step =  $fields_info['options']['number_step'];
                                                        $range_min =  $fields_info['options']['number_min'];
                                                        $range_max =  $fields_info['options']['number_max'];                                            
                                                    }
                                            ?>
                                                <div class="pods_ui_slider_range search_fields search_field_<?php echo $fields_info['name'] . ' ' .$post_type.' '.$fields_info['pod'].' search_field'.$i; ?>">
                                                    <script>
                                                         jQuery( function() {
                                                            jQuery( "#slider-range-<?php echo $fields_info['pod'].'_'.$fields_info['name']; ?>_<?php echo $slider_rand; ?>" ).slider({
                                                                range: true,
                                                                min:<?php echo $range_min; ?>,
                                                                max: <?php echo $range_max; ?>,
                                                                values: [ <?php echo  $range_min; ?>, <?php echo  $range_max; ?> ],
                                                                step:<?php echo  $range_step ?>,
                                                                slide: function( event, ui ) {
                                                                    var attname = jQuery(this).data('name');
                                                                    jQuery( "#"+attname+"-min" ).val(ui.values[ 0 ]);
                                                                    jQuery( "#"+attname+"-max" ).val(ui.values[ 1 ]);
                                                                    jQuery( "span."+attname+"-min" ).text(ui.values[ 0 ]);
                                                                    jQuery( "span."+attname+"-max" ).text(ui.values[ 1 ]);
                                                                }
                                                            });
                                                        } );
                                                    </script>
                                                        <?php echo '<label>'.$fields_info['label'].'</label>'; ?>
                                                        <input type="hidden" class="small-text" id="<?php echo $fields_info['pod'].'_'.$fields_info['name'].'_'.$item['user_role_type'].'-'.$tab_count; ?>-min" value="<?php echo $range_min; ?>"  name="<?php echo $item['user_role_type'].'-'.$fields_info['name'].'-'.$tab_count; ?>-min">
                                                        <input type="hidden" class="small-text" id="<?php echo $fields_info['pod'].'_'.$fields_info['name'].'_'.$item['user_role_type'].'-'.$tab_count; ?>-max" value="<?php echo $range_max; ?>" name="<?php echo $item['user_role_type'].'-'.$fields_info['name'].'-'.$tab_count; ?>-max" >
                                                        <span class="<?php echo $fields_info['pod'].'_'.$fields_info['name'].'_'.$item['user_role_type'].'-'.$tab_count; ?>-min label_min"> <?php echo $range_min; ?> </span>
                                                        <span class="<?php echo $fields_info['pod'].'_'.$fields_info['name'].'_'.$item['user_role_type'].'-'.$tab_count; ?>-max label_max"> <?php echo $range_max; ?> </span>                                               
                                                        <div id="slider-range-<?php echo $fields_info['pod'].'_'.$fields_info['name']; ?>_<?php echo $slider_rand; ?>" data-name="<?php echo $fields_info['pod'].'_'.$fields_info['name'].'_'.$item['user_role_type'].'-'.$tab_count; ?>"></div>
                                                    </div>  
                                                <?php                                   
                                            }
                                        }elseif(@$fields_info['type'] == 'pick'){
                                            if( $fields_info['options']['pick_format_type'] == 'single' ){
                                                if($fields_info['pick_object'] == 'us_state'){
                                                        echo '<div class="search_fields search_field_'.$fields_info['name'].' search_field'.$i.'">';
                                                            echo '<label>'.$fields_info['label'].'</label>';
                                                            echo '<select name="'.$fields_info['name'].'">';
                                                                echo '<option value="">--'.__('Select', 'ppd').'--</option>';
                                                                foreach ($pods_us_states as $key => $options) {
                                                                    if( !empty($options) ){
                                                                        echo '<option value="'.$key.'">'.$options.'</option>';
                                                                    }
                                                                }
                                                            echo '</select>';
                                                        echo '</div>';                      
                                                }else{
                                                    $select_range_field_name = $item['user_meta_search_range_fields'];
                                                    $custom_form_data = !empty($fields_info['options']['pick_custom']) ? $fields_info['options']['pick_custom'] : '';
                                                    $select_list_data = explode("\n", str_replace(array("\r\n","\n\r","\r"),"\n",$custom_form_data) );
                                                    if( !empty($select_range_field_name) ){
                                                        foreach ($select_range_field_name as $key => $compare_data) {
                                                            if( $compare_data == $fields_info['name'] ){
                                                                echo '<div  class="search_fields search_field_'.$fields_info['name'].' search_field'.$i.'">';
                                                                    echo '<label>'.$fields_info['label'].'</label>';
                                                                    echo '<select class="column2" name="'.$fields_info['name'].'-from">';
                                                                        echo '<option value="">--'.__('Select', 'ppd').'--</option>';
                                                                        foreach ($select_list_data as $key => $options) {
                                                                            if( !empty($options) ){
                                                                                echo '<option value="'.$options.'">'.$options.'</option>';
                                                                            }
                                                                        }
                                                                    echo '</select>';
                                                                    echo '<select class="column2" name="'.$fields_info['name'].'-to">';

                                                                        echo '<option value="">--'.__('Select', 'ppd').'--</option>';
                                                                        foreach ($select_list_data as $key => $options) {
                                                                            if( !empty($options) ){
                                                                                echo '<option value="'.$options.'">'.$options.'</option>';
                                                                            }
                                                                        }
                                                                    echo '</select>';
                                                                echo '</div>';
                                                            }
                                                        }
                                                    }else{
                                                        echo '<div  class="search_fields search_field_'.$fields_info['name'].' search_field'.$i.'">';
                                                            echo '<label>'.$fields_info['label'].'</label>';
                                                            echo '<select name="'.$fields_info['name'].'">';
                                                                echo '<option value="">--'.__('Select', 'ppd').'--</option>';
                                                                foreach ($select_list_data as $key => $options) {
                                                                    if( !empty($options) ){
                                                                        echo '<option value="'.$options.'">'.$options.'</option>';
                                                                    }
                                                                }
                                                            echo '</select>';
                                                        echo '</div>';  
                                                    }
                                                    if( !empty( $item['user_meta_search_range_fields']) ){
                                                        if(!in_array($fields_info['name'], $item['user_meta_search_range_fields'])){
                                                            if($fields_info['pick_object'] != 'ca_province'){
                                                                echo '<div  class="search_fields search_field_'.$fields_info['name'].' search_field'.$i.'">';
                                                                    echo '<label>'.$fields_info['label'].'</label>';
                                                                    echo '<select name="'.$fields_info['name'].'">';
                                                                        echo '<option value="">--'.__('Select', 'ppd').'--</option>';
                                                                        foreach ($select_list_data as $key => $options) {
                                                                            if( !empty($options) ){
                                                                                echo '<option value="'.$options.'">'.$options.'</option>';
                                                                            }
                                                                        }
                                                                    echo '</select>';
                                                                echo '</div>';
                                                            }
                                                        }
                                                    }
                                                }
                                            } // Multi Select
                                            elseif( $fields_info['options']['pick_format_type'] == 'multi' )
                                            {
                                                if( !empty($fields_info['options']['pick_custom']) ){
                                                    $select_list_data = explode("\n", str_replace(array("\r\n","\n\r","\r"),"\n",$fields_info['options']['pick_custom']) );
                                                    if( $fields_info['options']['pick_format_multi'] == 'autocomplete' ){
                                                        wp_enqueue_style( 'pods-select2' );
                                                        wp_enqueue_script( 'pods-select2' );
                                                         echo '<div class="autocomplete_wrapper search_fields search_field_'.$fields_info['name'].' search_field'.$i.'">';
                                                            echo '<label>'.$fields_info['label'].'</label>';
                                                            ?>
                                                                <script type="text/javascript">
                                                                    jQuery(document).ready(function() {
                                                                        jQuery("#<?php echo $fields_info['name']; ?>").select2();
                                                                    });
                                                                </script>
                                                            <?php
                                                            echo '<select  name="'.$fields_info['name'].'[]" id="'.$fields_info['name'].'" multiple="multiple">';
                                                                foreach ($select_list_data as $key => $options) {
                                                                    if( !empty($options) ){
                                                                        echo ' <option value="'.$options.'" >'.$options.'</option>';
                                                                    }
                                                                }
                                                            echo '</select>';
                                                        echo '</div>';   
                                                    }elseif( $fields_info['options']['pick_format_multi'] == 'multiselect' ){
                                                        echo '<div class="multiselectbox_wrapper search_fields search_field_'.$fields_info['name'].' search_field'.$i.'">';
                                                            echo '<label>'.$fields_info['label'].'</label>';
                                                            echo '<select  name="'.$fields_info['name'].'[]" id="'.$fields_info['name'].'" multiple="multiple">';
                                                                foreach ($select_list_data as $key => $options) {
                                                                    if( !empty($options) ){
                                                                        echo ' <option value="'.$options.'" >'.$options.'</option>';
                                                                    }
                                                                }
                                                            echo '</select>';
                                                        echo '</div>';  
                                                    }
                                                    else{
                                                    echo '<div class="checkbox_wrapper search_fields search_field_'.$fields_info['name'].' search_field'.$i.'">';
                                                        echo '<label>'.$fields_info['label'].'</label>';
                                                            foreach ($select_list_data as $key => $options) {
                                                                if( !empty($options) ){
                                                                    echo ' <label><input type="checkbox" name="'.$fields_info['name'].'[]" value="'.$options.'" />'.$options.'</label>';
                                                                }
                                                            }
                                                        //echo '</select>';
                                                    echo '</div>';
                                                    }
                                                }
                                            }
                                    $i++;   
                                    }
                             }   
                         }
                    echo '<input type="submit" value="'.( !empty($settings['search_button_text']) ? $settings['search_button_text'] : __('Search', 'ppd') ).'" name="submit" class="search_data_submit">';
                    
                //}
            echo '</form>';
            echo '<div class="clear"> </div>';
            //echo '<div class="search_ajax_response"> </div>';
            echo '</div>';
       endforeach;  
       echo '</div>'; 
        if( count($tabs) > 1 ){
           ?>
            <script>
                (function($){
                    $('.advanced_search_form_wrapper<?php echo $id_int; ?>').hide();
                    $('select.search-select-wrapper<?php echo $id_int; ?>').change(function(){
                        $('.advanced_search_form_wrapper<?php echo $id_int; ?>').hide();
                        $('#'+$(this).val()).show();
                    }).change();
                }(jQuery));
            </script>
        <?php
        }
    }
    protected function content_template() {
    }
}