<?php
namespace KayaWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Image_Size;
use Elementor\Utils;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly


class Kaya_Pods_Post_Grid_And_Table_View_Widget extends Widget_Base {

    public function get_name() {
        return 'kaya-pods-post-grid-and-table-view';
    }

    public function get_title() {
        return __('Kaya -Pods Post Grid & Table View', 'ppd');
    }

    public function get_icon() {
        return 'eicon-posts-grid';
    }

    public function get_categories() {
        return array('pods-post-view-addons');
    }

    protected function _register_controls() {

        $this->start_controls_section(
            'section_query',
            [
                'label' => __('Post Query', 'ppd'),
            ]
        );

        $this->add_control(
            'post_types',
            [
                'label' => __('Post Types', 'ppd'),
                'type' => Controls_Manager::SELECT2,
                'default' => 'post',
                'options' => kaya_cpt_post_types(),
                'multiple' => true
            ]
        );
        $this->add_control(
            'tax_query',
            [
                'label' => __('Taxonomies', 'ppd'),
                'type' => Controls_Manager::SELECT2,
                'options' => kaya_all_taxonomies(),
                'multiple' => true,
                'label_block' => true
            ]
        );

        $this->add_control(
            'advanced',
            [
                'label' => __('Advanced', 'ppd'),
                'type' => Controls_Manager::HEADING,
            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('Order By', 'ppd'),
                'type' => Controls_Manager::SELECT,
                'options' => array(
                    'none' => __('No order', 'ppd'),
                    'ID' => __('Post ID', 'ppd'),
                    'author' => __('Author', 'ppd'),
                    'title' => __('Title', 'ppd'),
                    'date' => __('Published date', 'ppd'),
                    'rand' => __('Random order', 'ppd'),
                    'menu_order' => __('Menu order', 'ppd'),
                ),
                'default' => 'date',
            ]
        );

        $this->add_control(
            'order',
            [
                'label' => __('Order', 'ppd'),
                'type' => Controls_Manager::SELECT,
                'options' => array(
                    'ASC' => __('Ascending', 'ppd'),
                    'DESC' => __('Descending', 'ppd'),
                ),
                'default' => 'DESC',
            ]
        );

        $this-> add_control(
            'pods_disp_style_view',
            [
                'label' => __('Pods Display View', 'ppd'),
                'type' => Controls_Manager::SELECT,
                'options' => array(
                    'pods_grid_view' => __('Grid View', 'ppd'),
                    'pods_table_view' => __('Table View', 'ppd'),
                ),
                'default' => 'pods_grid_view',
            ]
        );

        $this->end_controls_section();

        // Imge settings

        $this->start_controls_section(
            'section_responsive',
            [
                'label' => __('Custom Settings', 'ppd'),
            ]
        );

        $this->add_group_control(
            Group_Control_Image_Size::get_type(),
            [
                'name' => 'image_size', // Actually its `image_size`.
                'label' => __( 'Image Size', 'ppd' ),
                'default' => 'medium',
            ]
        );
       
        $this->add_control(
            'per_line',
            [
                'label' => __('Columns', 'ppd'),
                'type' => Controls_Manager::NUMBER,
                'min' => 1,
                'max' => 8,
                'step' => 1,
                'default' => 4,
                'condition' => [
                    'pods_disp_style_view' => 'pods_grid_view',
                ],
            ]
        );

        $this->add_control(
            'posts_per_page',
            [
                'label' => __('Posts Per Page', 'ppd'),
                'type' => Controls_Manager::NUMBER,
                'default' => 12,
            ]
        );

        $this->add_control(
            'gutter',
            [
                'label' => __('Disable Gutter', 'ppd'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('Yes', 'ppd'),
                'label_off' => __('No', 'ppd'),
                'return_value' => 'yes',
                'default' => 'no',
                'condition' => [
                    'pods_disp_style_view' => 'pods_grid_view',
                ],
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'section_settings',
            [
                'label' => __('Filter Tabs Settings', 'ppd'),
                'condition' => [
                    'pods_disp_style_view' => 'pods_grid_view',
                ],
            ]
        );

        $this->add_control(
            'filterable',
            [
                'label' => __('Enable Filter Tabs', 'ppd'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('Yes', 'ppd'),
                'label_off' => __('No', 'ppd'),
                'return_value' => 'yes',
                'default' => 'no',
                'condition' => [
                    'pods_disp_style_view' => 'pods_grid_view',
                ],
            ]
        );

        $this->add_responsive_control(
            'filter_tab_align',
            [
                'label' => __( 'Alignment', 'ppd' ),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __( 'Left', 'ppd' ),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'ppd' ),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'ppd' ),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .filtertabs ul' => 'text-align: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'enable_right_border',
            [
                'label' => __('Tabs Menu Divider', 'ppd'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('Yes', 'ppd'),
                'label_off' => __('No', 'ppd'),
                'return_value' => 'yes',
                'default' => 'no',
            ]
        );

        $this->add_control(
            'filter_tab_message',
            [
                'type' => Controls_Manager::RAW_HTML,
                'raw' => __( 'Note: You have to select \'taxonomies\' in order to display the filter tabs.', 'ppd' ),
                'content_classes' => 'elementor-panel-alert',
            ]
        );

        $this->end_controls_section();
        
        // Post Meta List
        $this->start_controls_section(
            'section_post_meta_list',
            [
                'label' => __('CPT Post Meta list', 'ppd'),
            ]
        );

        $this->add_control(
            'post_meta_keys',
            [
                'label' => __('Choose Meta Key Name', 'ppd'),
                'type' => Controls_Manager::SELECT2,
                'options' => kaya_get_all_metakeys(),
                'multiple' => true,
                'label_block' => true
            ]
        ); 

         $this->add_control(
            'enable_category_name',
            [
                'label' => __('Enable Post Category', 'ppd'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('Yes', 'ppd'),
                'label_off' => __('No', 'ppd'),
                'return_value' => 'yes',
                'default' => 'no',
                'condition' => [
                    'pods_disp_style_view' => 'pods_grid_view',
                ],
            ]
        );

         $this->add_control(
            'meta_data_font_size',
           [
                'label' => __( 'Meta Data Font Size', 'ppd' ),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 13,
                ],
                'condition' => [
                    'pods_disp_style_view' => 'pods_grid_view',
                ],
                'range' => [
                    'px' => [
                        'max' => 50,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper .post-meta-general-info span, ' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'meta_data_font_weight',
            [
                'label' => __( 'Meta Data Font Weight', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'bold' => __( 'Bold', 'ppd' ),
                ],
                'condition' => [
                    'pods_disp_style_view' => 'pods_grid_view',
                ],
                'default' => 'bold',
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper .post-meta-general-info span' => 'font-weight:{{VALUE}};',
                ],
            ]
        );

         $this->add_control(
            'meta_data_font_style',
            [
                'label' => __( 'Meta Data Font Style', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'italic' => __( 'Italic', 'ppd' ),
                ],
                'condition' => [
                    'pods_disp_style_view' => 'pods_grid_view',
                ],
                'default' => 'normal',
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper .post-meta-general-info span' => 'font-style: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_section();
        $this->start_controls_section(
            'meta_data_font_settings',
            [
                'label' => __('Meta Data Font Settings', 'ppd'),
                'tab' => Controls_Manager::TAB_SETTINGS,
                'condition' => [
                    'pods_disp_style_view' => 'pods_table_view',
                ],
            ]
        );

        $this->add_control(
            'meta_data_table_view_font_size',
           [
                'label' => __( 'Meta Data Font Size', 'ppd' ),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 13,
                ],
                'range' => [
                    'px' => [
                        'max' => 50,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper td, .post-content-wrapper .post-meta-general-info span' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'meta_data_table_view_font_weight',
            [
                'label' => __( 'Meta Data Font Weight', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'bold' => __( 'Bold', 'ppd' ),
                ],
                'default' => 'bold',
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper td' => 'font-weight:{{VALUE}};',
                ],
            ]
        );

         $this->add_control(
            'meta_data_table_view_font_style',
            [
                'label' => __( 'Meta Data Font Style', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'italic' => __( 'Italic', 'ppd' ),
                ],
                'default' => 'normal',
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper td' => 'font-style: {{VALUE}}!important;',
                ],
            ]
        );
       
        $this->end_controls_section();

        $this->start_controls_section(
            'title_settings',
            [
                'label' => __('Post Title Settings', 'ppd'),
                'tab' => Controls_Manager::TAB_SETTINGS,
                'condition' => [
                    'pods_disp_style_view' => 'pods_grid_view',
                ],
            ]
        );

        $this->add_control(
            'title_font_size',
           [
                'label' => __( 'Title Font Size', 'ppd' ),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 20,
                ],
                'range' => [
                    'px' => [
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper h3, .grid-view-container h3' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'title_font_weight',
            [
                'label' => __( 'Title Font Weight', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'bold' => __( 'Bold', 'ppd' ),
                ],
                'default' => 'normal',
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper h3, .grid-view-container h3' => 'font-weight:{{VALUE}};',
                ],
            ]
        );

         $this->add_control(
            'title_font_style',
            [
                'label' => __( 'Title Font Style', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'italic' => __( 'Italic', 'ppd' ),
                ],
                'default' => 'normal',
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper h3, .grid-view-container h3' => 'font-style: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_section();

        // Filter Tabs
        $this->start_controls_section(
            'section_filters_styling',
            [
                'label' => __('Grid Filters', 'ppd'),
                'tab' => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'pods_disp_style_view' => 'pods_grid_view',
                ],
            ]
        );

        $this->add_control(
            'filter_tab_bg_color',
            [
                'label' => __( 'Filter Tabs BG Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .filtertabs ul li a' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'filter_tab_color',
            [
                'label' => __( 'Filter Tabs Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .filtertabs ul li a' => 'color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'filter_tab_active_bg_color',
            [
                'label' => __( 'Filter Tab Active BG Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .filtertabs ul li a.active, {{WRAPPER}} .filtertabs ul li a:hover,  {{WRAPPER}} .filter-tabs li.current a' => 'background: {{VALUE}};',
                     '{{WRAPPER}} .filter-tabs li.current a:after, {{WRAPPER}} .filter-tabs li a:hover:after' => 'border-top-color: {{VALUE}};'
                ],
            ]
        );

        $this->add_control(
            'filter_tab_active_color',
            [
                'label' => __( 'Filter Tab Active Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .filtertabs ul li a.active, {{WRAPPER}} .filtertabs > ul > li > a:hover, {{WRAPPER}} .filter-tabs li.current a' => 'color: {{VALUE}}!important;',
                ],
            ]
        );

         
         $this->add_control(
            'filter_tab_submenu_bg_color',
            [
                'label' => __( 'Filter Tabs Sub Menu BG Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} ul.filtersubmenu li a' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'filter_tab_submenu_color',
            [
                'label' => __( 'Filter Tabs Sub Menu Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} ul.filtersubmenu li a' => 'color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'filter_tab_submenu_active_bg_color',
            [
                'label' => __( 'Filter Tab Sub Menu Active BG Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} ul.filtersubmenu li.current a, {{WRAPPER}} ul.filtersubmenu li a:hover' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'filter_tab_submenu_active_color',
            [
                'label' => __( 'Filter Tab Sub Menu Active Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} ul.filtersubmenu li.current a, {{WRAPPER}} ul.filtersubmenu li a:hover ' => 'color: {{VALUE}}!important;',
                ],
            ]
        );
        $this->end_controls_section();

        $this->start_controls_section(
            'section_post_meta_fields',
            [
                'label' => __('Post Meta Fields', 'ppd'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'post_meta_color',
            [
                'label' => __( 'Post Meta Hover Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper span' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->add_control(
            'post_meta_bg_color',
            [
                'label' => __( 'Post Meta Background Hover Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .title-meta-data-wrapper' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'section_grid_thumbnail_styling',
            [
                'label' => __('Post Title', 'ppd'),
                'tab' => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'pods_disp_style_view' => 'pods_grid_view',
                ],
            ]
        );

        $this->add_control(
            'grid_posts_title_color',
            [
                'label' => __( 'Title Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper h3 a, .post-content-wrapper h3, {{WRAPPER}} .post-content-wrapper .title-category-wrapper p' => 'color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'title_hover_border_color',
            [
                'label' => __( 'Title Hover Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper h3 a:hover, .grid-view-container a:hover h3' => 'color: {{VALUE}}!important;',
                ],
            ]
        );
        
        $this->end_controls_section();

        $this->start_controls_section(
            'section_table_thumbnail_styling',
            [
                'label' => __('Post Title', 'ppd'),
                'tab' => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'pods_disp_style_view' => 'pods_table_view',
                ],
            ]
        );

        $this->add_control(
            'table_posts_title_color',
            [
                'label' => __( 'Title Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#ffffff',
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper th' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'title_BG_color',
            [
                'label' => __( 'Title BG Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#021738',
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper th' => 'background: {{VALUE}};',
                ],
            ]
        );
        
        $this->end_controls_section();

        $this->start_controls_section(
            'section_meta_data_colors',
            [
                'label' => __('Meta Data Color Settings', 'ppd'),
                'tab' => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'pods_disp_style_view' => 'pods_table_view',
                ],
            ]
        );

        $this-> add_control(
            'meta_data_odd_BG_color',
            [
                'label' => __('Meta Data Odd BG Color', 'ppd'),
                'type' => Controls_Manager::COLOR,
                'default' => '#e5e5e5',
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper tr.even-odd-color:nth-child(odd)' => 'background:{{VALUE}};',
                ]
            ]
        );

        $this->add_control(
            'meta_data_odd_color',
            [
                'label' => __('Meta Data Odd Color', 'ppd'),
                'type' => Controls_Manager::COLOR,
                'default' => '000000',
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper tr.even-odd-color:nth-child(odd)' => 'color:{{VALUE}};',
                ],
            ]
        );

        $this-> add_control(
            'meta_data_even_BG_color',
            [
                'label' => __('Meta Data Even BG Color', 'ppd'),
                'type' => Controls_Manager::COLOR,
                'default' => '#fffff',
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper tr.even-odd-color:nth-child(even)' => 'background:{{VALUE}};',
                ],
            ]
        );

        $this-> add_control(
            'meta_data_even_color',
            [
                'label' => __('Meta Data Even Color', 'ppd'),
                'type' => Controls_Manager::COLOR,
                'default' => '#000000',
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper tr.even-odd-color:nth-child(even)' => 'color:{{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings();
        $taxonomy_cats = !empty($settings['tax_query']) ? $settings['tax_query'] : '';
        $image_sizes = $settings['image_size_custom_dimension'];
       
         if( $settings['enable_right_border'] == 'yes' ){   
         if ( $settings['pods_disp_style_view'] == 'pods_grid_view' ) {?>
            <style>
                .filtertabs ul li a::before{
                    display: none;
                }
            </style>
        <?php  }
    }
        $tax_terms_data = array();
        if( !empty($taxonomy_cats) ){ 
            foreach ($taxonomy_cats as $tax_data) {
                list($tax, $term) = explode(':', $tax_data);
                $tax_terms_data[$tax][] = $term;
            }
        }
        $array_val = array();
            if( !empty($taxonomy_objects) ){
                foreach ($taxonomy_objects as $key => $taxonomies) {
                    $array_val[] = ( !empty( $instance['tax_term'][$taxonomy_objects[$key]] )) ? $instance['tax_term'][$taxonomy_objects[$key]] : '';
                }
            }
            $taxonomy_ids = array();
            if( !empty($array_val[0]) ){
                foreach ($array_val as $key => $taxonomy_array_ids) {
                    if(  !empty( $taxonomy_array_ids) ){
                        $taxonomy_ids = array_merge($taxonomy_ids, array_values($taxonomy_array_ids));
                    }
                }
            }else{
                $taxonomy_ids = '';
            }
        if( !isset($_REQUEST['tax']) ){            
            if( $taxonomy_cats ){
                foreach ($taxonomy_cats as $tax_data) {
                    list($tax, $term) = explode(':', $tax_data);
                    if (!empty($tax) || !empty($term)){
                        $the_taxes[] = array(  
                            'taxonomy' => $tax,
                            'field'    => 'slug',
                            'terms'    => $term,
                        );
                    }
                }
            }
        }else{
           $the_taxes[] = array(  
                'taxonomy' => $_REQUEST['tax'],
                'field'    => 'slug',
                'terms'    => $_REQUEST['slug'],
            );
        }
        global $wp_query, $paged, $post;                
        if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
        elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
        else { $paged = 1; }
        $the_taxes['relation'] = 'OR';
        if( $array_val ) {
            $args1 = array( 'paged' => $paged, 'post_type' => $settings['post_types'], 'orderby' => $settings['orderby'], 'posts_per_page' =>$settings['posts_per_page'],'order' => $settings['order'],  'tax_query' => $the_taxes);
        }else{
            $args1 = array('paged' => $paged, 'taxonomy' => '', 'post_type' => $settings['post_types'], 'orderby' => $settings['orderby'], 'posts_per_page' => $settings['posts_per_page'],'order' => $settings['order'] );
        }
        // Post Loop
        if(is_front_page()) {
            $paged = (get_query_var('page')) ? get_query_var('page') : 1;
        }else {
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        }
        $loop_query = new \WP_Query( array(  
            'post_type' => $settings['post_types'],
            'paged' =>$paged,
            'posts_per_page' =>  $settings['posts_per_page'],
            'orderby' => $settings['orderby'],
            'order' => $settings['order'],
            'tax_query' => $the_taxes,
        ));
        global $wp_query;
        // Put default query object in a temp variable
        $tmp_query = $wp_query;
        // Now wipe it out completely
        $wp_query = null;
        // Re-populate the global with our custom query
        $wp_query = $loop_query;
        // Post Filter Tabs
        if( $settings['filterable'] == 'yes' ){
            echo '<div class="filtertabs filtertab_'.$settings['filter_tab_align'].'">';
                // echo '<div class="filter" id="filter" >';
                        if( count($settings['post_types']) > 1 ){
                            echo '<ul>';
                            if( isset($_REQUEST['cpt']) ){
                                 $current_cpt = '';
                            }else{
                                  $current_cpt = 'active';
                            }
                            echo '<li><a class="'.$current_cpt.'" href="'.get_the_permalink().'">'.__('All', 'ppd').'</a></li>';
                            foreach ($settings['post_types'] as $key => $post_type) {
                                $obj = get_post_type_object( $post_type );
                                $taxonomy_data = get_object_taxonomies( $post_type );
                                if( isset($_REQUEST['cpt']) ){
                                   $current_cpt = ($_REQUEST['cpt'] == $post_type) ? 'active' : '';
                                }else{
                                    $current_cpt = '';
                                }
                                echo '<li class="cpt_categories_wrapper"><a class="'.$current_cpt.'" href="#">'.$obj->labels->singular_name.'</a>';
                                    foreach ($taxonomy_data as $key => $taxonomy) {
                                      if( !empty($taxonomy) ){
                                             $taxonomy_terms = get_terms($taxonomy);
                                        echo '<ul class="filtersubmenu sub-menu">';
                                            foreach ($taxonomy_terms as $key => $tax_term) {
                                                if( isset($_REQUEST['tax']) ){
                                                    $current = ($_REQUEST['slug'] == $tax_term->slug) ? 'current' : '';
                                                }else{
                                                    $current = '';
                                                }
                                                if( !empty($tax_terms_data[$taxonomy]) ){
                                                    if( !empty( $tax_terms_data[$taxonomy] ) && in_array( $tax_term->slug, $tax_terms_data[$taxonomy] ) ){
                                                        echo '<li class="tax-'.$tax_term->term_id.' '.$current.'"><a href="'.get_the_permalink().'?cpt='.$post_type.'&tax='.$taxonomy.'&slug='.$tax_term->slug.'">'.$tax_term->name.'</a></li>';
                                                    }
                                                }else{
                                                     echo '<li class="tax-'.$tax_term->term_id.' '.$current.'"><a href="'.get_the_permalink().'?cpt='.$post_type.'&tax='.$taxonomy.'&slug='.$tax_term->slug.'">'.$tax_term->name.'</a></li>';
                                                }
                                             }
                                        echo '</ul>';
                                        }
                                    }                        

                                echo '</li>';
                            }
                        }else{
                            echo '<ul class="filter-tabs">';
                             if( isset($_REQUEST['tax']) ){
                                 $current_cpt = '';
                            }else{
                                  $current_cpt = 'current';
                            }
                            if( count($tax_terms_data) > 0 ){
                                echo '<li class="'.$current_cpt.'"><a href="'.get_the_permalink().'">'.__('All', 'ppd').'</a></li>';
                                foreach ($tax_terms_data as $tax_name => $terms) {                                
                                    foreach ($terms as $key => $term) {                                    
                                        if( isset($_REQUEST['tax']) ){
                                            $current = ($_REQUEST['slug'] == $term) ? 'current' : '';
                                        }else{
                                            $current = '';
                                        }
                                        $tax_terms_list = get_term_by( 'slug', $term, $tax_name );
                                        echo '<li class="tax-'.$tax_terms_list->term_id.' '.$current.'"><a href="'.get_the_permalink().'/?tax='.$tax_name.'&slug='.$tax_terms_list->slug.'">'.$tax_terms_list->name.'</a> </li>';
                                    }                              
                                } 
                            }
                              echo '</ul>';
                        }
                    echo '</ul>';
              //  echo '</div>';
            echo '</div>';
        }
        
        if( !isset($_REQUEST['tax']) ){            
            if( $taxonomy_cats ){
                foreach ($taxonomy_cats as $tax_data) {
                    list($tax, $term) = explode(':', $tax_data);
                    if (!empty($tax) || !empty($term)){
                        $the_taxes[] = array(  
                            'taxonomy' => $tax,
                            'field'    => 'slug',
                            'terms'    => $term,
                        );
                    }
                }
            }else{
               
            }

        }else{
           $the_taxes[] = array(  
                'taxonomy' => $_REQUEST['tax'],
                'field'    => 'slug',
                'terms'    => $_REQUEST['slug'],
            );
        }

        if ( $settings['pods_disp_style_view'] == 'pods_grid_view' ) {
        echo '<div class="post-grid-content-wrapper">';
        $gutter = ($settings['gutter'] == 'yes') ? 'gutter' : '';
        if ( $loop_query->have_posts() ) : 
            echo '<ul class="column-extra '.$gutter.'">';
        $array_val = array();
            if( !empty($taxonomy_objects) ){
                foreach ($taxonomy_objects as $key => $taxonomies) {
                    $array_val[] = ( !empty( $instance['tax_term'][$taxonomy_objects[$key]] )) ? $instance['tax_term'][$taxonomy_objects[$key]] : '';
                }
            }
            $taxonomy_ids = array();
            if( !empty($array_val[0]) ){
                foreach ($array_val as $key => $taxonomy_array_ids) {
                    if(  !empty( $taxonomy_array_ids) ){
                        $taxonomy_ids = array_merge($taxonomy_ids, array_values($taxonomy_array_ids));
                    }
                }
            }else{
                $taxonomy_ids = '';
            }
                global $wp_query, $paged, $post;                
                if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
                elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
                else { $paged = 1; }
                $the_taxes['relation'] = 'OR';
                if( $array_val ) {
                    $args1 = array( 'paged' => $paged, 'post_type' => $settings['post_types'], 'orderby' => $settings['orderby'], 'posts_per_page' =>$settings['posts_per_page'],'order' => $settings['order'],  'tax_query' => $the_taxes);
                }else{
                    $args1 = array('paged' => $paged, 'taxonomy' => '', 'post_type' => $settings['post_types'], 'orderby' => $settings['orderby'], 'posts_per_page' => $settings['posts_per_page'],'order' => $settings['order'] );
                }
                // Post Loop
                if(is_front_page()) {
                    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
                }else {
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                }
                $loop_query = new \WP_Query( array(  
                    'post_type' => $settings['post_types'],
                    'paged' =>$paged,
                    'posts_per_page' =>  $settings['posts_per_page'],
                    'orderby' => $settings['orderby'],
                    'order' => $settings['order'],
                    'tax_query' => $the_taxes,
                ));
                global $wp_query;
                // Put default query object in a temp variable
                $tmp_query = $wp_query;
                // Now wipe it out completely
                $wp_query = null;
                // Re-populate the global with our custom query
                $wp_query = $loop_query;
                $template_file = locate_template( 'elementor-post-widget-styles.php' );
                 while ( $loop_query->have_posts() ) : $loop_query->the_post();
                // Seesion Data, storing cpt post short list IDS only
                    if(isset($_SESSION['shortlist'])) {
                        if ( in_array(get_the_ID(), $_SESSION['shortlist']) ) {
                            $selected = 'item_selected';
                        }else{
                            $selected = '';
                        }
                    }else{
                        $selected = '';
                    }
                    echo '<li class="column'. $settings['per_line'].' item all" id="'.get_the_ID().'">';
                    //Gray Scale Option
                    if( function_exists('gray_scale_grid_view')){
                        if(!empty($kaya_options->gray_scale_mode) ){
                            echo gray_scale_grid_view();
                        }
                        else{
                            echo normal_grid_style();
                        }
                    }
                     // Display thumbnail if enabled              
                        if( $template_file ){
                           include $template_file;
                        }else{
                            include KAYA_PCV_PLUGIN_PATH.'templates/elementor-post-widget-styles.php';
                        }
                    echo '</li>';
                endwhile;
            echo '</ul>'; 
        endif; 
        if(function_exists('kaya_pagination')){
                    echo kaya_pagination();
                }
            wp_reset_postdata(); 
            echo '</div>';
        $wp_query = null;
            $wp_query = $tmp_query;
    }

    //Table View
        if ( $settings['pods_disp_style_view'] == 'pods_table_view' ) {
            echo '<div class="post-content-table-wrapper">';
            $array_val = array();
            if( !empty($taxonomy_objects) ){
                foreach ($taxonomy_objects as $key => $taxonomies) {
                    $array_val[] = ( !empty( $instance['tax_term'][$taxonomy_objects[$key]] )) ? $instance['tax_term'][$taxonomy_objects[$key]] : '';
                }
            }
            $taxonomy_ids = array();
            if( !empty($array_val[0]) ){
                foreach ($array_val as $key => $taxonomy_array_ids) {
                    if(  !empty( $taxonomy_array_ids) ){
                        $taxonomy_ids = array_merge($taxonomy_ids, array_values($taxonomy_array_ids));
                    }
                }
            }else{
                $taxonomy_ids = '';
            }
                global $wp_query, $paged, $post;                
                if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
                elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
                else { $paged = 1; }
                $the_taxes['relation'] = 'OR';
                if( $array_val ) {
                    $args1 = array( 'paged' => $paged, 'post_type' => $settings['post_types'], 'orderby' => $settings['orderby'], 'posts_per_page' =>$settings['posts_per_page'],'order' => $settings['order'],  'tax_query' => $the_taxes);
                }else{
                    $args1 = array('paged' => $paged, 'taxonomy' => '', 'post_type' => $settings['post_types'], 'orderby' => $settings['orderby'], 'posts_per_page' => $settings['posts_per_page'],'order' => $settings['order'] );
                }
                // Post Loop
                if(is_front_page()) {
                    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
                }else {
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                }
                $loop_query = new \WP_Query( array(  
                    'post_type' => $settings['post_types'],
                    'paged' =>$paged,
                    'posts_per_page' =>  $settings['posts_per_page'],
                    'orderby' => $settings['orderby'],
                    'order' => $settings['order'],
                    'tax_query' => $the_taxes,
                ));
                global $wp_query;
                // Put default query object in a temp variable
                $tmp_query = $wp_query;
                // Now wipe it out completely
                $wp_query = null;
                // Re-populate the global with our custom query
                $wp_query = $loop_query;
                    echo '<table>';
                        echo '<tr>';
                            echo '<th>Portfolio</th>';
                                foreach ($settings['post_meta_keys'] as $key => $meta_key) {
                                    if( !function_exists('pods_api') ){
                                        return false;
                                    }
                                    $pods_options = pods_api()->load_pods( array( 'fields' => false ) );
                                    foreach ($pods_options as $key => $options) {     
                                        if( ($options['type'] == 'post_type')){
                                            $replace_str = $options['name'].'s';
                                            $meta_key_replace =  str_replace($replace_str, '', $meta_key);
                                            $meta_data = get_post_meta(get_the_ID(),$meta_key_replace, true);
                                            if( !empty($options['fields'][$meta_key_replace]['pod']) ){
                                                if( in_array( $options['fields'][$meta_key_replace]['pod'], $settings['post_types']) ){
                                                    $meta_key_data = ( $meta_key_replace == 'age' ) ? $meta_key_replace.'_filter' : $meta_key_replace;
                                                    if( !empty($options['fields'][$meta_key_replace]['pod']) ){
                                                     echo '<th>'.$options['fields'][$meta_key_replace]['label'].'</th>';  
                                                    }
                                                    else{
                                                        echo '<th style="text-align:center">---</th>';
                                                    }
                                                }

                                            }
                                        }
                                    }                                    
                                }
                        echo '</tr>';
                            if ( $loop_query->have_posts() ) : 
                                while ( $loop_query->have_posts() ) : $loop_query->the_post();
                                    $li_rand_color = rand(1,100);
                                    echo '<tr class="even-odd-color even-odd-li-'.$li_rand_color.'">';
                                        $image_sizes = ( $settings['image_size_size'] == 'custom' ) ? array_values($settings['image_size_custom_dimension']) : $settings['image_size_size'];
                                        //echo '<td><a href="' .get_post_meta($get_the_ID(), 'first_name', true).'"</a></td>';
                                            echo '<td>';
                                                echo '<a href="'.get_the_permalink().'">';
                                                    echo kaya_pod_featured_img($image_sizes, $settings['image_size_size']);
                                                echo '</a>';
                                            echo '</td>';
                                                //echo '<td>'.get_the_title().'</td>';
                                                    if( !empty($settings['post_meta_keys']) ){
                                                        foreach ($settings['post_meta_keys'] as $key => $meta_key) {
                                                            if( !function_exists('pods_api') ){
                                                                return false;
                                                            }
                                                            $pods_options = pods_api()->load_pods( array( 'fields' => false ) );
                                                            foreach ($pods_options as $key => $options) {     
                                                                if( ($options['type'] == 'post_type')){
                                                                    $replace_str = $options['name'].'s';
                                                                    $meta_key_replace =  str_replace($replace_str, '', $meta_key);
                                                                    $meta_data = get_post_meta(get_the_ID(),$meta_key_replace, true);
                                                                    if( !empty($options['fields'][$meta_key_replace]['pod']) ){
                                                                        if( in_array( $options['fields'][$meta_key_replace]['pod'], $settings['post_types']) ){
                                                                            $meta_key_data = ( $meta_key_replace == 'age' ) ? $meta_key_replace.'_filter' : $meta_key_replace;
                                                                            if( !empty($meta_data) ){
                                                                                echo '<td>'.get_post_meta(get_the_ID(),$meta_key_data, true).'</td>';  
                                                                            }
                                                                            else{
                                                                                echo '<td style="text-align:center">---</td>';
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }                                    
                                                        }
                                                    }
                                    echo '</tr>';
                                endwhile;
                            endif;
                    echo '</table>';
                    if(function_exists('kaya_pagination')){
                    echo kaya_pagination();
                }
            wp_reset_postdata(); 
            
        $wp_query = null;
            $wp_query = $tmp_query;
            echo '</div>';
        }
    }

    protected function content_template() {
    }

}