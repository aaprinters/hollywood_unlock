<?php
// CPT Search
function kaya_cpt_search_display(){
		global $kaya_options, $wp_query;
		$limit = !empty($_GET['limit']) ? $_GET['limit'] : '-1';
		$advance_search =  isset( $_GET['advance_search']  ) ? $_GET['advance_search'] : '';
		$post_type =  isset( $_GET['cpt_type']  ) ? $_GET['cpt_type'] : '';
		$post_type = str_replace('pods_', '', $post_type);
		$cpt_meta_fields = kaya_get_pods_cpt_fields($post_type);
		$taxonomy_objects = get_object_taxonomies( $post_type );
		 $tax_query ='';
		 
		if( !empty($taxonomy_objects) ){
			foreach ($taxonomy_objects as $key => $pods_taxonomy) {
				$taxonomy = 'pods_'.$pods_taxonomy;
				 if( !empty($_GET[$pods_taxonomy.'_tax_terms']) ){
 				$tax_query[] = array(
					'taxonomy' => $pods_taxonomy,
					'field' => 'slug',
					'terms' =>  $_GET[$pods_taxonomy.'_tax_terms'],
				);
				}
			}
		}else{
			 $tax_query ='';
		}
		
		if (get_query_var('paged'))
		{
			$paged = get_query_var('paged');
		}
		elseif (get_query_var('page'))
		{
			$paged = get_query_var('page');
		}
		else
		{
			$paged = 1;
		}
		$args = array(
		//'taxonomy' => $_GET['taxonomy_name'],
		'paged' => $paged,
		'post_type' => 'talent',
		'post_status' => 'publish',
		'posts_per_page' => $limit,
		'tax_query' => $tax_query,
		);
		if( !empty($cpt_meta_fields) ){
		foreach ($cpt_meta_fields as $key => $field_id) {	
				if( $field_id['type'] == 'pick' ){
					if( $field_id['options']['pick_format_type'] == 'single' ){							
						if( !empty( $_GET[$field_id['name'].'_range'] ) &&  ( $_GET[$field_id['name'].'_range'] == 'true' ) ){
							if( !empty($_GET[$field_id['name'].'-from']) ){
								$args['meta_query'][] = array(
									'key' => $field_id['name'],
									'value' => array( $_GET[$field_id['name'].'-from'], $_GET[$field_id['name'].'-to'] ),
									'compare' => 'BETWEEN',
									'type' => 'numeric',
								);	
							}									
						}else{
							if( !empty($_GET[$field_id['name']]) ){
									$args['meta_query'][] = array(
										'key' => $field_id['name'],
										'value' => $_GET[$field_id['name']],
									);
								}
						}
					}elseif( $field_id['options']['pick_format_type'] == 'multi' ){
							if( !empty($_GET[$field_id['name']]) ){
									$args['meta_query'][] = array(
										'key' => $field_id['name'],
										'value' => $_GET[$field_id['name']],
										'compare' => 'IN',
									);
								}else{
						if(!empty($_GET[$field_id['name']])){
							$args['meta_query'][] = array(
								'key' => $field_id['name'],
								'value' => $_GET[$field_id['name']],
							);
						}
					}

					}
				}elseif( $field_id['type'] == 'text' ){
					if( !empty($_GET[$field_id['name']] ) ){
						$args['meta_query'][] = array(
							'key' => $field_id['name'],
							'value' => $_GET[$field_id['name']],
							'compare' => 'LIKE'	
						);	
					}
				}elseif( $field_id['type'] == 'number' ){
					$num_range = $_GET['user_roles'].'-'.$field_id['name'];
					if( !empty($_GET[$num_range.'-min']) ){
						$args['meta_query'][] = array(
							'key' => $field_id['name'],
							'value' => array( $_GET[$num_range.'-min'], $_GET[$num_range.'-max'] ),
							'type' => 'numeric',
							'compare' => 'BETWEEN'	
						);	
					}
				}elseif( $field_id['type'] == 'date' ){
					$age_range = $_GET['user_roles'].'-'.$field_id['name'];
					if( $field_id['name'] == 'age' ){
						if( !empty($_GET[$age_range.'-min']) ){
							$args['meta_query'][] = array(
								'key' =>'age_filter',
								'value' => array( $_GET[$age_range.'-min'], $_GET[$age_range.'-max'] ),
								'type' => 'numeric',
								'compare' => 'BETWEEN'	
							);	
						}
					}
				}else{
					if( !empty($_GET[$field_id['name']]) ){
						$args['meta_query'][] = array(
							'key' => $field_id['name'],
							'value' => $_GET[$field_id['name']],
							'compare' => 'LIKE',
						);
					}
				}
			}
		}
		query_posts($args);
		 echo '<div class="post-content-wrapper">';
			echo '<ul class="column-extra">';
			if ( have_posts() ) {
			   while ( have_posts() ) {
			   		the_post();
			   		global $post;
			   		kaya_get_template_part( 'pods-taxonomy-view-style' );
			   }
			}else{
	        	echo '<p class="empty-results-text">'.(!empty($_REQUEST['error_msg']) ? html_entity_decode($_REQUEST['error_msg']) : __('Nothing Found', 'ppd')).'</p>';
	        }
			echo '</ul>';
		echo '</div>';
	echo kaya_pagination();
	wp_reset_query();
	wp_reset_postdata();
}

function kaya_cpt_post_ajax_search(){
	global $paged, $wp_query, $kaya_options;
	$search_data = $_POST['search_data'];
	parse_str($search_data);
	$limit = !empty($limit) ? $limit : '-1';
	$taxonomy_objects = get_object_taxonomies( $cpt_type );
	if( !empty($taxonomy_objects) ){
		foreach ($taxonomy_objects as $key => $pods_taxonomy) {
			$taxonomy = 'pods_'.$pods_taxonomy;
			if( !empty(${$pods_taxonomy.'_tax_terms'}) ){
				$tax_query[] = array(
				'taxonomy' => $pods_taxonomy,
				'field' => 'slug',
				'terms' =>  ${$pods_taxonomy.'_tax_terms'},
				);
			}
		}
	}
	if (get_query_var('paged'))
	{
		$paged = get_query_var('paged');
	}
	elseif (get_query_var('page'))
	{
		$paged = get_query_var('page');
	}
	else
	{
		$paged = 1;
	}
	$args = array(
		'paged' => !empty($_POST['paged']) ? $_POST['paged'] : '1',
		'post_type' => $cpt_type,
		'post_status' => 'publish',
		'posts_per_page' => $limit, 
		'tax_query' => $tax_query,
	);
	$cpt_meta_fields = kaya_get_pods_cpt_fields($cpt_type);
	foreach ($cpt_meta_fields as $key => $field_id) {
		if(  $field_id['type'] == 'text'){
			if( !empty(${$field_id['name']}) ){
				$args['meta_query'][] = array(
					'key' => $field_id['name'],
					'value' => ${$field_id['name']},
					'compare' => 'LIKE'	
				);
			}
		}elseif( $field_id['type'] == 'date' ){
			$num_range = $user_roles.'-'.$field_id['name'].'-'.$_POST['tab_count'];
			if( $field_id['name'] == 'age' ){
				if( !empty( ${$num_range.'-min'} ) ){
					$args['meta_query'][] = array(
						'key' =>'age_filter',
						'value' => array( ${$num_range.'-min'} ,${$num_range.'-max'} ),
						'type' => 'numeric',
						'compare' => 'BETWEEN'	
					);	
				}	
			}else{
				
			}
		}
		elseif( $field_id['type'] == 'number' ){
			$num_range = $user_roles.'-'.$field_id['name'].'-'.$_POST['tab_count'];
			if( !empty( ${$num_range.'-min'} ) ){
				$args['meta_query'][] = array(
					'key' => $field_id['name'],
					'value' => array( ${$num_range.'-min'} ,${$num_range.'-max'} ),
					'type' => 'numeric',
					'compare' => 'BETWEEN'	
				);	
			}
		}
		elseif( $field_id['type'] == 'pick' ){
			//echo  $field_id['type'];
			if( $field_id['options']['pick_format_type'] == 'single' ){
				$num_range = $user_roles.'-'.$field_id['name'];
				if( !empty( ${$field_id['name'].'_range'} ) &&  ( ${$field_id['name'].'_range'} == 'true' )  ){
					if( !empty(${$field_id['name'].'-from'}) ){
						$args['meta_query'][] = array(
							'key' => $field_id['name'],
							'value' => array( ${$field_id['name'].'-from'}, esc_html(${$field_id['name'].'-to'}) ),
							'compare' => 'BETWEEN',
							'type' => 'numeric',
						);	

					}else{
						if( !empty(${$field_id['name']}) ){
							$args['meta_query'][] = array(
								'key' => $field_id['name'],
								'value' => ${$field_id['name']},
							);
						}
					}
				}else{
				if( !empty(${$field_id['name']}) ){
						$args['meta_query'][] = array(
							'key' => $field_id['name'],
							'value' => ${$field_id['name']},
						);
					}
				}
			}elseif( $field_id['options']['pick_format_type'] == 'multi' ){
				if( !empty(${$field_id['name']}) ){
					$args['meta_query'][] = array(
						'key' => $field_id['name'],
						'value' => ${$field_id['name']},
						'compare' => 'IN',
					);
				}
			}
		}				
	}
	query_posts($args);
	
	  echo '<div class="search-content-wrapper kaya-post-content-wrapper container" id="'.$_POST['search_class'].'">';
     echo '</br>';
    echo '<h1>'.(!empty($search_result) ? html_entity_decode($search_result) : __('Search Results :', 'ppd' )).'</h1>';
			echo '<ul class="column-extra">';
			if ( have_posts() ) {
			   while ( have_posts() ) {
			   		the_post();
			   		global $post;
			   		kaya_get_template_part( 'pods-taxonomy-view-style' );
			   }
			}else{
				echo '<p class="empty-results-text">'.(!empty($error_msg) ? html_entity_decode($error_msg) : __('Nothing Found', 'ppd')).'</p>';
			}
			echo '</ul>';
		echo '<div class="cpt-post-pagination">';	
			echo kaya_pagination();
		echo '</div>';
		wp_reset_query();
		wp_reset_postdata();
		echo '</div>';
	die();
}
add_action('wp_ajax_kaya_cpt_post_ajax_search', 'kaya_cpt_post_ajax_search');
add_action('wp_ajax_nopriv_kaya_cpt_post_ajax_search', 'kaya_cpt_post_ajax_search');
?>