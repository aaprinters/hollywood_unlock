<?php
/**
 * CPT POst Thumbnail, if featured image exist or not
 * @param ( int ) $image_sizes
 */
function kaya_pod_featured_img($image_sizes, $img_crop=''){
	$img_url = get_the_post_thumbnail_url();

	if($img_url){
		if( ($img_crop == 'custom_thumb_sizes') || ($img_crop == 'custom_image_sizes') || ($img_crop == 'custom')  ){
			echo '<img src="'.kaya_image_sizes($img_url, $image_sizes[0], $image_sizes[1], 't' ).'" />';
		}else{
			the_post_thumbnail($image_sizes);
		}
	}else{
		echo '<img src="'.KAYA_PCV_PLUGIN_URL.'/images/default_image.jpg" height="400" width="400" />';
	}
}
/**
 * Image Resizer functionality
 */
if( !function_exists('kaya_image_sizes') ){
	function kaya_image_sizes($url, $width, $height=0, $align='') {
		return mr_image_resize($url, $width, $height, true, $align, false);
	}
}
function kaya_pods_data_exists(){
	global $pods_data;
	if( function_exists('pods_api') ){
	    $pods_data = pods_api()->load_pods( array( 'names' => true ) );
	}else{
	    $pods_data = ''; 
	}
}
kaya_pods_data_exists();
/**
 * Get POD CPT Fields names and information, based on cpt slug names
 * @param ( string ) $cpt_slug
 */
if( !function_exists('kaya_get_cpt_fields') ){
	function kaya_get_cpt_fields($cpt_slug=''){
		
	 	$pod_slug = pods_v( 'last', 'url' );
	    $pod_options_data = pods( $cpt_slug, $pod_slug );
	    if( !empty($pod_options_data) ){	    	
	      return $pod_options_data->api->fields;
	    }
	}
}

// Logged in pods dropdown menu
function kaya_user_dropdown_menu(){
     global $kaya_settings, $current_user;
    $current_user_data = get_userdata($current_user->ID);
    $hi_button_text = !empty( $kaya_settings['hi_button_text'] ) ? trim($kaya_settings['hi_button_text']) :  __( 'Hi', 'lum' );
    if ( ( is_user_logged_in() ) ) {
        echo '<div id="nav-user-dashboard-menu" class="menu">';
           echo '<ul id="user-main-menu" class="top-nav">';
            echo '<li>';
                echo '<a href="#">' .$hi_button_text. ' '. ucfirst($current_user_data->user_login) .'</a>';
            echo '</li>';
           echo '</ul>';
          if (has_nav_menu('loggedin-user-dropdown-menu')) {
                wp_nav_menu(array('container_id' => 'main-nav','menu_id'=> 'user-dashboard-menu', 'container_class' => 'menu user-dashboard','theme_location' => 'loggedin-user-dropdown-menu', 'menu_class'=> 'top-nav'));
          }else{
        
          }
          echo '</div>';
    }
}

function kaya_get_all_cpt_types() {
    global $wpdb;
    $results = array();
    if( !function_exists('pods_api') ){
        return false;
    }

    $pods_options = pods_api()->load_pods( array( 'fields' => false ) );
    if( !empty($pods_options) ){
	    foreach ($pods_options as $key => $options) {  
		     if( $options['type'] == 'post_type' ){	    		
		    	$results[$options['name']] = $options['label'];	    		
	    	}
		}
	}
	return $results;
}
function kaya_cpt_all_taxonomies() {
    global $wpdb;
    $results = array();
    if( !function_exists('pods_api') ){
        return false;
    }

    $pods_options = pods_api()->load_pods( array( 'fields' => false ) );
    if( !empty($pods_options) ){
	    foreach ($pods_options as $key => $options) {  
		     if( $options['type'] == 'taxonomy' ){
	    		$terms_list = get_terms($options['name']);
	    		foreach ( $terms_list as $key => $terms) {
		    		 $results[$options['name']] = $options['label'];
	    		}
	    	}
		}
	}
	return $results;
}

/**
 * Get POD CPT Fields names and information, based on cpt slug names
 * @param ( string ) $cpt_slug
 */
if( !function_exists('kaya_get_pods_cpt_fields') ){
	function kaya_get_pods_cpt_fields($cpt_slug=''){
		
	 	$pod_slug = pods_v( 'last', 'url' );
	    $pod_options_data = pods( $cpt_slug, $pod_slug );
	    if( !empty($pod_options_data) ){	    	
	      return $pod_options_data->api->fields;
	    }
	}
}

function kaya_cpt_all_taxonomies_terms() {
    global $wpdb;
    $results = array();
    if( !function_exists('pods_api') ){
        return false;
    }


    $pods_options = pods_api()->load_pods( array( 'fields' => false ) );
    if( !empty($pods_options) ){
	    foreach ($pods_options as $key => $options) {  
		     if( $options['type'] == 'taxonomy' ){
	    		$terms_list = get_terms($options['name']);
	    		foreach ( $terms_list as $key => $terms) {
		    		 $results[$terms->slug] = '<b>'.str_replace('_', ' ', ucwords($options['name'])).' - '.$terms->name.'</b>';
	    		}
	    	}
		}
	}
	return $results;
}

function kaya_get_cpt_all_metakeys(){
 
    if( !function_exists('pods_api') ){
        return false;
    }
    	$meta_keys = array();
	   	$pods_options = pods_api()->load_pods( array( 'fields' => false ) );
		foreach ($pods_options as $key => $options) {     
		    if( ($options['type'] == 'post_type')){
		    	foreach ($options['fields'] as $key => $fields_val) {
					 $meta_keys[$fields_val['name']] = ucwords($fields_val['pod']).' - '.$fields_val['label'];
		    	}
				
		    }
		} 
		return $meta_keys;

}

function kaya_cpt_post_range_all_metakeys(){ 
    if( !function_exists('pods_api') ){
        return false;
    }
	$meta_keys = array();
   	$pods_options = pods_api()->load_pods( array( 'fields' => false ) );
	foreach ($pods_options as $key => $options) {  
	    if( ($options['type'] == 'post_type')){
	    	foreach ($options['fields'] as $key => $fields_val) {
	    		if( $fields_val['type'] == 'pick' ){
	    			if( $fields_val['options']['pick_format_type'] == 'single' ){  		
						$meta_keys[$fields_val['name']] = $fields_val['label'];
					}	
				}
	    	}		    
	    }
	} 
	return $meta_keys;

}
/**
 * Get POD CPT fields meta information, based on cpt slug names, it's not displayed files meta data like: images, video and richtextarea... 
 * @param ( string ) $cpt_slug
 */
if( !function_exists('kaya_general_info_section') ){
	function kaya_general_info_section($cpt_slug){
		global $kaya_options;
		$cpt_meta_fields = kaya_get_cpt_fields($cpt_slug);
		$pod = pods( $cpt_slug, get_the_id() );
		if( !empty($cpt_meta_fields) ){
			echo '<div class="general-meta-fields-info-wrapper">';
				echo '<ul>';
					foreach ($cpt_meta_fields as $key => $meta_fields) {
						$opt_data = '';
						if($meta_fields['type'] == 'pick'){
							$post_meta_data_info = get_post_meta(get_the_ID(), $meta_fields['name'], false);
							$post_type_data_ids = $pod->display(  $meta_fields['name']);
							if( !empty($post_type_data_ids) ){
								if( $meta_fields['pick_object'] == 'post_type' ){
									$post_type_data_replace_text = str_replace('and ', ',', $post_type_data_ids);
									$post_type_data = explode(',', $post_type_data_replace_text);
									$get_post_type_info = array_map('trim',$post_type_data);
									$i=0;
									foreach (array_filter($get_post_type_info) as $key => $post_type_info) {
										if( !empty($post_type_info) ){
											$post_data[] = '<a href="'.get_the_permalink($post_meta_data_info[$i]['ID']).'">'.$post_type_info.'</a>';
										}
										$i++;
									}
									$opt_data .= implode(', ', $post_data);
								}elseif( $meta_fields['pick_object'] == 'taxonomy' ){
									$meta_data_id = $pod->display(  $meta_fields['name']);
									$meta_post_type_data = str_replace('and ', ',', $meta_data_id);
									$post_type_data_to_array = explode(',', $meta_post_type_data);
									$get_meta_info = array_map('trim',$post_type_data_to_array);
									$c=0;
									foreach (array_filter($get_meta_info) as $key => $post_info) {
										if( !empty($post_info) ){
											if( $meta_fields['pick_val'] == 'post_tag'){
												$post_data[] = '<a href="'.esc_url(home_url('/tag/'.$post_meta_data_info[$c]['slug'])).'?post_type='.$cpt_slug.'">'.$post_info.'</a>';
											}else{
												$post_data[] = '<a href="'.esc_url(home_url('/'.str_replace('_','-', $post_meta_data_info[$c]['taxonomy'])).'/'.$post_meta_data_info[$c]['slug']).'">'.$post_info.'</a>';
											}
										}
										$c++;
									}
									$opt_data .= implode(', ', $post_data);
								}else{
									$opt_data .= $pod->display(  $meta_fields['name']);
								}
							}          
						}
						elseif( ($meta_fields['type'] == 'date') && ($meta_fields['name'] == 'age')  ){
							$age = kaya_age_calculate($pod->display($meta_fields['name']), $meta_fields['options']['date_format']);
							if( $age == '0' ){
								$opt_data .= '<1';
							}else{
								$opt_data .= $age;
							}
						}
						elseif(($meta_fields['type'] == 'file') || ($meta_fields['type'] == 'wysiwyg')  ){ }
						elseif(($meta_fields['type'] == 'color') ){ }	
						else{
							$fields_data = get_post_meta(get_the_ID(), $meta_fields['name'], true);
							if( !empty($fields_data) && !empty($fields_data[0])){
								$opt_data .=  $fields_data;
							}
						}
						if(($meta_fields['type'] == 'file') || ($meta_fields['type'] == 'wysiwyg')  ){
							if( $meta_fields['type'] == 'file'){
								if( ($meta_fields['options']['file_format_type'] == 'single') && ($meta_fields['name'] != 'featured_image')){
									if( $meta_fields['options']['file_type'] == 'image'){
										echo $data = $pod->display( $meta_fields['name'] );
									}
								}
							}
						 }
						else{
							if( !empty($kaya_options->{'enable_'.$cpt_slug.'_data'}) ){
								$opt_array_info = array_combine( $kaya_options->{'enable_'.$cpt_slug.'_data'},  $kaya_options->{'enable_'.$cpt_slug.'_data'});
							}else{
								$opt_array_info = array('noelements');
							}
							if( !is_single() ){
								if( in_array($meta_fields['name'], $opt_array_info) ){
									if( !empty($opt_data)  ){
										echo '<li><strong>'.$meta_fields['label'].':</strong> &nbsp;<span>'.$opt_data.'</span> </li>';
									}	
								}	
							}else{
								if( !empty($opt_data)  ){
									echo '<li><strong>'.$meta_fields['label'].':</strong>  &nbsp; <span>'.$opt_data.'</span> </li>';
								}
							}					
						}
					} 
				echo '</ul>';
			echo '</div>';    
		}  
	}
}
/*
* Shortcode for general info section
* [general_profile_info]
*/
function general_info_section( $atts ) {
	kaya_general_info_section(get_query_var( 'post_type' ));
}
add_shortcode( 'general_profile_info', 'general_info_section' );

if( !function_exists('kaya_compcard_info_section') ){
	function kaya_compcard_info_section($cpt_slug){
		global $kaya_options;
		$compcard ='';
		$option_fields = kaya_get_cpt_fields($cpt_slug);
		if( !empty($kaya_options->{'enable_'.$cpt_slug.'_data'}) ){
			$opt_array_info = array_combine( $kaya_options->{'enable_'.$cpt_slug.'_data'},  $kaya_options->{'enable_'.$cpt_slug.'_data'});
		}else{
			$opt_array_info = array('noelements');
		}
		if( !empty($opt_array_info) ){
			foreach ($opt_array_info as $key => $opt_key) {
					if( ( $option_fields[$opt_key]['type'] == 'date') && ( $option_fields[$opt_key]['name'] == 'age')  ){
						$age = kaya_age_calculate(get_post_meta(get_the_ID(), $opt_key, true ));
							if( $age == '0' ){
								$fields_data = '<1';
							}else{
								$fields_data = $age;
							}
					}else{
						$fields_data = get_post_meta(get_the_ID(), $opt_key, true );
					}
					$compcard .= '<div><strong>'.$option_fields[$opt_key]['label'].':</strong> <span>'.$fields_data.'</span> </div>';
			}
		}
		return $compcard; 
	}
}
/**
 * It displayed 'files' meta data  as tab section, like: images, video and richtextarea...
 * @param ( string ) $cpt_slug
 */
function kaya_tab_section($cpt_slug){
	$cpt_meta_fields = kaya_get_cpt_fields($cpt_slug);
	$pod = pods( $cpt_slug, get_the_id() );
	if( !empty($cpt_meta_fields) ){
			echo '<ul class="tabs_content_wrapper">';
			kaya_user_custom_profile_tab();
				foreach ($cpt_meta_fields as $key => $meta_fields) {
					if(($meta_fields['type'] == 'file') || ($meta_fields['type'] == 'wysiwyg') || ($meta_fields['type'] == 'video') ){
						$fields_data = get_post_meta(get_the_ID(), $meta_fields['name'], false);

						if(!empty($fields_data) && !empty($fields_data[0])){
							//print_r($fields_data);
		          			echo '<li><a href="#'.$meta_fields['name'].'">'.$meta_fields['label'].'</a></li>';
						}
					}
				}
				kaya_users_tab_section();
			echo '</ul>';
	}
}

/**
 * It displayed 'files' meta data  as tab section, like: images, video and richtextarea...
 * @param ( string ) $cpt_slug
 */
function kaya_media_section($cpt_slug){
	$cpt_meta_fields = kaya_get_cpt_fields($cpt_slug);
	$pod = pods( $cpt_slug, get_the_id() );
	if( !empty($cpt_meta_fields) ){
		foreach ($cpt_meta_fields as $key => $meta_fields) {
			if($meta_fields['type'] == 'file'){
				
				if( ($meta_fields['name'] != 'featured_image') ){
					$images_urls = get_post_meta(get_the_ID(), $meta_fields['name'], false);
					if( !empty($images_urls) && ( !empty($images_urls[0])) ){ // Images
						echo '<div  id="'.$key.'" class="file-data-content-wrapper single-page-meta-content-wrapper">';
							echo '<h3>'.$meta_fields['label'].'</h3>';
							if(( $meta_fields['options']['file_type'] == 'video') ||  $meta_fields['options']['file_type'] == 'audio' ){
								$video_srcs = $pod->display(  $meta_fields['name']);
								$videos = explode(' ', $video_srcs);
								foreach ($videos as $key => $src) {
									if( $meta_fields['options']['file_type'] == 'video' ){
										echo wp_video_shortcode( array( 'src' => $src ) );
									}elseif($meta_fields['options']['file_type']== 'audio'){

										$attr = array(
										        'src'      => $src,
										        'loop'     => '',
										        'autoplay' => '',
										        'preload' => 'none'
										        );
										echo wp_audio_shortcode( $attr );

									}else{

									}
								}
							}else{
								if( $meta_fields['options']['file_type'] == 'text' ){
									$files = get_post_meta(get_the_ID(), $meta_fields['name'], false);
									if( !empty($files) ){
										foreach ($files as $key => $file) {
											if( ($meta_fields['options']['file_format_type'] != 'single')){
												echo '<a href="'.$file['guid'].'" target="_blank">'.$file['guid'].'</a><br />';
											}else{
												echo '<a href="'.$file['guid'].'" target="_blank">'.$meta_fields['label'].'</a><br />';
											}
										}
									}
									$data = $pod->display( $meta_fields['name'] );
								}else{
									echo $data = $pod->display( $meta_fields['name'] );
								}
							}
						echo '</div>';
					} 
				}
			}
			if($meta_fields['type'] == 'wysiwyg'){
			$richcontent = get_post_meta(get_the_ID(), $meta_fields['name'], true);
			if( !empty($richcontent) ){ // Images
			echo '<div id="'.$key.'" class="richtext-data-content-wrapper single-page-meta-content-wrapper">';
			echo '<h2>'.$meta_fields['label'].'</h2>';
			echo trim($richcontent);
			echo '</div>';
			}             
			}
		}   
	}
}

/**
 * Display the all cusrrent cpt post videos
 */
function kaya_cpt_post_videos($cpt_slug, $count="3"){
	$cpt_meta_fields = kaya_get_cpt_fields($cpt_slug);
	$pod = pods( $cpt_slug, get_the_id() );
	if( !empty($cpt_meta_fields) ){
		foreach ($cpt_meta_fields as $key => $meta_fields) {
			if($meta_fields['type'] == 'file'){				
				if( ($meta_fields['name'] != 'featured_image') ){
					$images_urls = get_post_meta(get_the_ID(), $meta_fields['name'], false);
					if( !empty($images_urls) && ( !empty($images_urls[0])) ){ // Images
						echo '<div  class="post-audio-files">';
							if($meta_fields['options']['file_type'] == 'audio' ){
								$video_srcs = $pod->display(  $meta_fields['name']);
								$videos = explode(' ', $video_srcs);
								$i=1;
								foreach ($videos as $key => $src) {
									if($meta_fields['options']['file_type']== 'audio'){
										$attr = array(
										        'src'      => $src,
										        'loop'     => '',
										        'autoplay' => '',
										        'preload' => 'none'
										        );
										echo wp_audio_shortcode( $attr );

									}else{

									}
									if($i==$count){
										break;
									}
									$i++;
								}
							}
						echo '</div>';
					} 
				}
			}
		}   
	}
}


/**
* Display the posts Based on user roles
*/
if( !function_exists('kaya_talent_filter_posts_list') ){
	add_action('pre_get_posts', 'kaya_talent_filter_posts_list');
	function kaya_talent_filter_posts_list($query)
	{
	    global $pagenow; 
	    global $current_user;
	    wp_get_current_user();
	    if(!current_user_can('administrator') && !current_user_can('editor')&& ('edit.php' == $pagenow)){
	        $query->set('author', $current_user->ID); 
	    }
	}
}

function kaya_get_template_part( $slug, $name = '' ) {
	$template = '';
	if ( $name ) {
		$template = locate_template( array( "{$slug}-{$name}.php", "{$slug}-{$name}.php" ) );
	}
	if ( ! $template && $name && file_exists( KAYA_PCV_PLUGIN_PATH . "/templates/{$slug}-{$name}.php" ) ) {
		$template = KAYA_PCV_PLUGIN_PATH . "/templates/{$slug}-{$name}.php";
	}
	if ( ! $template ) {
		$template = locate_template( array( "{$slug}.php", "{$slug}.php" ) );
	}
	if ( $template ) {
		load_template( $template, false );
	}

}

/**
 * Include the template files
 */
add_filter( 'template_include', 'kaya_set_template'  );
function kaya_set_template( $template ){
		$template_name = '';
        if ( is_tax() ) {
            if( file_exists(locate_template('taxonomy.php') )) {
                $template = locate_template( 'taxonomy.php');
            }
            else {
                $template = KAYA_PLUGIN_PATH . '/templates/taxonomy.php';
            }
        }

        return $template;   
        
    }

/**
 * Pods Cpt Fields data
 */
function kaya_get_pod_cpt_fields(){
	if( !function_exists('pods_api') ){
		return false;
	}
	return pods_api()->load_pods( array( 'fields' => false ) );
}

/**
 * Pagination
 */

if ( ! function_exists( 'kaya_pagination' ) ) :
function kaya_pagination() {
    // Don't print empty markup if there's only one page.
    if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
        return;
    }
    $paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
    $pagenum_link = html_entity_decode( get_pagenum_link() );
    $query_args   = array();
    $url_parts    = explode( '?', $pagenum_link );
    if ( isset( $url_parts[1] ) ) {
        wp_parse_str( $url_parts[1], $query_args );
    }
    $pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
    $pagenum_link = trailingslashit( $pagenum_link ) . '%_%';
    $format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
    $format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';
    // Set up paginated links.
    $links = paginate_links( array(
        'base'     => $pagenum_link,
        'format'   => $format,
        'total'    => $GLOBALS['wp_query']->max_num_pages,
        'current'  => $paged,
        'mid_size' => 3,
        //'add_args' => array_map( 'urlencode', $query_args ),
        'prev_text' => '<i class="fa fa-angle-left"></i>',
        'next_text' => '<i class="fa fa-angle-right"></i>',
        'type'      => 'list',
    ) );
    $pagination_allowed_tags = array(
        'a' => array(
            'href' => array(),
            'title' => array(),
            'class' => array()
        ),
        'i' => array(
            'class' => array()
        ),
        'span' => array(
            'class' => array()
        ),
        'ul' => array(
            'class' => array()
        ),
        'li' => array(),
    );
    if ( $links ) :
    ?>  <div class="pagination">
            <?php echo wp_kses($links,$pagination_allowed_tags); ?>
        </div>       
    <?php
    endif;
}
endif;
/*---------------------------------------------
Get Current Page URL
--------------------------------------------- */
if ( ! function_exists( 'kaya_get_current_page_url' ) ){
    function kaya_get_current_page_url(){
        global $wp;
        return add_query_arg( $_SERVER['QUERY_STRING'], '', home_url( $wp->request ) );
    }
}
if( !function_exists('kaya_get_current_page') ){
	function kaya_get_current_page(){
		kaya_get_current_page_url();
	}
}

/*---------------------------------------------------------------------------------------
 Calculating age automatically Based on year
---------------------------------------------------------------------------------------*/
function kaya_age_calculate($dob, $format='')
{ 
   // echo $format;
	if( !empty($dob) ){
    if(( $format == 'mdy') || (  $format == 'mdy_dash' ) || (  $format == 'mdy_dot' ) ){

		if( $format == 'mdy_dot' ){
			$dob=explode(".",$dob);
		}elseif( $format == 'mdy_dash' ){
			$dob=explode("-",$dob);
		}else{
			$dob=explode("/",$dob);
		}
    	$curMonth = date("m");
	    $curDay = date("j");
	    $curYear = date("Y");
	    $age = $curYear - $dob[2];
	    if($curMonth<$dob[0] || ($curMonth==$dob[0] && $curDay<$dob[1]))
	        $age--;

	}elseif(( $format == 'ymd_slash') || (  $format == 'ymd_dash' ) || (  $format == 'ymd_dot' ) ){

		if( $format == 'ymd_dot' ){
			$dob=explode(".",$dob);
		}elseif( $format == 'ymd_dash' ){
			$dob=explode("-",$dob);
		}else{
			$dob=explode("/",$dob);
		}

		
	    $curMonth = date("m");
	    $curDay = date("j");
	    $curYear = date("Y");
	    $age = $curYear - $dob[0];
	    if($curMonth<$dob[1] || ($curMonth==$dob[1] && $curDay<$dob[2]))
	        $age--;

	}elseif(( $format == 'dmy') || (  $format == 'dmy_dash' ) || (  $format == 'dmy_dot' ) ){

		if( $format == 'dmy_dot' ){
			$dob=explode(".",$dob);
		}elseif( $format == 'dmy_dash' ){
			$dob=explode("-",$dob);
		}else{
			$dob=explode("/",$dob);
		}

		
	    $curMonth = date("m");
	    $curDay = date("j");
	    $curYear = date("Y");
	    $age = $curYear - $dob[2];
	    if($curMonth<$dob[1] || ($curMonth==$dob[1] && $curDay<$dob[0]))
	        $age--;

	}else{
		$dob=explode("-",$dob);
	   // print_r( $dob); 
	    $curMonth = date("m");
	    $curDay = date("j");
	    $curYear = date("Y");
	    $age = $curYear - $dob[0];
	}
    return $age;
}
}

/*---------------------------------------------------------------------------------------
	age stored in database, if we need to get the age in years use this
 	id :age_filter 
---------------------------------------------------------------------------------------*/

add_action( 'init', 'my_init_function');

function my_init_function() {
	
function kaya_update_age_calculation(){
	/**
	* It will check the pods plugins exist or not if not return 
	*/
	if( !function_exists('pods_api') ){
		return false;
	}

	$pods_options = pods_api()->load_pods( array( 'fields' => false ) );
	foreach ($pods_options as $key => $options) {
		if( $options['type'] == 'post_type' ){
			 $args = array('post_type' => $options['name'], 'posts_per_page' => -1);
		    $loop = new WP_Query( $args );
		    while ( $loop->have_posts() ) : $loop->the_post();
		    	foreach ($options['fields'] as $fields_key => $field_val) {
		    		if( ($options['fields'][$fields_key]['type'] == 'text') && ($options['fields'][$fields_key]['name'] == 'year_of_birth') ||  ($options['fields'][$fields_key]['type'] == 'date') && ($options['fields'][$fields_key]['name'] == 'age') ){
		    			$age = get_post_meta(get_the_ID(), 'age', true);
		    			update_post_meta(get_the_ID(), 'age_filter', kaya_age_calculate($age, $options['fields'][$fields_key]['options']['date_format']));
		    		}
		    	}
		    endwhile;
		}
	}
}
	add_action('init', 'kaya_update_age_calculation');
}

function kaya_widget_loop_data(){
	//if( $instance['disable_post_thumbnail'] != 'on' ){
		echo '<a href="'.get_the_permalink().'">';
			$img_url = get_the_post_thumbnail_url();
			if($img_url){
				the_post_thumbnail($image_sizes);
			}else{
				echo '<img src="'.KAYA_PCV_PLUGIN_URL.'/images/default_image.jpg" height="400" width="400" />';
			}
		echo '</a>';
	//}
	echo '<div class="description">';
		$option_fields = kaya_get_cpt_fields($instance['post_type']);
		if( $instance['disable_post_title'] != 'on' ){
			echo '<h4><a href="'.get_the_permalink().'">'.get_the_title().'</a></h4>';
		}
		if( !empty($instance['enbale_selected_cpt_fields']) ){
			echo '<div class="post-meta-info-wrapper">';
				echo '<ul>';
				foreach ($instance['enbale_selected_cpt_fields'] as $key => $fields_data) {
					$meta_data = get_post_meta(get_the_ID(), $fields_data, true);
					if( !empty($meta_data) ){
						echo '<li><strong>'.$option_fields[$fields_data]['label'].'</strong>: '.$meta_data.'</li>';
					}
				}
				echo '</ul>';
			echo '</div>';
		}
		if( $instance['disable_post_content'] != 'on' ){
			echo '<p>'.wp_trim_words( get_the_content(), $instance['post_content_limit'], null ).'</p>';
		}
	echo '</div>';
}
// Get post type
function kaya_get_post_type(){
	return get_post_type( get_the_ID() );
}

/**
 * Disable restict cpt meta box panels in kaya roles manager
 */
function kaya_remove_metabox_panel(){ ?>
	<style>
		.restrict-metabox-panels, .nav-tab-wrapper .nav-tab.cpt_meta_box_restrict, .kta-users-note{
			display: none;
		}
	</style>
<?php }
add_action('admin_head', 'kaya_remove_metabox_panel');


add_action('pre_get_posts', 'kaya_taxonomy_post_per_page');
function kaya_taxonomy_post_per_page( $query ){
	global $kaya_options;
	$limit = !empty($kaya_options->post_limit) ? $kaya_options->post_limit : '-1';
      if ( !is_admin() && $query->is_tax() && $query->is_main_query() || ( ($query->is_search) && !is_admin() ) ):
        $query->set('posts_per_page', $limit);
        return;
    endif;
}


/**
 * Pods Cpt post taxonomy & Featured Image
 */

function kaya_pods_cpt_taxonomy_post_init(){
	if( !function_exists('pods_api') ){
        return false;
    }
         
	$pods_options = pods_api()->load_pods( array( 'fields' => false ) );
	foreach ($pods_options as $key => $options) {		
		if( ($options['type'] == 'post_type')){
			//add_action( "pods_api_post_save_pod_item_{$options['name']}", 'kaya_actors_pods_cpt_taxonomy_update', 10, 3 );
		}
	}
}
//add_action('pods_api_post_save_pod_item_talent', 'kaya_actors_pods_cpt_taxonomy_update');
function kaya_actors_pods_cpt_taxonomy_update( $pieces, $is_new_item, $id ) {
	// Pods cpt categories
	$pods_options = pods_api()->load_pods( array( 'fields' => false ) );
	$terms = $pieces[ 'fields' ]['post_categories'][ 'value' ];
	
	if ( empty( $terms ) ) {
		$terms = null;
	} else {
		if ( ! is_array($terms) ) {
			// create an array out of the comma separated values
			$terms = explode(',', $terms);
		}
        	$terms = array_map('intval', $terms);
	}
    foreach ($pods_options as $key => $options) {
    	if( $options['type'] == 'taxonomy' ){
	        //wp_set_object_terms( $id, $terms, $options[ 'name' ], false );
    	}
	}
	// Pods cpt Featured Image
	$featured_image = $pieces['fields']['featured_image']['value'];
	$array_keys = array_keys($featured_image);
	set_post_thumbnail( $id, $array_keys[0] );	
}

function pods_update_form_post_meta($post_id, $field_name, $value = '', $val_array = 'true')
{
    if (empty($value) OR !$value)
    {
        delete_post_meta($post_id, $field_name);
    }
    elseif (!get_post_meta($post_id, $field_name))
    {
        add_post_meta($post_id, $field_name, $value);
    }
    else
    {
        update_post_meta($post_id, $field_name, $value);
    }
}

/**
 * Multiple Images Upload
 * @param Image ID
 * @param Post ID
 */
function pods_get_featured_image_id_by_base64($image_id, $post_id)
{
    //print_r($_POST['upload_pf_profile_cover_image']);
 /*    if (!empty($_FILES[$image_id]['name']))
    { */
//$arr = array_keys($_POST); // Get post

$base_64_data = $_POST['feature_img_data'];
if($base_64_data){
$base64_img = $base_64_data; // base 64 data
$title = "talent_profile";
// Upload dir.
$upload_dir  = wp_upload_dir();
$upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

$img             = str_replace( 'data:image/png;base64,', '', $base64_img );
$img             = str_replace( ' ', '+', $img );
$decoded         = base64_decode( $img );
$filename        = $title . '.png';
$file_type       = 'image/png';
$hashed_filename = md5( $filename . microtime() ) . '_' . $filename;

// Save the image in the uploads directory.
$upload_file = file_put_contents( $upload_path . $hashed_filename, $decoded );

$attachment = array(
	'post_mime_type' => $file_type,
	'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $hashed_filename ) ),
	'post_content'   => '',
	'post_status'    => 'inherit',
	'guid'           => $upload_dir['url'] . '/' . basename( $hashed_filename )
);

$attach_id = wp_insert_attachment( $attachment, $upload_dir['path'] . '/' . $hashed_filename );
        require_once (ABSPATH . 'wp-admin/includes/image.php');

        $attach_data1 = wp_generate_attachment_metadata($attach_id, $upload['file']);
        wp_update_attachment_metadata($attach_id, $attach_data1);
        update_post_meta($post_id, '_thumbnail_id', $attach_id);
    }
}
function pods_get_featured_image_id($image_id, $post_id)
{
    //print_r($_POST['upload_pf_profile_cover_image']);
    if (!empty($_FILES[$image_id]['name']))
    {
        $upload = wp_upload_bits($_FILES[$image_id]['name'], null, file_get_contents($_FILES[$image_id]['tmp_name']));
        $wp_filetype = wp_check_filetype(basename($upload['file']) , null);
        $wp_upload_dir = wp_upload_dir();
        $attachment = array(
            'guid' => $wp_upload_dir['baseurl'] . _wp_relative_upload_path($upload['file']) ,
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => preg_replace('/\.[^.]+$/', '', basename($upload['file'])) ,
            'post_content' => '',
            'post_status' => 'inherit'
        );
        $attach_id = wp_insert_attachment($attachment, $upload['file'], $post_id);
        require_once (ABSPATH . 'wp-admin/includes/image.php');

        $attach_data1 = wp_generate_attachment_metadata($attach_id, $upload['file']);
        wp_update_attachment_metadata($attach_id, $attach_data1);
        update_post_meta($post_id, '_thumbnail_id', $attach_id);
    }
}

function kaya_pods_post_tags($cpt_slug){
	global $post;
	$terms = wp_get_post_tags( $post->ID );
	echo '<strong>'.__('Tags', 'ppd').':</strong>';
	$i=1;
	$count = count($terms);
	foreach( $terms as $term ){
		$add_coma = ( $i == $count ) ? '' : ', ';
    	echo '<a href="'.esc_url(home_url('/tag/'. strtolower(str_replace(' ', '-', $term->name)))).'?post_type='.$cpt_slug.'">'.$term->name. ' </a>'.$add_coma;
    	$i++;
    }
}

// Widgets cpt meta fields information
function kaya_general_info_section_widget($meta_info='on', $cpt_name = '', $cpt_fields_info='', $style="1"){
	global $kaya_shortlist_options;
	$option_fields = kaya_get_cpt_fields($cpt_name);
	// Post meta information
	if( $meta_info != 'on'){
		 $style_type = ( $style == '1' ) ? 'general-meta-fields-info-wrapper' : 'style2-general-meta-fields-info-wrapper';
		if( !empty($cpt_fields_info) ){
			echo '<div class="'.$style_type.'">';
				echo '<ul>';
					foreach ( $cpt_fields_info as $key => $fields_data) {
						$meta_data = get_post_meta(get_the_ID(), $fields_data, true);
						if( !empty($meta_data) ){
							if( $option_fields[$fields_data]['name'] ==  'age' ){
								if( kaya_age_calculate($meta_data) == '0' ){
									$age = '<1';
								}else{
									$age = kaya_age_calculate($meta_data);
								}
									echo '<li><strong>'.$option_fields[$fields_data]['label'].': &nbsp;</strong> '.$age.'</li>';
								}else{
									echo '<li><strong>'.$option_fields[$fields_data]['label'].': &nbsp;</strong> '.$meta_data.'</li>';
								}
						}
					}
				echo '</ul>';							
			echo '</div>';
		} // End
	}
}

// Woocommerce Enable Admin Acccess for all users
add_filter( 'woocommerce_prevent_admin_access', '__return_false' );
add_filter( 'woocommerce_disable_admin_bar', '__return_false' );

function kaya_dynamic_strings_replace($post, $stings){
	$admin_email = get_option('admin_email');
	$author = get_userdata($post->post_author);
	$stings = str_replace('[user_name]', $author->display_name, $stings);
	$stings = str_replace('[post_title]', $post->post_title, $stings);
	$stings = str_replace('[post_url]', get_permalink( $post->ID ), $stings);
	return stripslashes($stings);
}

//Remove "Wordpress" when receiving email 
function kaya_remove_from_wordpress($email){
	$from = get_option('blogname');
	return $from;
}

function kaya_change_sender_email( $original_email_address ) {
    return get_option('admin_email');
}
 
add_filter( 'wp_mail_from', 'kaya_change_sender_email' );
add_filter('wp_mail_from_name', 'kaya_remove_from_wordpress');


function kaya_cpt_post_types() {
   /* $cpt_post_types = get_post_types(array('public' => true), 'objects');
    $post_type_name = array();

    foreach ($cpt_post_types as $post_type) {
        $post_type_name[$post_type->name] = $post_type->label;
    }*/

     if( !function_exists('pods_api') ){
        return false;
    }
	$post_type_name = array();
	$pods_options = pods_api()->load_pods( array( 'fields' => false ) );
	foreach ($pods_options as $key => $options) {     
	    if( ($options['type'] == 'post_type')){
	    	$post_type_name[$options['name']] = $options['label'];
	    }
    }
    $post_type_name['post'] = __('Post', 'ppd');
    return $post_type_name;
}

function kaya_get_all_metakeys(){
 
    if( !function_exists('pods_api') ){
        return false;
    }
    	$meta_keys = array();
	   	$pods_options = pods_api()->load_pods( array( 'fields' => false ) );
		foreach ($pods_options as $key => $options) {     
		    if( ($options['type'] == 'post_type')){
		    	foreach ($options['fields'] as $key => $fields_val) {
					 $meta_keys[strtolower(str_replace(' ', '_', $fields_val['pod'])).'s'.$fields_val['name']] = ucwords($fields_val['pod']).' - '.$fields_val['label'];
		    	}
				
		    }
		} 
		return $meta_keys;  


}
function kaya_all_taxonomies() {
    global $wpdb;
    $results = array();
    if( !function_exists('pods_api') ){
        return false;
    }

    $pods_options = pods_api()->load_pods( array( 'fields' => false ) );
    if( !empty($pods_options) ){
	    foreach ($pods_options as $key => $options) {  
		     if( $options['type'] == 'taxonomy' ){
	    		$terms_list = get_terms($options['name']);
	    		foreach ( $terms_list as $key => $terms) {
		    		 $results[$options['name'].':'.$terms->slug] = str_replace('_', ' ', ucwords($options['name'])).':'.$terms->name;
	    		}
	    	}
		}
	}
	$post_cats = get_categories();
	foreach ($post_cats as $key => $post_cat) {
		 $results['category:'.$post_cat->slug] = $post_cat->name.' : '.$post_cat->name;
	}
    return $results;
}
// Enable Shortcode to excerpt content
add_filter( 'the_excerpt', 'do_shortcode');

function kaya_post_post_general_info($settings){
    if( !empty($settings['post_meta_keys']) ){
        echo '<div class="post-meta-general-info">';
            foreach ($settings['post_meta_keys'] as $key => $meta_key) {
                 if( !function_exists('pods_api') ){
                    return false;
                }
                $pods_options = pods_api()->load_pods( array( 'fields' => false ) );
                foreach ($pods_options as $key => $options) {     
                    if( ($options['type'] == 'post_type')){
                    	
                    	$replace_str = $options['name'].'s';
                    	$meta_key_replace =  str_replace($replace_str, '', $meta_key);
                    	$meta_data = get_post_meta(get_the_ID(),$meta_key_replace, true);
                        if( !empty($options['fields'][$meta_key_replace]['pod']) ){
                        if( in_array( $options['fields'][$meta_key_replace]['pod'], $settings['post_types']) ){
                            $meta_key_data = ( $meta_key_replace == 'age' ) ? $meta_key_replace.'_filter' : $meta_key_replace;
                            if( !empty($meta_data) ){
                                echo '<span  style="color:'.$settings['post_meta_color'].'"><b>'.$options['fields'][$meta_key_replace]['label'].':</b> '.get_post_meta(get_the_ID(),$meta_key_data, true).'</span>';
                            }
                        }

                    	}
                    }
                }                                    
            }
        echo '</div>';
    }
}

function kaya_post_terms_list($post_types){
    $taxonomies=get_taxonomies('','names');
    return get_the_term_list( get_the_ID(), $taxonomies, '', ', ' );
}
// Restrict the images based on user roles
add_filter( 'ajax_query_attachments_args', 'kaya_current_user_attachments' );
function kaya_current_user_attachments( $query ) {
    $user_id = get_current_user_id();
    if ( $user_id && !current_user_can('activate_plugins') && !current_user_can('edit_others_posts
') ) {
        $query['author'] = $user_id;
    }
    return $query;
} 

// Get the user roles
function kaya_get_user_roles() {
    global $wp_roles;
    $user_role = array();
    $all_roles = $wp_roles->roles;
    $editable_roles = apply_filters('editable_roles', $all_roles);
   foreach ($editable_roles as $key => $role) {
		$user_role[$key] = $role['name'];  
   }
    return $user_role;
}

function kaya_get_current_user_role() {
	global $wp_roles;
	$current_user = wp_get_current_user();
	$roles = $current_user->roles;
	return $role = array_shift($roles);
}

function kaya_get_current_profile_user_role() {
	global $wp_roles;
	$current_user = wp_get_current_user();
	$roles = $current_user->roles;
	return $role = array_shift($roles);
}
//New Tab 1st position of tab section
function kaya_user_custom_profile_tab(){
	global $pods_tabs_data;
	$cpt_meta_fields = kaya_get_cpt_fields($cpt_slug);
	$pod = pods( $cpt_slug, $user);
	$pods_tabs_data = array();
	$kaya_user_settings = get_option('kaya_options');
	$user_role = kaya_get_current_profile_user_role();
	if( !empty($cpt_meta_fields) ){
		$user_all_roles =  kaya_get_user_roles();

		$profile_tab1_restrct_user_role = !empty($kaya_user_settings['profile_tab1_restrct_user_role']) ? $kaya_user_settings['profile_tab1_restrct_user_role'] : array_keys($user_all_roles);

		$choose_role_to_display_profile = !empty($kaya_user_settings['choose_role_to_display_profile']) ? $kaya_user_settings['choose_role_to_display_profile'] : '';

		if(!empty($kaya_user_settings['profile_tab1_title'])){
			echo '<li><a href="#profile_tab1_content">'.$kaya_user_settings['profile_tab1_title'].'</a></li>';
		}
	}
}
//New Function for displaying 1st position tab
function kaya_profile_tab_section(){
	$kaya_user_settings =  get_option( "kaya_options" );
    if( function_exists('kaya_get_current_profile_user_role') ){
        $user_role = kaya_get_current_profile_user_role();
        $user_all_roles =  kaya_get_user_roles();
    }else{
        $user_role = '';
        $user_all_roles = '';
    }
    $profile_tab1_restrct_user_role = !empty($kaya_user_settings['profile_tab1_restrct_user_role']) ? $kaya_user_settings['profile_tab1_restrct_user_role'] : array_keys($user_all_roles);

    $choose_role_to_display_profile = !empty($kaya_user_settings['choose_role_to_display_profile']) ? $kaya_user_settings['choose_role_to_display_profile'] : '';

    // Start Profile New tab1 code
    if( $choose_role_to_display_profile == 'anyone'){    
        if( $kaya_user_settings['profile_tab1_content'] ){
            echo '<div id="profile_tab1_content" class="single-page-meta-content-wrapper">';
            	echo '<h2>';
            		echo $kaya_user_settings['profile_tab1_title'];
            	echo '</h2>';
              echo do_shortcode(stripslashes($kaya_user_settings['profile_tab1_content']));      
            echo '</div>';
        }
    }elseif($choose_role_to_display_profile == 'srole'){
    	    echo '<div id="profile_tab1_content" class="single-page-meta-content-wrapper">';
           if( in_array($user_role,  $profile_tab1_restrct_user_role) ) {
           	echo '<h2>';
            		echo $kaya_user_settings['profile_tab1_title'];
            	echo '</h2>';
               echo do_shortcode(stripslashes($kaya_user_settings['profile_tab1_content']));
        } 
        else{
        	echo '<h2>';
            		echo $kaya_user_settings['profile_tab1_title'];
            	echo '</h2>';
        	echo do_shortcode(stripslashes($kaya_user_settings['tab1_restrict_msg_tab']));
        }
        echo '</div>';
    }else{
       echo '<div id="profile_tab1_content" class="single-page-meta-content-wrapper">';
    	if( is_user_logged_in() ){
    		echo '<h2>';
            		echo $kaya_user_settings['profile_tab1_title'];
            	echo '</h2>';
               echo do_shortcode(stripslashes($kaya_user_settings['profile_tab1_content'])); 
        }else{
        	echo '<h2>';
            		echo $kaya_user_settings['profile_tab1_title'];
            	echo '</h2>';
        	echo do_shortcode(stripslashes($kaya_user_settings['tab1_restrict_msg_tab']));
        }
         echo '</div>';
    }
}

// User tabs section
function kaya_users_tab_section(){
	global $pods_tabs_data;
	$cpt_meta_fields = kaya_get_cpt_fields($cpt_slug);
	$pod = pods( $cpt_slug, $user);
	$pods_tabs_data = array();
	$kaya_user_settings = get_option('kaya_options');

	$user_role = kaya_get_current_user_role();
	if( !empty($cpt_meta_fields) ){
		$user_all_roles =  kaya_get_user_roles();
		$tab1_restrct_user_role = !empty($kaya_user_settings['tab1_restrct_user_role']) ? $kaya_user_settings['tab1_restrct_user_role'] : array_keys($user_all_roles);
		$tab2_restrct_user_role = !empty($kaya_user_settings['tab2_restrct_user_role']) ? $kaya_user_settings['tab2_restrct_user_role'] : array_keys($user_all_roles);
		$tab3_restrct_user_role = !empty($kaya_user_settings['tab3_restrct_user_role']) ? $kaya_user_settings['tab3_restrct_user_role'] : array_keys($user_all_roles);

		$choose_role_to_display1 = !empty($kaya_user_settings['choose_role_to_display1']) ? $kaya_user_settings['choose_role_to_display1'] : '';
		$choose_role_to_display2 = !empty($kaya_user_settings['choose_role_to_display2']) ? $kaya_user_settings['choose_role_to_display2'] : '';
		$choose_role_to_display3 = !empty($kaya_user_settings['choose_role_to_display3']) ? $kaya_user_settings['choose_role_to_display3'] : '';

				//echo '<ul class="tabs_content_wrapper">';
				if(!empty($kaya_user_settings['custom_profile_tab1_title'])){
					echo '<li><a href="#custom_profile_tab1">'.$kaya_user_settings['custom_profile_tab1_title'].'</a></li>';
				}
				if(!empty($kaya_user_settings['custom_profile_tab2_title'])){
					echo '<li><a href="#custom_profile_tab2">'.$kaya_user_settings['custom_profile_tab2_title'].'</a></li>';
				}
				if(!empty($kaya_user_settings['custom_profile_tab3_title'])){
					echo '<li><a href="#custom_profile_tab3">'.$kaya_user_settings['custom_profile_tab3_title'].'</a></li>';
				}
				/*foreach ($cpt_meta_fields as $key => $meta_fields) {
					if(($meta_fields['type'] == 'file') || ($meta_fields['type'] == 'wysiwyg') || ($meta_fields['type'] == 'video') ){
						$data = $pod->display($meta_fields['name']);
						if( !empty($data) ){
							$pods_tabs_data[] = 'true';
		          			echo '<li><a href="#'.$meta_fields['name'].'">'.$meta_fields['label'].'</a></li>';
		          		}
					}
				}*/
			//echo '</ul>';
	}
}

// Tab Content
function kaya_custom_tabs_data(){
    $kaya_user_settings =  get_option( "kaya_options" );
    if( function_exists('kaya_get_current_user_role') ){
        $user_role = kaya_get_current_user_role();
        $user_all_roles =  kaya_get_user_roles();
    }else{
        $user_role = '';
        $user_all_roles = '';
    }
    $tab1_restrct_user_role = !empty($kaya_user_settings['tab1_restrct_user_role']) ? $kaya_user_settings['tab1_restrct_user_role'] : array_keys($user_all_roles);
    $tab2_restrct_user_role = !empty($kaya_user_settings['tab2_restrct_user_role']) ? $kaya_user_settings['tab2_restrct_user_role'] : array_keys($user_all_roles);
    $tab3_restrct_user_role = !empty($kaya_user_settings['tab3_restrct_user_role']) ? $kaya_user_settings['tab3_restrct_user_role'] : array_keys($user_all_roles);
    
    $choose_role_to_display1 = !empty($kaya_user_settings['choose_role_to_display1']) ? $kaya_user_settings['choose_role_to_display1'] : '';
    $choose_role_to_display2 = !empty($kaya_user_settings['choose_role_to_display2']) ? $kaya_user_settings['choose_role_to_display2'] : '';
    $choose_role_to_display3 = !empty($kaya_user_settings['choose_role_to_display3']) ? $kaya_user_settings['choose_role_to_display3'] : '';

    // Start tab1 code
    if( $choose_role_to_display1 == 'anyone'){    
        if( $kaya_user_settings['custom_profile_tab1'] ){
            echo '<div id="custom_profile_tab1" class="single-page-meta-content-wrapper">';
            echo '<h2>';
            	echo $kaya_user_settings['custom_profile_tab1_title'];
            echo '</h2>';
              echo do_shortcode(stripslashes($kaya_user_settings['custom_profile_tab1']));      
            echo '</div>';
        }
    }elseif($choose_role_to_display1 == 'srole'){
    	    echo '<div id="custom_profile_tab1" class="single-page-meta-content-wrapper">';
           if( in_array($user_role,  $tab1_restrct_user_role) ) {
           	echo '<h2>';
            	echo $kaya_user_settings['custom_profile_tab1_title'];
            echo '</h2>';
               echo do_shortcode(stripslashes($kaya_user_settings['custom_profile_tab1']));
        } 
        else{
        	echo '<h2>';
            	echo $kaya_user_settings['custom_profile_tab1_title'];
            echo '</h2>';
        	echo do_shortcode(stripslashes($kaya_user_settings['tab1_restrict_msg']));
        }
        echo '</div>';
    }else{
       echo '<div id="custom_profile_tab1" class="single-page-meta-content-wrapper">';
    	if( is_user_logged_in() ){
    		echo '<h2>';
            	echo $kaya_user_settings['custom_profile_tab1_title'];
            echo '</h2>';
               echo do_shortcode(stripslashes($kaya_user_settings['custom_profile_tab1'])); 
        }else{
        	echo '<h2>';
            	echo $kaya_user_settings['custom_profile_tab1_title'];
            echo '</h2>';
        	echo do_shortcode(stripslashes($kaya_user_settings['tab1_restrict_msg']));
        }
         echo '</div>';
    }

    if( $choose_role_to_display2 == 'anyone'){
        if( $kaya_user_settings['custom_profile_tab2'] ){
            echo '<div id="custom_profile_tab2" class="single-page-meta-content-wrapper">';
            echo '<h2>';
            	echo $kaya_user_settings['custom_profile_tab2_title'];
            echo '</h2>';
                echo do_shortcode(stripslashes($kaya_user_settings['custom_profile_tab2']));  
            echo '</div>';
        }
    }elseif($choose_role_to_display2 == 'srole'){
    	 echo '<div id="custom_profile_tab2" class="single-page-meta-content-wrapper">';
        if( in_array($user_role,  $tab2_restrct_user_role) ) {
        	echo '<h2>';
            	echo $kaya_user_settings['custom_profile_tab2_title'];
            echo '</h2>';
            echo do_shortcode(stripslashes($kaya_user_settings['custom_profile_tab2'])); 
        }else{
        	echo '<h2>';
            	echo $kaya_user_settings['custom_profile_tab2_title'];
            echo '</h2>';
        	echo do_shortcode(stripslashes($kaya_user_settings['tab2_restrict_msg']));
        } 
         echo '</div>';
    }else{
    	echo '<div id="custom_profile_tab2" class="single-page-meta-content-wrapper">';
	        if(is_user_logged_in()){
	        	echo '<h2>';
            	echo $kaya_user_settings['custom_profile_tab2_title'];
            echo '</h2>';
	            echo do_shortcode(stripslashes($kaya_user_settings['custom_profile_tab2']));
	        }else{
	        	echo '<h2>';
            	echo $kaya_user_settings['custom_profile_tab2_title'];
            echo '</h2>';
	        	echo do_shortcode(stripslashes($kaya_user_settings['tab2_restrict_msg']));
	        }
        echo '</div>';
    }

    if( $choose_role_to_display3 == 'anyone'){
       if( $kaya_user_settings['custom_profile_tab3'] ){
            echo '<div id="custom_profile_tab3" class="single-page-meta-content-wrapper">';
            echo '<h2>';
            	echo $kaya_user_settings['custom_profile_tab3_title'];
            echo '</h2>';
                echo do_shortcode(stripslashes($kaya_user_settings['custom_profile_tab3'])); 
            echo '</div>';            
        }
    }elseif($choose_role_to_display3 == 'srole'){
        echo '<div id="custom_profile_tab3" class="single-page-meta-content-wrapper">';    
	        if( in_array($user_role,  $tab3_restrct_user_role) ) {
	        	if( $kaya_user_settings['custom_profile_tab3'] ){
	        		echo '<h2>';
            	echo $kaya_user_settings['custom_profile_tab3_title'];
            echo '</h2>';
	            echo do_shortcode(stripslashes($kaya_user_settings['custom_profile_tab3']));
	            } 
	        }else{
	        	echo '<h2>';
            	echo $kaya_user_settings['custom_profile_tab3_title'];
            echo '</h2>';
	        	echo do_shortcode(stripslashes($kaya_user_settings['tab3_restrict_msg']));
	        } 
        echo '</div>';
    }else{
    	echo '<div id="custom_profile_tab3" class="single-page-meta-content-wrapper">';
	        if( is_user_logged_in() ){
	        	echo '<h2>';
            	echo $kaya_user_settings['custom_profile_tab3_title'];
            echo '</h2>';
	            echo do_shortcode(stripslashes($kaya_user_settings['custom_profile_tab3']));
	        }else{
	        	echo '<h2>';
            	echo $kaya_user_settings['custom_profile_tab3_title'];
            echo '</h2>';
	        	echo do_shortcode(stripslashes($kaya_user_settings['tab3_restrict_msg']));
	        }
        echo '</div>';
    }
}

/**
 * Creating user role
 */
if( !function_exists('kaya_add_user_role') ){
    function kaya_add_user_role(){
        add_role('user', 'User', array(
        'read' => true, // True allows that capability, False specifically removes it.
        'edit_posts' => false,
        'delete_posts' => false,
        'edit_published_posts' => false,
        'upload_files' => true //last in array needs no comma!
    ));
    }
    kaya_add_user_role(); // Creation New User Role
}

add_action( 'admin_init', 'ppd_admin_capabilities');
function ppd_admin_capabilities($cap){
    if( !function_exists('pods_api') ){
        return false;
    }

    $pods_options = pods_api()->load_pods( array( 'fields' => false ) );
    $admins = get_role( 'administrator' );
    foreach ($pods_options as $key => $cpts) {
        if( $cpts['options']['capability_type'] == 'custom' ){
            if($cpts['type'] == 'post_type'){        
                $admins->add_cap( 'create_'.$cpts['options']['capability_type_custom'].'s' );
                $admins->add_cap( 'edit_'.$cpts['options']['capability_type_custom'] ); 
                $admins->add_cap( 'edit_'.$cpts['options']['capability_type_custom'].'s' ); 
                $admins->add_cap( 'edit_others_'.$cpts['options']['capability_type_custom'].'s' ); 
                $admins->add_cap( 'publish_'.$cpts['options']['capability_type_custom'].'s' ); 
                $admins->add_cap( 'read_'.$cpts['options']['capability_type_custom'] ); 
                $admins->add_cap( 'read_private_'.$cpts['options']['capability_type_custom'].'s' ); 
                $admins->add_cap( 'delete_'.$cpts['options']['capability_type_custom'] );
                $admins->add_cap( 'edit_private_'.$cpts['options']['capability_type_custom'].'s' );
                $admins->add_cap( 'edit_published_'.$cpts['options']['capability_type_custom'].'s' );
                $admins->add_cap( 'delete_others_'.$cpts['options']['capability_type_custom'].'s' );
                $admins->add_cap( 'delete_published_'.$cpts['options']['capability_type_custom'].'s' );
                $admins->add_cap( 'delete_private_'.$cpts['options']['capability_type_custom'].'s' );
            }
            if($cpts['type'] == 'taxonomy'){
                $admins->add_cap( 'manage_'.$cpts['options']['capability_type_custom'].'_terms' );
                $admins->add_cap( 'delete_'.$cpts['options']['capability_type_custom'].'_terms' );
                $admins->add_cap( 'delete_'.$cpts['options']['capability_type_custom'].'_terms' );
                $admins->add_cap( 'assign_'.$cpts['options']['capability_type_custom'].'_terms' );
                $admins->add_cap( 'edit_'.$cpts['options']['capability_type_custom'].'_terms' );
            }
        }
        else{
        if($cpts['type'] == 'post_type'){
            $admins->add_cap( 'create_'.$cpts['name'].'s' );
            $admins->add_cap( 'edit_'.$cpts['name'] ); 
            $admins->add_cap( 'edit_'.$cpts['name'].'s' ); 
            $admins->add_cap( 'edit_others_'.$cpts['name'].'s' ); 
            $admins->add_cap( 'publish_'.$cpts['name'].'s' ); 
            $admins->add_cap( 'read_'.$cpts['name'] ); 
            $admins->add_cap( 'read_private_'.$cpts['name'].'s' ); 
            $admins->add_cap( 'delete_'.$cpts['name'] );
            $admins->add_cap( 'edit_private_'.$cpts['name'].'s' );
            $admins->add_cap( 'edit_published_'.$cpts['name'].'s' );
            $admins->add_cap( 'delete_others_'.$cpts['name'].'s' );
            $admins->add_cap( 'delete_published_'.$cpts['name'].'s' );
            $admins->add_cap( 'delete_private_'.$cpts['name'].'s' );
        }
        if($cpts['type'] == 'taxonomy'){
            $admins->add_cap( 'manage_'.$cpts['name'].'_terms' );
            $admins->add_cap( 'delete_'.$cpts['name'].'_terms' );
            $admins->add_cap( 'delete_'.$cpts['name'].'_terms' );
            $admins->add_cap( 'assign_'.$cpts['name'].'_terms' );
            $admins->add_cap( 'edit_'.$cpts['name'].'_terms' );
        }
    }
    }
}

//Nav Menus
function kaya_nav_menu_selction() {
	$menus = wp_get_nav_menus();
	$items = array();
	$i     = 0;
	foreach ( $menus as $menu ) {
		if ( $i == 0 ) {
			$default = $menu->slug;
			$i ++;
		}
		$items[ $menu->slug ] = $menu->name;
	}
	return $items;
}
remove_filter( 'nav_menu_desc', 'strip_tags' );

function get_menus( $object_id, $args = array() ) {
	$menu_item_ids = $this->get_menu_item_ids( $object_id );
	if ( ! $menu_item_ids ) {
		return array();
	}
	$menu_ids = wp_get_object_terms( $menu_item_ids, 'nav_menu', $args );
	if ( ! $menu_ids || is_wp_error( $menu_ids ) ) {
		return array();
	}
	return $menu_ids;
}

function gray_scale_grid_view(){ ?>
	<style>
		.grid-view-container .taxonomy-style, .grid-view-image img {
		    -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
		    filter: grayscale(100%);
		}
		.grid-view-container .taxonomy-style, .grid-view-image img:hover {
		    -webkit-filter: grayscale(0%); /* Safari 6.0 - 9.0 */
		    filter: grayscale(0%);
		}
	</style>
<?php }

function normal_grid_style() { ?>
	<style>
		.grid-view-container .overlay-hd {
		    background-color:rgba(0,0,0,0.25);
		    opacity:1;
		    transition:opacity 0.3s ease-out;
		    position:absolute;
		    top: 0;
		    height:100%;
		    width:100%;
		}
	</style>
<?php }

function talents_single_page_tabs_section_enable(){
    ?>
    <style>
        ul.tabs_content_wrapper{
            display:none;
        }
        .single-page-meta-content-wrapper{
            display:block!important;
        }  
        .single-page-meta-content-wrapper h2, .single-page-meta-content-wrapper h3{
        	font-size: 30px;
        }
        .single-page-meta-content-wrapper h4{
        	font-size: 20px;
        }
    </style>
    <?php
}
function talents_single_page_tabs_section_disable(){
    ?>
    <style>
        ul.tabs_content_wrapper{
                display:block;
            }
        .single-page-meta-content-wrapper{
            display:block;
        }  
        .single-page-meta-content-wrapper h2, .single-page-meta-content-wrapper h3, .single-page-meta-content-wrapper hr{
        	display: none!important;
        }
        .single-page-meta-content-wrapper h4{
        	font-size: 20px;
        }
    </style>
    <?php
}
?>