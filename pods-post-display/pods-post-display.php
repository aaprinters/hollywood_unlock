<?php
/**
 * Plugin Name:Pods - Post Display
 * Plugin URI: http://themeforest.net/user/kayapati
 * Description: Pods Add-on allow you to display posts custom fields data and Taxonomy layout styles like number of column, thumbnail sizes and number of posts to display.
 * Version: 1.2.4
 * Author: Venisha IT Team
 * Author URI: http://themeforest.net/user/kayapati
 * Text Domain: ppd
 * Domain Path: /languages
 */

namespace KayaWidgets;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
/**
* Main PODs CPT Views Classes
*/

if( !class_exists('Kaya_Pods_Post_Display') ){
	class Kaya_Pods_Post_Display{
		function __construct(){
			add_action( 'wp_enqueue_scripts', array( &$this, 'kaya_pods_cpt_enqueue_styles' ) );
			add_action( 'admin_enqueue_scripts', array( &$this, 'kaya_pods_cpt_admin_scripts' ) );
			add_action('plugins_loaded', array(&$this,'kaya_pods_cpt_plugin_textdomain'));
			$this->kaya_pods_cpt_include_files();
			$this->kaya_elementor_add_actions();
			add_action('elementor/editor/before_enqueue_scripts', array(&$this, 'kaya_elementor_front_end_styles'));
			//add_action('after_setup_theme', array(&$this, 'kaya_user_dashboard_menu'));
		}
		/**
		 * Load all files and functions
		 */
		function kaya_pods_cpt_include_files(){
			define( 'KAYA_PCV_PLUGIN_PATH',plugin_dir_path( __FILE__ ) );
			define( 'KAYA_PCV_PLUGIN_URL',plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) );
			require_once plugin_dir_path( __FILE__ ) . 'includes/functions.php';
			require_once plugin_dir_path( __FILE__ ) . 'includes/taxonomy-cpt-fields-settings.php';
			require_once plugin_dir_path( __FILE__ ) . 'widgets/pod-cpt-post-grid-view.php';
			require_once plugin_dir_path( __FILE__ ) . 'widgets/pod-cpt-post-slider.php';
			require_once plugin_dir_path( __FILE__ ) . 'widgets/pod-cpt-front-end-multiple-view-edit-forms.php';
			require_once plugin_dir_path( __FILE__ ) . 'widgets/select-box.php';
			require_once plugin_dir_path( __FILE__ ) . 'widgets/pod-cpt-front-end-single-posting-form.php';
			require_once plugin_dir_path( __FILE__ ) . 'widgets/advanced_search.php';
			require_once plugin_dir_path( __FILE__ ) . 'includes/mr-image-resize.php';
			require_once plugin_dir_path( __FILE__ ) . 'includes/search.php';
			require_once plugin_dir_path( __FILE__ ) . 'includes/search-po.php';
		}
		/**
		 * Loading Front End Css & Js Files
		 */
		public function kaya_pods_cpt_enqueue_styles() {
			wp_enqueue_style('ppd-jquery-ui-css', plugins_url('css/jquery-ui-css.css', __FILE__ ));
			wp_enqueue_script('masonry');
			wp_enqueue_style('ppd-styles', plugins_url('css/styles.css', __FILE__ ));
			wp_enqueue_style('owl.carousel.min', plugins_url('css/owl.carousel.min.css', __FILE__ ));
			wp_enqueue_script( 'owl.carousel.min', plugin_dir_url( __FILE__ ) . 'js/owl.carousel.min.js', array(),'', 'true' );
			wp_enqueue_script('pod-cpt-scripts', plugin_dir_url( __FILE__ ) . 'js/scripts.js', array(),'', 'true' );
			wp_enqueue_style('ppd-responsive', plugins_url('css/responsive.css', __FILE__));
			wp_localize_script( 'jquery', 'kaya_ajax_url', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
			wp_enqueue_script( 'jquery-ui-accordion' );
    		wp_enqueue_script( 'jquery-ui-autocomplete' );
			wp_enqueue_script('jquery-ui-slider');
		}
		
		/**
		 * Loading Admin Css & Js Files
		 */
		public function kaya_pods_cpt_admin_scripts(){
			wp_enqueue_script( 'wp-color-picker' );
			wp_enqueue_script( 'ppd-admin-scripts', plugin_dir_url( __FILE__ ) . 'js/admin-scripts.js', array(),'', 'true' );
			wp_enqueue_style( 'ppd-admin-styles', plugins_url( 'css/admin-styles.css', __FILE__ ));
		}
		
		/** 
		* Load language Translation Text Domain
		*/
	    public  function kaya_pods_cpt_plugin_textdomain() {
	        $locale = apply_filters( 'plugin_locale', get_locale(), 'ppd' );
	        load_textdomain( 'ppd', trailingslashit( WP_LANG_DIR ) . '/' . $locale . '.mo' );
	        load_plugin_textdomain( 'ppd', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
	    }

	    /**
		 * Add Actions
		 *
		 * @since 1.0.0
		 *
		 * @access private
		 */
		private function kaya_elementor_add_actions() {
			add_action( 'elementor/widgets/widgets_registered', [ $this, 'kaya_elementor_widgets_include' ] );
			add_action( 'elementor/frontend/after_register_scripts', function() {
				wp_enqueue_style('elementor-fe-styles', plugins_url('css/elementor-front-end-styles.css', __FILE__));
			} );
		}

		public function kaya_elementor_front_end_styles(){
			wp_enqueue_style('elementor-fe-styles', plugins_url('css/elementor-front-end-styles.css', __FILE__));
		}

		public function kaya_elementor_widgets_include($widgets_manager) {
			require_once KAYA_PCV_PLUGIN_PATH . '/widgets/post-grid-view.php';
			require_once KAYA_PCV_PLUGIN_PATH . '/widgets/post-slider-view.php';
			require_once KAYA_PCV_PLUGIN_PATH . '/widgets/kaya-custom-logo.php';
			require_once KAYA_PCV_PLUGIN_PATH . '/widgets/pods-post-table-view.php';
			require_once KAYA_PCV_PLUGIN_PATH . '/widgets/pods-post-grid-and-table-view.php';
			require_once KAYA_PCV_PLUGIN_PATH . '/widgets/pods-taxonomy-search.php';
			require_once KAYA_PCV_PLUGIN_PATH . '/widgets/kaya-custom-selectbox.php';
			require_once KAYA_PCV_PLUGIN_PATH . '/widgets/kaya-nav-menu.php';
			$widgets_manager->register_widget_type(new \KayaWidgets\Widgets\Kaya_Portfolio_Widget());
			$widgets_manager->register_widget_type(new \KayaWidgets\Widgets\Kaya_Post_Slider_Widget());
			$widgets_manager->register_widget_type(new \KayaWidgets\Widgets\Kaya_Custom_Logo());
			$widgets_manager->register_widget_type(new \KayaWidgets\Widgets\Kaya_Pods_Post_Table_View_Widget());
			$widgets_manager->register_widget_type(new \KayaWidgets\Widgets\Kaya_Pods_Post_Grid_And_Table_View_Widget());
			$widgets_manager->register_widget_type(new \KayaWidgets\Widgets\Kaya_Taxonomies_Search_Widget());
			$widgets_manager->register_widget_type(new \KayaWidgets\Widgets\Kaya_custom_select_box());
			$widgets_manager->register_widget_type(new \KayaWidgets\Widgets\Kaya_Nav_Menu_Widget());
		}
		/**
		 * admin user dashboard menu
		 */
		/*function kaya_user_dashboard_menu(){
			register_nav_menu( 'loggedin-user-dropdown-menu', __( 'User Drop-Down Menu', 'ppd' ) );
		}*/
	}
}

global $kaya_pods_taxonomy;
$kaya_pods_taxonomy = new Kaya_Pods_Post_Display();  ?>