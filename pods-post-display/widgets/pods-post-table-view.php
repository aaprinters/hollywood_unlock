<?php
namespace KayaWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Image_Size;
use Elementor\Utils;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

class Kaya_Pods_Post_Table_View_Widget extends Widget_Base {

    public function get_name() {
        return 'kaya-pods-post-table-view';
    }

    public function get_title() {
        return __('Kaya -Pods Post Table View', 'ppd');
    }

    public function get_icon() {
        return 'eicon-posts-grid';
    }

    public function get_categories() {
        return array('pods-post-view-addons');
    }

    protected function _register_controls() {

        $this->start_controls_section(
            'section_query',
            [
                'label' => __('Post Query', 'ppd'),
            ]
        );

        $this->add_control(
            'post_types',
            [
                'label' => __('Post Types', 'ppd'),
                'type' => Controls_Manager::SELECT2,
                'default' => 'post',
                'options' => kaya_cpt_post_types(),
                'multiple' => true
            ]
        );
        $this->add_control(
            'tax_query',
            [
                'label' => __('Taxonomies', 'ppd'),
                'type' => Controls_Manager::SELECT2,
                'options' => kaya_all_taxonomies(),
                'multiple' => true,
                'label_block' => true
            ]
        );

        $this->add_control(
            'advanced',
            [
                'label' => __('Advanced', 'ppd'),
                'type' => Controls_Manager::HEADING,
            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('Order By', 'ppd'),
                'type' => Controls_Manager::SELECT,
                'options' => array(
                    'none' => __('No order', 'ppd'),
                    'ID' => __('Post ID', 'ppd'),
                    'author' => __('Author', 'ppd'),
                    'title' => __('Title', 'ppd'),
                    'date' => __('Published date', 'ppd'),
                    'rand' => __('Random order', 'ppd'),
                    'menu_order' => __('Menu order', 'ppd'),
                ),
                'default' => 'date',
            ]
        );

        $this->add_control(
            'order',
            [
                'label' => __('Order', 'ppd'),
                'type' => Controls_Manager::SELECT,
                'options' => array(
                    'ASC' => __('Ascending', 'ppd'),
                    'DESC' => __('Descending', 'ppd'),
                ),
                'default' => 'DESC',
            ]
        );

        $this->end_controls_section();
        // Imge settings
        $this->start_controls_section(
            'section_responsive',
            [
                'label' => __('Column Settings', 'ppd'),
            ]
        );

        $this->add_group_control(
            Group_Control_Image_Size::get_type(),
            [
                'name' => 'image_size', // Actually its `image_size`.
                'label' => __( 'Image Size', 'ppd' ),
                'default' => 'medium',
            ]
        );
       
        $this->add_control(
            'posts_per_page',
            [
                'label' => __('Posts Per Page', 'ppd'),
                'type' => Controls_Manager::NUMBER,
                'default' => 12,
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'title_settings',
            [
                'label' => __('Post Title Settings', 'ppd'),
                'tab' => Controls_Manager::TAB_SETTINGS,
            ]
        );

        $this->add_control(
            'title_font_size',
           [
                'label' => __( 'Title Font Size', 'ppd' ),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 20,
                ],
                'range' => [
                    'px' => [
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper th' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'title_font_weight',
            [
                'label' => __( 'Title Font Weight', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'bold' => __( 'Bold', 'ppd' ),
                ],
                'default' => 'normal',
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper th' => 'font-weight:{{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'title_font_style',
            [
                'label' => __( 'Title Font Style', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'italic' => __( 'Italic', 'ppd' ),
                ],
                'default' => 'normal',
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper th' => 'font-style: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'section_grid_thumbnail_styling',
            [
                'label' => __('Post Title', 'ppd'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'grid_posts_title_color',
            [
                'label' => __( 'Title Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#ffffff',
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper th' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'grid_posts_header_BG_color',
            [
                'label' => __( 'Header BG Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#021738',
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper th' => 'background: {{VALUE}};',
                ],
            ]
        );
        $this->end_controls_section();
        // Post Meta List
        $this->start_controls_section(
            'section_post_meta_list',
            [
                'label' => __('CPT Post Meta list', 'ppd'),
            ]
        );

        $this->add_control(
            'post_meta_keys',
            [
                'label' => __('Choose Meta Key Name', 'ppd'),
                'type' => Controls_Manager::SELECT2,
                'options' => kaya_get_all_metakeys(),
                'multiple' => true,
                'label_block' => true
            ]
        ); 

        $this->add_control(
            'meta_data_font_size',
           [
                'label' => __( 'Meta Data Font Size', 'ppd' ),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 13,
                ],
                'range' => [
                    'px' => [
                        'max' => 50,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper td' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'meta_data_font_weight',
            [
                'label' => __( 'Meta Data Font Weight', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'bold' => __( 'Bold', 'ppd' ),
                ],
                'default' => 'bold',
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper td' => 'font-weight:{{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'meta_data_font_style',
            [
                'label' => __( 'Meta Data Font Style', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'italic' => __( 'Italic', 'ppd' ),
                ],
                'default' => 'normal',
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper td' => 'font-style: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'post_meta_color',
            [
                'label' => __( 'Post Meta Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .post-content-table-wrapper td' => 'color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings();
        $taxonomy_cats = !empty($settings['tax_query']) ? $settings['tax_query'] : '';
        $image_sizes = $settings['image_size_custom_dimension'];
        $tax_terms_data = array();
        if( !empty($taxonomy_cats) ){ 
            foreach ($taxonomy_cats as $tax_data) {
                list($tax, $term) = explode(':', $tax_data);
                $tax_terms_data[$tax][] = $term;
            }
        }
        $array_val = array();
            if( !empty($taxonomy_objects) ){
                foreach ($taxonomy_objects as $key => $taxonomies) {
                    $array_val[] = ( !empty( $instance['tax_term'][$taxonomy_objects[$key]] )) ? $instance['tax_term'][$taxonomy_objects[$key]] : '';
                }
            }
            $taxonomy_ids = array();
            if( !empty($array_val[0]) ){
                foreach ($array_val as $key => $taxonomy_array_ids) {
                    if(  !empty( $taxonomy_array_ids) ){
                        $taxonomy_ids = array_merge($taxonomy_ids, array_values($taxonomy_array_ids));
                    }
                }
            }else{
                $taxonomy_ids = '';
            }
        if( !isset($_REQUEST['tax']) ){            
            if( $taxonomy_cats ){
                foreach ($taxonomy_cats as $tax_data) {
                    list($tax, $term) = explode(':', $tax_data);
                    if (!empty($tax) || !empty($term)){
                        $the_taxes[] = array(  
                            'taxonomy' => $tax,
                            'field'    => 'slug',
                            'terms'    => $term,
                        );
                    }
                }
            }
        }else{
           $the_taxes[] = array(  
                'taxonomy' => $_REQUEST['tax'],
                'field'    => 'slug',
                'terms'    => $_REQUEST['slug'],
            );
        }
        global $wp_query, $paged, $post;                
        if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
        elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
        else { $paged = 1; }
        $the_taxes['relation'] = 'OR';
        if( $array_val ) {
            $args1 = array( 'paged' => $paged, 'post_type' => $settings['post_types'], 'orderby' => $settings['orderby'], 'posts_per_page' =>$settings['posts_per_page'],'order' => $settings['order'],  'tax_query' => $the_taxes);
        }else{
            $args1 = array('paged' => $paged, 'taxonomy' => '', 'post_type' => $settings['post_types'], 'orderby' => $settings['orderby'], 'posts_per_page' => $settings['posts_per_page'],'order' => $settings['order'] );
        }
        // Post Loop
        if(is_front_page()) {
            $paged = (get_query_var('page')) ? get_query_var('page') : 1;
        }else {
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        }
        $loop_query = new \WP_Query( array(  
            'post_type' => $settings['post_types'],
            'paged' =>$paged,
            'posts_per_page' =>  $settings['posts_per_page'],
            'orderby' => $settings['orderby'],
            'order' => $settings['order'],
            'tax_query' => $the_taxes,
        ));
        global $wp_query;
        // Put default query object in a temp variable
        $tmp_query = $wp_query;
        // Now wipe it out completely
        $wp_query = null;
        // Re-populate the global with our custom query
        $wp_query = $loop_query;
        if( function_exists('gray_scale_grid_view')){
            if(!empty($kaya_options->gray_scale_mode) ){
                echo gray_scale_grid_view();
            }
            else{
                echo normal_grid_style();
            }
        }
        echo '<div class="post-content-table-wrapper">';
            echo '<table>';
                echo '<tr>';
                    echo '<th>Portfolio</th>';
                        foreach ($settings['post_meta_keys'] as $key => $meta_key) {
                            if( !function_exists('pods_api') ){
                                return false;
                            }
                            $pods_options = pods_api()->load_pods( array( 'fields' => false ) );
                            foreach ($pods_options as $key => $options) {     
                                if( ($options['type'] == 'post_type')){
                                    $replace_str = $options['name'].'s';
                                    $meta_key_replace =  str_replace($replace_str, '', $meta_key);
                                    $meta_data = get_post_meta(get_the_ID(),$meta_key_replace, true);
                                    if( !empty($options['fields'][$meta_key_replace]['pod']) ){
                                        if( in_array( $options['fields'][$meta_key_replace]['pod'], $settings['post_types']) ){
                                            $meta_key_data = ( $meta_key_replace == 'age' ) ? $meta_key_replace.'_filter' : $meta_key_replace;
                                            if( !empty($options['fields'][$meta_key_replace]['pod']) ){
                                             echo '<th>'.$options['fields'][$meta_key_replace]['label'].'</th>';  
                                             
                                            }
                                            else{
                                                echo '<th style="text-align:center">---</th>';
                                            }
                                        }

                                    }
                                }
                            }                                    
                        }
                echo '</tr>';
                    if ( $loop_query->have_posts() ) : 
                        while ( $loop_query->have_posts() ) : $loop_query->the_post();
                            echo '<tr>';
                                $image_sizes = ( $settings['image_size_size'] == 'custom' ) ? array_values($settings['image_size_custom_dimension']) : $settings['image_size_size'];
                                //echo '<td><a href="' .get_post_meta($get_the_ID(), 'first_name', true).'"</a></td>';
                                    echo '<td>';
                                        echo '<a href="'.get_the_permalink().'">';
                                            echo kaya_pod_featured_img($image_sizes, $settings['image_size_size']);
                                        echo '</a>';
                                    echo '</td>';
                                    echo '<div class="title-meta-data-wrapper">';
                                        //echo '<td>'.get_the_title().'</td>';
                                            if( !empty($settings['post_meta_keys']) ){
                                                echo '<div class="post-meta-general-info">';
                                                    foreach ($settings['post_meta_keys'] as $key => $meta_key) {
                                                        if( !function_exists('pods_api') ){
                                                            return false;
                                                        }
                                                        $pods_options = pods_api()->load_pods( array( 'fields' => false ) );
                                                        foreach ($pods_options as $key => $options) {     
                                                            if( ($options['type'] == 'post_type')){
                                                                $replace_str = $options['name'].'s';
                                                                $meta_key_replace =  str_replace($replace_str, '', $meta_key);
                                                                $meta_data = get_post_meta(get_the_ID(),$meta_key_replace, true);
                                                                if( !empty($options['fields'][$meta_key_replace]['pod']) ){
                                                                    if( in_array( $options['fields'][$meta_key_replace]['pod'], $settings['post_types']) ){
                                                                        $meta_key_data = ( $meta_key_replace == 'age' ) ? $meta_key_replace.'_filter' : $meta_key_replace;
                                                                        if( !empty($meta_data) ){
                                                                            echo '<td>'.get_post_meta(get_the_ID(),$meta_key_data, true).'</td>';  
                                                                        }
                                                                        else{
                                                                            echo '<td style="text-align:center">---</td>';
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }                                    
                                                    }
                                                echo '</div>';
                                            }
                                    echo '</div>';
                            echo '</tr>';
                        endwhile;
                    endif;
            echo '</table>';
                if(function_exists('kaya_pagination')){
                    echo kaya_pagination();
                }
                wp_reset_postdata();
        echo '</div>';
            $wp_query = null;
            $wp_query = $tmp_query;
        }

    protected function content_template() {
    }

}