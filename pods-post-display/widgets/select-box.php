<?php
class Kaya_Select_Box extends WP_Widget{
	public function __construct(){
		parent::__construct('kaya-select-box',__('Kaya - Select Box','ppd'),
			array('description1' => __('Use this widget to add  cpt posts as a grid view','ppd'))
		);
	}
	public function widget( $args,$instance){
		$instance = wp_parse_args($instance,array(
			'title1' => 'Title1',
			'description1' => 'Description 1',
			'title2' => 'title 2',
			'description2' => 'Description 2',
		));
		
		echo '<div class="selcet-box-wraper">';
			echo '<select id="option-select">';
				echo '<option value="title1">'.$instance['title1'].'</option>';
				echo '<option value="title2">'.$instance['title2'].'</option>';
			echo '</select>';
			echo '<div class="description-wrapper title1">';
				echo do_shortcode($instance['description1']);
			echo '</div>';
			echo '<div class="description-wrapper title2">';
				echo do_shortcode($instance['description2']);
			echo '</div>';
		echo '</div>';

	}
	public function form($instance){
		$instance = wp_parse_args($instance,array(
			'title1' => 'title1',
			'description1' => 'description1',
			'title1' => 'Title1',
			'description1' => 'Description 1',
			'title2' => 'title 2',
			'description2' => 'Description 2',
		)); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title1') ?>">  <?php _e('Title1', 'ppd') ?>  </label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id("title1"); ?>" name="<?php echo $this->get_field_name("title1"); ?>" value="<?php echo $instance["title1"]; ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('description1') ?>">  <?php _e('Description1', 'ppd') ?>  </label>
			<textarea class="widefat" id="<?php echo $this->get_field_id("description1"); ?>" name="<?php echo $this->get_field_name("description1"); ?>" ><?php echo $instance["description1"]; ?> </textarea>
		  	
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('title2') ?>">  <?php _e('Title2', 'ppd') ?>  </label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id("title2"); ?>" name="<?php echo $this->get_field_name("title2"); ?>" value="<?php echo $instance["title2"]; ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('description2') ?>">  <?php _e('Description2', 'ppd') ?>  </label>
			<textarea class="widefat" id="<?php echo $this->get_field_id("description2"); ?>" name="<?php echo $this->get_field_name("description2"); ?>" ><?php echo $instance["description2"]; ?> </textarea>
		  	
		</p>
	<?php	

	}
}
add_action('widgets_init',  function(){
	register_widget( 'Kaya_Select_Box' );
});

?>