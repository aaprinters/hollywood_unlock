<?php
namespace KayaWidgets\Widgets;
use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Utils;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Kaya_Custom_Logo extends Widget_Base {
	public function get_name() {
        return 'kaya-custom-logo';
    }
    public function get_title() {
        return __('Kaya - Custom Logo', 'ppd');
    }
    public function get_icon() {
        return 'eicon-banner';
    }
    protected function _register_controls() {
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Custom Logo', 'ppd' ),
			]
		);

		$this->add_control(
			'logo_type',
			[
				'label'       => __( 'Choose Logo Type', 'ppd' ),
				//'description' => __( 'Your theme must declare the "add_theme_support( \'custom-logo\')" for the logo to work', 'ppd' ),
				'type'        => Controls_Manager::SELECT, 'options' => [
					'logo'  => __( 'Logo', 'ppd' ),
					'title' => __( 'Title', 'ppd' ),					
				],
				'default'     => 'logo',
			]
		);

		$this->add_control(
			'custom_logo_upload',
			[
				'label' => __( 'Upload Custom Logo', 'ppd' ),
				'type' => Controls_Manager::MEDIA,
				'condition' => [
					'logo_type' => 'logo',
				],
				'default' => [
                    'url' => get_template_directory_uri().'/images/logo.png',
                ], 
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'        => __( 'Alignment', 'ppd' ),
				'type'         => Controls_Manager::CHOOSE,
				'options'      => [
					'left'   => [
						'title' => __( 'Left', 'ppd' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'ppd' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => __( 'Right', 'ppd' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .site-content-wrapper' => 'text-align: {{VALUE}}; float:{{ VALUE }}',
					'{{WRAPPER}} .site-content-wrapper figure img' => 'margin: 0px auto',
				],
			]
		);

		$this->end_controls_section();
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Title Settings', 'ppd' ),
				'condition' => [
					'logo_type' => 'title',
				],
			]
		);
		$this->add_control(
			'title_color',
			[
				'label'     => __( 'Title Color', 'ppd' ),
				'type'      => Controls_Manager::COLOR,

				'condition' => [
					'logo_type' => 'title',
				],
				'default'   => '#333333',
				'selectors' => [
					'{{WRAPPER}} .site-content-wrapper .site-title a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'title_hover_color',
			[
				'label'     => __( 'Hover', 'ppd' ),
				'type'      => Controls_Manager::COLOR,
				'condition' => [
					'logo_type' => 'title',
				],
				'selectors' => [
					'{{WRAPPER}} .site-content-wrapper .site-title a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'      => 'title_typography',
				'label'     => __( 'Typography', 'ppd' ),
				'condition' => [
					'logo_type' => 'title',
				],
				'scheme'    => Scheme_Typography::TYPOGRAPHY_1,
				'selector'  => '{{WRAPPER}} .site-content-wrapper .site-title a',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_desc_style',
			[
				'label'     => __( 'Description Settings', 'ppd' ),
				'condition' => [
					'logo_type' => 'title',
				],
			]
		);

		$this->add_control(
			'description_color',
			[
				'label'     => __( 'Description Color', 'ppd' ),
				'type'      => Controls_Manager::COLOR,
				'condition' => [
					'logo_type' => 'title',
				],
				'selectors' => [
					'{{WRAPPER}} .site-content-wrapper .site-description' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'      => 'desc_typography',
				'label'     => __( 'Typography', 'ppd' ),
				'condition' => [
					'logo_type' => 'title',
				],
				'scheme'    => Scheme_Typography::TYPOGRAPHY_1,
				'selector'  => '{{WRAPPER}} .site-content-wrapper .site-description',
			]
		);

		$this->end_controls_section();
	}

	protected function render_title() {
	?>
		<span class="site-title">
			<?php
				$title = get_bloginfo( 'name' );
			?>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( $title ); /* WPCS: xss ok. */ ?>" alt="<?php echo esc_attr( $title ); ?>">
					<?php bloginfo( 'name' ); ?>
				</a>		
		</span>
		<?php
			$description = get_bloginfo( 'description', 'display' );
		if ( $description || is_customize_preview() ) :
		?>
			<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
		<?php
		endif;
	}

	protected function render_logo() {
		$image = $this->get_settings( 'custom_logo_upload' );
		$settings = $this->get_settings();?>

		<figure class="custom-logo">
			<?php
             if (!empty($image['url'])){
        	    echo '<a href="'.get_the_permalink().'"><img src="'.$image['url'].'"></a>';
        	}
            else{
            	echo '<a href="'.get_the_permalink().'"><img src='.get_template_directory_uri().'/images/logo.png></a>';
            }?>
        </figure>
        <?php
	}

	protected function render() {
		$settings = $this->get_settings();?>
		<div class="site-content-wrapper">
			<div class="header-title">
				<?php if ( $settings['logo_type'] == 'title' ) {
						$this->render_title();
					} elseif ( $settings['logo_type'] == 'logo' ) {
						$this->render_logo();
					} 
				?>
			</div>
		</div>
		<?php
	}

	protected function _content_template() {}
}