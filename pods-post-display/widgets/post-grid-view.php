<?php
namespace KayaWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Image_Size;
use Elementor\Utils;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly


class Kaya_Portfolio_Widget extends Widget_Base {

    public function get_name() {
        return 'kaya-post-grid-view';
    }

    public function get_title() {
        return __('Kaya - Post Grid View', 'ppd');
    }

    public function get_icon() {
        return 'eicon-posts-grid';
    }

    public function get_categories() {
        return array('pods-post-view-addons');
    }

    protected function _register_controls() {

        $this->start_controls_section(
            'section_query',
            [
                'label' => __('Post Query', 'ppd'),
            ]
        );

        $this->add_control(
            'post_types',
            [
                'label' => __('Post Types', 'ppd'),
                'type' => Controls_Manager::SELECT2,
                'default' => 'post',
                'options' => kaya_cpt_post_types(),
                'multiple' => true
            ]
        );
        $this->add_control(
            'tax_query',
            [
                'label' => __('Taxonomies', 'ppd'),
                'type' => Controls_Manager::SELECT2,
                'options' => kaya_all_taxonomies(),
                'multiple' => true,
                'label_block' => true
            ]
        );

        $this->add_control(
            'advanced',
            [
                'label' => __('Advanced', 'ppd'),
                'type' => Controls_Manager::HEADING,
            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('Order By', 'ppd'),
                'type' => Controls_Manager::SELECT,
                'options' => array(
                    'none' => __('No order', 'ppd'),
                    'ID' => __('Post ID', 'ppd'),
                    'author' => __('Author', 'ppd'),
                    'title' => __('Title', 'ppd'),
                    'date' => __('Published date', 'ppd'),
                    'rand' => __('Random order', 'ppd'),
                    'menu_order' => __('Menu order', 'ppd'),
                ),
                'default' => 'date',
            ]
        );

        $this->add_control(
            'order',
            [
                'label' => __('Order', 'ppd'),
                'type' => Controls_Manager::SELECT,
                'options' => array(
                    'ASC' => __('Ascending', 'ppd'),
                    'DESC' => __('Descending', 'ppd'),
                ),
                'default' => 'DESC',
            ]
        );

        $this->end_controls_section();

        // Imge settings

        $this->start_controls_section(
            'section_responsive',
            [
                'label' => __('Column Settings', 'ppd'),
            ]
        );

        $this->add_group_control(
            Group_Control_Image_Size::get_type(),
            [
                'name' => 'image_size', // Actually its `image_size`.
                'label' => __( 'Image Size', 'ppd' ),
                'default' => 'medium',
            ]
        );
       

         $this->add_control(
            'per_line',
            [
                'label' => __('Columns', 'ppd'),
                'type' => Controls_Manager::NUMBER,
                'min' => 1,
                'max' => 8,
                'step' => 1,
                'default' => 4,
            ]
        );

        $this->add_control(
            'posts_per_page',
            [
                'label' => __('Posts Per Page', 'ppd'),
                'type' => Controls_Manager::NUMBER,
                'default' => 12,
            ]
        );

        $this->add_control(
            'gutter',
            [
                'label' => __('Disable Gutter', 'ppd'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('Yes', 'ppd'),
                'label_off' => __('No', 'ppd'),
                'return_value' => 'yes',
                'default' => 'no',
            ]
        );

        $this->end_controls_section();


        $this->start_controls_section(
            'section_settings',
            [
                'label' => __('Filter Tabs Settings', 'ppd'),
            ]
        );


        $this->add_control(
            'filterable',
            [
                'label' => __('Enable Filter Tabs', 'ppd'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('Yes', 'ppd'),
                'label_off' => __('No', 'ppd'),
                'return_value' => 'yes',
                'default' => 'no',
            ]
        );

        $this->add_control(
            'choose_filter_pods_tab_text',
            [
                'label' => __('Choose Filter Tab Text', 'ppd'),
                'type' => Controls_Manager::TEXT,
                'default' => '',
            ]
        );

         $this->add_responsive_control(
            'filter_tab_align',
            [
                'label' => __( 'Alignment', 'ppd' ),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __( 'Left', 'ppd' ),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'ppd' ),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'ppd' ),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .filtertabs ul' => 'text-align: {{VALUE}};',
                ],
            ]
        );


        $this->add_control(
            'enable_right_border',
            [
                'label' => __('Tabs Menu Divider', 'ppd'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('Yes', 'ppd'),
                'label_off' => __('No', 'ppd'),
                'return_value' => 'yes',
                'default' => 'no',
            ]
        );

        $this->add_control(
            'filter_tab_message',
            [
                'type' => Controls_Manager::RAW_HTML,
                'raw' => __( 'Note: You have to select \'taxonomies\' in order to display the filter tabs.', 'ppd' ),
                'content_classes' => 'elementor-panel-alert',
            ]
        );

        $this->end_controls_section();

         $this->start_controls_section(
            'title_settings',
            [
                'label' => __('Post Title Settings', 'ppd'),
                'tab' => Controls_Manager::TAB_SETTINGS,
            ]
        );


         $this->add_control(
            'title_font_size',
           [
                'label' => __( 'Title Font Size', 'ppd' ),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 20,
                ],
                'range' => [
                    'px' => [
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper h3, .talent_title h4 a' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'title_font_weight',
            [
                'label' => __( 'Title Font Weight', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'bold' => __( 'Bold', 'ppd' ),
                ],
                'default' => 'normal',
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper h3, .talent_title h4 a' => 'font-weight:{{VALUE}};',
                ],
            ]
        );

         $this->add_control(
            'title_font_style',
            [
                'label' => __( 'Title Font Style', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'italic' => __( 'Italic', 'ppd' ),
                ],
                'default' => 'normal',
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper h3, .talent_title h4 a' => 'font-style: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_section();

        // Filter Tabs
        $this->start_controls_section(
            'section_filters_styling',
            [
                'label' => __('Grid Filters', 'ppd'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'filter_tab_bg_color',
            [
                'label' => __( 'Filter Tabs BG Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .filtertabs ul li a' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'filter_tab_color',
            [
                'label' => __( 'Filter Tabs Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .filtertabs ul li a' => 'color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'filter_tab_active_bg_color',
            [
                'label' => __( 'Filter Tab Active BG Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .filtertabs ul li a.active, {{WRAPPER}} .filtertabs ul li a:hover,  {{WRAPPER}} .filter-tabs li.current a' => 'background: {{VALUE}};',
                     '{{WRAPPER}} .filter-tabs li.current a:after, {{WRAPPER}} .filter-tabs li a:hover:after' => 'border-top-color: {{VALUE}};'
                ],
            ]
        );

        $this->add_control(
            'filter_tab_active_color',
            [
                'label' => __( 'Filter Tab Active Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .filtertabs ul li a.active, {{WRAPPER}} .filtertabs > ul > li > a:hover, {{WRAPPER}} .filter-tabs li.current a' => 'color: {{VALUE}}!important;',
                ],
            ]
        );

         
         $this->add_control(
            'filter_tab_submenu_bg_color',
            [
                'label' => __( 'Filter Tabs Sub Menu BG Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} ul.filtersubmenu li a' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'filter_tab_submenu_color',
            [
                'label' => __( 'Filter Tabs Sub Menu Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} ul.filtersubmenu li a' => 'color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'filter_tab_submenu_active_bg_color',
            [
                'label' => __( 'Filter Tab Sub Menu Active BG Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} ul.filtersubmenu li.current a, {{WRAPPER}} ul.filtersubmenu li a:hover' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'filter_tab_submenu_active_color',
            [
                'label' => __( 'Filter Tab Sub Menu Active Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} ul.filtersubmenu li.current a, {{WRAPPER}} ul.filtersubmenu li a:hover ' => 'color: {{VALUE}}!important;',
                ],
            ]
        );
        $this->end_controls_section();
        $this->start_controls_section(
            'section_post_meta_fields',
            [
                'label' => __('Post Meta Fields', 'ppd'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'post_meta_color',
            [
                'label' => __( 'Post Meta Hover Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper span' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->add_control(
            'post_meta_bg_color',
            [
                'label' => __( 'Post Meta Background Hover Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .title-meta-data-wrapper' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'section_grid_thumbnail_styling',
            [
                'label' => __('Post Title', 'ppd'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'grid_posts_title_color',
            [
                'label' => __( 'Title Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper h3 a, .post-content-wrapper h3, {{WRAPPER}} .post-content-wrapper .title-category-wrapper p, .talent_title h4 a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'title_hover_border_color',
            [
                'label' => __( 'Title Hover Color', 'ppd' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper h3 a:hover, .grid-view-container a:hover h3, .talent_title h4 a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );
        
        $this->end_controls_section();

        // Post Meta List
        $this->start_controls_section(
            'section_post_meta_list',
            [
                'label' => __('CPT Post Meta list', 'ppd'),
            ]
        );

        $this->add_control(
            'post_meta_keys',
            [
                'label' => __('Choose Meta Key Name', 'ppd'),
                'type' => Controls_Manager::SELECT2,
                'options' => kaya_get_all_metakeys(),
                'multiple' => true,
                'label_block' => true
            ]
        );

         $this->add_control(
            'meta_data_font_size',
           [
                'label' => __( 'Meta Data Font Size', 'ppd' ),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 13,
                ],
                'range' => [
                    'px' => [
                        'max' => 50,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .general-meta-fields-info-wrapper span' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'meta_data_font_weight',
            [
                'label' => __( 'Meta Data Font Weight', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'bold' => __( 'Bold', 'ppd' ),
                ],
                'default' => 'bold',
                'selectors' => [
                    '{{WRAPPER}} .general-meta-fields-info-wrapper span b' => 'font-weight:{{VALUE}};',
                ],
            ]
        );

         $this->add_control(
            'meta_data_font_style',
            [
                'label' => __( 'Meta Data Font Style', 'ppd' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'normal' => __( 'Normal', 'ppd' ),
                    'italic' => __( 'Italic', 'ppd' ),
                ],
                'default' => 'normal',
                'selectors' => [
                    '{{WRAPPER}} .post-content-wrapper  .general-meta-fields-info-wrapper span' => 'font-style: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_section();

    }
    protected function render() {
        $settings = $this->get_settings();
        $taxonomy_cats = !empty($settings['tax_query']) ? $settings['tax_query'] : '';
        $image_sizes = $settings['image_size_custom_dimension'];
       
         if( $settings['enable_right_border'] == 'yes' ){   ?>
            <style>
                .filtertabs ul li a::before{
                    display: none;
                }
            </style>
        <?php  }
        $tax_terms_data = array();
        if( !empty($taxonomy_cats) ){ 
            foreach ($taxonomy_cats as $tax_data) {
                list($tax, $term) = explode(':', $tax_data);
                $tax_terms_data[$tax][] = $term;
            }
        }
        // Post Filter Tabs
        if( $settings['filterable'] == 'yes' ){
            echo '<div class="filtertabs filtertab_'.$settings['filter_tab_align'].'">';
                // echo '<div class="filter" id="filter" >';
                        if( count($settings['post_types']) > 1 ){
                            echo '<ul>';
                            if( isset($_REQUEST['cpt']) ){
                                 $current_cpt = '';
                            }else{
                                  $current_cpt = 'active';
                            }
                            if(!empty($settings['choose_filter_pods_tab_text'])){
                                echo '<li class="cpt_categories_wrapper"><a href="'.get_the_permalink().'" class="'.$current_cpt.'">'.(!empty($settings['choose_filter_pods_tab_text']) ? $settings['choose_filter_pods_tab_text'] : __('All', 'ppd')).'</a></li>';
                            }
                            foreach ($settings['post_types'] as $key => $post_type) {
                                $obj = get_post_type_object( $post_type );
                                $taxonomy_data = get_object_taxonomies( $post_type );
                                if( isset($_REQUEST['cpt']) ){
                                   $current_cpt = ($_REQUEST['cpt'] == $post_type) ? 'active' : '';
                                }else{
                                    $current_cpt = '';
                                }
                                foreach ($taxonomy_data as $key => $taxonomy) {
                                  if( !empty($taxonomy) ){
                                         $taxonomy_terms = get_terms($taxonomy);
                                    echo '<ul class="filtersubmenu sub-menu">';
                                        foreach ($taxonomy_terms as $key => $tax_term) {
                                            if( isset($_REQUEST['tax']) ){
                                                $current = ($_REQUEST['slug'] == $tax_term->slug) ? 'current' : '';
                                            }else{
                                                $current = '';
                                            }
                                            if( !empty($tax_terms_data[$taxonomy]) ){
                                                if( !empty( $tax_terms_data[$taxonomy] ) && in_array( $tax_term->slug, $tax_terms_data[$taxonomy] ) ){
                                                    echo '<li class="tax-'.$tax_term->term_id.' '.$current.'"><a href="'.get_the_permalink().'?cpt='.$post_type.'&tax='.$taxonomy.'&slug='.$tax_term->slug.'">'.$tax_term->name.'</a></li>';
                                                }
                                            }else{
                                                 echo '<li class="tax-'.$tax_term->term_id.' '.$current.'"><a href="'.get_the_permalink().'?cpt='.$post_type.'&tax='.$taxonomy.'&slug='.$tax_term->slug.'">'.$tax_term->name.'</a></li>';
                                            }
                                         }
                                    echo '</ul>';
                                    }
                                }                        

                                echo '</li>';
                            }
                        }else{
                            echo '<ul class="filter-tabs">';
                             if( isset($_REQUEST['tax']) ){
                                 $current_cpt = '';
                            }else{
                                  $current_cpt = 'current';
                            }
                            if( count($tax_terms_data) > 0 ){
                                if(!empty($settings['choose_filter_pods_tab_text'])){
                                    echo '<li class="cpt_categories_wrapper '.$current_cpt.'"><a href="'.get_the_permalink().'" class="'.$current_cpt.'">'.(!empty($settings['choose_filter_pods_tab_text']) ? $settings['choose_filter_pods_tab_text'] : __('All', 'ppd')).'</a></li>';
                                }
                                foreach ($tax_terms_data as $tax_name => $terms) {                                
                                    foreach ($terms as $key => $term) {                                    
                                        if( isset($_REQUEST['tax']) ){
                                            $current = ($_REQUEST['slug'] == $term) ? 'current' : '';
                                        }elseif(empty($settings['choose_filter_pods_tab_text'])){
                                            $tax_data_first_ele = explode(':', $settings['tax_query'][0]);
                                            $current = $current = ($tax_data_first_ele['1'] == $term) ? 'current' : '';
                                        }else{
                                            $current = '';
                                        }
                                        $tax_terms_list = get_term_by( 'slug', $term, $tax_name );
                                        echo '<li class="tax-'.$tax_terms_list->term_id.' '.$current.'"><a href="'.get_the_permalink().'/?tax='.$tax_name.'&slug='.$tax_terms_list->slug.'">'.$tax_terms_list->name.'</a> </li>';
                                    }                              
                                } 
                            }
                              echo '</ul>';
                        }
                    echo '</ul>';
              //  echo '</div>';
            echo '</div>';
        }
        $array_val = array();
            if( !empty($taxonomy_objects) ){
                foreach ($taxonomy_objects as $key => $taxonomies) {
                    $array_val[] = ( !empty( $instance['tax_term'][$taxonomy_objects[$key]] )) ? $instance['tax_term'][$taxonomy_objects[$key]] : '';
                }
            }
            $taxonomy_ids = array();
            if( !empty($array_val[0]) ){
                foreach ($array_val as $key => $taxonomy_array_ids) {
                    if(  !empty( $taxonomy_array_ids) ){
                        $taxonomy_ids = array_merge($taxonomy_ids, array_values($taxonomy_array_ids));
                    }
                }
            }else{
                $taxonomy_ids = '';
            }
        if( !isset($_REQUEST['tax']) ){
  
            if( $taxonomy_cats ){
                foreach ($taxonomy_cats as $tax_data) {
                    if(!empty($settings['choose_filter_pods_tab_text'])){
                        list($tax, $term) = explode(':', $tax_data);
                    }else{
                         $tax_data_first_ele = explode(':', $settings['tax_query'][0]);
                         $tax = $tax_data_first_ele[0];
                        $term = $tax_data_first_ele[1];
                    }
                    if (!empty($tax) || !empty($term)){
                        $the_taxes[] = array(  
                            'taxonomy' => $tax,
                            'field'    => 'slug',
                            'terms'    => $term,
                        );
                    }
                }
            }else{
               
            }

        }else{
           $the_taxes[] = array(  
                'taxonomy' => $_REQUEST['tax'],
                'field'    => 'slug',
                'terms'    => $_REQUEST['slug'],
            );
        }
        global $wp_query, $paged, $post;                
        if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
        elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
        else { $paged = 1; }
        $the_taxes['relation'] = 'OR';
        if( $array_val ) {
            $args1 = array( 'paged' => $paged, 'post_type' => $settings['post_types'], 'orderby' => $settings['orderby'], 'posts_per_page' =>$settings['posts_per_page'],'order' => $settings['order'],  'tax_query' => $the_taxes);
        }else{
            $args1 = array('paged' => $paged, 'taxonomy' => '', 'post_type' => $settings['post_types'], 'orderby' => $settings['orderby'], 'posts_per_page' => $settings['posts_per_page'],'order' => $settings['order'] );
        }
        // Post Loop
        if(is_front_page()) {
            $paged = (get_query_var('page')) ? get_query_var('page') : 1;
        }else {
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        }
        $loop_query = new \WP_Query( array(  
            'post_type' => $settings['post_types'],
            'paged' =>$paged,
            'posts_per_page' =>  $settings['posts_per_page'],
            'orderby' => $settings['orderby'],
            'order' => $settings['order'],
            'tax_query' => $the_taxes,
        ));
        global $wp_query;
        // Put default query object in a temp variable
        $tmp_query = $wp_query;
        // Now wipe it out completely
        $wp_query = null;
        // Re-populate the global with our custom query
        $wp_query = $loop_query;
        echo '<div class="post-content-wrapper">';
        $gutter = ($settings['gutter'] == 'yes') ? 'gutter' : '';
        if ( $loop_query->have_posts() ) : 
            echo '<ul class="column-extra '.$gutter.'">';
                $template_file = locate_template( 'elementor-post-widget-styles.php' );
                 while ( $loop_query->have_posts() ) : $loop_query->the_post();
                // Seesion Data, storing cpt post short list IDS only
                    if(isset($_SESSION['shortlist'])) {
                        if ( in_array(get_the_ID(), $_SESSION['shortlist']) ) {
                            $selected = 'item_selected';
                        }else{
                            $selected = '';
                        }
                    }else{
                        $selected = '';
                    }
                    echo '<li class="column'. $settings['per_line'].' item all" id="'.get_the_ID().'">';
                    if( function_exists('gray_scale_grid_view')){
                        if(!empty($kaya_options->gray_scale_mode) ){
                            echo gray_scale_grid_view();
                        }
                        else{
                            echo normal_grid_style();
                        }
                    }
                     // Display thumbnail if enabled              
                        if( $template_file ){
                           include $template_file;
                        }else{
                            include KAYA_PCV_PLUGIN_PATH.'templates/elementor-post-widget-styles.php';
                        }
                    echo '</li>';
                endwhile;
            endif; 
            echo '</ul>'; 
        if(function_exists('kaya_pagination')){
            echo kaya_pagination();
        }
            wp_reset_postdata(); 
            echo '</div>';
        $wp_query = null;
        $wp_query = $tmp_query;
    }

    protected function content_template() {
    }
}