<?php 
function kaya_users_search_query(){
	if( is_search()){
		add_action( 'pre_user_query', function( $user_query ) {
		  $user_query->query_fields = 'SQL_CALC_FOUND_ROWS DISTINCT ' . $user_query->query_fields;
		} );

		add_filter( 'found_users_query', function( $sql ) {
		  return 'SELECT FOUND_ROWS()';
		} );

		$user_meta_fields = kaya_get_user_fields('user');
		$limit=!empty($_REQUEST['limit']) ? $_REQUEST['limit'] : '100';// total no of author to display
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        if($paged==1){
            $offset=0;  
        }else {
            $offset= ($paged-1)*$limit;
        }
        $user_data =  unserialize(stripslashes($_GET['user_data'])); 
		$args = array(
			'number' => $limit,
			'paged' =>$paged,
			'role__in' => !empty($_REQUEST['user_roles']) ? $_REQUEST['user_roles'] : array(),
			'relation' => 'OR', 
		 );

		foreach ($user_meta_fields as $key => $user_meta_data) {
			if( $user_meta_data['type'] == 'text' ){
				if( !empty($_REQUEST[$user_meta_data['name']]) ){
					$args['meta_query'][$user_meta_data['name']] = array(
						'key' => $user_meta_data['name'],
						'value' => $_GET[$user_meta_data['name']],
						'compare' => 'LIKE',
					);
				}
			}elseif( $user_meta_data['type'] == 'number' ){
				
				if( !empty($_GET[$user_meta_data['name'].'-min']) ){
					$args['meta_query'][$user_meta_data['name']] = array(
						'key' => $user_meta_data['name'],
						'value' => array( $_GET[$user_meta_data['name'].'-min'], $_GET[$user_meta_data['name'].'-max'] ),
						'type' => 'numeric',
						'compare' => 'BETWEEN'	
					);	
				}
			}elseif( $user_meta_data['type'] == 'date' ){
				if( $user_meta_data['name'] == 'age' ){
					$user_role = $_REQUEST['user_roles'];
					//echo $_GET[$user_role.'-'.$user_meta_data['name'].'-min'].'-'.$_GET[$user_role.'-'.$user_meta_data['name'].'-max']
					if( !empty($_GET[$user_role.'-'.$user_meta_data['name'].'-min']) ){
						$args['meta_query'][$user_meta_data['name']] = array(
							'key' =>'user_age_filter',
							'value' => array( $_GET[$user_role.'-'.$user_meta_data['name'].'-min'], $_GET[$user_role.'-'.$user_meta_data['name'].'-max'] ),
							'type' => 'numeric',
							'compare' => 'BETWEEN'	
							);	
					}	
				}else{
				}
			}elseif( @$user_meta_data['options']['pick_format_type'] == 'single' ){

				if( !empty( $_GET[$user_meta_data['name'].'_range'] ) &&  ( $_GET[$user_meta_data['name'].'_range'] == 'true' ) ){
					if( !empty($_GET[$user_meta_data['name'].'-from']) ){
						$args['meta_query'][$user_meta_data['name']] = array(
							'key' => $user_meta_data['name'],
							'value' => array( $_GET[$user_meta_data['name'].'-from'], $_GET[$user_meta_data['name'].'-to'] ),
							'compare' => 'BETWEEN',
							'type' => 'numeric',
						);	
					}									
				}else{
					if( !empty($_GET[$user_meta_data['name']]) ){
						$args['meta_query'][$user_meta_data['name']] = array(
							'key' => $user_meta_data['name'],
							'value' => $_GET[$user_meta_data['name']],
							);
						}
				}
			}elseif( @$user_meta_data['options']['pick_format_type'] == 'multi' ){
					if( !empty($_GET[$user_meta_data['name']]) ){
						$args['meta_query'][$user_meta_data['name']] = array(
							'key' => $user_meta_data['name'],
							'value' => $_GET[$user_meta_data['name']],
							'compare' => 'IN',
						);
				}else{
					if(!empty($_GET[$user_meta_data['name']])){
						$args['meta_query'][$user_meta_data['name']] = array(
							'key' => $user_meta_data['name'],
							'value' => $_GET[$user_meta_data['name']],
						);
					}
				}

			}
		}
		
	$users = new WP_User_Query( $args );
	echo '<div class="user-data-wrapper">';	
		if ( ! empty( $users->results ) ) {
			//print_r($users->results);
			echo '<ul>';
			//print_r($user_data);
			$settings = $user_data;
			 $template_file = locate_template( 'elementor-user-widget-styles.php' ); 
	            foreach ( $users->results as $user ) {
	                $user_meta_info = get_user_meta($user->ID );
	                $user_id = $user->ID;
	                $user_name =  $user->display_name;
	                echo '<li class="column'.$settings['per_line'].'">';
	                    if( $template_file ){
	                       include $template_file;
	                    }else{
	                       include KAYA_USER_PLUGIN_PATH.'templates/elementor-user-widget-styles.php';
	                    }
	                echo '</li>';
	            }
	        echo '</ul>';    
	        }else{
	        	echo '<p class="empty-results-text">'.(!empty($_REQUEST['error_msg']) ? html_entity_decode($_REQUEST['error_msg']) : __('Nothing Found', 'pud')).'</p>';
	        }

	        // Pagination Section
	        $total_user = $users->total_users;  
	        if( $total_user >= $limit ){
	        	 $total_pages = ceil($total_user/$limit);
		        echo '<div classs="users-pagination">';
		             echo paginate_links(array(  
		                  'format' => '?paged=%#%',  
		                  'current' => $paged,  
		                  'total' => $total_pages,  
		                  'prev_text' => '<<',  
		                  'next_text' => '>>',
		                  'type'     => 'list',
		                )); 
		        echo '</div>';           
	    	}

    echo '</div>';    
	}
}

function kaya_users_ajax_search(){
	add_action( 'pre_user_query', function( $user_query ) {
		  $user_query->query_fields = 'SQL_CALC_FOUND_ROWS DISTINCT ' . $user_query->query_fields;
		} );

		add_filter( 'found_users_query', function( $sql ) {
		  return 'SELECT FOUND_ROWS()';
		} );
		$search_sting =  $_POST['search_data'];
		parse_str($search_sting);
		 $user_data =  unserialize(stripslashes($user_data)); 
		$user_meta_fields = kaya_get_user_fields('user');
		$limit=!empty($limit) ? $limit : '1';// total no of author to display
       	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$args = array(
			'number' => $limit,
			'paged' => trim($_POST['paged']),
			'role__in' => !empty($user_roles) ? $user_roles : array(),
			//'relation' => 'OR', 
		 );

		foreach ($user_meta_fields as $key => $user_meta_data) {
			if( $user_meta_data['type'] == 'text' ){
				if( !empty(${ $user_meta_data['name'] })){
					$args['meta_query'][] = array(
						'key' => $user_meta_data['name'],
						'value' => ${$user_meta_data['name']},
						'compare' => 'LIKE',
					);
				}
			}elseif( $user_meta_data['type'] == 'number' ){
				
				if( !empty( ${$user_meta_data['name'].'-min'} ) ){
					$args['meta_query'][] = array(
						'key' => $user_meta_data['name'],
						'value' => array( ${ $user_meta_data['name'].'-min' }, ${$user_meta_data['name'].'-max'} ),
						'type' => 'numeric',
						'compare' => 'BETWEEN'	
					);	
				}
			}elseif( $user_meta_data['type'] == 'date' ){
				//echo $user_roles;
				if( $user_meta_data['name'] == 'age' ){
					if( !empty(${$user_roles.'-'.$user_meta_data['name'].'-min'}) ){
						$args['meta_query'][] = array(
							'key' =>'user_age_filter',
							'value' => array( ${$user_roles.'-'.$user_meta_data['name'].'-min'}, ${$user_roles.'-'.$user_meta_data['name'].'-max'} ),
							'type' => 'numeric',
							'compare' => 'BETWEEN'	
							);	
					}	
				}else{
				}
			}elseif( $user_meta_data['options']['pick_format_type'] == 'single' ){	
				
				if( !empty( ${$user_meta_data['name'].'_range'} ) &&  ( ${$user_meta_data['name'].'_range'} == 'true' ) ){

					if( !empty(${$user_meta_data['name'].'-from'}) ){
						$args['meta_query'][] = array(
							'key' => $user_meta_data['name'],
							'value' => array( ${$user_meta_data['name'].'-from'}, ${$user_meta_data['name'].'-to'} ),
							'compare' => 'BETWEEN',
							'type' => 'numeric',
						);	
					}									
				}else{
					if( !empty(${$user_meta_data['name']}) ){
						$args['meta_query'][] = array(
							'key' => $user_meta_data['name'],
							'value' => ${$user_meta_data['name']},
							);
						}
				}
			}elseif( $user_meta_data['options']['pick_format_type'] == 'multi' ){
					if( !empty(${$user_meta_data['name']}) ){
						$args['meta_query'][] = array(
							'key' => $user_meta_data['name'],
							'value' => ${$user_meta_data['name']},
							'compare' => 'IN',
						);
				}else{
					if(!empty(${$user_meta_data['name']})){
						$args['meta_query'][] = array(
							'key' => $user_meta_data['name'],
							'value' => ${$user_meta_data['name']},
						);
					}
				}

			}
		}
		
	$users = new WP_User_Query( $args );
	echo '<div class="user-data-wrapper" id="'.$user_roles.'">';	
		if ( ! empty( $users->results ) ) {
			//print_r($users->results);
			$settings = $user_data;
			echo '<ul>';
			 $template_file = locate_template( 'elementor-user-widget-styles.php' ); 
	            foreach ( $users->results as $user ) {
	                $user_meta_info = get_user_meta($user->ID );
	                $user_id = $user->ID;
	                $user_name =  $user->display_name;
	                echo '<li class="column'.$settings['per_line'].'">';
	                    if( $template_file ){
	                       include $template_file;
	                    }else{
	                       include KAYA_USER_PLUGIN_PATH.'templates/elementor-user-widget-styles.php';
	                    }
	                echo '</li>';
	            }
	        echo '</ul>';    
	        }else{
	        	echo '<p class="empty-results-text">'.(!empty($error_msg) ? html_entity_decode($error_msg) : __('Nothing Found', 'pud')).'</p>';
	        }

	        // Pagination Section
	        //if( $total_user >= $limit ){
	        	$total_user = $users->total_users;  
	        	$total_pages = ceil($total_user/$limit);
		        echo '<div class="users-pagination ajax-pagination">';
		             echo paginate_links(array(  
		                  'format' => '?paged=%#%',  
		                  'current' => $paged,  
		                  'total' => $total_pages,
		                    'mid_size' =>100,  
		                  'prev_text' => '',  
		                  'next_text' => '',
		                  'type'     => 'list',
		                )); 
		        echo '</div>';           
	    	//}

    echo '</div>';  
	die();

}
add_action('wp_ajax_kaya_users_ajax_search', 'kaya_users_ajax_search');
add_action('wp_ajax_nopriv_kaya_users_ajax_search', 'kaya_users_ajax_search');
?>