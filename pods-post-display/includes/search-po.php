<?php
function kaya_search_data(){
	
//	echo '<a href="'.get_the_permalink().'">'.get_the_title().'</a><br>';
if( !is_author() ){
	$columns = !empty($kaya_options->taxonomy_columns) ? $kaya_options->taxonomy_columns : '4';
}else{
	$columns = '3';
}
$image_cropping_type = !empty($kaya_options->choose_image_sizes) ? $kaya_options->choose_image_sizes : 'wp_image_sizes';
if( $image_cropping_type == 'wp_image_sizes' ){
	$image_sizes = !empty($kaya_options->choose_image_sizes) ? $kaya_options->choose_image_sizes : 'full';
}else{
	$image_size_width = !empty($kaya_options->taxonomy_gallery_width) ? $kaya_options->taxonomy_gallery_width : '380';
	$image_size_height = !empty($kaya_options->taxonomy_gallery_height) ? $kaya_options->taxonomy_gallery_height : '600';
	$image_sizes = array( $image_size_width, $image_size_height );
}
// Post Loop
	// Do loop here
	echo '<li class="column'.$columns.' item" id="'.get_the_ID().'">';
		
		echo '<div class="grid-view-container taxonomy-style">';
			//echo '<strong>'.get_the_title().'</strong>';
			echo '<a href="'.get_the_permalink().'">';
				echo '<div class="grid-view-image">';
				echo kaya_pod_featured_img( $image_sizes, $image_cropping_type );
				echo '<div class="overlay-hd">';
				echo '</div>';
				if( function_exists('kaya_general_info_section') ){
					echo '<div class="title-meta-data-wrapper">';
					kaya_general_info_section($cpt_slug_name);
					echo '</div>';
				}
				echo '</div>';
				echo '<h3>'; the_title(); echo'</h3>';
				echo '</a>';
			echo '<div class="shortlist-wrap">';
			do_action('kaya_pods_cpt_shortlist_icons'); // Shortlist Icons
			echo '</div>';
		echo '</div>';
	echo '</li>';
	
}
?>