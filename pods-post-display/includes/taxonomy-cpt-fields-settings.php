<?php
/**
 * POD CPT Custom fields Enable settings for taxonomy pages 
 */
include_once 'export-options.php';
class Kaya_Taxonomy_Cpt_Fields_Settings{
      private $pods_options;
      function __construct(){
         add_action( 'admin_menu', array($this, 'kaya_settings_page_init'),0,11 );
         add_action( 'init', array($this, 'kaya_options_data') );
         add_action( "init", array($this, 'kaya_load_settings_page') );
      }
      // Creating Menu Page, admin settings
      function kaya_settings_page_init() {
         add_menu_page(__('Pods Post Display', 'ppd'), __('Pods Post Display', 'ppd'), 'manage_options', 'kaya-settings', array($this, 'kaya_page_settings_options'));      
      }
      function kaya_options_data() {
         if( !function_exists('pods_api') ){
             return false;
         }

         $this->pods_options = pods_api()->load_pods( array( 'fields' => false ) );
         $settings = get_option( "kaya_options" );
         $pods_fields = array();
           foreach ($this->pods_options as $key => $options) {
               if( $options['type'] == 'post_type' ){ 
                  $pods_fields['enable_'.$options['name'].'_data'] = '0';
               }
          }
         // Settings Options Data
         if ( empty( $settings ) ) {
            $settings = array(
               'taxonomy_columns' => '4',
               'choose_image_sizes' => 'wp_image_sizes',
               'taxonomy_gallery_width' => '',
               'taxonomy_gallery_height' => '',
               'wp_default_img_sizes' => '',
               'post_limit' => '-1',
               'gray_scale_mode' => '',
               'admin_post_notification_cpts' => '',
               'post_pending_email_notification' => '',
               'post_publish_email_mesage' => '',
               'post_email_form_subject' => '',
               'talent_profile_notification' => '',
               'talents_single_page_option' => '',
               //'user_talent_post_note' => '',
               
               'profile_tab1_title' => '',
               'profile_tab1_content' => '',
               'profile_tab1_restrct_user_role' => '',
               'choose_role_to_display_profile' => '',
               'tab1_restrict_msg_tab' => '',
               'profile_tab_empty_desc' => '',
               'pods_profile_fields_empty_msg' => '',
               'custom_profile_tab1_title' => '',
               'custom_profile_tab2_title' => '',
               'custom_profile_tab3_title' => '',
               'custom_profile_tab1' => '',
               'custom_profile_tab2' => '',
               'custom_profile_tab3' => '',
               'tab1_restrct_user_role' => '',
               'tab2_restrct_user_role' => '',
               'tab3_restrct_user_role' => '',
               'choose_role_to_display1' => '',
               'choose_role_to_display2' => '',
               'choose_role_to_display3' => '',
               'tab1_restrict_msg'  => '',
               'tab2_restrict_msg' => '',
               'tab3_restrict_msg' => '',
            );
            $fields_opt_data = array_merge( $settings, $pods_fields );
            add_option( "kaya_options", $fields_opt_data, '', 'yes' );
         }   
      }
      // Save Settings page Options data
      function kaya_load_settings_page() {
         if ( isset($_POST["kaya-settings-submit"]) && ( $_POST["kaya-settings-submit"] == 'Y' ) ) {
            check_admin_referer( "kaya-settings-page" );
            $this->kta_save_theme_settings();
            $url_parameters = isset($_GET['tab']) ? 'updated=true&tab='.$_GET['tab'] : 'updated=true';
            wp_redirect(admin_url('admin.php?page=kaya-settings&'.$url_parameters));
            exit;
         }
      }
      // Updating The Options Data From Input Fields
      function kta_save_theme_settings(){
         global $pagenow;
         $settings = get_option( "kaya_options" );
         if ( $pagenow == 'admin.php' && $_GET['page'] == 'kaya-settings' ){ 
            if ( isset ( $_GET['tab'] ) )
            $tab = $_GET['tab'];
            else
            $tab = 'cpt_fields'; 
            switch ( $tab ){ 
               case 'general':
                  //$settings['user_talent_post_note'] = isset( $_POST['user_talent_post_note'] ) ? $_POST['user_talent_post_note'] : '';
                  break;
               case 'cpt_fields' :
                  foreach ($this->pods_options as $key => $options) {
                     if( $options['type'] == 'post_type' ){ 
                        $settings['enable_'.$options['name'].'_data'] = $_POST['enable_'.$options['name'].'_data'] ? $_POST['enable_'.$options['name'].'_data'] : '';
                     }
                   }
                   
                  break;
               case 'taxonomy' :
                  $settings['taxonomy_columns'] = !empty($_POST['taxonomy_columns']) ? $_POST['taxonomy_columns'] : '4';
                  $settings['talent_profile_notification'] = !empty($_POST['talent_profile_notification']) ? $_POST['talent_profile_notification'] : 'Text';
                  $settings['choose_image_sizes'] = !empty($_POST['choose_image_sizes']) ? $_POST['choose_image_sizes'] : 'wp_image_sizes';
                  $settings['taxonomy_gallery_width'] = !empty($_POST['taxonomy_gallery_width']) ? $_POST['taxonomy_gallery_width'] : '420';
                  $settings['taxonomy_gallery_height'] = !empty($_POST['taxonomy_gallery_height']) ? $_POST['taxonomy_gallery_height'] : '580';
                  $settings['wp_default_img_sizes'] = !empty($_POST['wp_default_img_sizes']) ? $_POST['wp_default_img_sizes'] : 'thumbnail';
                  $settings['post_limit'] = !empty($_POST['post_limit']) ? $_POST['post_limit'] : '-1';
                  $settings['talents_single_page_option'] = !empty($_POST['talents_single_page_option']) ? $_POST['talents_single_page_option'] : '';
                  $settings['gray_scale_mode'] = !empty($_POST['gray_scale_mode']) ? $_POST['gray_scale_mode'] : '';
                  break;
               
                case 'profile_tabs' :   
                  $settings['profile_tab1_title'] = !empty($_POST['profile_tab1_title']) ? $_POST['profile_tab1_title'] : '';
                  $settings['profile_tab1_content'] = !empty($_POST['profile_tab1_content']) ? $_POST['profile_tab1_content'] : '';
                  $settings['profile_tab1_restrct_user_role'] = !empty($_POST['profile_tab1_restrct_user_role']) ? $_POST['profile_tab1_restrct_user_role'] : '';
                  $settings['choose_role_to_display_profile'] = !empty($_POST['choose_role_to_display_profile']) ? $_POST['choose_role_to_display_profile'] : '';
                  $settings['tab1_restrict_msg_tab'] = !empty($_POST['tab1_restrict_msg_tab']) ? $_POST['tab1_restrict_msg_tab'] : '';
                  $settings['profile_tab_empty_desc'] = !empty($_POST['profile_tab_empty_desc']) ? $_POST['profile_tab_empty_desc'] : '';
                  $settings['pods_profile_fields_empty_msg'] = !empty($_POST['pods_profile_fields_empty_msg']) ? $_POST['pods_profile_fields_empty_msg'] : '';
                  $settings['custom_profile_tab1_title'] = !empty($_POST['custom_profile_tab1_title']) ? $_POST['custom_profile_tab1_title'] : '';
                  $settings['custom_profile_tab2_title'] = !empty($_POST['custom_profile_tab2_title']) ? $_POST['custom_profile_tab2_title'] : '';
                  $settings['custom_profile_tab3_title'] = !empty($_POST['custom_profile_tab3_title']) ? $_POST['custom_profile_tab3_title'] : '';
                  $settings['custom_profile_tab1'] = !empty($_POST['custom_profile_tab1']) ? $_POST['custom_profile_tab1'] : '';
                  $settings['custom_profile_tab2'] = !empty($_POST['custom_profile_tab2']) ? $_POST['custom_profile_tab2'] : '';
                  $settings['custom_profile_tab3'] = !empty($_POST['custom_profile_tab3']) ? $_POST['custom_profile_tab3'] : '';
                  $settings['tab1_restrct_user_role'] = !empty($_POST['tab1_restrct_user_role']) ? $_POST['tab1_restrct_user_role'] : '';
                  $settings['tab2_restrct_user_role'] = !empty($_POST['tab2_restrct_user_role']) ? $_POST['tab2_restrct_user_role'] : '';
                  $settings['tab3_restrct_user_role'] = !empty($_POST['tab3_restrct_user_role']) ? $_POST['tab3_restrct_user_role'] : '';
                  $settings['choose_role_to_display1'] = !empty($_POST['choose_role_to_display1']) ? $_POST['choose_role_to_display1'] : '';
                  $settings['choose_role_to_display2'] = !empty($_POST['choose_role_to_display2']) ? $_POST['choose_role_to_display2'] : '';
                  $settings['choose_role_to_display3'] = !empty($_POST['choose_role_to_display3']) ? $_POST['choose_role_to_display3'] : '';
                  $settings['tab1_restrict_msg'] = !empty($_POST['tab1_restrict_msg']) ? $_POST['tab1_restrict_msg'] : '';
                  $settings['tab2_restrict_msg'] = !empty($_POST['tab2_restrict_msg']) ? $_POST['tab2_restrict_msg'] : '';
                  $settings['tab3_restrict_msg'] = !empty($_POST['tab3_restrict_msg']) ? $_POST['tab3_restrict_msg'] : '';
                  break; 
            }
         }
         $updated = update_option( "kaya_options", $settings );
      }
      // Tabs Section
      function kta_admin_tabs( $current = 'cpt_fields' ) { 
         $tabs = array(
            //'general' => __('General', 'ppd'),
            'cpt_fields' => __('CPT Fields','ppd'), 
            'taxonomy' => __('Taxonomy','ppd'),
            'profile_tabs' => __('Custom Profile Tabs', 'ppd'),
         );
         $links = array();
         echo '<div id="icon-admin" class="icon32"><br></div>';
         echo '<h2 class="nav-tab-wrapper">';
         foreach( $tabs as $tab => $name ){
            $class = ( $tab == $current ) ? ' nav-tab-active' : '';
            echo "<a class='nav-tab$class' href='?page=kaya-settings&tab=$tab'>$name</a>";
         }
         echo '</h2>';
      }

      // Cpt Fields Options Settings Page Data
      function kaya_page_settings_options() {
         global $pagenow;
         $settings = array_filter(get_option( "kaya_options" )); ?>
         <div class="wrap kaya-admin-options-dashboard">
            <script type='text/javascript'>
               jQuery(document).ready(function($) {
                  jQuery('.opt_color_pickr').each(function(){
                     jQuery(this).wpColorPicker();
                  }); 
               });
            </script>
            <div class="kaya-header-title">
               <h1><?php _e('Options Settings','ppd'); ?></h1> 
            </div>   
            <?php
            if ( isset($_GET['updated']) && ( 'true' == esc_attr( $_GET['updated'] ) ) ) echo '<div class="updated" ><p>'.__('Theme Settings updated.', 'ppd').'</p></div>';
            if ( isset ( $_GET['tab'] ) ) $this->kta_admin_tabs($_GET['tab']); else $this->kta_admin_tabs('cpt_fields');?>
            <div class="tax_poststuff">
            <div id="poststuff">
               <script type="text/javascript">
               jQuery(document).ready(function(){
                  "use strict";
                  jQuery('select#choose_role_to_display_profile').on('change',function(){
                    jQuery('#profile_tab1_restrct_user_role').hide();
                     var role_data = jQuery(this).find('option:selected').val();
                     if( role_data == 'srole' ){
                        jQuery('#profile_tab1_restrct_user_role').show();
                        jQuery('#tab1_restrict_msg_tab').parent().parent().show();
                     }
                     else if( role_data == 'guests' ){
                        jQuery('#tab1_restrict_msg_tab').parent().parent().show();
                     }
                     else{
                        jQuery('#tab1_restrict_msg_tab').parent().parent().hide();
                     }
                  }).change();

                  jQuery('select#choose_role_to_display1').on('change',function(){
                    jQuery('#tab1_restrct_user_role').hide();
                     var role_data = jQuery(this).find('option:selected').val();
                     if( role_data == 'srole' ){
                        jQuery('#tab1_restrct_user_role').show();
                        jQuery('#tab1_restrict_msg').parent().parent().show();
                     }
                     else if( role_data == 'guests' ){
                        jQuery('#tab1_restrict_msg').parent().parent().show();
                     }else{
                        jQuery('#tab1_restrict_msg').parent().parent().hide();
                     }
                  }).change(); 

                  jQuery('select#choose_role_to_display2').on('change',function(){
                    jQuery('#tab2_restrct_user_role').hide();
                     var role_data = jQuery(this).find('option:selected').val();
                     if( role_data == 'srole' ){
                        jQuery('#tab2_restrct_user_role').show();
                        jQuery('#tab2_restrict_msg').parent().parent().show();
                     }
                     else if( role_data == 'guests' ){
                        jQuery('#tab2_restrict_msg').parent().parent().show();
                     }
                     else{
                        jQuery('#tab2_restrict_msg').parent().parent().hide();
                     }
                  }).change(); 


                  jQuery('select#choose_role_to_display3').on('change',function(){
                    jQuery('#tab3_restrct_user_role').hide();
                     var role_data = jQuery(this).find('option:selected').val();
                     if( role_data == 'srole' ){
                        jQuery('#tab3_restrct_user_role').show();
                        jQuery('#tab3_restrict_msg').parent().parent().show();
                     }
                     else if( role_data == 'guests' ){
                        jQuery('#tab3_restrict_msg').parent().parent().show();
                     }
                     else{
                        jQuery('#tab3_restrict_msg').parent().parent().hide();
                     }
                  }).change();
               });
               </script>
               <form method="post" action="<?php admin_url( 'admin.php?page=kaya-settings' ); ?>"> 
                  <?php
                  wp_nonce_field( "kaya-settings-page" );                
                  if ( $pagenow == 'admin.php' && $_GET['page'] == 'kaya-settings' ){ 
                  if ( isset ( $_GET['tab'] ) ) $tab = $_GET['tab'];
                     else $tab = 'cpt_fields';

                     switch ( $tab ){
                        case 'general':
                           echo '<table class="form-table">';
                              /*echo '<tr>';
                                 echo '<th><label for="user_talent_post_note">'.__('User Talent Profile Note', 'ppd').'</label></th>';
                                 echo '<td>';
                                      echo '<textarea id="user_talent_post_note" rows="3" cols="45" name="user_talent_post_note">'.(isset($settings["user_talent_post_note"]) ? stripslashes(__($settings["user_talent_post_note"], "ppd")) :"No Talent Post Has Been Created, To Create Click 'Add New' Button And Fill The Form").'</textarea>';
                                 echo '</td>';  
                              echo '</tr>';*/
                           echo '</table>';
                           break;
                        case 'cpt_fields' : ?>
                           <div class="kta-admin-panel">
                           <?php echo '<table class="form-table">'; ?>                             
                                    <?php                                     
                                        foreach ($this->pods_options as $key => $options) {
                                          if( $options['type'] == 'post_type' ){
                                             echo ' <tr>
                                                <th>'.__('Enable ', 'ppd').' '.$options['label'].' '.__('CPT Fields','ppd').'</th>
                                                <td>';                                         
                                                   echo '<h3>'.$options['label'].'</h3>';
                                                   echo '<div class="cpt_options_fields" id="cpt_'.$options['name'].'">';
                                                      foreach ($options['fields'] as $fields_key => $field_val) {
                                                         echo '<div>';   
                                                            echo '<label>';                                                        
                                                               if( isset($settings['enable_'.$options['name'].'_data']) ){
                                                                  $checked = in_array($fields_key,  $settings['enable_'.$options['name'].'_data']) ? 'checked' : '';
                                                               }else{
                                                                  $checked ='';
                                                               }
                                                               if( $options['fields'][$fields_key]['type'] != 'file' ){   ?>
                                                                  <input type="checkbox" class="" id="enable_<?php echo $options['name']; ?>_data"  value="<?php echo trim($fields_key) ?>" name="enable_<?php echo $options['name']; ?>_data[]" <?php echo $checked; ?> />
                                                                  <?php echo $field_val['label']; 
                                                               }        
                                                            echo '</label>';
                                                         echo '</div>';
                                                      }                                                   
                                                   echo '</div>';
                                                 echo '</td>
                                             </tr>';
                                          }                                            
                                       }
                                    ?>                                                                                     
                                 </table>
                           </div>  

                          <?php break;
                          case 'taxonomy': ?>
                              <table class="form-table">
                              <tr>
                              <th> <label for="taxonomy_columns"><?php _e('Taxonomy Columns','ppd'); ?></label> </th>
                              <td>
                                 <select name="taxonomy_columns">
                                    <option val="6" <?php selected('6', ( isset($settings['taxonomy_columns']) ? $settings['taxonomy_columns'] : '' )) ?> >6</option>
                                    <option val="5" <?php selected('5', ( isset($settings['taxonomy_columns']) ? $settings['taxonomy_columns'] : '' )) ?> >5</option>
                                    <option val="4" <?php selected('4', ( isset($settings['taxonomy_columns']) ? $settings['taxonomy_columns'] : '' )) ?> >4</option>
                                    <option val="3" <?php selected('3', ( isset($settings['taxonomy_columns']) ? $settings['taxonomy_columns'] : '' )) ?> >3</option>
                                    <option val="2" <?php selected('2', ( isset($settings['taxonomy_columns']) ? $settings['taxonomy_columns'] : '' )) ?> >2</option>
                                 </select>
                              </td>
                           </tr>
                           <tr>
                              <th> <label for="choose_image_sizes"><?php _e('Choose Image Size','ppd'); ?></label> </th>
                              <td>
                                 <select name="choose_image_sizes" class="choose_image_sizes">
                                    <option value="wp_image_sizes" <?php selected('wp_image_sizes', ( isset($settings['choose_image_sizes']) ? $settings['choose_image_sizes'] : '' )) ?> ><?php _e(' Wordpress Default Image Sizes', 'ppd'); ?></option>
                                    <option value="custom_image_sizes" <?php selected('custom_image_sizes', ( isset($settings['choose_image_sizes']) ? $settings['choose_image_sizes'] : '' )) ?> ><?php _e('Custom Image Sizes', 'ppd'); ?></option>
                                 </select>
                              </td>
                           </tr>
                           <tr>
                              <th> <label for="wp_default_img_sizes"><?php _e('Wordpress Default Image Sizes','ppd'); ?></label> </th>
                              <td>
                              <?php  
                                 $default_image_sizes = array( 'thumbnail', 'medium', 'large' );
                                 echo '<select name="wp_default_img_sizes" class="wp_default_img_columns">';
                                 foreach ($default_image_sizes as $key => $image_size) { ?>
                                    <option value="<?php echo $image_size; ?>" <?php selected($image_size, ( isset($settings['wp_default_img_sizes']) ? $settings['wp_default_img_sizes'] : '' )) ?> ><?php echo ucfirst($image_size); ?></option>
                                 <?php } ?>
                              </td>
                           </tr>
                            <tr>
                              <th><label for="taxonomy_gallery_width"><?php _e('Images Custom Width & Height','ppd'); ?></label></th>
                              <td>
                                 <input type="text" name="taxonomy_gallery_width" id="taxonomy_gallery_width" value="<?php echo isset($settings['taxonomy_gallery_width']) ? $settings['taxonomy_gallery_width'] : '420'; ?>" class="small-text" />X
                                 <input type="text" name="taxonomy_gallery_height" id="taxonomy_gallery_height" value="<?php echo isset($settings['taxonomy_gallery_height']) ? $settings['taxonomy_gallery_height'] : '580'; ?>" class="small-text" /><?php esc_html_e('px','ppd'); ?>
                              </td>
                           </tr>
                           <tr>
                              <th><label for="images"><?php _e('Limit','ppd'); ?></label></th>
                              <td>
                                 <input type="text" name="post_limit" id="post_limit" value="<?php echo !empty($settings['post_limit']) ? $settings['post_limit'] : '-1'; ?>" class="small-text" />
                              </td>
                           </tr>
                           <tr>
                              <th><label for="talents_single_page_option"><?php _e('Talent Single-Page Tabs Section Disable','ppd'); ?></label></th>
                              <td>
                                 <input type="checkbox" class="checkbox" id="talents_single_page_option" name="talents_single_page_option" <?php checked( (bool) $settings["talents_single_page_option"], true ); ?> />
                              </td>
                           </tr>  
                          
                           <tr>
                              <th><label for="gray_scale_mode"><?php _e('Gray Scale Enable','ppd'); ?></label></th>
                              <td>
                                 <input type="checkbox" class="checkbox" id="gray_scale_mode" name="gray_scale_mode" <?php checked( (bool) $settings["gray_scale_mode"], true ); ?> />
                              </td>
                           </tr>  
                           <?php break;
                           echo '</table>';
                           break;

                            case 'profile_tabs' : 
                           echo '<table class="form-table">'; 
                           $user_roles = kaya_get_user_roles(); ?>
                        <tr>
                           <th><small><strong style="color:#ff0000;">Note: </strong></small><?php _e('This tab is generated in Talent Single page as a First Tab for displaying Talent General Profile Information, this tab can be controlled by Admin only.', 'ppd'); ?></th>
                        </tr>
                       <tr class="tab1_profile_top_heading">
                        <td>
                           <fieldset>
                              <legend><?php echo _e('General Profile Information', 'ppd') ?></legend>
                              <table>
                                 <tr>
                                    <th><label for="profile_tab1_title"><?php _e('Tab Title','ppd'); ?></label></th>
                                    <td>
                                       <input id="profile_tab1_title" name="profile_tab1_title" type="text" placeholder="<?php _e('Add Your Title', 'ppd') ?>" value="<?php echo !empty($settings["profile_tab1_title"]) ? stripslashes(__($settings["profile_tab1_title"], 'ppd')) : ''; ?>">
                                    </td>
                                 </tr>
                                 <tr>
                                    <th><label for="profile_tab1_content"><?php _e('Tab Content','ppd'); ?></label></th>
                                    <td>
                                       <textarea id="profile_tab1_content" rows="3" cols="45" name="profile_tab1_content" type="text" placeholder="<?php _e('Add Your content', 'ppd') ?>" ><?php echo !empty($settings["profile_tab1_content"]) ? stripslashes(__($settings["profile_tab1_content"], 'ppd')) : ''; ?></textarea>
                                    </td>
                                    
                                 </tr>
                                 <th></th><td class="shortcode_content"><small><strong style="color:#ff0000;">Note: </strong></small><?php _e('Use This Shortcode to display Talent Profile Information"[general_profile_info]"', 'ppd'); ?></td>
                                 
                                 <tr>
                                    <th class="choose_role_to_display_new"><label for="profile_tab1_restrct_user_role"><?php _e('Choose Role For Whom to be Visible','ppd'); ?></label></th>
                                    <td>
                                       <?php
                                       echo '<select name="choose_role_to_display_profile" id="choose_role_to_display_profile" style="clear:both;display:block;">';
                                          echo '<option value="anyone" '.selected('anyone', $settings['choose_role_to_display_profile']).'>'.__('Anyone', 'ppd').'</option>';
                                          echo '<option value="guests" '.selected('guests', $settings['choose_role_to_display_profile']).'>'.__('Guests Only', 'ppd').'</option>';
                                          echo '<option value="srole" '.selected('srole', $settings['choose_role_to_display_profile']).'>'.__('Specific Role Only', 'ppd').'</option>';
                                       echo '</select>';
                                       if( !empty($user_roles) ){
                                          echo '<select name="profile_tab1_restrct_user_role[]" multiple id="profile_tab1_restrct_user_role" style="clear:both;display:block;">';
                                          foreach ($user_roles as $key => $user_role) { 
                                             $selected = !empty( $settings['profile_tab1_restrct_user_role']) ?  (in_array($key, $settings['profile_tab1_restrct_user_role']) ? 'selected' : '') : ''; ?>
                                             <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $user_role; ?> </option>                               
                                             <?php 
                                          }
                                          echo '</select>';
                                       } ?>
                                    </td>
                                 </tr>
                                 <tr>
                                    <th><label for="tab1_restrict_msg_tab"><?php _e('Restrict Tab Content Message', 'ppd'); ?></th>
                                    <td>
                                       <textarea id="tab1_restrict_msg_tab" rows="3" cols="45" name="tab1_restrict_msg_tab" type="text" placeholder="<?php _e('Add Your content', 'ppd') ?>" ><?php echo !empty($settings["tab1_restrict_msg_tab"]) ? stripslashes(__($settings["tab1_restrict_msg_tab"], 'ppd')) : ''; ?></textarea>
                                    </td>
                                 </tr>
                           </table>
                        </fieldset>
                        </td>
                     </tr>

                     <tr>
                        <th><small><strong style="color:#ff0000;">Note: </strong></small><?php _e('These are all tabs are generated in Talent Single page at the end of those tabs which are generated by Pods Plugin and can be controlled by Admin only.', 'ppd'); ?></th>
                     </tr>
                     <tr class="tab1_top_heading">
                        <td>
                           <fieldset>
                              <legend><?php echo _e('Custom Profile Tab 1', 'ppd') ?></legend>
                              <table>
                                 <tr>
                                    <th><label for="custom_profile_tab1_title"><?php _e('Tab Title','ppd'); ?></label></th>
                                    <td>
                                       <input id="custom_profile_tab1_title" name="custom_profile_tab1_title" type="text" placeholder="<?php _e('Add Your Title', 'ppd') ?>" value="<?php echo !empty($settings["custom_profile_tab1_title"]) ? stripslashes(__($settings["custom_profile_tab1_title"], 'ppd')) : ''; ?>">
                                    </td>
                                 </tr>
                                 <tr>
                                    <th><label for="custom_profile_tab1"><?php _e('Tab Content','ppd'); ?></label></th>
                                    <td>
                                       <textarea id="custom_profile_tab1" rows="3" cols="45" name="custom_profile_tab1" type="text" placeholder="<?php _e('Add Your content', 'ppd') ?>" ><?php echo !empty($settings["custom_profile_tab1"]) ? stripslashes(__($settings["custom_profile_tab1"], 'ppd')) : ''; ?></textarea>
                                    </td>
                                 </tr>
                                 <tr>
                                    <th class="choose_role_to_display"><label for="tab1_restrct_user_role"><?php _e('Choose Role For Whom to be Visible','ppd'); ?></label></th>
                                    <td>
                                       <?php
                                       echo '<select name="choose_role_to_display1" id="choose_role_to_display1" style="clear:both;display:block;">';
                                          echo '<option value="anyone" '.selected('anyone', $settings['choose_role_to_display1']).'>'.__('Anyone', 'ppd').'</option>';
                                          echo '<option value="guests" '.selected('guests', $settings['choose_role_to_display1']).'>'.__('Guests Only', 'ppd').'</option>';
                                          echo '<option value="srole" '.selected('srole', $settings['choose_role_to_display1']).'>'.__('Specific Role Only', 'ppd').'</option>';
                                       echo '</select>';
                                       if( !empty($user_roles) ){
                                          echo '<select name="tab1_restrct_user_role[]" multiple id="tab1_restrct_user_role" style="clear:both;display:block;">';
                                          foreach ($user_roles as $key => $user_role) { 
                                             $selected = !empty( $settings['tab1_restrct_user_role']) ?  (in_array($key, $settings['tab1_restrct_user_role']) ? 'selected' : '') : ''; ?>
                                             <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $user_role; ?> </option>                               
                                             <?php 
                                          }
                                          echo '</select>';
                                       } ?>
                                    </td>
                                 </tr>
                                 <tr>
                                    <th><label for="tab1_restrict_msg"><?php _e('Restrict Tab Content Message', 'ppd'); ?></th>
                                    <td>
                                       <textarea id="tab1_restrict_msg" rows="3" cols="45" name="tab1_restrict_msg" type="text" placeholder="<?php _e('Add Your content', 'ppd') ?>" ><?php echo !empty($settings["tab1_restrict_msg"]) ? stripslashes(__($settings["tab1_restrict_msg"], 'ppd')) : ''; ?></textarea>
                                    </td>
                                 </tr>
                              </table>
                           </fieldset>
                        </td>
                     </tr>
                     <tr class="tab2_top_heading">
                        <td>
                           <fieldset>
                              <legend><?php _e('Custom Profile Tab 2', 'ppd') ?></legend>
                              <table>
                                 <tr>
                                    <th><label for="custom_profile_tab2_title"><?php _e('Tab Title','ppd'); ?></label></th>
                                    <td>
                                       <input id="custom_profile_tab2_title" name="custom_profile_tab2_title" type="text" placeholder="<?php _e('Add Your Title', 'ppd') ?>" value="<?php echo !empty($settings["custom_profile_tab2_title"]) ? stripslashes(__($settings["custom_profile_tab2_title"], 'ppd')) : ''; ?>">
                                    </td>
                                 </tr>
                                 <tr>
                                    <th><label for="custom_profile_tab2"><?php _e('Tab Content','ppd'); ?></label></th>
                                    <td>
                                       <textarea id="custom_profile_tab2" rows="3" cols="45" name="custom_profile_tab2" type="text" placeholder="<?php _e('Add Your content', 'ppd') ?>" ><?php echo !empty($settings["custom_profile_tab2"]) ? stripslashes(__($settings["custom_profile_tab2"], 'ppd')) :''; ?></textarea>
                                    </td>
                                 </tr>
                                  <tr>
                                    <th class=", 
                                     "><label for="tab2_restrct_user_role"><?php _e('Choose Role For Whom to be Visible','ppd'); ?></label></th>
                                    <td>
                                       <?php
                                       echo '<select name="choose_role_to_display2" id="choose_role_to_display2" style="clear:both;display:block;">';
                                          echo '<option value="anyone" '.selected('anyone', $settings['choose_role_to_display2']).'>'.__('Anyone', 'ppd').'</option>';
                                          echo '<option value="guests" '.selected('guests', $settings['choose_role_to_display2']).'>'.__('Guests Only', 'ppd').'</option>';
                                          echo '<option value="srole" '.selected('srole', $settings['choose_role_to_display2']).'>'.__('Specific Role Only', 'ppd').'</option>';
                                       echo '</select>';
                                       if( !empty($user_roles) ){
                                          echo '<select name="tab2_restrct_user_role[]" multiple id="tab2_restrct_user_role" style="clear:both;display:block;">';
                                          foreach ($user_roles as $key => $user_role) { 
                                             $selected = !empty( $settings['tab2_restrct_user_role']) ?  (in_array($key, $settings['tab2_restrct_user_role']) ? 'selected' : '') : ''; ?>
                                             <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $user_role; ?> </option>                               
                                             <?php 
                                          }
                                          echo '</select>';
                                       } ?>
                                    </td>
                                 </tr>
                                 <tr>
                                    <th><label for="tab2_restrict_msg"><?php _e('Restrict Tab Content Message', 'ppd') ?></th>
                                    <td>
                                       <textarea id="tab2_restrict_msg" name="tab2_restrict_msg" rows="3" cols="45" type="text" placeholder="<?php _e('Add Your Content', 'ppd') ?>"><?php echo !empty($settings["tab2_restrict_msg"]) ? stripslashes(__($settings["tab2_restrict_msg"], 'ppd')) : ''; ?></textarea>
                                    </td>
                                 </tr>
                              </table>
                           </fieldset>
                        </td>
                     </tr>
                     <tr class="tab3_top_heading">
                        <td>
                           <fieldset>
                              <legend><?php _e('Custom Profile Tab 3', 'ppd') ?></legend>
                              <table>
                                 <tr>
                                    <th><label for="custom_profile_tab3_title"><?php _e('Tab Title','ppd'); ?></label></th>
                                    <td>
                                       <input id="custom_profile_tab3_title" name="custom_profile_tab3_title" type="text" placeholder="<?php _e('Add Your Title', 'ppd') ?>" value="<?php echo !empty($settings["custom_profile_tab3_title"]) ? stripslashes(__($settings["custom_profile_tab3_title"], 'ppd')) : ''; ?>">
                                    </td>
                                 </tr>
                                 <tr>
                                    <th><label for="custom_profile_tab3"><?php _e('Tab Content','ppd'); ?></label></th>
                                    <td>
                                       <textarea id="custom_profile_tab3" rows="3" cols="45" name="custom_profile_tab3" type="text" placeholder="<?php _e('Add Your Content', 'ppd') ?>" ><?php echo !empty($settings["custom_profile_tab3"]) ? stripslashes(__($settings["custom_profile_tab3"], 'ppd')) : ''; ?></textarea>
                                    </td>
                                 </tr>
                                 <tr>
                                    <th class="choose_role_to_display"><label for="tab3_restrct_user_role"><?php _e('Choose Role For Whom to be Visible','ppd'); ?></label></th>
                                    <td>
                                       <?php
                                       echo '<select name="choose_role_to_display3" id="choose_role_to_display3" style="clear:both;display:block;">';
                                          echo '<option value="anyone" '.selected('anyone', $settings['choose_role_to_display3']).'>'.__('Anyone', 'ppd').'</option>';
                                          echo '<option value="guests" '.selected('guests', $settings['choose_role_to_display3']).'>'.__('Guests Only', 'ppd').'</option>';
                                          echo '<option value="srole" '.selected('srole', $settings['choose_role_to_display3']).'>'.__('Specific Role Only', 'ppd').'</option>';
                                       echo '</select>';
                                       if( !empty($user_roles) ){
                                          echo '<select name="tab3_restrct_user_role[]" multiple id="tab3_restrct_user_role" style="clear:both;display:block;">';
                                          foreach ($user_roles as $key => $user_role) { 
                                             $selected = !empty( $settings["tab3_restrct_user_role"]) ?  (in_array($key, $settings["tab3_restrct_user_role"]) ? 'selected' : '') : ''; ?>
                                             <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $user_role; ?> </option>                               
                                             <?php 
                                          }
                                          echo '</select>';
                                       } ?>
                                    </td>
                                 </tr>
                                 <tr>
                                    <th><label for="tab3_restrict_msg"><?php _e('Restrict Tab Content Message', 'ppd'); ?></th>
                                    <td>
                                       <textarea id="tab3_restrict_msg" rows="3" cols="45" name="tab3_restrict_msg" type="text" placeholder="<?php _e('Add Your content', 'ppd') ?>" ><?php echo !empty($settings["tab3_restrict_msg"]) ? stripslashes(__($settings["tab3_restrict_msg"], 'ppd')) : ''; ?></textarea>
                                    </td>
                                 </tr>
                              </table>
                           </fieldset>
                        </td>
                     </tr>
                        <tr>
                              <td><small><strong style="color:#ff0000;">Note: </strong></small><?php _e('If you want to disable the Tab Title, keep it empty', 'ppd'); ?></td>
                        </tr>
                     <?php echo '</table>';
                     break;
                     }
                  echo '</table>';

                  }
                  ?>
                  <p class="submit" style="clear: both;">
                     <input type="submit" name="Submit"  class="button-primary" value="<?php esc_html_e('Update Settings', 'ppd'); ?>" />
                     <input type="hidden" name="kaya-settings-submit" value="Y" />
                  </p>
               </form>
            </div>
         </div>
      </div>
         <?php
      }
   }
   // Options data object
   new Kaya_Taxonomy_Cpt_Fields_Settings;

   // initilizing options as a globally, just call  global  $kaya_options; where you need options
   function kaya_options(){
      global $kaya_options;
      $kaya_options = (object) get_option( "kaya_options" );
      return $kaya_options;
   }
   kaya_options();
 ?>